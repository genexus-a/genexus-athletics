/*
               File: AthleteRegistros
        Description: Stub for AthleteRegistros
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/7/2022 12:37:50.74
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class athleteregistros : GXProcedure
   {
      public athleteregistros( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public athleteregistros( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref short aP0_AthleteId )
      {
         this.AV2AthleteId = aP0_AthleteId;
         initialize();
         executePrivate();
         aP0_AthleteId=this.AV2AthleteId;
      }

      public short executeUdp( )
      {
         this.AV2AthleteId = aP0_AthleteId;
         initialize();
         executePrivate();
         aP0_AthleteId=this.AV2AthleteId;
         return AV2AthleteId ;
      }

      public void executeSubmit( ref short aP0_AthleteId )
      {
         athleteregistros objathleteregistros;
         objathleteregistros = new athleteregistros();
         objathleteregistros.AV2AthleteId = aP0_AthleteId;
         objathleteregistros.context.SetSubmitInitialConfig(context);
         objathleteregistros.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objathleteregistros);
         aP0_AthleteId=this.AV2AthleteId;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((athleteregistros)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(short)AV2AthleteId} ;
         ClassLoader.Execute("aathleteregistros","GeneXus.Programs","aathleteregistros", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 1 ) )
         {
            AV2AthleteId = (short)(args[0]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV2AthleteId ;
      private IGxDataStore dsDefault ;
      private short aP0_AthleteId ;
      private Object[] args ;
   }

}
