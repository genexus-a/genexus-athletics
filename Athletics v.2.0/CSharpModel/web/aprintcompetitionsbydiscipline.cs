/*
               File: PrintCompetitionsByDiscipline
        Description: Print Competitions By Discipline
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/15/2022 3:49:58.84
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aprintcompetitionsbydiscipline : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("Carmine");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aprintcompetitionsbydiscipline( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public aprintcompetitionsbydiscipline( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         aprintcompetitionsbydiscipline objaprintcompetitionsbydiscipline;
         objaprintcompetitionsbydiscipline = new aprintcompetitionsbydiscipline();
         objaprintcompetitionsbydiscipline.context.SetSubmitInitialConfig(context);
         objaprintcompetitionsbydiscipline.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaprintcompetitionsbydiscipline);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aprintcompetitionsbydiscipline)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            H0E0( false, 94) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 16, false, false, false, false, 0, 60, 30, 255, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Competitions", 300, Gx_line+17, 467, Gx_line+44, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+94);
            /* Using cursor P000E2 */
            pr_default.execute(0);
            while ( (pr_default.getStatus(0) != 101) )
            {
               BRK0E2 = false;
               A24CompetitionDisciplineId = P000E2_A24CompetitionDisciplineId[0];
               A22CompetitionId = P000E2_A22CompetitionId[0];
               A25CompetitionDisciplineName = P000E2_A25CompetitionDisciplineName[0];
               A23CompetitionDate = P000E2_A23CompetitionDate[0];
               A25CompetitionDisciplineName = P000E2_A25CompetitionDisciplineName[0];
               H0E0( false, 135) ;
               getPrinter().GxDrawLine(33, Gx_line+83, 716, Gx_line+83, 1, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A22CompetitionId), "ZZZ9")), 42, Gx_line+67, 234, Gx_line+82, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A23CompetitionDate, "99/99/99"), 333, Gx_line+67, 516, Gx_line+82, 2, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 12, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A25CompetitionDisciplineName, "")), 33, Gx_line+17, 466, Gx_line+39, 0, 0, 0, 0) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A14Teamid), "ZZZ9")), 100, Gx_line+100, 200, Gx_line+115, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A2DisciplineName, "")), 250, Gx_line+100, 564, Gx_line+115, 0+256, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+135);
               while ( (pr_default.getStatus(0) != 101) && ( StringUtil.StrCmp(P000E2_A25CompetitionDisciplineName[0], A25CompetitionDisciplineName) == 0 ) )
               {
                  BRK0E2 = false;
                  A24CompetitionDisciplineId = P000E2_A24CompetitionDisciplineId[0];
                  A22CompetitionId = P000E2_A22CompetitionId[0];
                  /* Using cursor P000E3 */
                  pr_default.execute(1, new Object[] {A22CompetitionId});
                  while ( (pr_default.getStatus(1) != 101) )
                  {
                     A14Teamid = P000E3_A14Teamid[0];
                     pr_default.readNext(1);
                  }
                  pr_default.close(1);
                  BRK0E2 = true;
                  pr_default.readNext(0);
               }
               if ( ! BRK0E2 )
               {
                  BRK0E2 = true;
                  pr_default.readNext(0);
               }
            }
            pr_default.close(0);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H0E0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( context.WillRedirect( ) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void H0E0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P000E2_A24CompetitionDisciplineId = new short[1] ;
         P000E2_A22CompetitionId = new short[1] ;
         P000E2_A25CompetitionDisciplineName = new String[] {""} ;
         P000E2_A23CompetitionDate = new DateTime[] {DateTime.MinValue} ;
         A25CompetitionDisciplineName = "";
         A23CompetitionDate = DateTime.MinValue;
         A2DisciplineName = "";
         P000E3_A22CompetitionId = new short[1] ;
         P000E3_A14Teamid = new short[1] ;
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aprintcompetitionsbydiscipline__default(),
            new Object[][] {
                new Object[] {
               P000E2_A24CompetitionDisciplineId, P000E2_A22CompetitionId, P000E2_A25CompetitionDisciplineName, P000E2_A23CompetitionDate
               }
               , new Object[] {
               P000E3_A22CompetitionId, P000E3_A14Teamid
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short A24CompetitionDisciplineId ;
      private short A22CompetitionId ;
      private short A14Teamid ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A25CompetitionDisciplineName ;
      private String A2DisciplineName ;
      private DateTime A23CompetitionDate ;
      private bool entryPointCalled ;
      private bool BRK0E2 ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] P000E2_A24CompetitionDisciplineId ;
      private short[] P000E2_A22CompetitionId ;
      private String[] P000E2_A25CompetitionDisciplineName ;
      private DateTime[] P000E2_A23CompetitionDate ;
      private short[] P000E3_A22CompetitionId ;
      private short[] P000E3_A14Teamid ;
   }

   public class aprintcompetitionsbydiscipline__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000E2 ;
          prmP000E2 = new Object[] {
          } ;
          Object[] prmP000E3 ;
          prmP000E3 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000E2", "SELECT T1.[CompetitionDisciplineId] AS CompetitionDisciplineId, T1.[CompetitionId], T2.[DisciplineName] AS CompetitionDisciplineName, T1.[CompetitionDate] FROM ([Competition] T1 INNER JOIN [Discipline] T2 ON T2.[DisciplineId] = T1.[CompetitionDisciplineId]) ORDER BY T2.[DisciplineName] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000E2,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("P000E3", "SELECT [CompetitionId], [Teamid] FROM [CompetitionTeam] WHERE [CompetitionId] = @CompetitionId ORDER BY [CompetitionId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000E3,100, GxCacheFrequency.OFF ,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 60) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
