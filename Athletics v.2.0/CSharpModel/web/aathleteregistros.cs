/*
               File: AthleteRegistros
        Description: Athlete Registros
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/8/2022 13:4:29.80
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aathleteregistros : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("Carmine");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               A6AthleteId = (short)(NumberUtil.Val( gxfirstwebparm, "."));
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aathleteregistros( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public aathleteregistros( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref short aP0_AthleteId )
      {
         this.A6AthleteId = aP0_AthleteId;
         initialize();
         executePrivate();
         aP0_AthleteId=this.A6AthleteId;
      }

      public short executeUdp( )
      {
         this.A6AthleteId = aP0_AthleteId;
         initialize();
         executePrivate();
         aP0_AthleteId=this.A6AthleteId;
         return A6AthleteId ;
      }

      public void executeSubmit( ref short aP0_AthleteId )
      {
         aathleteregistros objaathleteregistros;
         objaathleteregistros = new aathleteregistros();
         objaathleteregistros.A6AthleteId = aP0_AthleteId;
         objaathleteregistros.context.SetSubmitInitialConfig(context);
         objaathleteregistros.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaathleteregistros);
         aP0_AthleteId=this.A6AthleteId;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aathleteregistros)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            /* Using cursor P000A2 */
            pr_default.execute(0, new Object[] {A6AthleteId});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A40000AthletePhoto_GXI = P000A2_A40000AthletePhoto_GXI[0];
               A3CountryId = P000A2_A3CountryId[0];
               A4CountryName = P000A2_A4CountryName[0];
               A20AthleteGender = P000A2_A20AthleteGender[0];
               A8AthleteDateOfBirth = P000A2_A8AthleteDateOfBirth[0];
               A9AthletePhoto = P000A2_A9AthletePhoto[0];
               A4CountryName = P000A2_A4CountryName[0];
               GXt_dtime1 = DateTimeUtil.ResetTime( A8AthleteDateOfBirth ) ;
               A21AthleteAge = (short)(DateTimeUtil.Age( GXt_dtime1, DateTimeUtil.Now( context)));
               H0A0( false, 100) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A6AthleteId), "ZZZ9")), 17, Gx_line+33, 109, Gx_line+66, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A7AthleteName, "")), 133, Gx_line+33, 291, Gx_line+66, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A8AthleteDateOfBirth, "99/99/99"), 308, Gx_line+33, 408, Gx_line+66, 2, 0, 0, 0) ;
               sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : A9AthletePhoto);
               getPrinter().GxDrawBitMap(sImgUrl, 425, Gx_line+17, 508, Gx_line+67) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A20AthleteGender, "")), 517, Gx_line+33, 600, Gx_line+66, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A21AthleteAge), "ZZZ9")), 617, Gx_line+33, 675, Gx_line+66, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A4CountryName, "")), 750, Gx_line+33, 817, Gx_line+50, 0, 0, 0, 0) ;
               getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(A3CountryId), "ZZZ9")), 692, Gx_line+33, 734, Gx_line+66, 2, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+100);
               /* Exiting from a For First loop. */
               if (true) break;
            }
            pr_default.close(0);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H0A0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( context.WillRedirect( ) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void H0A0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P000A2_A6AthleteId = new short[1] ;
         P000A2_A40000AthletePhoto_GXI = new String[] {""} ;
         P000A2_A3CountryId = new short[1] ;
         P000A2_A4CountryName = new String[] {""} ;
         P000A2_A20AthleteGender = new String[] {""} ;
         P000A2_A8AthleteDateOfBirth = new DateTime[] {DateTime.MinValue} ;
         P000A2_A9AthletePhoto = new String[] {""} ;
         A40000AthletePhoto_GXI = "";
         A4CountryName = "";
         A20AthleteGender = "";
         A8AthleteDateOfBirth = DateTime.MinValue;
         A9AthletePhoto = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         A7AthleteName = "";
         sImgUrl = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aathleteregistros__default(),
            new Object[][] {
                new Object[] {
               P000A2_A6AthleteId, P000A2_A40000AthletePhoto_GXI, P000A2_A3CountryId, P000A2_A4CountryName, P000A2_A20AthleteGender, P000A2_A8AthleteDateOfBirth, P000A2_A9AthletePhoto
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short A6AthleteId ;
      private short GxWebError ;
      private short A3CountryId ;
      private short A21AthleteAge ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A4CountryName ;
      private String A20AthleteGender ;
      private String A7AthleteName ;
      private String sImgUrl ;
      private DateTime GXt_dtime1 ;
      private DateTime A8AthleteDateOfBirth ;
      private bool entryPointCalled ;
      private String A40000AthletePhoto_GXI ;
      private String A9AthletePhoto ;
      private IGxDataStore dsDefault ;
      private short aP0_AthleteId ;
      private IDataStoreProvider pr_default ;
      private short[] P000A2_A6AthleteId ;
      private String[] P000A2_A40000AthletePhoto_GXI ;
      private short[] P000A2_A3CountryId ;
      private String[] P000A2_A4CountryName ;
      private String[] P000A2_A20AthleteGender ;
      private DateTime[] P000A2_A8AthleteDateOfBirth ;
      private String[] P000A2_A9AthletePhoto ;
   }

   public class aathleteregistros__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000A2 ;
          prmP000A2 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000A2", "SELECT T1.[AthleteId], T1.[AthletePhoto_GXI], T1.[CountryId], T2.[CountryName], T1.[AthleteGender], T1.[AthleteDateOfBirth], T1.[AthletePhoto] FROM ([Athlete] T1 INNER JOIN [Country] T2 ON T2.[CountryId] = T1.[CountryId]) WHERE T1.[AthleteId] = @AthleteId ORDER BY T1.[AthleteId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000A2,1, GxCacheFrequency.OFF ,false,true )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 60) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 50) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(6) ;
                ((String[]) buf[6])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(2)) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
