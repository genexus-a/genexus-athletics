/*
               File: Discipline_DataProvider
        Description: Stub for Discipline_DataProvider
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/7/2022 13:28:18.65
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class discipline_dataprovider : GXProcedure
   {
      public discipline_dataprovider( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public discipline_dataprovider( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out GXBCCollection<SdtDiscipline> aP0_ReturnValue )
      {
         this.AV2ReturnValue = new GXBCCollection<SdtDiscipline>( context, "Discipline", "TeamAthlete") ;
         initialize();
         executePrivate();
         aP0_ReturnValue=this.AV2ReturnValue;
      }

      public GXBCCollection<SdtDiscipline> executeUdp( )
      {
         this.AV2ReturnValue = new GXBCCollection<SdtDiscipline>( context, "Discipline", "TeamAthlete") ;
         initialize();
         executePrivate();
         aP0_ReturnValue=this.AV2ReturnValue;
         return AV2ReturnValue ;
      }

      public void executeSubmit( out GXBCCollection<SdtDiscipline> aP0_ReturnValue )
      {
         discipline_dataprovider objdiscipline_dataprovider;
         objdiscipline_dataprovider = new discipline_dataprovider();
         objdiscipline_dataprovider.AV2ReturnValue = new GXBCCollection<SdtDiscipline>( context, "Discipline", "TeamAthlete") ;
         objdiscipline_dataprovider.context.SetSubmitInitialConfig(context);
         objdiscipline_dataprovider.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objdiscipline_dataprovider);
         aP0_ReturnValue=this.AV2ReturnValue;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((discipline_dataprovider)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(GXBCCollection<SdtDiscipline>)AV2ReturnValue} ;
         ClassLoader.Execute("adiscipline_dataprovider","GeneXus.Programs","adiscipline_dataprovider", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 1 ) )
         {
            AV2ReturnValue = (GXBCCollection<SdtDiscipline>)(args[0]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private IGxDataStore dsDefault ;
      private Object[] args ;
      private GXBCCollection<SdtDiscipline> aP0_ReturnValue ;
      private GXBCCollection<SdtDiscipline> AV2ReturnValue ;
   }

}
