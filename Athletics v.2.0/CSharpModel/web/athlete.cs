/*
               File: Athlete
        Description: Athlete
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/15/2022 3:50:0.8
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class athlete : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A3CountryId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3CountryId", StringUtil.LTrim( StringUtil.Str( (decimal)(A3CountryId), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A3CountryId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_5-135614", 0) ;
            Form.Meta.addItem("description", "Athlete", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtAthleteId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public athlete( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public athlete( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbAthleteGender = new GXCombobox();
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbAthleteGender.ItemCount > 0 )
         {
            A20AthleteGender = cmbAthleteGender.getValidValue(A20AthleteGender);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20AthleteGender", A20AthleteGender);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbAthleteGender.CurrentValue = StringUtil.RTrim( A20AthleteGender);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAthleteGender_Internalname, "Values", cmbAthleteGender.ToJavascriptSource(), true);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Athlete", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0030.aspx"+"',["+"{Ctrl:gx.dom.el('"+"ATHLETEID"+"'), id:'"+"ATHLETEID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtAthleteId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtAthleteId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtAthleteId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A6AthleteId), 4, 0, ".", "")), ((edtAthleteId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A6AthleteId), "ZZZ9")) : context.localUtil.Format( (decimal)(A6AthleteId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAthleteId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtAthleteId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtAthlete_Name_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtAthlete_Name_Internalname, "Athlete_Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtAthlete_Name_Internalname, StringUtil.RTrim( A29Athlete_Name), StringUtil.RTrim( context.localUtil.Format( A29Athlete_Name, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAthlete_Name_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtAthlete_Name_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbAthleteGender_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, cmbAthleteGender_Internalname, "Gender", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         /* ComboBox */
         GxWebStd.gx_combobox_ctrl1( context, cmbAthleteGender, cmbAthleteGender_Internalname, StringUtil.RTrim( A20AthleteGender), 1, cmbAthleteGender_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbAthleteGender.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "", true, "HLP_Athlete.htm");
         cmbAthleteGender.CurrentValue = StringUtil.RTrim( A20AthleteGender);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAthleteGender_Internalname, "Values", (String)(cmbAthleteGender.ToJavascriptSource()), true);
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtAthleteDateOfBirth_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtAthleteDateOfBirth_Internalname, "Of Birth", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
         context.WriteHtmlText( "<div id=\""+edtAthleteDateOfBirth_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( context, edtAthleteDateOfBirth_Internalname, context.localUtil.Format(A8AthleteDateOfBirth, "99/99/99"), context.localUtil.Format( A8AthleteDateOfBirth, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onblur(this,49);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAthleteDateOfBirth_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtAthleteDateOfBirth_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Athlete.htm");
         GxWebStd.gx_bitmap( context, edtAthleteDateOfBirth_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtAthleteDateOfBirth_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Athlete.htm");
         context.WriteHtmlTextNl( "</div>") ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtAthleteAge_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtAthleteAge_Internalname, "Age", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtAthleteAge_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A21AthleteAge), 4, 0, ".", "")), ((edtAthleteAge_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A21AthleteAge), "ZZZ9")) : context.localUtil.Format( (decimal)(A21AthleteAge), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAthleteAge_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtAthleteAge_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+imgAthletePhoto_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, "", "Photo", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Static Bitmap Variable */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
         ClassString = "Attribute";
         StyleString = "";
         A9AthletePhoto_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000AthletePhoto_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)));
         sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : context.PathToRelativeUrl( A9AthletePhoto));
         GxWebStd.gx_bitmap( context, imgAthletePhoto_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, imgAthletePhoto_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "", "", "", 0, A9AthletePhoto_IsBlob, true, context.GetImageSrcSet( sImgUrl), "HLP_Athlete.htm");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : context.PathToRelativeUrl( A9AthletePhoto)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "IsBlob", StringUtil.BoolToStr( A9AthletePhoto_IsBlob), true);
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCountryId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtCountryId_Internalname, "Country Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtCountryId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A3CountryId), 4, 0, ".", "")), ((edtCountryId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A3CountryId), "ZZZ9")) : context.localUtil.Format( (decimal)(A3CountryId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCountryId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCountryId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Athlete.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_3_Internalname, sImgUrl, imgprompt_3_Link, "", "", context.GetTheme( ), imgprompt_3_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCountryName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtCountryName_Internalname, "Country Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtCountryName_Internalname, StringUtil.RTrim( A4CountryName), StringUtil.RTrim( context.localUtil.Format( A4CountryName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCountryName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCountryName_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+imgCountryFlag_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, "", "Country Flag", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Static Bitmap Variable */
         ClassString = "Attribute";
         StyleString = "";
         A5CountryFlag_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag))&&String.IsNullOrEmpty(StringUtil.RTrim( A40001CountryFlag_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)));
         sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.PathToRelativeUrl( A5CountryFlag));
         GxWebStd.gx_bitmap( context, imgCountryFlag_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, imgCountryFlag_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 0, A5CountryFlag_IsBlob, true, context.GetImageSrcSet( sImgUrl), "HLP_Athlete.htm");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.PathToRelativeUrl( A5CountryFlag)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "IsBlob", StringUtil.BoolToStr( A5CountryFlag_IsBlob), true);
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Athlete.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtAthleteId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAthleteId_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "ATHLETEID");
               AnyError = 1;
               GX_FocusControl = edtAthleteId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A6AthleteId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
            }
            else
            {
               A6AthleteId = (short)(context.localUtil.CToN( cgiGet( edtAthleteId_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
            }
            A29Athlete_Name = cgiGet( edtAthlete_Name_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Athlete_Name", A29Athlete_Name);
            cmbAthleteGender.CurrentValue = cgiGet( cmbAthleteGender_Internalname);
            A20AthleteGender = cgiGet( cmbAthleteGender_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20AthleteGender", A20AthleteGender);
            if ( context.localUtil.VCDate( cgiGet( edtAthleteDateOfBirth_Internalname), 1) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Athlete Date Of Birth"}), 1, "ATHLETEDATEOFBIRTH");
               AnyError = 1;
               GX_FocusControl = edtAthleteDateOfBirth_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A8AthleteDateOfBirth = DateTime.MinValue;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AthleteDateOfBirth", context.localUtil.Format(A8AthleteDateOfBirth, "99/99/99"));
            }
            else
            {
               A8AthleteDateOfBirth = context.localUtil.CToD( cgiGet( edtAthleteDateOfBirth_Internalname), 1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AthleteDateOfBirth", context.localUtil.Format(A8AthleteDateOfBirth, "99/99/99"));
            }
            A21AthleteAge = (short)(context.localUtil.CToN( cgiGet( edtAthleteAge_Internalname), ".", ","));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21AthleteAge", StringUtil.LTrim( StringUtil.Str( (decimal)(A21AthleteAge), 4, 0)));
            A9AthletePhoto = cgiGet( imgAthletePhoto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9AthletePhoto", A9AthletePhoto);
            if ( ( ( context.localUtil.CToN( cgiGet( edtCountryId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtCountryId_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "COUNTRYID");
               AnyError = 1;
               GX_FocusControl = edtCountryId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A3CountryId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3CountryId", StringUtil.LTrim( StringUtil.Str( (decimal)(A3CountryId), 4, 0)));
            }
            else
            {
               A3CountryId = (short)(context.localUtil.CToN( cgiGet( edtCountryId_Internalname), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3CountryId", StringUtil.LTrim( StringUtil.Str( (decimal)(A3CountryId), 4, 0)));
            }
            A4CountryName = cgiGet( edtCountryName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", A4CountryName);
            A5CountryFlag = cgiGet( imgCountryFlag_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5CountryFlag", A5CountryFlag);
            /* Read saved values. */
            Z6AthleteId = (short)(context.localUtil.CToN( cgiGet( "Z6AthleteId"), ".", ","));
            Z29Athlete_Name = cgiGet( "Z29Athlete_Name");
            Z20AthleteGender = cgiGet( "Z20AthleteGender");
            Z8AthleteDateOfBirth = context.localUtil.CToD( cgiGet( "Z8AthleteDateOfBirth"), 0);
            Z3CountryId = (short)(context.localUtil.CToN( cgiGet( "Z3CountryId"), ".", ","));
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
            Gx_mode = cgiGet( "Mode");
            A40000AthletePhoto_GXI = cgiGet( "ATHLETEPHOTO_GXI");
            A40001CountryFlag_GXI = cgiGet( "COUNTRYFLAG_GXI");
            Gx_mode = cgiGet( "vMODE");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            getMultimediaValue(imgAthletePhoto_Internalname, ref  A9AthletePhoto, ref  A40000AthletePhoto_GXI);
            getMultimediaValue(imgCountryFlag_Internalname, ref  A5CountryFlag, ref  A40001CountryFlag_GXI);
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A6AthleteId = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll033( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes033( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption030( )
      {
      }

      protected void ZM033( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z29Athlete_Name = T00033_A29Athlete_Name[0];
               Z20AthleteGender = T00033_A20AthleteGender[0];
               Z8AthleteDateOfBirth = T00033_A8AthleteDateOfBirth[0];
               Z3CountryId = T00033_A3CountryId[0];
            }
            else
            {
               Z29Athlete_Name = A29Athlete_Name;
               Z20AthleteGender = A20AthleteGender;
               Z8AthleteDateOfBirth = A8AthleteDateOfBirth;
               Z3CountryId = A3CountryId;
            }
         }
         if ( GX_JID == -4 )
         {
            Z6AthleteId = A6AthleteId;
            Z29Athlete_Name = A29Athlete_Name;
            Z20AthleteGender = A20AthleteGender;
            Z8AthleteDateOfBirth = A8AthleteDateOfBirth;
            Z9AthletePhoto = A9AthletePhoto;
            Z40000AthletePhoto_GXI = A40000AthletePhoto_GXI;
            Z3CountryId = A3CountryId;
            Z4CountryName = A4CountryName;
            Z5CountryFlag = A5CountryFlag;
            Z40001CountryFlag_GXI = A40001CountryFlag_GXI;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_3_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0020.aspx"+"',["+"{Ctrl:gx.dom.el('"+"COUNTRYID"+"'), id:'"+"COUNTRYID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load033( )
      {
         /* Using cursor T00035 */
         pr_default.execute(3, new Object[] {A6AthleteId});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound3 = 1;
            A29Athlete_Name = T00035_A29Athlete_Name[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Athlete_Name", A29Athlete_Name);
            A20AthleteGender = T00035_A20AthleteGender[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20AthleteGender", A20AthleteGender);
            A8AthleteDateOfBirth = T00035_A8AthleteDateOfBirth[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AthleteDateOfBirth", context.localUtil.Format(A8AthleteDateOfBirth, "99/99/99"));
            A40000AthletePhoto_GXI = T00035_A40000AthletePhoto_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A9AthletePhoto))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A9AthletePhoto), true);
            A4CountryName = T00035_A4CountryName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", A4CountryName);
            A40001CountryFlag_GXI = T00035_A40001CountryFlag_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
            A3CountryId = T00035_A3CountryId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3CountryId", StringUtil.LTrim( StringUtil.Str( (decimal)(A3CountryId), 4, 0)));
            A9AthletePhoto = T00035_A9AthletePhoto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9AthletePhoto", A9AthletePhoto);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A9AthletePhoto))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A9AthletePhoto), true);
            A5CountryFlag = T00035_A5CountryFlag[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5CountryFlag", A5CountryFlag);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
            ZM033( -4) ;
         }
         pr_default.close(3);
         OnLoadActions033( ) ;
      }

      protected void OnLoadActions033( )
      {
         GXt_dtime1 = DateTimeUtil.ResetTime( A8AthleteDateOfBirth ) ;
         A21AthleteAge = (short)(DateTimeUtil.Age( GXt_dtime1, DateTimeUtil.Now( context)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21AthleteAge", StringUtil.LTrim( StringUtil.Str( (decimal)(A21AthleteAge), 4, 0)));
      }

      protected void CheckExtendedTable033( )
      {
         nIsDirty_3 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A8AthleteDateOfBirth) || ( A8AthleteDateOfBirth >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field Athlete Date Of Birth is out of range", "OutOfRange", 1, "ATHLETEDATEOFBIRTH");
            AnyError = 1;
            GX_FocusControl = edtAthleteDateOfBirth_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         nIsDirty_3 = 1;
         GXt_dtime1 = DateTimeUtil.ResetTime( A8AthleteDateOfBirth ) ;
         A21AthleteAge = (short)(DateTimeUtil.Age( GXt_dtime1, DateTimeUtil.Now( context)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21AthleteAge", StringUtil.LTrim( StringUtil.Str( (decimal)(A21AthleteAge), 4, 0)));
         if ( A21AthleteAge < 18 )
         {
            GX_msglist.addItem("The athlete must be older than 18 years old", 0, "");
         }
         /* Using cursor T00034 */
         pr_default.execute(2, new Object[] {A3CountryId});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No matching 'Country'.", "ForeignKeyNotFound", 1, "COUNTRYID");
            AnyError = 1;
            GX_FocusControl = edtCountryId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A4CountryName = T00034_A4CountryName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", A4CountryName);
         A40001CountryFlag_GXI = T00034_A40001CountryFlag_GXI[0];
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
         A5CountryFlag = T00034_A5CountryFlag[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5CountryFlag", A5CountryFlag);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors033( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( short A3CountryId )
      {
         /* Using cursor T00036 */
         pr_default.execute(4, new Object[] {A3CountryId});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No matching 'Country'.", "ForeignKeyNotFound", 1, "COUNTRYID");
            AnyError = 1;
            GX_FocusControl = edtCountryId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A4CountryName = T00036_A4CountryName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", A4CountryName);
         A40001CountryFlag_GXI = T00036_A40001CountryFlag_GXI[0];
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
         A5CountryFlag = T00036_A5CountryFlag[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5CountryFlag", A5CountryFlag);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A4CountryName))+"\""+","+"\""+GXUtil.EncodeJSConstant( A5CountryFlag)+"\""+","+"\""+GXUtil.EncodeJSConstant( A40001CountryFlag_GXI)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(4);
      }

      protected void GetKey033( )
      {
         /* Using cursor T00037 */
         pr_default.execute(5, new Object[] {A6AthleteId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound3 = 1;
         }
         else
         {
            RcdFound3 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00033 */
         pr_default.execute(1, new Object[] {A6AthleteId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM033( 4) ;
            RcdFound3 = 1;
            A6AthleteId = T00033_A6AthleteId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
            A29Athlete_Name = T00033_A29Athlete_Name[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Athlete_Name", A29Athlete_Name);
            A20AthleteGender = T00033_A20AthleteGender[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20AthleteGender", A20AthleteGender);
            A8AthleteDateOfBirth = T00033_A8AthleteDateOfBirth[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AthleteDateOfBirth", context.localUtil.Format(A8AthleteDateOfBirth, "99/99/99"));
            A40000AthletePhoto_GXI = T00033_A40000AthletePhoto_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A9AthletePhoto))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A9AthletePhoto), true);
            A3CountryId = T00033_A3CountryId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3CountryId", StringUtil.LTrim( StringUtil.Str( (decimal)(A3CountryId), 4, 0)));
            A9AthletePhoto = T00033_A9AthletePhoto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9AthletePhoto", A9AthletePhoto);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A9AthletePhoto))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A9AthletePhoto), true);
            Z6AthleteId = A6AthleteId;
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load033( ) ;
            if ( AnyError == 1 )
            {
               RcdFound3 = 0;
               InitializeNonKey033( ) ;
            }
            Gx_mode = sMode3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound3 = 0;
            InitializeNonKey033( ) ;
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey033( ) ;
         if ( RcdFound3 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound3 = 0;
         /* Using cursor T00038 */
         pr_default.execute(6, new Object[] {A6AthleteId});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T00038_A6AthleteId[0] < A6AthleteId ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T00038_A6AthleteId[0] > A6AthleteId ) ) )
            {
               A6AthleteId = T00038_A6AthleteId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
               RcdFound3 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound3 = 0;
         /* Using cursor T00039 */
         pr_default.execute(7, new Object[] {A6AthleteId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T00039_A6AthleteId[0] > A6AthleteId ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T00039_A6AthleteId[0] < A6AthleteId ) ) )
            {
               A6AthleteId = T00039_A6AthleteId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
               RcdFound3 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey033( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtAthleteId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert033( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound3 == 1 )
            {
               if ( A6AthleteId != Z6AthleteId )
               {
                  A6AthleteId = Z6AthleteId;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "ATHLETEID");
                  AnyError = 1;
                  GX_FocusControl = edtAthleteId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtAthleteId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update033( ) ;
                  GX_FocusControl = edtAthleteId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A6AthleteId != Z6AthleteId )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtAthleteId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert033( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "ATHLETEID");
                     AnyError = 1;
                     GX_FocusControl = edtAthleteId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtAthleteId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert033( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A6AthleteId != Z6AthleteId )
         {
            A6AthleteId = Z6AthleteId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "ATHLETEID");
            AnyError = 1;
            GX_FocusControl = edtAthleteId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtAthleteId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "ATHLETEID");
            AnyError = 1;
            GX_FocusControl = edtAthleteId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtAthlete_Name_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart033( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtAthlete_Name_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd033( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtAthlete_Name_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtAthlete_Name_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart033( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound3 != 0 )
            {
               ScanNext033( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtAthlete_Name_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd033( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency033( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00032 */
            pr_default.execute(0, new Object[] {A6AthleteId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Athlete"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z29Athlete_Name, T00032_A29Athlete_Name[0]) != 0 ) || ( StringUtil.StrCmp(Z20AthleteGender, T00032_A20AthleteGender[0]) != 0 ) || ( Z8AthleteDateOfBirth != T00032_A8AthleteDateOfBirth[0] ) || ( Z3CountryId != T00032_A3CountryId[0] ) )
            {
               if ( StringUtil.StrCmp(Z29Athlete_Name, T00032_A29Athlete_Name[0]) != 0 )
               {
                  GXUtil.WriteLog("athlete:[seudo value changed for attri]"+"Athlete_Name");
                  GXUtil.WriteLogRaw("Old: ",Z29Athlete_Name);
                  GXUtil.WriteLogRaw("Current: ",T00032_A29Athlete_Name[0]);
               }
               if ( StringUtil.StrCmp(Z20AthleteGender, T00032_A20AthleteGender[0]) != 0 )
               {
                  GXUtil.WriteLog("athlete:[seudo value changed for attri]"+"AthleteGender");
                  GXUtil.WriteLogRaw("Old: ",Z20AthleteGender);
                  GXUtil.WriteLogRaw("Current: ",T00032_A20AthleteGender[0]);
               }
               if ( Z8AthleteDateOfBirth != T00032_A8AthleteDateOfBirth[0] )
               {
                  GXUtil.WriteLog("athlete:[seudo value changed for attri]"+"AthleteDateOfBirth");
                  GXUtil.WriteLogRaw("Old: ",Z8AthleteDateOfBirth);
                  GXUtil.WriteLogRaw("Current: ",T00032_A8AthleteDateOfBirth[0]);
               }
               if ( Z3CountryId != T00032_A3CountryId[0] )
               {
                  GXUtil.WriteLog("athlete:[seudo value changed for attri]"+"CountryId");
                  GXUtil.WriteLogRaw("Old: ",Z3CountryId);
                  GXUtil.WriteLogRaw("Current: ",T00032_A3CountryId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Athlete"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert033( )
      {
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable033( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM033( 0) ;
            CheckOptimisticConcurrency033( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm033( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert033( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000310 */
                     pr_default.execute(8, new Object[] {A29Athlete_Name, A20AthleteGender, A8AthleteDateOfBirth, A9AthletePhoto, A40000AthletePhoto_GXI, A3CountryId});
                     A6AthleteId = T000310_A6AthleteId[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Athlete") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption030( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load033( ) ;
            }
            EndLevel033( ) ;
         }
         CloseExtendedTableCursors033( ) ;
      }

      protected void Update033( )
      {
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable033( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency033( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm033( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate033( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000311 */
                     pr_default.execute(9, new Object[] {A29Athlete_Name, A20AthleteGender, A8AthleteDateOfBirth, A3CountryId, A6AthleteId});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Athlete") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Athlete"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate033( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption030( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel033( ) ;
         }
         CloseExtendedTableCursors033( ) ;
      }

      protected void DeferredUpdate033( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T000312 */
            pr_default.execute(10, new Object[] {A9AthletePhoto, A40000AthletePhoto_GXI, A6AthleteId});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("Athlete") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency033( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls033( ) ;
            AfterConfirm033( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete033( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000313 */
                  pr_default.execute(11, new Object[] {A6AthleteId});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("Athlete") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound3 == 0 )
                        {
                           InitAll033( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption030( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode3 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel033( ) ;
         Gx_mode = sMode3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls033( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            GXt_dtime1 = DateTimeUtil.ResetTime( A8AthleteDateOfBirth ) ;
            A21AthleteAge = (short)(DateTimeUtil.Age( GXt_dtime1, DateTimeUtil.Now( context)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21AthleteAge", StringUtil.LTrim( StringUtil.Str( (decimal)(A21AthleteAge), 4, 0)));
            /* Using cursor T000314 */
            pr_default.execute(12, new Object[] {A3CountryId});
            A4CountryName = T000314_A4CountryName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", A4CountryName);
            A40001CountryFlag_GXI = T000314_A40001CountryFlag_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
            A5CountryFlag = T000314_A5CountryFlag[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5CountryFlag", A5CountryFlag);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
            pr_default.close(12);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000315 */
            pr_default.execute(13, new Object[] {A6AthleteId});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Athlete"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel033( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete033( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            context.CommitDataStores("athlete",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues030( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            context.RollbackDataStores("athlete",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart033( )
      {
         /* Using cursor T000316 */
         pr_default.execute(14);
         RcdFound3 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound3 = 1;
            A6AthleteId = T000316_A6AthleteId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext033( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound3 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound3 = 1;
            A6AthleteId = T000316_A6AthleteId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
         }
      }

      protected void ScanEnd033( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm033( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert033( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate033( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete033( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete033( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate033( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes033( )
      {
         edtAthleteId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthleteId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthleteId_Enabled), 5, 0)), true);
         edtAthlete_Name_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthlete_Name_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthlete_Name_Enabled), 5, 0)), true);
         cmbAthleteGender.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAthleteGender_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbAthleteGender.Enabled), 5, 0)), true);
         edtAthleteDateOfBirth_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthleteDateOfBirth_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthleteDateOfBirth_Enabled), 5, 0)), true);
         edtAthleteAge_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAthleteAge_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAthleteAge_Enabled), 5, 0)), true);
         imgAthletePhoto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgAthletePhoto_Enabled), 5, 0)), true);
         edtCountryId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCountryId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCountryId_Enabled), 5, 0)), true);
         edtCountryName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCountryName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCountryName_Enabled), 5, 0)), true);
         imgCountryFlag_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgCountryFlag_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes033( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues030( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 135614), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxcfg.js", "?2022615350180", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("athlete.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z6AthleteId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z6AthleteId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z29Athlete_Name", StringUtil.RTrim( Z29Athlete_Name));
         GxWebStd.gx_hidden_field( context, "Z20AthleteGender", StringUtil.RTrim( Z20AthleteGender));
         GxWebStd.gx_hidden_field( context, "Z8AthleteDateOfBirth", context.localUtil.DToC( Z8AthleteDateOfBirth, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z3CountryId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z3CountryId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "ATHLETEPHOTO_GXI", A40000AthletePhoto_GXI);
         GxWebStd.gx_hidden_field( context, "COUNTRYFLAG_GXI", A40001CountryFlag_GXI);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXCCtlgxBlob = "ATHLETEPHOTO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A9AthletePhoto);
         GXCCtlgxBlob = "COUNTRYFLAG" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A5CountryFlag);
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("athlete.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Athlete" ;
      }

      public override String GetPgmdesc( )
      {
         return "Athlete" ;
      }

      protected void InitializeNonKey033( )
      {
         A21AthleteAge = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21AthleteAge", StringUtil.LTrim( StringUtil.Str( (decimal)(A21AthleteAge), 4, 0)));
         A29Athlete_Name = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Athlete_Name", A29Athlete_Name);
         A20AthleteGender = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20AthleteGender", A20AthleteGender);
         A8AthleteDateOfBirth = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AthleteDateOfBirth", context.localUtil.Format(A8AthleteDateOfBirth, "99/99/99"));
         A9AthletePhoto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9AthletePhoto", A9AthletePhoto);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A9AthletePhoto))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A9AthletePhoto), true);
         A40000AthletePhoto_GXI = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A9AthletePhoto)) ? A40000AthletePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A9AthletePhoto))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgAthletePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A9AthletePhoto), true);
         A3CountryId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3CountryId", StringUtil.LTrim( StringUtil.Str( (decimal)(A3CountryId), 4, 0)));
         A4CountryName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", A4CountryName);
         A5CountryFlag = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5CountryFlag", A5CountryFlag);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
         A40001CountryFlag_GXI = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A5CountryFlag)) ? A40001CountryFlag_GXI : context.convertURL( context.PathToRelativeUrl( A5CountryFlag))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgCountryFlag_Internalname, "SrcSet", context.GetImageSrcSet( A5CountryFlag), true);
         Z29Athlete_Name = "";
         Z20AthleteGender = "";
         Z8AthleteDateOfBirth = DateTime.MinValue;
         Z3CountryId = 0;
      }

      protected void InitAll033( )
      {
         A6AthleteId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6AthleteId", StringUtil.LTrim( StringUtil.Str( (decimal)(A6AthleteId), 4, 0)));
         InitializeNonKey033( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2022615350191", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("athlete.js", "?2022615350191", false, true);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtAthleteId_Internalname = "ATHLETEID";
         edtAthlete_Name_Internalname = "ATHLETE_NAME";
         cmbAthleteGender_Internalname = "ATHLETEGENDER";
         edtAthleteDateOfBirth_Internalname = "ATHLETEDATEOFBIRTH";
         edtAthleteAge_Internalname = "ATHLETEAGE";
         imgAthletePhoto_Internalname = "ATHLETEPHOTO";
         edtCountryId_Internalname = "COUNTRYID";
         edtCountryName_Internalname = "COUNTRYNAME";
         imgCountryFlag_Internalname = "COUNTRYFLAG";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_3_Internalname = "PROMPT_3";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Athlete";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         imgCountryFlag_Enabled = 0;
         edtCountryName_Jsonclick = "";
         edtCountryName_Enabled = 0;
         imgprompt_3_Visible = 1;
         imgprompt_3_Link = "";
         edtCountryId_Jsonclick = "";
         edtCountryId_Enabled = 1;
         imgAthletePhoto_Enabled = 1;
         edtAthleteAge_Jsonclick = "";
         edtAthleteAge_Enabled = 0;
         edtAthleteDateOfBirth_Jsonclick = "";
         edtAthleteDateOfBirth_Enabled = 1;
         cmbAthleteGender_Jsonclick = "";
         cmbAthleteGender.Enabled = 1;
         edtAthlete_Name_Jsonclick = "";
         edtAthlete_Name_Enabled = 1;
         edtAthleteId_Jsonclick = "";
         edtAthleteId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         cmbAthleteGender.Name = "ATHLETEGENDER";
         cmbAthleteGender.WebTags = "";
         cmbAthleteGender.addItem("Female", "Female", 0);
         cmbAthleteGender.addItem("Male", "Male", 0);
         if ( cmbAthleteGender.ItemCount > 0 )
         {
            A20AthleteGender = cmbAthleteGender.getValidValue(A20AthleteGender);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20AthleteGender", A20AthleteGender);
         }
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtAthlete_Name_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Athleteid( )
      {
         A20AthleteGender = cmbAthleteGender.CurrentValue;
         cmbAthleteGender.CurrentValue = A20AthleteGender;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         if ( cmbAthleteGender.ItemCount > 0 )
         {
            A20AthleteGender = cmbAthleteGender.getValidValue(A20AthleteGender);
            cmbAthleteGender.CurrentValue = A20AthleteGender;
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbAthleteGender.CurrentValue = StringUtil.RTrim( A20AthleteGender);
         }
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29Athlete_Name", StringUtil.RTrim( A29Athlete_Name));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20AthleteGender", StringUtil.RTrim( A20AthleteGender));
         cmbAthleteGender.CurrentValue = StringUtil.RTrim( A20AthleteGender);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbAthleteGender_Internalname, "Values", cmbAthleteGender.ToJavascriptSource(), true);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AthleteDateOfBirth", context.localUtil.Format(A8AthleteDateOfBirth, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A9AthletePhoto", context.PathToRelativeUrl( A9AthletePhoto));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40000AthletePhoto_GXI", A40000AthletePhoto_GXI);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3CountryId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A3CountryId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21AthleteAge", StringUtil.LTrim( StringUtil.NToC( (decimal)(A21AthleteAge), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", StringUtil.RTrim( A4CountryName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5CountryFlag", context.PathToRelativeUrl( A5CountryFlag));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40001CountryFlag_GXI", A40001CountryFlag_GXI);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z6AthleteId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z6AthleteId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z29Athlete_Name", StringUtil.RTrim( Z29Athlete_Name));
         GxWebStd.gx_hidden_field( context, "Z20AthleteGender", StringUtil.RTrim( Z20AthleteGender));
         GxWebStd.gx_hidden_field( context, "Z8AthleteDateOfBirth", context.localUtil.Format(Z8AthleteDateOfBirth, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "Z9AthletePhoto", context.PathToRelativeUrl( Z9AthletePhoto));
         GxWebStd.gx_hidden_field( context, "Z40000AthletePhoto_GXI", Z40000AthletePhoto_GXI);
         GxWebStd.gx_hidden_field( context, "Z3CountryId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z3CountryId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z21AthleteAge", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z21AthleteAge), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z4CountryName", StringUtil.RTrim( Z4CountryName));
         GxWebStd.gx_hidden_field( context, "Z5CountryFlag", context.PathToRelativeUrl( Z5CountryFlag));
         GxWebStd.gx_hidden_field( context, "Z40001CountryFlag_GXI", Z40001CountryFlag_GXI);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Athletedateofbirth( )
      {
         if ( ! ( (DateTime.MinValue==A8AthleteDateOfBirth) || ( A8AthleteDateOfBirth >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field Athlete Date Of Birth is out of range", "OutOfRange", 1, "ATHLETEDATEOFBIRTH");
            AnyError = 1;
            GX_FocusControl = edtAthleteDateOfBirth_Internalname;
         }
         GXt_dtime1 = DateTimeUtil.ResetTime( A8AthleteDateOfBirth ) ;
         A21AthleteAge = (short)(DateTimeUtil.Age( GXt_dtime1, DateTimeUtil.Now( context)));
         if ( A21AthleteAge < 18 )
         {
            GX_msglist.addItem("The athlete must be older than 18 years old", 0, "");
         }
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A21AthleteAge", StringUtil.LTrim( StringUtil.NToC( (decimal)(A21AthleteAge), 4, 0, ".", "")));
      }

      public void Valid_Countryid( )
      {
         /* Using cursor T000314 */
         pr_default.execute(12, new Object[] {A3CountryId});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("No matching 'Country'.", "ForeignKeyNotFound", 1, "COUNTRYID");
            AnyError = 1;
            GX_FocusControl = edtCountryId_Internalname;
         }
         A4CountryName = T000314_A4CountryName[0];
         A40001CountryFlag_GXI = T000314_A40001CountryFlag_GXI[0];
         A5CountryFlag = T000314_A5CountryFlag[0];
         pr_default.close(12);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4CountryName", StringUtil.RTrim( A4CountryName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5CountryFlag", context.PathToRelativeUrl( A5CountryFlag));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40001CountryFlag_GXI", A40001CountryFlag_GXI);
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_ATHLETEID","{handler:'Valid_Athleteid',iparms:[{av:'cmbAthleteGender'},{av:'A20AthleteGender',fld:'ATHLETEGENDER',pic:''},{av:'A6AthleteId',fld:'ATHLETEID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_ATHLETEID",",oparms:[{av:'A29Athlete_Name',fld:'ATHLETE_NAME',pic:''},{av:'cmbAthleteGender'},{av:'A20AthleteGender',fld:'ATHLETEGENDER',pic:''},{av:'A8AthleteDateOfBirth',fld:'ATHLETEDATEOFBIRTH',pic:''},{av:'A9AthletePhoto',fld:'ATHLETEPHOTO',pic:''},{av:'A40000AthletePhoto_GXI',fld:'ATHLETEPHOTO_GXI',pic:''},{av:'A3CountryId',fld:'COUNTRYID',pic:'ZZZ9'},{av:'A21AthleteAge',fld:'ATHLETEAGE',pic:'ZZZ9'},{av:'A4CountryName',fld:'COUNTRYNAME',pic:''},{av:'A5CountryFlag',fld:'COUNTRYFLAG',pic:''},{av:'A40001CountryFlag_GXI',fld:'COUNTRYFLAG_GXI',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z6AthleteId'},{av:'Z29Athlete_Name'},{av:'Z20AthleteGender'},{av:'Z8AthleteDateOfBirth'},{av:'Z9AthletePhoto'},{av:'Z40000AthletePhoto_GXI'},{av:'Z3CountryId'},{av:'Z21AthleteAge'},{av:'Z4CountryName'},{av:'Z5CountryFlag'},{av:'Z40001CountryFlag_GXI'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_ATHLETEDATEOFBIRTH","{handler:'Valid_Athletedateofbirth',iparms:[{av:'A8AthleteDateOfBirth',fld:'ATHLETEDATEOFBIRTH',pic:''},{av:'A21AthleteAge',fld:'ATHLETEAGE',pic:'ZZZ9'}]");
         setEventMetadata("VALID_ATHLETEDATEOFBIRTH",",oparms:[{av:'A21AthleteAge',fld:'ATHLETEAGE',pic:'ZZZ9'}]}");
         setEventMetadata("VALID_ATHLETEAGE","{handler:'Valid_Athleteage',iparms:[]");
         setEventMetadata("VALID_ATHLETEAGE",",oparms:[]}");
         setEventMetadata("VALID_COUNTRYID","{handler:'Valid_Countryid',iparms:[{av:'A3CountryId',fld:'COUNTRYID',pic:'ZZZ9'},{av:'A4CountryName',fld:'COUNTRYNAME',pic:''},{av:'A5CountryFlag',fld:'COUNTRYFLAG',pic:''},{av:'A40001CountryFlag_GXI',fld:'COUNTRYFLAG_GXI',pic:''}]");
         setEventMetadata("VALID_COUNTRYID",",oparms:[{av:'A4CountryName',fld:'COUNTRYNAME',pic:''},{av:'A5CountryFlag',fld:'COUNTRYFLAG',pic:''},{av:'A40001CountryFlag_GXI',fld:'COUNTRYFLAG_GXI',pic:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z29Athlete_Name = "";
         Z20AthleteGender = "";
         Z8AthleteDateOfBirth = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         A20AthleteGender = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A29Athlete_Name = "";
         A8AthleteDateOfBirth = DateTime.MinValue;
         A9AthletePhoto = "";
         A40000AthletePhoto_GXI = "";
         sImgUrl = "";
         A4CountryName = "";
         A5CountryFlag = "";
         A40001CountryFlag_GXI = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z9AthletePhoto = "";
         Z40000AthletePhoto_GXI = "";
         Z4CountryName = "";
         Z5CountryFlag = "";
         Z40001CountryFlag_GXI = "";
         T00035_A6AthleteId = new short[1] ;
         T00035_A29Athlete_Name = new String[] {""} ;
         T00035_A20AthleteGender = new String[] {""} ;
         T00035_A8AthleteDateOfBirth = new DateTime[] {DateTime.MinValue} ;
         T00035_A40000AthletePhoto_GXI = new String[] {""} ;
         T00035_A4CountryName = new String[] {""} ;
         T00035_A40001CountryFlag_GXI = new String[] {""} ;
         T00035_A3CountryId = new short[1] ;
         T00035_A9AthletePhoto = new String[] {""} ;
         T00035_A5CountryFlag = new String[] {""} ;
         T00034_A4CountryName = new String[] {""} ;
         T00034_A40001CountryFlag_GXI = new String[] {""} ;
         T00034_A5CountryFlag = new String[] {""} ;
         T00036_A4CountryName = new String[] {""} ;
         T00036_A40001CountryFlag_GXI = new String[] {""} ;
         T00036_A5CountryFlag = new String[] {""} ;
         T00037_A6AthleteId = new short[1] ;
         T00033_A6AthleteId = new short[1] ;
         T00033_A29Athlete_Name = new String[] {""} ;
         T00033_A20AthleteGender = new String[] {""} ;
         T00033_A8AthleteDateOfBirth = new DateTime[] {DateTime.MinValue} ;
         T00033_A40000AthletePhoto_GXI = new String[] {""} ;
         T00033_A3CountryId = new short[1] ;
         T00033_A9AthletePhoto = new String[] {""} ;
         sMode3 = "";
         T00038_A6AthleteId = new short[1] ;
         T00039_A6AthleteId = new short[1] ;
         T00032_A6AthleteId = new short[1] ;
         T00032_A29Athlete_Name = new String[] {""} ;
         T00032_A20AthleteGender = new String[] {""} ;
         T00032_A8AthleteDateOfBirth = new DateTime[] {DateTime.MinValue} ;
         T00032_A40000AthletePhoto_GXI = new String[] {""} ;
         T00032_A3CountryId = new short[1] ;
         T00032_A9AthletePhoto = new String[] {""} ;
         T000310_A6AthleteId = new short[1] ;
         T000314_A4CountryName = new String[] {""} ;
         T000314_A40001CountryFlag_GXI = new String[] {""} ;
         T000314_A5CountryFlag = new String[] {""} ;
         T000315_A14Teamid = new short[1] ;
         T000315_A6AthleteId = new short[1] ;
         T000316_A6AthleteId = new short[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         ZZ29Athlete_Name = "";
         ZZ20AthleteGender = "";
         ZZ8AthleteDateOfBirth = DateTime.MinValue;
         ZZ9AthletePhoto = "";
         ZZ40000AthletePhoto_GXI = "";
         ZZ4CountryName = "";
         ZZ5CountryFlag = "";
         ZZ40001CountryFlag_GXI = "";
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.athlete__default(),
            new Object[][] {
                new Object[] {
               T00032_A6AthleteId, T00032_A29Athlete_Name, T00032_A20AthleteGender, T00032_A8AthleteDateOfBirth, T00032_A40000AthletePhoto_GXI, T00032_A3CountryId, T00032_A9AthletePhoto
               }
               , new Object[] {
               T00033_A6AthleteId, T00033_A29Athlete_Name, T00033_A20AthleteGender, T00033_A8AthleteDateOfBirth, T00033_A40000AthletePhoto_GXI, T00033_A3CountryId, T00033_A9AthletePhoto
               }
               , new Object[] {
               T00034_A4CountryName, T00034_A40001CountryFlag_GXI, T00034_A5CountryFlag
               }
               , new Object[] {
               T00035_A6AthleteId, T00035_A29Athlete_Name, T00035_A20AthleteGender, T00035_A8AthleteDateOfBirth, T00035_A40000AthletePhoto_GXI, T00035_A4CountryName, T00035_A40001CountryFlag_GXI, T00035_A3CountryId, T00035_A9AthletePhoto, T00035_A5CountryFlag
               }
               , new Object[] {
               T00036_A4CountryName, T00036_A40001CountryFlag_GXI, T00036_A5CountryFlag
               }
               , new Object[] {
               T00037_A6AthleteId
               }
               , new Object[] {
               T00038_A6AthleteId
               }
               , new Object[] {
               T00039_A6AthleteId
               }
               , new Object[] {
               T000310_A6AthleteId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000314_A4CountryName, T000314_A40001CountryFlag_GXI, T000314_A5CountryFlag
               }
               , new Object[] {
               T000315_A14Teamid, T000315_A6AthleteId
               }
               , new Object[] {
               T000316_A6AthleteId
               }
            }
         );
      }

      private short Z6AthleteId ;
      private short Z3CountryId ;
      private short GxWebError ;
      private short A3CountryId ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A6AthleteId ;
      private short A21AthleteAge ;
      private short GX_JID ;
      private short RcdFound3 ;
      private short nIsDirty_3 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short Z21AthleteAge ;
      private short ZZ6AthleteId ;
      private short ZZ3CountryId ;
      private short ZZ21AthleteAge ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtAthleteId_Enabled ;
      private int edtAthlete_Name_Enabled ;
      private int edtAthleteDateOfBirth_Enabled ;
      private int edtAthleteAge_Enabled ;
      private int imgAthletePhoto_Enabled ;
      private int edtCountryId_Enabled ;
      private int imgprompt_3_Visible ;
      private int edtCountryName_Enabled ;
      private int imgCountryFlag_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String Z29Athlete_Name ;
      private String Z20AthleteGender ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtAthleteId_Internalname ;
      private String A20AthleteGender ;
      private String cmbAthleteGender_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtAthleteId_Jsonclick ;
      private String edtAthlete_Name_Internalname ;
      private String A29Athlete_Name ;
      private String edtAthlete_Name_Jsonclick ;
      private String cmbAthleteGender_Jsonclick ;
      private String edtAthleteDateOfBirth_Internalname ;
      private String edtAthleteDateOfBirth_Jsonclick ;
      private String edtAthleteAge_Internalname ;
      private String edtAthleteAge_Jsonclick ;
      private String imgAthletePhoto_Internalname ;
      private String sImgUrl ;
      private String edtCountryId_Internalname ;
      private String edtCountryId_Jsonclick ;
      private String imgprompt_3_Internalname ;
      private String imgprompt_3_Link ;
      private String edtCountryName_Internalname ;
      private String A4CountryName ;
      private String edtCountryName_Jsonclick ;
      private String imgCountryFlag_Internalname ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z4CountryName ;
      private String sMode3 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String ZZ29Athlete_Name ;
      private String ZZ20AthleteGender ;
      private String ZZ4CountryName ;
      private DateTime GXt_dtime1 ;
      private DateTime Z8AthleteDateOfBirth ;
      private DateTime A8AthleteDateOfBirth ;
      private DateTime ZZ8AthleteDateOfBirth ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A9AthletePhoto_IsBlob ;
      private bool A5CountryFlag_IsBlob ;
      private String A40000AthletePhoto_GXI ;
      private String A40001CountryFlag_GXI ;
      private String Z40000AthletePhoto_GXI ;
      private String Z40001CountryFlag_GXI ;
      private String ZZ40000AthletePhoto_GXI ;
      private String ZZ40001CountryFlag_GXI ;
      private String A9AthletePhoto ;
      private String A5CountryFlag ;
      private String Z9AthletePhoto ;
      private String Z5CountryFlag ;
      private String ZZ9AthletePhoto ;
      private String ZZ5CountryFlag ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbAthleteGender ;
      private IDataStoreProvider pr_default ;
      private short[] T00035_A6AthleteId ;
      private String[] T00035_A29Athlete_Name ;
      private String[] T00035_A20AthleteGender ;
      private DateTime[] T00035_A8AthleteDateOfBirth ;
      private String[] T00035_A40000AthletePhoto_GXI ;
      private String[] T00035_A4CountryName ;
      private String[] T00035_A40001CountryFlag_GXI ;
      private short[] T00035_A3CountryId ;
      private String[] T00035_A9AthletePhoto ;
      private String[] T00035_A5CountryFlag ;
      private String[] T00034_A4CountryName ;
      private String[] T00034_A40001CountryFlag_GXI ;
      private String[] T00034_A5CountryFlag ;
      private String[] T00036_A4CountryName ;
      private String[] T00036_A40001CountryFlag_GXI ;
      private String[] T00036_A5CountryFlag ;
      private short[] T00037_A6AthleteId ;
      private short[] T00033_A6AthleteId ;
      private String[] T00033_A29Athlete_Name ;
      private String[] T00033_A20AthleteGender ;
      private DateTime[] T00033_A8AthleteDateOfBirth ;
      private String[] T00033_A40000AthletePhoto_GXI ;
      private short[] T00033_A3CountryId ;
      private String[] T00033_A9AthletePhoto ;
      private short[] T00038_A6AthleteId ;
      private short[] T00039_A6AthleteId ;
      private short[] T00032_A6AthleteId ;
      private String[] T00032_A29Athlete_Name ;
      private String[] T00032_A20AthleteGender ;
      private DateTime[] T00032_A8AthleteDateOfBirth ;
      private String[] T00032_A40000AthletePhoto_GXI ;
      private short[] T00032_A3CountryId ;
      private String[] T00032_A9AthletePhoto ;
      private short[] T000310_A6AthleteId ;
      private String[] T000314_A4CountryName ;
      private String[] T000314_A40001CountryFlag_GXI ;
      private String[] T000314_A5CountryFlag ;
      private short[] T000315_A14Teamid ;
      private short[] T000315_A6AthleteId ;
      private short[] T000316_A6AthleteId ;
      private GXWebForm Form ;
   }

   public class athlete__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT00035 ;
          prmT00035 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00034 ;
          prmT00034 = new Object[] {
          new Object[] {"@CountryId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00036 ;
          prmT00036 = new Object[] {
          new Object[] {"@CountryId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00037 ;
          prmT00037 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00033 ;
          prmT00033 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00038 ;
          prmT00038 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00039 ;
          prmT00039 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00032 ;
          prmT00032 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000310 ;
          prmT000310 = new Object[] {
          new Object[] {"@Athlete_Name",SqlDbType.NChar,60,0} ,
          new Object[] {"@AthleteGender",SqlDbType.NChar,50,0} ,
          new Object[] {"@AthleteDateOfBirth",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AthletePhoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@AthletePhoto_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@CountryId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000311 ;
          prmT000311 = new Object[] {
          new Object[] {"@Athlete_Name",SqlDbType.NChar,60,0} ,
          new Object[] {"@AthleteGender",SqlDbType.NChar,50,0} ,
          new Object[] {"@AthleteDateOfBirth",SqlDbType.DateTime,8,0} ,
          new Object[] {"@CountryId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000312 ;
          prmT000312 = new Object[] {
          new Object[] {"@AthletePhoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@AthletePhoto_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000313 ;
          prmT000313 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000315 ;
          prmT000315 = new Object[] {
          new Object[] {"@AthleteId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000316 ;
          prmT000316 = new Object[] {
          } ;
          Object[] prmT000314 ;
          prmT000314 = new Object[] {
          new Object[] {"@CountryId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00032", "SELECT [AthleteId], [Athlete_Name], [AthleteGender], [AthleteDateOfBirth], [AthletePhoto_GXI], [CountryId], [AthletePhoto] FROM [Athlete] WITH (UPDLOCK) WHERE [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00032,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00033", "SELECT [AthleteId], [Athlete_Name], [AthleteGender], [AthleteDateOfBirth], [AthletePhoto_GXI], [CountryId], [AthletePhoto] FROM [Athlete] WHERE [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00033,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00034", "SELECT [CountryName], [CountryFlag_GXI], [CountryFlag] FROM [Country] WHERE [CountryId] = @CountryId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00034,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00035", "SELECT TM1.[AthleteId], TM1.[Athlete_Name], TM1.[AthleteGender], TM1.[AthleteDateOfBirth], TM1.[AthletePhoto_GXI], T2.[CountryName], T2.[CountryFlag_GXI], TM1.[CountryId], TM1.[AthletePhoto], T2.[CountryFlag] FROM ([Athlete] TM1 INNER JOIN [Country] T2 ON T2.[CountryId] = TM1.[CountryId]) WHERE TM1.[AthleteId] = @AthleteId ORDER BY TM1.[AthleteId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00035,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00036", "SELECT [CountryName], [CountryFlag_GXI], [CountryFlag] FROM [Country] WHERE [CountryId] = @CountryId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00036,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00037", "SELECT [AthleteId] FROM [Athlete] WHERE [AthleteId] = @AthleteId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00037,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00038", "SELECT TOP 1 [AthleteId] FROM [Athlete] WHERE ( [AthleteId] > @AthleteId) ORDER BY [AthleteId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00038,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T00039", "SELECT TOP 1 [AthleteId] FROM [Athlete] WHERE ( [AthleteId] < @AthleteId) ORDER BY [AthleteId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00039,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000310", "INSERT INTO [Athlete]([Athlete_Name], [AthleteGender], [AthleteDateOfBirth], [AthletePhoto], [AthletePhoto_GXI], [CountryId]) VALUES(@Athlete_Name, @AthleteGender, @AthleteDateOfBirth, @AthletePhoto, @AthletePhoto_GXI, @CountryId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000310)
             ,new CursorDef("T000311", "UPDATE [Athlete] SET [Athlete_Name]=@Athlete_Name, [AthleteGender]=@AthleteGender, [AthleteDateOfBirth]=@AthleteDateOfBirth, [CountryId]=@CountryId  WHERE [AthleteId] = @AthleteId", GxErrorMask.GX_NOMASK,prmT000311)
             ,new CursorDef("T000312", "UPDATE [Athlete] SET [AthletePhoto]=@AthletePhoto, [AthletePhoto_GXI]=@AthletePhoto_GXI  WHERE [AthleteId] = @AthleteId", GxErrorMask.GX_NOMASK,prmT000312)
             ,new CursorDef("T000313", "DELETE FROM [Athlete]  WHERE [AthleteId] = @AthleteId", GxErrorMask.GX_NOMASK,prmT000313)
             ,new CursorDef("T000314", "SELECT [CountryName], [CountryFlag_GXI], [CountryFlag] FROM [Country] WHERE [CountryId] = @CountryId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000314,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000315", "SELECT TOP 1 [Teamid], [AthleteId] FROM [TeamAthlete] WHERE [AthleteId] = @AthleteId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000315,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000316", "SELECT [AthleteId] FROM [Athlete] ORDER BY [AthleteId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000316,100, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 60) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((String[]) buf[6])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(5)) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 60) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((String[]) buf[6])[0] = rslt.getMultimediaFile(7, rslt.getVarchar(5)) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaFile(3, rslt.getVarchar(2)) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 60) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 50) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDate(4) ;
                ((String[]) buf[4])[0] = rslt.getMultimediaUri(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 60) ;
                ((String[]) buf[6])[0] = rslt.getMultimediaUri(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(9, rslt.getVarchar(5)) ;
                ((String[]) buf[9])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(7)) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaFile(3, rslt.getVarchar(2)) ;
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((String[]) buf[2])[0] = rslt.getMultimediaFile(3, rslt.getVarchar(2)) ;
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 14 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameterBlob(4, (String)parms[3], false);
                stmt.SetParameterMultimedia(5, (String)parms[4], (String)parms[3], "Athlete", "AthletePhoto");
                stmt.SetParameter(6, (short)parms[5]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (DateTime)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                return;
             case 10 :
                stmt.SetParameterBlob(1, (String)parms[0], false);
                stmt.SetParameterMultimedia(2, (String)parms[1], (String)parms[0], "Athlete", "AthletePhoto");
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
