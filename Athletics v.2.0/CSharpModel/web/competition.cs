/*
               File: Competition
        Description: Competition
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/15/2022 3:50:9.15
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class competition : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A22CompetitionId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A22CompetitionId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A24CompetitionDisciplineId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A24CompetitionDisciplineId), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A24CompetitionDisciplineId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A14Teamid = (short)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A14Teamid) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A1DisciplineId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A1DisciplineId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridcompetition_team") == 0 )
         {
            nRC_GXsfl_63 = (int)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_63_idx = (int)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_63_idx = GetNextPar( );
            Gx_mode = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridcompetition_team_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_5-135614", 0) ;
            Form.Meta.addItem("description", "Competition", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtCompetitionId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public competition( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public competition( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Competition", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Select", bttBtn_select_Jsonclick, 4, "Select", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0060.aspx"+"',["+"{Ctrl:gx.dom.el('"+"COMPETITIONID"+"'), id:'"+"COMPETITIONID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCompetitionId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtCompetitionId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtCompetitionId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A22CompetitionId), 4, 0, ".", "")), ((edtCompetitionId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A22CompetitionId), "ZZZ9")) : context.localUtil.Format( (decimal)(A22CompetitionId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCompetitionId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCompetitionId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCompetitionDate_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtCompetitionDate_Internalname, "Date", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         context.WriteHtmlText( "<div id=\""+edtCompetitionDate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( context, edtCompetitionDate_Internalname, context.localUtil.Format(A23CompetitionDate, "99/99/99"), context.localUtil.Format( A23CompetitionDate, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'MDY',0,12,'eng',false,0);"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCompetitionDate_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCompetitionDate_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Competition.htm");
         GxWebStd.gx_bitmap( context, edtCompetitionDate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtCompetitionDate_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Competition.htm");
         context.WriteHtmlTextNl( "</div>") ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCompetitionDisciplineId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtCompetitionDisciplineId_Internalname, "Discipline Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtCompetitionDisciplineId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A24CompetitionDisciplineId), 4, 0, ".", "")), ((edtCompetitionDisciplineId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A24CompetitionDisciplineId), "ZZZ9")) : context.localUtil.Format( (decimal)(A24CompetitionDisciplineId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCompetitionDisciplineId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCompetitionDisciplineId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Competition.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_24_Internalname, sImgUrl, imgprompt_24_Link, "", "", context.GetTheme( ), imgprompt_24_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCompetitionDisciplineName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtCompetitionDisciplineName_Internalname, "Discipline Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtCompetitionDisciplineName_Internalname, StringUtil.RTrim( A25CompetitionDisciplineName), StringUtil.RTrim( context.localUtil.Format( A25CompetitionDisciplineName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCompetitionDisciplineName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCompetitionDisciplineName_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCompetitionTeamsQuality_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtCompetitionTeamsQuality_Internalname, "Teams Quality", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtCompetitionTeamsQuality_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A26CompetitionTeamsQuality), 4, 0, ".", "")), ((edtCompetitionTeamsQuality_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A26CompetitionTeamsQuality), "ZZZ9")) : context.localUtil.Format( (decimal)(A26CompetitionTeamsQuality), "ZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCompetitionTeamsQuality_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCompetitionTeamsQuality_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTeamtable_Internalname, 1, 0, "px", 0, "px", "LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitleteam_Internalname, "Team", "", "", lblTitleteam_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         gxdraw_Gridcompetition_team( ) ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirm", bttBtn_enter_Jsonclick, 5, "Confirm", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancel", bttBtn_cancel_Jsonclick, 1, "Cancel", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Delete", bttBtn_delete_Jsonclick, 5, "Delete", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Competition.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void gxdraw_Gridcompetition_team( )
      {
         /*  Grid Control  */
         Gridcompetition_teamContainer.AddObjectProperty("GridName", "Gridcompetition_team");
         Gridcompetition_teamContainer.AddObjectProperty("Header", subGridcompetition_team_Header);
         Gridcompetition_teamContainer.AddObjectProperty("Class", "Grid");
         Gridcompetition_teamContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcompetition_team_Backcolorstyle), 1, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("CmpContext", "");
         Gridcompetition_teamContainer.AddObjectProperty("InMasterPage", "false");
         Gridcompetition_teamColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridcompetition_teamColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A14Teamid), 4, 0, ".", "")));
         Gridcompetition_teamColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTeamid_Enabled), 5, 0, ".", "")));
         Gridcompetition_teamContainer.AddColumnProperties(Gridcompetition_teamColumn);
         Gridcompetition_teamColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridcompetition_teamContainer.AddColumnProperties(Gridcompetition_teamColumn);
         Gridcompetition_teamColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridcompetition_teamColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1DisciplineId), 4, 0, ".", "")));
         Gridcompetition_teamColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtDisciplineId_Enabled), 5, 0, ".", "")));
         Gridcompetition_teamContainer.AddColumnProperties(Gridcompetition_teamColumn);
         Gridcompetition_teamColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridcompetition_teamColumn.AddObjectProperty("Value", StringUtil.RTrim( A2DisciplineName));
         Gridcompetition_teamColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtDisciplineName_Enabled), 5, 0, ".", "")));
         Gridcompetition_teamContainer.AddColumnProperties(Gridcompetition_teamColumn);
         Gridcompetition_teamColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridcompetition_teamColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A28CompetitionTeamPoints), 4, 0, ".", "")));
         Gridcompetition_teamColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCompetitionTeamPoints_Enabled), 5, 0, ".", "")));
         Gridcompetition_teamContainer.AddColumnProperties(Gridcompetition_teamColumn);
         Gridcompetition_teamContainer.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcompetition_team_Selectedindex), 4, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcompetition_team_Allowselection), 1, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcompetition_team_Selectioncolor), 9, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcompetition_team_Allowhovering), 1, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcompetition_team_Hoveringcolor), 9, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcompetition_team_Allowcollapsing), 1, 0, ".", "")));
         Gridcompetition_teamContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcompetition_team_Collapsed), 1, 0, ".", "")));
         nGXsfl_63_idx = 0;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount7 = 5;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               /* Display confirmed (stored) records */
               nRcdExists_7 = 1;
               ScanStart067( ) ;
               while ( RcdFound7 != 0 )
               {
                  init_level_properties7( ) ;
                  getByPrimaryKey067( ) ;
                  AddRow067( ) ;
                  ScanNext067( ) ;
               }
               ScanEnd067( ) ;
               nBlankRcdCount7 = 5;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            B26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            standaloneNotModal067( ) ;
            standaloneModal067( ) ;
            sMode7 = Gx_mode;
            while ( nGXsfl_63_idx < nRC_GXsfl_63 )
            {
               bGXsfl_63_Refreshing = true;
               ReadRow067( ) ;
               edtTeamid_Enabled = (int)(context.localUtil.CToN( cgiGet( "TEAMID_"+sGXsfl_63_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTeamid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTeamid_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
               edtDisciplineId_Enabled = (int)(context.localUtil.CToN( cgiGet( "DISCIPLINEID_"+sGXsfl_63_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtDisciplineId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtDisciplineId_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
               edtDisciplineName_Enabled = (int)(context.localUtil.CToN( cgiGet( "DISCIPLINENAME_"+sGXsfl_63_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtDisciplineName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtDisciplineName_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
               edtCompetitionTeamPoints_Enabled = (int)(context.localUtil.CToN( cgiGet( "COMPETITIONTEAMPOINTS_"+sGXsfl_63_idx+"Enabled"), ".", ","));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCompetitionTeamPoints_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCompetitionTeamPoints_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
               imgprompt_24_Link = cgiGet( "PROMPT_14_"+sGXsfl_63_idx+"Link");
               if ( ( nRcdExists_7 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal067( ) ;
               }
               SendRow067( ) ;
               bGXsfl_63_Refreshing = false;
            }
            Gx_mode = sMode7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            A26CompetitionTeamsQuality = B26CompetitionTeamsQuality;
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount7 = 5;
            nRcdExists_7 = 1;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               ScanStart067( ) ;
               while ( RcdFound7 != 0 )
               {
                  sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx+1), 4, 0)), 4, "0");
                  SubsflControlProps_637( ) ;
                  init_level_properties7( ) ;
                  standaloneNotModal067( ) ;
                  getByPrimaryKey067( ) ;
                  standaloneModal067( ) ;
                  AddRow067( ) ;
                  ScanNext067( ) ;
               }
               ScanEnd067( ) ;
            }
         }
         /* Initialize fields for 'new' records and send them. */
         sMode7 = Gx_mode;
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx+1), 4, 0)), 4, "0");
         SubsflControlProps_637( ) ;
         InitAll067( ) ;
         init_level_properties7( ) ;
         B26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
         n26CompetitionTeamsQuality = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         nRcdExists_7 = 0;
         nIsMod_7 = 0;
         nRcdDeleted_7 = 0;
         nBlankRcdCount7 = (short)(nBlankRcdUsr7+nBlankRcdCount7);
         fRowAdded = 0;
         while ( nBlankRcdCount7 > 0 )
         {
            standaloneNotModal067( ) ;
            standaloneModal067( ) ;
            AddRow067( ) ;
            if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
            {
               fRowAdded = 1;
               GX_FocusControl = edtTeamid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nBlankRcdCount7 = (short)(nBlankRcdCount7-1);
         }
         Gx_mode = sMode7;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         A26CompetitionTeamsQuality = B26CompetitionTeamsQuality;
         n26CompetitionTeamsQuality = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         sStyleString = "";
         context.WriteHtmlText( "<div id=\""+"Gridcompetition_teamContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridcompetition_team", Gridcompetition_teamContainer);
         if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridcompetition_teamContainerData", Gridcompetition_teamContainer.ToJavascriptSource());
         }
         if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridcompetition_teamContainerData"+"V", Gridcompetition_teamContainer.GridValuesHidden());
         }
         else
         {
            context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridcompetition_teamContainerData"+"V"+"\" value='"+Gridcompetition_teamContainer.GridValuesHidden()+"'/>") ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11062 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( ( ( context.localUtil.CToN( cgiGet( edtCompetitionId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtCompetitionId_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "COMPETITIONID");
                  AnyError = 1;
                  GX_FocusControl = edtCompetitionId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A22CompetitionId = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
               }
               else
               {
                  A22CompetitionId = (short)(context.localUtil.CToN( cgiGet( edtCompetitionId_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
               }
               if ( context.localUtil.VCDate( cgiGet( edtCompetitionDate_Internalname), 1) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Competition Date"}), 1, "COMPETITIONDATE");
                  AnyError = 1;
                  GX_FocusControl = edtCompetitionDate_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A23CompetitionDate = DateTime.MinValue;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23CompetitionDate", context.localUtil.Format(A23CompetitionDate, "99/99/99"));
               }
               else
               {
                  A23CompetitionDate = context.localUtil.CToD( cgiGet( edtCompetitionDate_Internalname), 1);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23CompetitionDate", context.localUtil.Format(A23CompetitionDate, "99/99/99"));
               }
               if ( ( ( context.localUtil.CToN( cgiGet( edtCompetitionDisciplineId_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtCompetitionDisciplineId_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "COMPETITIONDISCIPLINEID");
                  AnyError = 1;
                  GX_FocusControl = edtCompetitionDisciplineId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A24CompetitionDisciplineId = 0;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A24CompetitionDisciplineId), 4, 0)));
               }
               else
               {
                  A24CompetitionDisciplineId = (short)(context.localUtil.CToN( cgiGet( edtCompetitionDisciplineId_Internalname), ".", ","));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A24CompetitionDisciplineId), 4, 0)));
               }
               A25CompetitionDisciplineName = cgiGet( edtCompetitionDisciplineName_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25CompetitionDisciplineName", A25CompetitionDisciplineName);
               A26CompetitionTeamsQuality = (short)(context.localUtil.CToN( cgiGet( edtCompetitionTeamsQuality_Internalname), ".", ","));
               n26CompetitionTeamsQuality = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
               /* Read saved values. */
               Z22CompetitionId = (short)(context.localUtil.CToN( cgiGet( "Z22CompetitionId"), ".", ","));
               Z23CompetitionDate = context.localUtil.CToD( cgiGet( "Z23CompetitionDate"), 0);
               Z24CompetitionDisciplineId = (short)(context.localUtil.CToN( cgiGet( "Z24CompetitionDisciplineId"), ".", ","));
               O26CompetitionTeamsQuality = (short)(context.localUtil.CToN( cgiGet( "O26CompetitionTeamsQuality"), ".", ","));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ".", ","));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ".", ","));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_63 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_63"), ".", ","));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A22CompetitionId = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E11062 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: After Trn */
                           E12062 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E12062 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll066( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes066( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_067( )
      {
         s26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
         n26CompetitionTeamsQuality = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         nGXsfl_63_idx = 0;
         while ( nGXsfl_63_idx < nRC_GXsfl_63 )
         {
            ReadRow067( ) ;
            if ( ( nRcdExists_7 != 0 ) || ( nIsMod_7 != 0 ) )
            {
               GetKey067( ) ;
               if ( ( nRcdExists_7 == 0 ) && ( nRcdDeleted_7 == 0 ) )
               {
                  if ( RcdFound7 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate067( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable067( ) ;
                        CloseExtendedTableCursors067( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                        O26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
                        n26CompetitionTeamsQuality = false;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                     }
                  }
                  else
                  {
                     GXCCtl = "TEAMID_" + sGXsfl_63_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtTeamid_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound7 != 0 )
                  {
                     if ( nRcdDeleted_7 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey067( ) ;
                        Load067( ) ;
                        BeforeValidate067( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls067( ) ;
                           O26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
                           n26CompetitionTeamsQuality = false;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                        }
                     }
                     else
                     {
                        if ( nIsMod_7 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate067( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable067( ) ;
                              CloseExtendedTableCursors067( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                              O26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
                              n26CompetitionTeamsQuality = false;
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_7 == 0 )
                     {
                        GXCCtl = "TEAMID_" + sGXsfl_63_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtTeamid_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtTeamid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A14Teamid), 4, 0, ".", ""))) ;
            ChangePostValue( edtDisciplineId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1DisciplineId), 4, 0, ".", ""))) ;
            ChangePostValue( edtDisciplineName_Internalname, StringUtil.RTrim( A2DisciplineName)) ;
            ChangePostValue( edtCompetitionTeamPoints_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A28CompetitionTeamPoints), 4, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z14Teamid_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z14Teamid), 4, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z28CompetitionTeamPoints_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z28CompetitionTeamPoints), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdDeleted_7_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_7), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_7_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_7), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_7_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_7), 4, 0, ".", ""))) ;
            if ( nIsMod_7 != 0 )
            {
               ChangePostValue( "TEAMID_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTeamid_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "DISCIPLINEID_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtDisciplineId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "DISCIPLINENAME_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtDisciplineName_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "COMPETITIONTEAMPOINTS_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCompetitionTeamPoints_Enabled), 5, 0, ".", ""))) ;
            }
         }
         O26CompetitionTeamsQuality = s26CompetitionTeamsQuality;
         n26CompetitionTeamsQuality = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption060( )
      {
      }

      protected void E11062( )
      {
         /* Start Routine */
      }

      protected void E12062( )
      {
         /* After Trn Routine */
      }

      protected void ZM066( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z23CompetitionDate = T00067_A23CompetitionDate[0];
               Z24CompetitionDisciplineId = T00067_A24CompetitionDisciplineId[0];
            }
            else
            {
               Z23CompetitionDate = A23CompetitionDate;
               Z24CompetitionDisciplineId = A24CompetitionDisciplineId;
            }
         }
         if ( GX_JID == -3 )
         {
            Z22CompetitionId = A22CompetitionId;
            Z23CompetitionDate = A23CompetitionDate;
            Z24CompetitionDisciplineId = A24CompetitionDisciplineId;
            Z26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
            Z25CompetitionDisciplineName = A25CompetitionDisciplineName;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_24_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"COMPETITIONDISCIPLINEID"+"'), id:'"+"COMPETITIONDISCIPLINEID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load066( )
      {
         /* Using cursor T000612 */
         pr_default.execute(8, new Object[] {A22CompetitionId});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound6 = 1;
            A23CompetitionDate = T000612_A23CompetitionDate[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23CompetitionDate", context.localUtil.Format(A23CompetitionDate, "99/99/99"));
            A25CompetitionDisciplineName = T000612_A25CompetitionDisciplineName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25CompetitionDisciplineName", A25CompetitionDisciplineName);
            A24CompetitionDisciplineId = T000612_A24CompetitionDisciplineId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A24CompetitionDisciplineId), 4, 0)));
            A26CompetitionTeamsQuality = T000612_A26CompetitionTeamsQuality[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            n26CompetitionTeamsQuality = T000612_n26CompetitionTeamsQuality[0];
            ZM066( -3) ;
         }
         pr_default.close(8);
         OnLoadActions066( ) ;
      }

      protected void OnLoadActions066( )
      {
         O26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
         n26CompetitionTeamsQuality = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
      }

      protected void CheckExtendedTable066( )
      {
         nIsDirty_6 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T000610 */
         pr_default.execute(7, new Object[] {A22CompetitionId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            A26CompetitionTeamsQuality = T000610_A26CompetitionTeamsQuality[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            n26CompetitionTeamsQuality = T000610_n26CompetitionTeamsQuality[0];
         }
         else
         {
            nIsDirty_6 = 1;
            A26CompetitionTeamsQuality = 0;
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         }
         pr_default.close(7);
         if ( ! ( (DateTime.MinValue==A23CompetitionDate) || ( A23CompetitionDate >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Field Competition Date is out of range", "OutOfRange", 1, "COMPETITIONDATE");
            AnyError = 1;
            GX_FocusControl = edtCompetitionDate_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00068 */
         pr_default.execute(6, new Object[] {A24CompetitionDisciplineId});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No matching 'Competition Team'.", "ForeignKeyNotFound", 1, "COMPETITIONDISCIPLINEID");
            AnyError = 1;
            GX_FocusControl = edtCompetitionDisciplineId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A25CompetitionDisciplineName = T00068_A25CompetitionDisciplineName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25CompetitionDisciplineName", A25CompetitionDisciplineName);
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors066( )
      {
         pr_default.close(7);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( short A22CompetitionId )
      {
         /* Using cursor T000614 */
         pr_default.execute(9, new Object[] {A22CompetitionId});
         if ( (pr_default.getStatus(9) != 101) )
         {
            A26CompetitionTeamsQuality = T000614_A26CompetitionTeamsQuality[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            n26CompetitionTeamsQuality = T000614_n26CompetitionTeamsQuality[0];
         }
         else
         {
            A26CompetitionTeamsQuality = 0;
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A26CompetitionTeamsQuality), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(9);
      }

      protected void gxLoad_4( short A24CompetitionDisciplineId )
      {
         /* Using cursor T000615 */
         pr_default.execute(10, new Object[] {A24CompetitionDisciplineId});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("No matching 'Competition Team'.", "ForeignKeyNotFound", 1, "COMPETITIONDISCIPLINEID");
            AnyError = 1;
            GX_FocusControl = edtCompetitionDisciplineId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A25CompetitionDisciplineName = T000615_A25CompetitionDisciplineName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25CompetitionDisciplineName", A25CompetitionDisciplineName);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A25CompetitionDisciplineName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(10);
      }

      protected void GetKey066( )
      {
         /* Using cursor T000616 */
         pr_default.execute(11, new Object[] {A22CompetitionId});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound6 = 1;
         }
         else
         {
            RcdFound6 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00067 */
         pr_default.execute(5, new Object[] {A22CompetitionId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            ZM066( 3) ;
            RcdFound6 = 1;
            A22CompetitionId = T00067_A22CompetitionId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
            A23CompetitionDate = T00067_A23CompetitionDate[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23CompetitionDate", context.localUtil.Format(A23CompetitionDate, "99/99/99"));
            A24CompetitionDisciplineId = T00067_A24CompetitionDisciplineId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A24CompetitionDisciplineId), 4, 0)));
            Z22CompetitionId = A22CompetitionId;
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load066( ) ;
            if ( AnyError == 1 )
            {
               RcdFound6 = 0;
               InitializeNonKey066( ) ;
            }
            Gx_mode = sMode6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound6 = 0;
            InitializeNonKey066( ) ;
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(5);
      }

      protected void getEqualNoModal( )
      {
         GetKey066( ) ;
         if ( RcdFound6 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound6 = 0;
         /* Using cursor T000617 */
         pr_default.execute(12, new Object[] {A22CompetitionId});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T000617_A22CompetitionId[0] < A22CompetitionId ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T000617_A22CompetitionId[0] > A22CompetitionId ) ) )
            {
               A22CompetitionId = T000617_A22CompetitionId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
               RcdFound6 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound6 = 0;
         /* Using cursor T000618 */
         pr_default.execute(13, new Object[] {A22CompetitionId});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T000618_A22CompetitionId[0] > A22CompetitionId ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T000618_A22CompetitionId[0] < A22CompetitionId ) ) )
            {
               A22CompetitionId = T000618_A22CompetitionId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
               RcdFound6 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey066( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            GX_FocusControl = edtCompetitionId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert066( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound6 == 1 )
            {
               if ( A22CompetitionId != Z22CompetitionId )
               {
                  A22CompetitionId = Z22CompetitionId;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "COMPETITIONID");
                  AnyError = 1;
                  GX_FocusControl = edtCompetitionId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
                  n26CompetitionTeamsQuality = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtCompetitionId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
                  n26CompetitionTeamsQuality = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                  Update066( ) ;
                  GX_FocusControl = edtCompetitionId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A22CompetitionId != Z22CompetitionId )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
                  n26CompetitionTeamsQuality = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                  GX_FocusControl = edtCompetitionId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert066( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "COMPETITIONID");
                     AnyError = 1;
                     GX_FocusControl = edtCompetitionId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
                     n26CompetitionTeamsQuality = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                     GX_FocusControl = edtCompetitionId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert066( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A22CompetitionId != Z22CompetitionId )
         {
            A22CompetitionId = Z22CompetitionId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "COMPETITIONID");
            AnyError = 1;
            GX_FocusControl = edtCompetitionId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtCompetitionId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "COMPETITIONID");
            AnyError = 1;
            GX_FocusControl = edtCompetitionId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtCompetitionDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart066( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtCompetitionDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd066( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtCompetitionDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtCompetitionDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart066( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound6 != 0 )
            {
               ScanNext066( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtCompetitionDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd066( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency066( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00066 */
            pr_default.execute(4, new Object[] {A22CompetitionId});
            if ( (pr_default.getStatus(4) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Competition"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(4) == 101) || ( Z23CompetitionDate != T00066_A23CompetitionDate[0] ) || ( Z24CompetitionDisciplineId != T00066_A24CompetitionDisciplineId[0] ) )
            {
               if ( Z23CompetitionDate != T00066_A23CompetitionDate[0] )
               {
                  GXUtil.WriteLog("competition:[seudo value changed for attri]"+"CompetitionDate");
                  GXUtil.WriteLogRaw("Old: ",Z23CompetitionDate);
                  GXUtil.WriteLogRaw("Current: ",T00066_A23CompetitionDate[0]);
               }
               if ( Z24CompetitionDisciplineId != T00066_A24CompetitionDisciplineId[0] )
               {
                  GXUtil.WriteLog("competition:[seudo value changed for attri]"+"CompetitionDisciplineId");
                  GXUtil.WriteLogRaw("Old: ",Z24CompetitionDisciplineId);
                  GXUtil.WriteLogRaw("Current: ",T00066_A24CompetitionDisciplineId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Competition"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert066( )
      {
         BeforeValidate066( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable066( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM066( 0) ;
            CheckOptimisticConcurrency066( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm066( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert066( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000619 */
                     pr_default.execute(14, new Object[] {A23CompetitionDate, A24CompetitionDisciplineId});
                     A22CompetitionId = T000619_A22CompetitionId[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Competition") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel066( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                              ResetCaption060( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load066( ) ;
            }
            EndLevel066( ) ;
         }
         CloseExtendedTableCursors066( ) ;
      }

      protected void Update066( )
      {
         BeforeValidate066( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable066( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency066( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm066( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate066( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000620 */
                     pr_default.execute(15, new Object[] {A23CompetitionDate, A24CompetitionDisciplineId, A22CompetitionId});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("Competition") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Competition"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate066( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel066( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                              ResetCaption060( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel066( ) ;
         }
         CloseExtendedTableCursors066( ) ;
      }

      protected void DeferredUpdate066( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate066( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency066( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls066( ) ;
            AfterConfirm066( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete066( ) ;
               if ( AnyError == 0 )
               {
                  A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
                  n26CompetitionTeamsQuality = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                  ScanStart067( ) ;
                  while ( RcdFound7 != 0 )
                  {
                     getByPrimaryKey067( ) ;
                     Delete067( ) ;
                     ScanNext067( ) ;
                     O26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
                     n26CompetitionTeamsQuality = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                  }
                  ScanEnd067( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000621 */
                     pr_default.execute(16, new Object[] {A22CompetitionId});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("Competition") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound6 == 0 )
                           {
                              InitAll066( ) ;
                              Gx_mode = "INS";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                           ResetCaption060( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode6 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel066( ) ;
         Gx_mode = sMode6;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls066( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000623 */
            pr_default.execute(17, new Object[] {A22CompetitionId});
            if ( (pr_default.getStatus(17) != 101) )
            {
               A26CompetitionTeamsQuality = T000623_A26CompetitionTeamsQuality[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
               n26CompetitionTeamsQuality = T000623_n26CompetitionTeamsQuality[0];
            }
            else
            {
               A26CompetitionTeamsQuality = 0;
               n26CompetitionTeamsQuality = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            }
            pr_default.close(17);
            /* Using cursor T000624 */
            pr_default.execute(18, new Object[] {A24CompetitionDisciplineId});
            A25CompetitionDisciplineName = T000624_A25CompetitionDisciplineName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25CompetitionDisciplineName", A25CompetitionDisciplineName);
            pr_default.close(18);
         }
      }

      protected void ProcessNestedLevel067( )
      {
         s26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
         n26CompetitionTeamsQuality = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         nGXsfl_63_idx = 0;
         while ( nGXsfl_63_idx < nRC_GXsfl_63 )
         {
            ReadRow067( ) ;
            if ( ( nRcdExists_7 != 0 ) || ( nIsMod_7 != 0 ) )
            {
               standaloneNotModal067( ) ;
               GetKey067( ) ;
               if ( ( nRcdExists_7 == 0 ) && ( nRcdDeleted_7 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  Insert067( ) ;
               }
               else
               {
                  if ( RcdFound7 != 0 )
                  {
                     if ( ( nRcdDeleted_7 != 0 ) && ( nRcdExists_7 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        Delete067( ) ;
                     }
                     else
                     {
                        if ( nRcdExists_7 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           Update067( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_7 == 0 )
                     {
                        GXCCtl = "TEAMID_" + sGXsfl_63_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtTeamid_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
               O26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
               n26CompetitionTeamsQuality = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            }
            ChangePostValue( edtTeamid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A14Teamid), 4, 0, ".", ""))) ;
            ChangePostValue( edtDisciplineId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1DisciplineId), 4, 0, ".", ""))) ;
            ChangePostValue( edtDisciplineName_Internalname, StringUtil.RTrim( A2DisciplineName)) ;
            ChangePostValue( edtCompetitionTeamPoints_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A28CompetitionTeamPoints), 4, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z14Teamid_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z14Teamid), 4, 0, ".", ""))) ;
            ChangePostValue( "ZT_"+"Z28CompetitionTeamPoints_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z28CompetitionTeamPoints), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdDeleted_7_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_7), 4, 0, ".", ""))) ;
            ChangePostValue( "nRcdExists_7_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_7), 4, 0, ".", ""))) ;
            ChangePostValue( "nIsMod_7_"+sGXsfl_63_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_7), 4, 0, ".", ""))) ;
            if ( nIsMod_7 != 0 )
            {
               ChangePostValue( "TEAMID_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTeamid_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "DISCIPLINEID_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtDisciplineId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "DISCIPLINENAME_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtDisciplineName_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "COMPETITIONTEAMPOINTS_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCompetitionTeamPoints_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll067( ) ;
         if ( AnyError != 0 )
         {
            O26CompetitionTeamsQuality = s26CompetitionTeamsQuality;
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         }
         nRcdExists_7 = 0;
         nIsMod_7 = 0;
         nRcdDeleted_7 = 0;
      }

      protected void ProcessLevel066( )
      {
         /* Save parent mode. */
         sMode6 = Gx_mode;
         ProcessNestedLevel067( ) ;
         if ( AnyError != 0 )
         {
            O26CompetitionTeamsQuality = s26CompetitionTeamsQuality;
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         }
         /* Restore parent mode. */
         Gx_mode = sMode6;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel066( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(4);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete066( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(5);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(18);
            pr_default.close(17);
            pr_default.close(2);
            pr_default.close(3);
            context.CommitDataStores("competition",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues060( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(5);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(18);
            pr_default.close(17);
            pr_default.close(2);
            pr_default.close(3);
            context.RollbackDataStores("competition",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart066( )
      {
         /* Scan By routine */
         /* Using cursor T000625 */
         pr_default.execute(19);
         RcdFound6 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound6 = 1;
            A22CompetitionId = T000625_A22CompetitionId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext066( )
      {
         /* Scan next routine */
         pr_default.readNext(19);
         RcdFound6 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound6 = 1;
            A22CompetitionId = T000625_A22CompetitionId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
         }
      }

      protected void ScanEnd066( )
      {
         pr_default.close(19);
      }

      protected void AfterConfirm066( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert066( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate066( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete066( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete066( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate066( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes066( )
      {
         edtCompetitionId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCompetitionId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCompetitionId_Enabled), 5, 0)), true);
         edtCompetitionDate_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCompetitionDate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCompetitionDate_Enabled), 5, 0)), true);
         edtCompetitionDisciplineId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCompetitionDisciplineId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCompetitionDisciplineId_Enabled), 5, 0)), true);
         edtCompetitionDisciplineName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCompetitionDisciplineName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCompetitionDisciplineName_Enabled), 5, 0)), true);
         edtCompetitionTeamsQuality_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCompetitionTeamsQuality_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCompetitionTeamsQuality_Enabled), 5, 0)), true);
      }

      protected void ZM067( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z28CompetitionTeamPoints = T00063_A28CompetitionTeamPoints[0];
            }
            else
            {
               Z28CompetitionTeamPoints = A28CompetitionTeamPoints;
            }
         }
         if ( GX_JID == -6 )
         {
            Z22CompetitionId = A22CompetitionId;
            Z28CompetitionTeamPoints = A28CompetitionTeamPoints;
            Z14Teamid = A14Teamid;
            Z1DisciplineId = A1DisciplineId;
            Z2DisciplineName = A2DisciplineName;
         }
      }

      protected void standaloneNotModal067( )
      {
      }

      protected void standaloneModal067( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtTeamid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTeamid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTeamid_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
         }
         else
         {
            edtTeamid_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTeamid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTeamid_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
         }
      }

      protected void Load067( )
      {
         /* Using cursor T000626 */
         pr_default.execute(20, new Object[] {A22CompetitionId, A14Teamid});
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound7 = 1;
            A2DisciplineName = T000626_A2DisciplineName[0];
            A28CompetitionTeamPoints = T000626_A28CompetitionTeamPoints[0];
            A1DisciplineId = T000626_A1DisciplineId[0];
            ZM067( -6) ;
         }
         pr_default.close(20);
         OnLoadActions067( ) ;
      }

      protected void OnLoadActions067( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            A26CompetitionTeamsQuality = (short)(O26CompetitionTeamsQuality+1);
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
               n26CompetitionTeamsQuality = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  A26CompetitionTeamsQuality = (short)(O26CompetitionTeamsQuality-1);
                  n26CompetitionTeamsQuality = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
               }
            }
         }
      }

      protected void CheckExtendedTable067( )
      {
         nIsDirty_7 = 0;
         Gx_BScreen = 1;
         standaloneModal067( ) ;
         /* Using cursor T00064 */
         pr_default.execute(2, new Object[] {A14Teamid});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GXCCtl = "TEAMID_" + sGXsfl_63_idx;
            GX_msglist.addItem("No matching 'Team'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtTeamid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1DisciplineId = T00064_A1DisciplineId[0];
         pr_default.close(2);
         /* Using cursor T00065 */
         pr_default.execute(3, new Object[] {A1DisciplineId});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No matching 'Discipline'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A2DisciplineName = T00065_A2DisciplineName[0];
         pr_default.close(3);
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
         {
            nIsDirty_7 = 1;
            A26CompetitionTeamsQuality = (short)(O26CompetitionTeamsQuality+1);
            n26CompetitionTeamsQuality = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
            {
               nIsDirty_7 = 1;
               A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
               n26CompetitionTeamsQuality = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
               {
                  nIsDirty_7 = 1;
                  A26CompetitionTeamsQuality = (short)(O26CompetitionTeamsQuality-1);
                  n26CompetitionTeamsQuality = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
               }
            }
         }
      }

      protected void CloseExtendedTableCursors067( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable067( )
      {
      }

      protected void gxLoad_7( short A14Teamid )
      {
         /* Using cursor T000627 */
         pr_default.execute(21, new Object[] {A14Teamid});
         if ( (pr_default.getStatus(21) == 101) )
         {
            GXCCtl = "TEAMID_" + sGXsfl_63_idx;
            GX_msglist.addItem("No matching 'Team'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtTeamid_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A1DisciplineId = T000627_A1DisciplineId[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.LTrim( StringUtil.NToC( (decimal)(A1DisciplineId), 4, 0, ".", "")))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(21) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(21);
      }

      protected void gxLoad_8( short A1DisciplineId )
      {
         /* Using cursor T000628 */
         pr_default.execute(22, new Object[] {A1DisciplineId});
         if ( (pr_default.getStatus(22) == 101) )
         {
            GX_msglist.addItem("No matching 'Discipline'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A2DisciplineName = T000628_A2DisciplineName[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A2DisciplineName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(22) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(22);
      }

      protected void GetKey067( )
      {
         /* Using cursor T000629 */
         pr_default.execute(23, new Object[] {A22CompetitionId, A14Teamid});
         if ( (pr_default.getStatus(23) != 101) )
         {
            RcdFound7 = 1;
         }
         else
         {
            RcdFound7 = 0;
         }
         pr_default.close(23);
      }

      protected void getByPrimaryKey067( )
      {
         /* Using cursor T00063 */
         pr_default.execute(1, new Object[] {A22CompetitionId, A14Teamid});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM067( 6) ;
            RcdFound7 = 1;
            InitializeNonKey067( ) ;
            A28CompetitionTeamPoints = T00063_A28CompetitionTeamPoints[0];
            A14Teamid = T00063_A14Teamid[0];
            Z22CompetitionId = A22CompetitionId;
            Z14Teamid = A14Teamid;
            sMode7 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal067( ) ;
            Load067( ) ;
            Gx_mode = sMode7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound7 = 0;
            InitializeNonKey067( ) ;
            sMode7 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal067( ) ;
            Gx_mode = sMode7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes067( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency067( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00062 */
            pr_default.execute(0, new Object[] {A22CompetitionId, A14Teamid});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CompetitionTeam"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z28CompetitionTeamPoints != T00062_A28CompetitionTeamPoints[0] ) )
            {
               if ( Z28CompetitionTeamPoints != T00062_A28CompetitionTeamPoints[0] )
               {
                  GXUtil.WriteLog("competition:[seudo value changed for attri]"+"CompetitionTeamPoints");
                  GXUtil.WriteLogRaw("Old: ",Z28CompetitionTeamPoints);
                  GXUtil.WriteLogRaw("Current: ",T00062_A28CompetitionTeamPoints[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"CompetitionTeam"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert067( )
      {
         BeforeValidate067( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable067( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM067( 0) ;
            CheckOptimisticConcurrency067( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm067( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert067( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000630 */
                     pr_default.execute(24, new Object[] {A22CompetitionId, A28CompetitionTeamPoints, A14Teamid});
                     pr_default.close(24);
                     dsDefault.SmartCacheProvider.SetUpdated("CompetitionTeam") ;
                     if ( (pr_default.getStatus(24) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load067( ) ;
            }
            EndLevel067( ) ;
         }
         CloseExtendedTableCursors067( ) ;
      }

      protected void Update067( )
      {
         BeforeValidate067( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable067( ) ;
         }
         if ( ( nIsMod_7 != 0 ) || ( nIsDirty_7 != 0 ) )
         {
            if ( AnyError == 0 )
            {
               CheckOptimisticConcurrency067( ) ;
               if ( AnyError == 0 )
               {
                  AfterConfirm067( ) ;
                  if ( AnyError == 0 )
                  {
                     BeforeUpdate067( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Using cursor T000631 */
                        pr_default.execute(25, new Object[] {A28CompetitionTeamPoints, A22CompetitionId, A14Teamid});
                        pr_default.close(25);
                        dsDefault.SmartCacheProvider.SetUpdated("CompetitionTeam") ;
                        if ( (pr_default.getStatus(25) == 103) )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CompetitionTeam"}), "RecordIsLocked", 1, "");
                           AnyError = 1;
                        }
                        DeferredUpdate067( ) ;
                        if ( AnyError == 0 )
                        {
                           /* Start of After( update) rules */
                           /* End of After( update) rules */
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey067( ) ;
                           }
                        }
                        else
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                           AnyError = 1;
                        }
                     }
                  }
               }
               EndLevel067( ) ;
            }
         }
         CloseExtendedTableCursors067( ) ;
      }

      protected void DeferredUpdate067( )
      {
      }

      protected void Delete067( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate067( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency067( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls067( ) ;
            AfterConfirm067( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete067( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000632 */
                  pr_default.execute(26, new Object[] {A22CompetitionId, A14Teamid});
                  pr_default.close(26);
                  dsDefault.SmartCacheProvider.SetUpdated("CompetitionTeam") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode7 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel067( ) ;
         Gx_mode = sMode7;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls067( )
      {
         standaloneModal067( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000633 */
            pr_default.execute(27, new Object[] {A14Teamid});
            A1DisciplineId = T000633_A1DisciplineId[0];
            pr_default.close(27);
            /* Using cursor T000634 */
            pr_default.execute(28, new Object[] {A1DisciplineId});
            A2DisciplineName = T000634_A2DisciplineName[0];
            pr_default.close(28);
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               A26CompetitionTeamsQuality = (short)(O26CompetitionTeamsQuality+1);
               n26CompetitionTeamsQuality = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
            }
            else
            {
               if ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )  )
               {
                  A26CompetitionTeamsQuality = O26CompetitionTeamsQuality;
                  n26CompetitionTeamsQuality = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
               }
               else
               {
                  if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
                  {
                     A26CompetitionTeamsQuality = (short)(O26CompetitionTeamsQuality-1);
                     n26CompetitionTeamsQuality = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
                  }
               }
            }
         }
      }

      protected void EndLevel067( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart067( )
      {
         /* Scan By routine */
         /* Using cursor T000635 */
         pr_default.execute(29, new Object[] {A22CompetitionId});
         RcdFound7 = 0;
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound7 = 1;
            A14Teamid = T000635_A14Teamid[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext067( )
      {
         /* Scan next routine */
         pr_default.readNext(29);
         RcdFound7 = 0;
         if ( (pr_default.getStatus(29) != 101) )
         {
            RcdFound7 = 1;
            A14Teamid = T000635_A14Teamid[0];
         }
      }

      protected void ScanEnd067( )
      {
         pr_default.close(29);
      }

      protected void AfterConfirm067( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert067( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate067( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete067( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete067( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate067( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes067( )
      {
         edtTeamid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTeamid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTeamid_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
         edtDisciplineId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtDisciplineId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtDisciplineId_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
         edtDisciplineName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtDisciplineName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtDisciplineName_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
         edtCompetitionTeamPoints_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCompetitionTeamPoints_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCompetitionTeamPoints_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
      }

      protected void send_integrity_lvl_hashes067( )
      {
      }

      protected void send_integrity_lvl_hashes066( )
      {
      }

      protected void SubsflControlProps_637( )
      {
         edtTeamid_Internalname = "TEAMID_"+sGXsfl_63_idx;
         imgprompt_14_Internalname = "PROMPT_14_"+sGXsfl_63_idx;
         edtDisciplineId_Internalname = "DISCIPLINEID_"+sGXsfl_63_idx;
         edtDisciplineName_Internalname = "DISCIPLINENAME_"+sGXsfl_63_idx;
         edtCompetitionTeamPoints_Internalname = "COMPETITIONTEAMPOINTS_"+sGXsfl_63_idx;
      }

      protected void SubsflControlProps_fel_637( )
      {
         edtTeamid_Internalname = "TEAMID_"+sGXsfl_63_fel_idx;
         imgprompt_14_Internalname = "PROMPT_14_"+sGXsfl_63_fel_idx;
         edtDisciplineId_Internalname = "DISCIPLINEID_"+sGXsfl_63_fel_idx;
         edtDisciplineName_Internalname = "DISCIPLINENAME_"+sGXsfl_63_fel_idx;
         edtCompetitionTeamPoints_Internalname = "COMPETITIONTEAMPOINTS_"+sGXsfl_63_fel_idx;
      }

      protected void AddRow067( )
      {
         nGXsfl_63_idx = (int)(nGXsfl_63_idx+1);
         sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
         SubsflControlProps_637( ) ;
         SendRow067( ) ;
      }

      protected void SendRow067( )
      {
         Gridcompetition_teamRow = GXWebRow.GetNew(context);
         if ( subGridcompetition_team_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridcompetition_team_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridcompetition_team_Class, "") != 0 )
            {
               subGridcompetition_team_Linesclass = subGridcompetition_team_Class+"Odd";
            }
         }
         else if ( subGridcompetition_team_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridcompetition_team_Backstyle = 0;
            subGridcompetition_team_Backcolor = subGridcompetition_team_Allbackcolor;
            if ( StringUtil.StrCmp(subGridcompetition_team_Class, "") != 0 )
            {
               subGridcompetition_team_Linesclass = subGridcompetition_team_Class+"Uniform";
            }
         }
         else if ( subGridcompetition_team_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridcompetition_team_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridcompetition_team_Class, "") != 0 )
            {
               subGridcompetition_team_Linesclass = subGridcompetition_team_Class+"Odd";
            }
            subGridcompetition_team_Backcolor = (int)(0x0);
         }
         else if ( subGridcompetition_team_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridcompetition_team_Backstyle = 1;
            if ( ((int)((nGXsfl_63_idx) % (2))) == 0 )
            {
               subGridcompetition_team_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridcompetition_team_Class, "") != 0 )
               {
                  subGridcompetition_team_Linesclass = subGridcompetition_team_Class+"Even";
               }
            }
            else
            {
               subGridcompetition_team_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridcompetition_team_Class, "") != 0 )
               {
                  subGridcompetition_team_Linesclass = subGridcompetition_team_Class+"Odd";
               }
            }
         }
         imgprompt_14_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0)||(StringUtil.StrCmp(Gx_mode, "UPD")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0050.aspx"+"',["+"{Ctrl:gx.dom.el('"+"TEAMID_"+sGXsfl_63_idx+"'), id:'"+"TEAMID_"+sGXsfl_63_idx+"'"+",IOType:'out'}"+"],"+"gx.dom.form()."+"nIsMod_7_"+sGXsfl_63_idx+","+"'', false"+","+"false"+");");
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_7_" + sGXsfl_63_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 64,'',false,'" + sGXsfl_63_idx + "',63)\"";
         ROClassString = "Attribute";
         Gridcompetition_teamRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTeamid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A14Teamid), 4, 0, ".", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A14Teamid), "ZZZ9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,64);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTeamid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtTeamid_Enabled,(short)1,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         Gridcompetition_teamRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgprompt_14_Internalname,(String)sImgUrl,(String)imgprompt_14_Link,(String)"",(String)"",context.GetTheme( ),(int)imgprompt_14_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridcompetition_teamRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtDisciplineId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A1DisciplineId), 4, 0, ".", "")),((edtDisciplineId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1DisciplineId), "ZZZ9")) : context.localUtil.Format( (decimal)(A1DisciplineId), "ZZZ9")),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtDisciplineId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtDisciplineId_Enabled,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridcompetition_teamRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtDisciplineName_Internalname,StringUtil.RTrim( A2DisciplineName),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtDisciplineName_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtDisciplineName_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)60,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)-1,(bool)true,(String)"Name",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_7_" + sGXsfl_63_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_63_idx + "',63)\"";
         ROClassString = "Attribute";
         Gridcompetition_teamRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCompetitionTeamPoints_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A28CompetitionTeamPoints), 4, 0, ".", "")),((edtCompetitionTeamPoints_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A28CompetitionTeamPoints), "ZZZ9")) : context.localUtil.Format( (decimal)(A28CompetitionTeamPoints), "ZZZ9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,',');"+";gx.evt.onblur(this,67);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCompetitionTeamPoints_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtCompetitionTeamPoints_Enabled,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)63,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(Gridcompetition_teamRow);
         send_integrity_lvl_hashes067( ) ;
         GXCCtl = "Z14Teamid_" + sGXsfl_63_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z14Teamid), 4, 0, ".", "")));
         GXCCtl = "Z28CompetitionTeamPoints_" + sGXsfl_63_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z28CompetitionTeamPoints), 4, 0, ".", "")));
         GXCCtl = "nRcdDeleted_7_" + sGXsfl_63_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_7), 4, 0, ".", "")));
         GXCCtl = "nRcdExists_7_" + sGXsfl_63_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_7), 4, 0, ".", "")));
         GXCCtl = "nIsMod_7_" + sGXsfl_63_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_7), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TEAMID_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTeamid_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DISCIPLINEID_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtDisciplineId_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "DISCIPLINENAME_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtDisciplineName_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "COMPETITIONTEAMPOINTS_"+sGXsfl_63_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtCompetitionTeamPoints_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROMPT_14_"+sGXsfl_63_idx+"Link", StringUtil.RTrim( imgprompt_14_Link));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridcompetition_teamContainer.AddRow(Gridcompetition_teamRow);
      }

      protected void ReadRow067( )
      {
         nGXsfl_63_idx = (int)(nGXsfl_63_idx+1);
         sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
         SubsflControlProps_637( ) ;
         edtTeamid_Enabled = (int)(context.localUtil.CToN( cgiGet( "TEAMID_"+sGXsfl_63_idx+"Enabled"), ".", ","));
         edtDisciplineId_Enabled = (int)(context.localUtil.CToN( cgiGet( "DISCIPLINEID_"+sGXsfl_63_idx+"Enabled"), ".", ","));
         edtDisciplineName_Enabled = (int)(context.localUtil.CToN( cgiGet( "DISCIPLINENAME_"+sGXsfl_63_idx+"Enabled"), ".", ","));
         edtCompetitionTeamPoints_Enabled = (int)(context.localUtil.CToN( cgiGet( "COMPETITIONTEAMPOINTS_"+sGXsfl_63_idx+"Enabled"), ".", ","));
         imgprompt_24_Link = cgiGet( "PROMPT_14_"+sGXsfl_63_idx+"Link");
         if ( ( ( context.localUtil.CToN( cgiGet( edtTeamid_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTeamid_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
         {
            GXCCtl = "TEAMID_" + sGXsfl_63_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtTeamid_Internalname;
            wbErr = true;
            A14Teamid = 0;
         }
         else
         {
            A14Teamid = (short)(context.localUtil.CToN( cgiGet( edtTeamid_Internalname), ".", ","));
         }
         A1DisciplineId = (short)(context.localUtil.CToN( cgiGet( edtDisciplineId_Internalname), ".", ","));
         A2DisciplineName = cgiGet( edtDisciplineName_Internalname);
         if ( ( ( context.localUtil.CToN( cgiGet( edtCompetitionTeamPoints_Internalname), ".", ",") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtCompetitionTeamPoints_Internalname), ".", ",") > Convert.ToDecimal( 9999 )) ) )
         {
            GXCCtl = "COMPETITIONTEAMPOINTS_" + sGXsfl_63_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtCompetitionTeamPoints_Internalname;
            wbErr = true;
            A28CompetitionTeamPoints = 0;
         }
         else
         {
            A28CompetitionTeamPoints = (short)(context.localUtil.CToN( cgiGet( edtCompetitionTeamPoints_Internalname), ".", ","));
         }
         GXCCtl = "Z14Teamid_" + sGXsfl_63_idx;
         Z14Teamid = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "Z28CompetitionTeamPoints_" + sGXsfl_63_idx;
         Z28CompetitionTeamPoints = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdDeleted_7_" + sGXsfl_63_idx;
         nRcdDeleted_7 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nRcdExists_7_" + sGXsfl_63_idx;
         nRcdExists_7 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
         GXCCtl = "nIsMod_7_" + sGXsfl_63_idx;
         nIsMod_7 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ".", ","));
      }

      protected void assign_properties_default( )
      {
         defedtTeamid_Enabled = edtTeamid_Enabled;
      }

      protected void ConfirmValues060( )
      {
         nGXsfl_63_idx = 0;
         sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
         SubsflControlProps_637( ) ;
         while ( nGXsfl_63_idx < nRC_GXsfl_63 )
         {
            nGXsfl_63_idx = (int)(nGXsfl_63_idx+1);
            sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
            SubsflControlProps_637( ) ;
            ChangePostValue( "Z14Teamid_"+sGXsfl_63_idx, cgiGet( "ZT_"+"Z14Teamid_"+sGXsfl_63_idx)) ;
            DeletePostValue( "ZT_"+"Z14Teamid_"+sGXsfl_63_idx) ;
            ChangePostValue( "Z28CompetitionTeamPoints_"+sGXsfl_63_idx, cgiGet( "ZT_"+"Z28CompetitionTeamPoints_"+sGXsfl_63_idx)) ;
            DeletePostValue( "ZT_"+"Z28CompetitionTeamPoints_"+sGXsfl_63_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 135614), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxcfg.js", "?20226153501153", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-en.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("competition.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z22CompetitionId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z22CompetitionId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z23CompetitionDate", context.localUtil.DToC( Z23CompetitionDate, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z24CompetitionDisciplineId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "O26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.NToC( (decimal)(O26CompetitionTeamsQuality), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_63", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_63_idx), 8, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("competition.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Competition" ;
      }

      public override String GetPgmdesc( )
      {
         return "Competition" ;
      }

      protected void InitializeNonKey066( )
      {
         A23CompetitionDate = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23CompetitionDate", context.localUtil.Format(A23CompetitionDate, "99/99/99"));
         A24CompetitionDisciplineId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.Str( (decimal)(A24CompetitionDisciplineId), 4, 0)));
         A25CompetitionDisciplineName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25CompetitionDisciplineName", A25CompetitionDisciplineName);
         A26CompetitionTeamsQuality = 0;
         n26CompetitionTeamsQuality = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         O26CompetitionTeamsQuality = A26CompetitionTeamsQuality;
         n26CompetitionTeamsQuality = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.Str( (decimal)(A26CompetitionTeamsQuality), 4, 0)));
         Z23CompetitionDate = DateTime.MinValue;
         Z24CompetitionDisciplineId = 0;
      }

      protected void InitAll066( )
      {
         A22CompetitionId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A22CompetitionId", StringUtil.LTrim( StringUtil.Str( (decimal)(A22CompetitionId), 4, 0)));
         InitializeNonKey066( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey067( )
      {
         A1DisciplineId = 0;
         A2DisciplineName = "";
         A28CompetitionTeamPoints = 0;
         Z28CompetitionTeamPoints = 0;
      }

      protected void InitAll067( )
      {
         A14Teamid = 0;
         InitializeNonKey067( ) ;
      }

      protected void StandaloneModalInsert067( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20226153501160", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.eng.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("competition.js", "?20226153501160", false, true);
         /* End function include_jscripts */
      }

      protected void init_level_properties7( )
      {
         edtTeamid_Enabled = defedtTeamid_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTeamid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTeamid_Enabled), 5, 0)), !bGXsfl_63_Refreshing);
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtCompetitionId_Internalname = "COMPETITIONID";
         edtCompetitionDate_Internalname = "COMPETITIONDATE";
         edtCompetitionDisciplineId_Internalname = "COMPETITIONDISCIPLINEID";
         edtCompetitionDisciplineName_Internalname = "COMPETITIONDISCIPLINENAME";
         edtCompetitionTeamsQuality_Internalname = "COMPETITIONTEAMSQUALITY";
         lblTitleteam_Internalname = "TITLETEAM";
         edtTeamid_Internalname = "TEAMID";
         edtDisciplineId_Internalname = "DISCIPLINEID";
         edtDisciplineName_Internalname = "DISCIPLINENAME";
         edtCompetitionTeamPoints_Internalname = "COMPETITIONTEAMPOINTS";
         divTeamtable_Internalname = "TEAMTABLE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_24_Internalname = "PROMPT_24";
         imgprompt_14_Internalname = "PROMPT_14";
         subGridcompetition_team_Internalname = "GRIDCOMPETITION_TEAM";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Competition";
         edtCompetitionTeamPoints_Jsonclick = "";
         edtDisciplineName_Jsonclick = "";
         edtDisciplineId_Jsonclick = "";
         imgprompt_14_Visible = 1;
         imgprompt_14_Link = "";
         imgprompt_24_Visible = 1;
         edtTeamid_Jsonclick = "";
         subGridcompetition_team_Class = "Grid";
         subGridcompetition_team_Backcolorstyle = 0;
         subGridcompetition_team_Allowcollapsing = 0;
         subGridcompetition_team_Allowselection = 0;
         edtCompetitionTeamPoints_Enabled = 1;
         edtDisciplineName_Enabled = 0;
         edtDisciplineId_Enabled = 0;
         edtTeamid_Enabled = 1;
         subGridcompetition_team_Header = "";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtCompetitionTeamsQuality_Jsonclick = "";
         edtCompetitionTeamsQuality_Enabled = 0;
         edtCompetitionDisciplineName_Jsonclick = "";
         edtCompetitionDisciplineName_Enabled = 0;
         imgprompt_24_Visible = 1;
         imgprompt_24_Link = "";
         edtCompetitionDisciplineId_Jsonclick = "";
         edtCompetitionDisciplineId_Enabled = 1;
         edtCompetitionDate_Jsonclick = "";
         edtCompetitionDate_Enabled = 1;
         edtCompetitionId_Jsonclick = "";
         edtCompetitionId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridcompetition_team_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_637( ) ;
         while ( nGXsfl_63_idx <= nRC_GXsfl_63 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal067( ) ;
            standaloneModal067( ) ;
            init_web_controls( ) ;
            dynload_actions( ) ;
            SendRow067( ) ;
            nGXsfl_63_idx = (int)(nGXsfl_63_idx+1);
            sGXsfl_63_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_63_idx), 4, 0)), 4, "0");
            SubsflControlProps_637( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Gridcompetition_teamContainer));
         /* End function gxnrGridcompetition_team_newrow */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtCompetitionDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Competitionid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         /* Using cursor T000623 */
         pr_default.execute(17, new Object[] {A22CompetitionId});
         if ( (pr_default.getStatus(17) != 101) )
         {
            A26CompetitionTeamsQuality = T000623_A26CompetitionTeamsQuality[0];
            n26CompetitionTeamsQuality = T000623_n26CompetitionTeamsQuality[0];
         }
         else
         {
            A26CompetitionTeamsQuality = 0;
            n26CompetitionTeamsQuality = false;
         }
         pr_default.close(17);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23CompetitionDate", context.localUtil.Format(A23CompetitionDate, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A24CompetitionDisciplineId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.NToC( (decimal)(A26CompetitionTeamsQuality), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25CompetitionDisciplineName", StringUtil.RTrim( A25CompetitionDisciplineName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z22CompetitionId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z22CompetitionId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z23CompetitionDate", context.localUtil.Format(Z23CompetitionDate, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "Z24CompetitionDisciplineId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z24CompetitionDisciplineId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z26CompetitionTeamsQuality), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z25CompetitionDisciplineName", StringUtil.RTrim( Z25CompetitionDisciplineName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "O26CompetitionTeamsQuality", StringUtil.LTrim( StringUtil.NToC( (decimal)(O26CompetitionTeamsQuality), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Competitiondisciplineid( )
      {
         /* Using cursor T000624 */
         pr_default.execute(18, new Object[] {A24CompetitionDisciplineId});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("No matching 'Competition Team'.", "ForeignKeyNotFound", 1, "COMPETITIONDISCIPLINEID");
            AnyError = 1;
            GX_FocusControl = edtCompetitionDisciplineId_Internalname;
         }
         A25CompetitionDisciplineName = T000624_A25CompetitionDisciplineName[0];
         pr_default.close(18);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A25CompetitionDisciplineName", StringUtil.RTrim( A25CompetitionDisciplineName));
      }

      public void Valid_Teamid( )
      {
         /* Using cursor T000633 */
         pr_default.execute(27, new Object[] {A14Teamid});
         if ( (pr_default.getStatus(27) == 101) )
         {
            GX_msglist.addItem("No matching 'Team'.", "ForeignKeyNotFound", 1, "TEAMID");
            AnyError = 1;
            GX_FocusControl = edtTeamid_Internalname;
         }
         A1DisciplineId = T000633_A1DisciplineId[0];
         pr_default.close(27);
         /* Using cursor T000634 */
         pr_default.execute(28, new Object[] {A1DisciplineId});
         if ( (pr_default.getStatus(28) == 101) )
         {
            GX_msglist.addItem("No matching 'Discipline'.", "ForeignKeyNotFound", 1, "");
            AnyError = 1;
         }
         A2DisciplineName = T000634_A2DisciplineName[0];
         pr_default.close(28);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1DisciplineId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A1DisciplineId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2DisciplineName", StringUtil.RTrim( A2DisciplineName));
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12062',iparms:[]");
         setEventMetadata("AFTER TRN",",oparms:[]}");
         setEventMetadata("VALID_COMPETITIONID","{handler:'Valid_Competitionid',iparms:[{av:'A22CompetitionId',fld:'COMPETITIONID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_COMPETITIONID",",oparms:[{av:'A23CompetitionDate',fld:'COMPETITIONDATE',pic:''},{av:'A24CompetitionDisciplineId',fld:'COMPETITIONDISCIPLINEID',pic:'ZZZ9'},{av:'A26CompetitionTeamsQuality',fld:'COMPETITIONTEAMSQUALITY',pic:'ZZZ9'},{av:'A25CompetitionDisciplineName',fld:'COMPETITIONDISCIPLINENAME',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z22CompetitionId'},{av:'Z23CompetitionDate'},{av:'Z24CompetitionDisciplineId'},{av:'Z26CompetitionTeamsQuality'},{av:'Z25CompetitionDisciplineName'},{av:'O26CompetitionTeamsQuality'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_COMPETITIONDATE","{handler:'Valid_Competitiondate',iparms:[]");
         setEventMetadata("VALID_COMPETITIONDATE",",oparms:[]}");
         setEventMetadata("VALID_COMPETITIONDISCIPLINEID","{handler:'Valid_Competitiondisciplineid',iparms:[{av:'A24CompetitionDisciplineId',fld:'COMPETITIONDISCIPLINEID',pic:'ZZZ9'},{av:'A25CompetitionDisciplineName',fld:'COMPETITIONDISCIPLINENAME',pic:''}]");
         setEventMetadata("VALID_COMPETITIONDISCIPLINEID",",oparms:[{av:'A25CompetitionDisciplineName',fld:'COMPETITIONDISCIPLINENAME',pic:''}]}");
         setEventMetadata("VALID_TEAMID","{handler:'Valid_Teamid',iparms:[{av:'A14Teamid',fld:'TEAMID',pic:'ZZZ9'},{av:'A1DisciplineId',fld:'DISCIPLINEID',pic:'ZZZ9'},{av:'A2DisciplineName',fld:'DISCIPLINENAME',pic:''}]");
         setEventMetadata("VALID_TEAMID",",oparms:[{av:'A1DisciplineId',fld:'DISCIPLINEID',pic:'ZZZ9'},{av:'A2DisciplineName',fld:'DISCIPLINENAME',pic:''}]}");
         setEventMetadata("VALID_DISCIPLINEID","{handler:'Valid_Disciplineid',iparms:[]");
         setEventMetadata("VALID_DISCIPLINEID",",oparms:[]}");
         setEventMetadata("VALID_COMPETITIONTEAMPOINTS","{handler:'Valid_Competitionteampoints',iparms:[]");
         setEventMetadata("VALID_COMPETITIONTEAMPOINTS",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(27);
         pr_default.close(28);
         pr_default.close(5);
         pr_default.close(18);
         pr_default.close(17);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z23CompetitionDate = DateTime.MinValue;
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         Gx_mode = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A23CompetitionDate = DateTime.MinValue;
         sImgUrl = "";
         A25CompetitionDisciplineName = "";
         lblTitleteam_Jsonclick = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gridcompetition_teamContainer = new GXWebGrid( context);
         Gridcompetition_teamColumn = new GXWebColumn();
         A2DisciplineName = "";
         sMode7 = "";
         sStyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         Z25CompetitionDisciplineName = "";
         T000612_A22CompetitionId = new short[1] ;
         T000612_A23CompetitionDate = new DateTime[] {DateTime.MinValue} ;
         T000612_A25CompetitionDisciplineName = new String[] {""} ;
         T000612_A24CompetitionDisciplineId = new short[1] ;
         T000612_A26CompetitionTeamsQuality = new short[1] ;
         T000612_n26CompetitionTeamsQuality = new bool[] {false} ;
         T000610_A26CompetitionTeamsQuality = new short[1] ;
         T000610_n26CompetitionTeamsQuality = new bool[] {false} ;
         T00068_A25CompetitionDisciplineName = new String[] {""} ;
         T000614_A26CompetitionTeamsQuality = new short[1] ;
         T000614_n26CompetitionTeamsQuality = new bool[] {false} ;
         T000615_A25CompetitionDisciplineName = new String[] {""} ;
         T000616_A22CompetitionId = new short[1] ;
         T00067_A22CompetitionId = new short[1] ;
         T00067_A23CompetitionDate = new DateTime[] {DateTime.MinValue} ;
         T00067_A24CompetitionDisciplineId = new short[1] ;
         sMode6 = "";
         T000617_A22CompetitionId = new short[1] ;
         T000618_A22CompetitionId = new short[1] ;
         T00066_A22CompetitionId = new short[1] ;
         T00066_A23CompetitionDate = new DateTime[] {DateTime.MinValue} ;
         T00066_A24CompetitionDisciplineId = new short[1] ;
         T000619_A22CompetitionId = new short[1] ;
         T000623_A26CompetitionTeamsQuality = new short[1] ;
         T000623_n26CompetitionTeamsQuality = new bool[] {false} ;
         T000624_A25CompetitionDisciplineName = new String[] {""} ;
         T000625_A22CompetitionId = new short[1] ;
         Z2DisciplineName = "";
         T000626_A22CompetitionId = new short[1] ;
         T000626_A2DisciplineName = new String[] {""} ;
         T000626_A28CompetitionTeamPoints = new short[1] ;
         T000626_A14Teamid = new short[1] ;
         T000626_A1DisciplineId = new short[1] ;
         T00064_A1DisciplineId = new short[1] ;
         T00065_A2DisciplineName = new String[] {""} ;
         T000627_A1DisciplineId = new short[1] ;
         T000628_A2DisciplineName = new String[] {""} ;
         T000629_A22CompetitionId = new short[1] ;
         T000629_A14Teamid = new short[1] ;
         T00063_A22CompetitionId = new short[1] ;
         T00063_A28CompetitionTeamPoints = new short[1] ;
         T00063_A14Teamid = new short[1] ;
         T00062_A22CompetitionId = new short[1] ;
         T00062_A28CompetitionTeamPoints = new short[1] ;
         T00062_A14Teamid = new short[1] ;
         T000633_A1DisciplineId = new short[1] ;
         T000634_A2DisciplineName = new String[] {""} ;
         T000635_A22CompetitionId = new short[1] ;
         T000635_A14Teamid = new short[1] ;
         Gridcompetition_teamRow = new GXWebRow();
         subGridcompetition_team_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         ZZ23CompetitionDate = DateTime.MinValue;
         ZZ25CompetitionDisciplineName = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.competition__default(),
            new Object[][] {
                new Object[] {
               T00062_A22CompetitionId, T00062_A28CompetitionTeamPoints, T00062_A14Teamid
               }
               , new Object[] {
               T00063_A22CompetitionId, T00063_A28CompetitionTeamPoints, T00063_A14Teamid
               }
               , new Object[] {
               T00064_A1DisciplineId
               }
               , new Object[] {
               T00065_A2DisciplineName
               }
               , new Object[] {
               T00066_A22CompetitionId, T00066_A23CompetitionDate, T00066_A24CompetitionDisciplineId
               }
               , new Object[] {
               T00067_A22CompetitionId, T00067_A23CompetitionDate, T00067_A24CompetitionDisciplineId
               }
               , new Object[] {
               T00068_A25CompetitionDisciplineName
               }
               , new Object[] {
               T000610_A26CompetitionTeamsQuality, T000610_n26CompetitionTeamsQuality
               }
               , new Object[] {
               T000612_A22CompetitionId, T000612_A23CompetitionDate, T000612_A25CompetitionDisciplineName, T000612_A24CompetitionDisciplineId, T000612_A26CompetitionTeamsQuality, T000612_n26CompetitionTeamsQuality
               }
               , new Object[] {
               T000614_A26CompetitionTeamsQuality, T000614_n26CompetitionTeamsQuality
               }
               , new Object[] {
               T000615_A25CompetitionDisciplineName
               }
               , new Object[] {
               T000616_A22CompetitionId
               }
               , new Object[] {
               T000617_A22CompetitionId
               }
               , new Object[] {
               T000618_A22CompetitionId
               }
               , new Object[] {
               T000619_A22CompetitionId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000623_A26CompetitionTeamsQuality, T000623_n26CompetitionTeamsQuality
               }
               , new Object[] {
               T000624_A25CompetitionDisciplineName
               }
               , new Object[] {
               T000625_A22CompetitionId
               }
               , new Object[] {
               T000626_A22CompetitionId, T000626_A2DisciplineName, T000626_A28CompetitionTeamPoints, T000626_A14Teamid, T000626_A1DisciplineId
               }
               , new Object[] {
               T000627_A1DisciplineId
               }
               , new Object[] {
               T000628_A2DisciplineName
               }
               , new Object[] {
               T000629_A22CompetitionId, T000629_A14Teamid
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000633_A1DisciplineId
               }
               , new Object[] {
               T000634_A2DisciplineName
               }
               , new Object[] {
               T000635_A22CompetitionId, T000635_A14Teamid
               }
            }
         );
      }

      private short nIsMod_7 ;
      private short Z22CompetitionId ;
      private short Z24CompetitionDisciplineId ;
      private short O26CompetitionTeamsQuality ;
      private short Z14Teamid ;
      private short Z28CompetitionTeamPoints ;
      private short nRcdDeleted_7 ;
      private short nRcdExists_7 ;
      private short GxWebError ;
      private short A22CompetitionId ;
      private short A24CompetitionDisciplineId ;
      private short A14Teamid ;
      private short A1DisciplineId ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A26CompetitionTeamsQuality ;
      private short subGridcompetition_team_Backcolorstyle ;
      private short A28CompetitionTeamPoints ;
      private short subGridcompetition_team_Allowselection ;
      private short subGridcompetition_team_Allowhovering ;
      private short subGridcompetition_team_Allowcollapsing ;
      private short subGridcompetition_team_Collapsed ;
      private short nBlankRcdCount7 ;
      private short RcdFound7 ;
      private short B26CompetitionTeamsQuality ;
      private short nBlankRcdUsr7 ;
      private short s26CompetitionTeamsQuality ;
      private short GX_JID ;
      private short Z26CompetitionTeamsQuality ;
      private short RcdFound6 ;
      private short nIsDirty_6 ;
      private short Gx_BScreen ;
      private short Z1DisciplineId ;
      private short nIsDirty_7 ;
      private short subGridcompetition_team_Backstyle ;
      private short gxajaxcallmode ;
      private short ZZ22CompetitionId ;
      private short ZZ24CompetitionDisciplineId ;
      private short ZZ26CompetitionTeamsQuality ;
      private short ZO26CompetitionTeamsQuality ;
      private int nRC_GXsfl_63 ;
      private int nGXsfl_63_idx=1 ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtCompetitionId_Enabled ;
      private int edtCompetitionDate_Enabled ;
      private int edtCompetitionDisciplineId_Enabled ;
      private int imgprompt_24_Visible ;
      private int edtCompetitionDisciplineName_Enabled ;
      private int edtCompetitionTeamsQuality_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int edtTeamid_Enabled ;
      private int edtDisciplineId_Enabled ;
      private int edtDisciplineName_Enabled ;
      private int edtCompetitionTeamPoints_Enabled ;
      private int subGridcompetition_team_Selectedindex ;
      private int subGridcompetition_team_Selectioncolor ;
      private int subGridcompetition_team_Hoveringcolor ;
      private int fRowAdded ;
      private int subGridcompetition_team_Backcolor ;
      private int subGridcompetition_team_Allbackcolor ;
      private int imgprompt_14_Visible ;
      private int defedtTeamid_Enabled ;
      private int idxLst ;
      private long GRIDCOMPETITION_TEAM_nFirstRecordOnPage ;
      private String sPrefix ;
      private String sGXsfl_63_idx="0001" ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtCompetitionId_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtCompetitionId_Jsonclick ;
      private String edtCompetitionDate_Internalname ;
      private String edtCompetitionDate_Jsonclick ;
      private String edtCompetitionDisciplineId_Internalname ;
      private String edtCompetitionDisciplineId_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_24_Internalname ;
      private String imgprompt_24_Link ;
      private String edtCompetitionDisciplineName_Internalname ;
      private String A25CompetitionDisciplineName ;
      private String edtCompetitionDisciplineName_Jsonclick ;
      private String edtCompetitionTeamsQuality_Internalname ;
      private String edtCompetitionTeamsQuality_Jsonclick ;
      private String divTeamtable_Internalname ;
      private String lblTitleteam_Internalname ;
      private String lblTitleteam_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String subGridcompetition_team_Header ;
      private String A2DisciplineName ;
      private String sMode7 ;
      private String edtTeamid_Internalname ;
      private String edtDisciplineId_Internalname ;
      private String edtDisciplineName_Internalname ;
      private String edtCompetitionTeamPoints_Internalname ;
      private String sStyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Z25CompetitionDisciplineName ;
      private String sMode6 ;
      private String Z2DisciplineName ;
      private String imgprompt_14_Internalname ;
      private String sGXsfl_63_fel_idx="0001" ;
      private String subGridcompetition_team_Class ;
      private String subGridcompetition_team_Linesclass ;
      private String imgprompt_14_Link ;
      private String ROClassString ;
      private String edtTeamid_Jsonclick ;
      private String edtDisciplineId_Jsonclick ;
      private String edtDisciplineName_Jsonclick ;
      private String edtCompetitionTeamPoints_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridcompetition_team_Internalname ;
      private String ZZ25CompetitionDisciplineName ;
      private DateTime Z23CompetitionDate ;
      private DateTime A23CompetitionDate ;
      private DateTime ZZ23CompetitionDate ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n26CompetitionTeamsQuality ;
      private bool bGXsfl_63_Refreshing=false ;
      private GXWebGrid Gridcompetition_teamContainer ;
      private GXWebRow Gridcompetition_teamRow ;
      private GXWebColumn Gridcompetition_teamColumn ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] T000612_A22CompetitionId ;
      private DateTime[] T000612_A23CompetitionDate ;
      private String[] T000612_A25CompetitionDisciplineName ;
      private short[] T000612_A24CompetitionDisciplineId ;
      private short[] T000612_A26CompetitionTeamsQuality ;
      private bool[] T000612_n26CompetitionTeamsQuality ;
      private short[] T000610_A26CompetitionTeamsQuality ;
      private bool[] T000610_n26CompetitionTeamsQuality ;
      private String[] T00068_A25CompetitionDisciplineName ;
      private short[] T000614_A26CompetitionTeamsQuality ;
      private bool[] T000614_n26CompetitionTeamsQuality ;
      private String[] T000615_A25CompetitionDisciplineName ;
      private short[] T000616_A22CompetitionId ;
      private short[] T00067_A22CompetitionId ;
      private DateTime[] T00067_A23CompetitionDate ;
      private short[] T00067_A24CompetitionDisciplineId ;
      private short[] T000617_A22CompetitionId ;
      private short[] T000618_A22CompetitionId ;
      private short[] T00066_A22CompetitionId ;
      private DateTime[] T00066_A23CompetitionDate ;
      private short[] T00066_A24CompetitionDisciplineId ;
      private short[] T000619_A22CompetitionId ;
      private short[] T000623_A26CompetitionTeamsQuality ;
      private bool[] T000623_n26CompetitionTeamsQuality ;
      private String[] T000624_A25CompetitionDisciplineName ;
      private short[] T000625_A22CompetitionId ;
      private short[] T000626_A22CompetitionId ;
      private String[] T000626_A2DisciplineName ;
      private short[] T000626_A28CompetitionTeamPoints ;
      private short[] T000626_A14Teamid ;
      private short[] T000626_A1DisciplineId ;
      private short[] T00064_A1DisciplineId ;
      private String[] T00065_A2DisciplineName ;
      private short[] T000627_A1DisciplineId ;
      private String[] T000628_A2DisciplineName ;
      private short[] T000629_A22CompetitionId ;
      private short[] T000629_A14Teamid ;
      private short[] T00063_A22CompetitionId ;
      private short[] T00063_A28CompetitionTeamPoints ;
      private short[] T00063_A14Teamid ;
      private short[] T00062_A22CompetitionId ;
      private short[] T00062_A28CompetitionTeamPoints ;
      private short[] T00062_A14Teamid ;
      private short[] T000633_A1DisciplineId ;
      private String[] T000634_A2DisciplineName ;
      private short[] T000635_A22CompetitionId ;
      private short[] T000635_A14Teamid ;
      private GXWebForm Form ;
   }

   public class competition__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new ForEachCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new UpdateCursor(def[26])
         ,new ForEachCursor(def[27])
         ,new ForEachCursor(def[28])
         ,new ForEachCursor(def[29])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000612 ;
          prmT000612 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000610 ;
          prmT000610 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00068 ;
          prmT00068 = new Object[] {
          new Object[] {"@CompetitionDisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000614 ;
          prmT000614 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000615 ;
          prmT000615 = new Object[] {
          new Object[] {"@CompetitionDisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000616 ;
          prmT000616 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00067 ;
          prmT00067 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000617 ;
          prmT000617 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000618 ;
          prmT000618 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00066 ;
          prmT00066 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000619 ;
          prmT000619 = new Object[] {
          new Object[] {"@CompetitionDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@CompetitionDisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000620 ;
          prmT000620 = new Object[] {
          new Object[] {"@CompetitionDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@CompetitionDisciplineId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000621 ;
          prmT000621 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000625 ;
          prmT000625 = new Object[] {
          } ;
          Object[] prmT000626 ;
          prmT000626 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00064 ;
          prmT00064 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00065 ;
          prmT00065 = new Object[] {
          new Object[] {"@DisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000627 ;
          prmT000627 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000628 ;
          prmT000628 = new Object[] {
          new Object[] {"@DisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000629 ;
          prmT000629 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00063 ;
          prmT00063 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT00062 ;
          prmT00062 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000630 ;
          prmT000630 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CompetitionTeamPoints",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000631 ;
          prmT000631 = new Object[] {
          new Object[] {"@CompetitionTeamPoints",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000632 ;
          prmT000632 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000635 ;
          prmT000635 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000623 ;
          prmT000623 = new Object[] {
          new Object[] {"@CompetitionId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000624 ;
          prmT000624 = new Object[] {
          new Object[] {"@CompetitionDisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000633 ;
          prmT000633 = new Object[] {
          new Object[] {"@Teamid",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000634 ;
          prmT000634 = new Object[] {
          new Object[] {"@DisciplineId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T00062", "SELECT [CompetitionId], [CompetitionTeamPoints], [Teamid] FROM [CompetitionTeam] WITH (UPDLOCK) WHERE [CompetitionId] = @CompetitionId AND [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00062,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00063", "SELECT [CompetitionId], [CompetitionTeamPoints], [Teamid] FROM [CompetitionTeam] WHERE [CompetitionId] = @CompetitionId AND [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00063,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00064", "SELECT [DisciplineId] FROM [Team] WHERE [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT00064,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00065", "SELECT [DisciplineName] FROM [Discipline] WHERE [DisciplineId] = @DisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00065,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00066", "SELECT [CompetitionId], [CompetitionDate], [CompetitionDisciplineId] AS CompetitionDisciplineId FROM [Competition] WITH (UPDLOCK) WHERE [CompetitionId] = @CompetitionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00066,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00067", "SELECT [CompetitionId], [CompetitionDate], [CompetitionDisciplineId] AS CompetitionDisciplineId FROM [Competition] WHERE [CompetitionId] = @CompetitionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00067,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T00068", "SELECT [DisciplineName] AS CompetitionDisciplineName FROM [Discipline] WHERE [DisciplineId] = @CompetitionDisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT00068,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000610", "SELECT COALESCE( T1.[CompetitionTeamsQuality], 0) AS CompetitionTeamsQuality FROM (SELECT COUNT(*) AS CompetitionTeamsQuality, [CompetitionId] FROM [CompetitionTeam] WITH (UPDLOCK) GROUP BY [CompetitionId] ) T1 WHERE T1.[CompetitionId] = @CompetitionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000610,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000612", "SELECT TM1.[CompetitionId], TM1.[CompetitionDate], T3.[DisciplineName] AS CompetitionDisciplineName, TM1.[CompetitionDisciplineId] AS CompetitionDisciplineId, COALESCE( T2.[CompetitionTeamsQuality], 0) AS CompetitionTeamsQuality FROM (([Competition] TM1 LEFT JOIN (SELECT COUNT(*) AS CompetitionTeamsQuality, [CompetitionId] FROM [CompetitionTeam] GROUP BY [CompetitionId] ) T2 ON T2.[CompetitionId] = TM1.[CompetitionId]) INNER JOIN [Discipline] T3 ON T3.[DisciplineId] = TM1.[CompetitionDisciplineId]) WHERE TM1.[CompetitionId] = @CompetitionId ORDER BY TM1.[CompetitionId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000612,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000614", "SELECT COALESCE( T1.[CompetitionTeamsQuality], 0) AS CompetitionTeamsQuality FROM (SELECT COUNT(*) AS CompetitionTeamsQuality, [CompetitionId] FROM [CompetitionTeam] WITH (UPDLOCK) GROUP BY [CompetitionId] ) T1 WHERE T1.[CompetitionId] = @CompetitionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000614,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000615", "SELECT [DisciplineName] AS CompetitionDisciplineName FROM [Discipline] WHERE [DisciplineId] = @CompetitionDisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000615,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000616", "SELECT [CompetitionId] FROM [Competition] WHERE [CompetitionId] = @CompetitionId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000616,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000617", "SELECT TOP 1 [CompetitionId] FROM [Competition] WHERE ( [CompetitionId] > @CompetitionId) ORDER BY [CompetitionId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000617,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000618", "SELECT TOP 1 [CompetitionId] FROM [Competition] WHERE ( [CompetitionId] < @CompetitionId) ORDER BY [CompetitionId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000618,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000619", "INSERT INTO [Competition]([CompetitionDate], [CompetitionDisciplineId]) VALUES(@CompetitionDate, @CompetitionDisciplineId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000619)
             ,new CursorDef("T000620", "UPDATE [Competition] SET [CompetitionDate]=@CompetitionDate, [CompetitionDisciplineId]=@CompetitionDisciplineId  WHERE [CompetitionId] = @CompetitionId", GxErrorMask.GX_NOMASK,prmT000620)
             ,new CursorDef("T000621", "DELETE FROM [Competition]  WHERE [CompetitionId] = @CompetitionId", GxErrorMask.GX_NOMASK,prmT000621)
             ,new CursorDef("T000623", "SELECT COALESCE( T1.[CompetitionTeamsQuality], 0) AS CompetitionTeamsQuality FROM (SELECT COUNT(*) AS CompetitionTeamsQuality, [CompetitionId] FROM [CompetitionTeam] WITH (UPDLOCK) GROUP BY [CompetitionId] ) T1 WHERE T1.[CompetitionId] = @CompetitionId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000623,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000624", "SELECT [DisciplineName] AS CompetitionDisciplineName FROM [Discipline] WHERE [DisciplineId] = @CompetitionDisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000624,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000625", "SELECT [CompetitionId] FROM [Competition] ORDER BY [CompetitionId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000625,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000626", "SELECT T1.[CompetitionId], T3.[DisciplineName], T1.[CompetitionTeamPoints], T1.[Teamid], T2.[DisciplineId] FROM (([CompetitionTeam] T1 INNER JOIN [Team] T2 ON T2.[Teamid] = T1.[Teamid]) INNER JOIN [Discipline] T3 ON T3.[DisciplineId] = T2.[DisciplineId]) WHERE T1.[CompetitionId] = @CompetitionId and T1.[Teamid] = @Teamid ORDER BY T1.[CompetitionId], T1.[Teamid] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000626,11, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000627", "SELECT [DisciplineId] FROM [Team] WHERE [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000627,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000628", "SELECT [DisciplineName] FROM [Discipline] WHERE [DisciplineId] = @DisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000628,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000629", "SELECT [CompetitionId], [Teamid] FROM [CompetitionTeam] WHERE [CompetitionId] = @CompetitionId AND [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000629,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000630", "INSERT INTO [CompetitionTeam]([CompetitionId], [CompetitionTeamPoints], [Teamid]) VALUES(@CompetitionId, @CompetitionTeamPoints, @Teamid)", GxErrorMask.GX_NOMASK,prmT000630)
             ,new CursorDef("T000631", "UPDATE [CompetitionTeam] SET [CompetitionTeamPoints]=@CompetitionTeamPoints  WHERE [CompetitionId] = @CompetitionId AND [Teamid] = @Teamid", GxErrorMask.GX_NOMASK,prmT000631)
             ,new CursorDef("T000632", "DELETE FROM [CompetitionTeam]  WHERE [CompetitionId] = @CompetitionId AND [Teamid] = @Teamid", GxErrorMask.GX_NOMASK,prmT000632)
             ,new CursorDef("T000633", "SELECT [DisciplineId] FROM [Team] WHERE [Teamid] = @Teamid ",true, GxErrorMask.GX_NOMASK, false, this,prmT000633,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000634", "SELECT [DisciplineName] FROM [Discipline] WHERE [DisciplineId] = @DisciplineId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000634,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000635", "SELECT [CompetitionId], [Teamid] FROM [CompetitionTeam] WHERE [CompetitionId] = @CompetitionId ORDER BY [CompetitionId], [Teamid] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000635,11, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 3 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 60) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 9 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 11 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 14 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 17 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 19 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 20 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 60) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                return;
             case 21 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 22 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 23 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 27 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 28 :
                ((String[]) buf[0])[0] = rslt.getString(1, 60) ;
                return;
             case 29 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 15 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 16 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 20 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 21 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 22 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 23 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 24 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 25 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 26 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 27 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 28 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 29 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
