/*
               File: Country_DataProvider
        Description: Country_Data Provider
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/7/2022 13:18:59.54
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class acountry_dataprovider : GXProcedure
   {
      public acountry_dataprovider( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public acountry_dataprovider( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out GXBCCollection<SdtCountry> aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GXBCCollection<SdtCountry>( context, "Country", "TeamAthlete") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public GXBCCollection<SdtCountry> executeUdp( )
      {
         this.Gxm2rootcol = new GXBCCollection<SdtCountry>( context, "Country", "TeamAthlete") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out GXBCCollection<SdtCountry> aP0_Gxm2rootcol )
      {
         acountry_dataprovider objacountry_dataprovider;
         objacountry_dataprovider = new acountry_dataprovider();
         objacountry_dataprovider.Gxm2rootcol = new GXBCCollection<SdtCountry>( context, "Country", "TeamAthlete") ;
         objacountry_dataprovider.context.SetSubmitInitialConfig(context);
         objacountry_dataprovider.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objacountry_dataprovider);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((acountry_dataprovider)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1country = new SdtCountry(context);
         Gxm2rootcol.Add(Gxm1country, 0);
         Gxm1country.gxTpr_Countryid = 1;
         Gxm1country.gxTpr_Countryname = "Uruguay";
         Gxm1country = new SdtCountry(context);
         Gxm2rootcol.Add(Gxm1country, 0);
         Gxm1country.gxTpr_Countryid = 2;
         Gxm1country.gxTpr_Countryname = "Brazil";
         Gxm1country = new SdtCountry(context);
         Gxm2rootcol.Add(Gxm1country, 0);
         Gxm1country.gxTpr_Countryid = 3;
         Gxm1country.gxTpr_Countryname = "Argentina";
         Gxm1country = new SdtCountry(context);
         Gxm2rootcol.Add(Gxm1country, 0);
         Gxm1country.gxTpr_Countryid = 4;
         Gxm1country.gxTpr_Countryname = "M�xico";
         Gxm1country = new SdtCountry(context);
         Gxm2rootcol.Add(Gxm1country, 0);
         Gxm1country.gxTpr_Countryid = 5;
         Gxm1country.gxTpr_Countryname = "China";
         Gxm1country = new SdtCountry(context);
         Gxm2rootcol.Add(Gxm1country, 0);
         Gxm1country.gxTpr_Countryid = 6;
         Gxm1country.gxTpr_Countryname = "Rusia";
         Gxm1country = new SdtCountry(context);
         Gxm2rootcol.Add(Gxm1country, 0);
         Gxm1country.gxTpr_Countryid = 7;
         Gxm1country.gxTpr_Countryname = "Sweden";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1country = new SdtCountry(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private GXBCCollection<SdtCountry> aP0_Gxm2rootcol ;
      private GXBCCollection<SdtCountry> Gxm2rootcol ;
      private SdtCountry Gxm1country ;
   }

}
