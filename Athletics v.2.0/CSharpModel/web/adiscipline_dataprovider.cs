/*
               File: Discipline_DataProvider
        Description: Discipline_Data Provider
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/7/2022 13:28:18.70
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class adiscipline_dataprovider : GXProcedure
   {
      public adiscipline_dataprovider( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public adiscipline_dataprovider( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out GXBCCollection<SdtDiscipline> aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GXBCCollection<SdtDiscipline>( context, "Discipline", "TeamAthlete") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public GXBCCollection<SdtDiscipline> executeUdp( )
      {
         this.Gxm2rootcol = new GXBCCollection<SdtDiscipline>( context, "Discipline", "TeamAthlete") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out GXBCCollection<SdtDiscipline> aP0_Gxm2rootcol )
      {
         adiscipline_dataprovider objadiscipline_dataprovider;
         objadiscipline_dataprovider = new adiscipline_dataprovider();
         objadiscipline_dataprovider.Gxm2rootcol = new GXBCCollection<SdtDiscipline>( context, "Discipline", "TeamAthlete") ;
         objadiscipline_dataprovider.context.SetSubmitInitialConfig(context);
         objadiscipline_dataprovider.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objadiscipline_dataprovider);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((adiscipline_dataprovider)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1discipline = new SdtDiscipline(context);
         Gxm2rootcol.Add(Gxm1discipline, 0);
         Gxm1discipline.gxTpr_Disciplineid = 1;
         Gxm1discipline.gxTpr_Disciplinename = "Swimming";
         Gxm1discipline = new SdtDiscipline(context);
         Gxm2rootcol.Add(Gxm1discipline, 0);
         Gxm1discipline.gxTpr_Disciplineid = 2;
         Gxm1discipline.gxTpr_Disciplinename = "Athletics";
         Gxm1discipline = new SdtDiscipline(context);
         Gxm2rootcol.Add(Gxm1discipline, 0);
         Gxm1discipline.gxTpr_Disciplineid = 3;
         Gxm1discipline.gxTpr_Disciplinename = "Cycling";
         Gxm1discipline = new SdtDiscipline(context);
         Gxm2rootcol.Add(Gxm1discipline, 0);
         Gxm1discipline.gxTpr_Disciplineid = 4;
         Gxm1discipline.gxTpr_Disciplinename = "Rugby";
         Gxm1discipline = new SdtDiscipline(context);
         Gxm2rootcol.Add(Gxm1discipline, 0);
         Gxm1discipline.gxTpr_Disciplineid = 5;
         Gxm1discipline.gxTpr_Disciplinename = "Football";
         Gxm1discipline = new SdtDiscipline(context);
         Gxm2rootcol.Add(Gxm1discipline, 0);
         Gxm1discipline.gxTpr_Disciplineid = 6;
         Gxm1discipline.gxTpr_Disciplinename = "Taekwondo";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1discipline = new SdtDiscipline(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private GXBCCollection<SdtDiscipline> aP0_Gxm2rootcol ;
      private GXBCCollection<SdtDiscipline> Gxm2rootcol ;
      private SdtDiscipline Gxm1discipline ;
   }

}
