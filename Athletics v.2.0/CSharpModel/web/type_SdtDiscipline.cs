/*
               File: type_SdtDiscipline
        Description: Discipline
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/15/2022 3:50:3.60
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Discipline" )]
   [XmlType(TypeName =  "Discipline" , Namespace = "TeamAthlete" )]
   [Serializable]
   public class SdtDiscipline : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtDiscipline( )
      {
      }

      public SdtDiscipline( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( short AV1DisciplineId )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(short)AV1DisciplineId});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"DisciplineId", typeof(short)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Discipline");
         metadata.Set("BT", "Discipline");
         metadata.Set("PK", "[ \"DisciplineId\" ]");
         metadata.Set("PKAssigned", "[ \"DisciplineId\" ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GeneXus.Utils.GxStringCollection StateAttributes( )
      {
         GeneXus.Utils.GxStringCollection state = new GeneXus.Utils.GxStringCollection() ;
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Disciplineid_Z");
         state.Add("gxTpr_Disciplinename_Z");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         SdtDiscipline sdt ;
         sdt = (SdtDiscipline)(source);
         gxTv_SdtDiscipline_Disciplineid = sdt.gxTv_SdtDiscipline_Disciplineid ;
         gxTv_SdtDiscipline_Disciplinename = sdt.gxTv_SdtDiscipline_Disciplinename ;
         gxTv_SdtDiscipline_Mode = sdt.gxTv_SdtDiscipline_Mode ;
         gxTv_SdtDiscipline_Initialized = sdt.gxTv_SdtDiscipline_Initialized ;
         gxTv_SdtDiscipline_Disciplineid_Z = sdt.gxTv_SdtDiscipline_Disciplineid_Z ;
         gxTv_SdtDiscipline_Disciplinename_Z = sdt.gxTv_SdtDiscipline_Disciplinename_Z ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         ToJSON( includeState, true) ;
         return  ;
      }

      public override void ToJSON( bool includeState ,
                                   bool includeNonInitialized )
      {
         AddObjectProperty("DisciplineId", gxTv_SdtDiscipline_Disciplineid, false, includeNonInitialized);
         AddObjectProperty("DisciplineName", gxTv_SdtDiscipline_Disciplinename, false, includeNonInitialized);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtDiscipline_Mode, false, includeNonInitialized);
            AddObjectProperty("Initialized", gxTv_SdtDiscipline_Initialized, false, includeNonInitialized);
            AddObjectProperty("DisciplineId_Z", gxTv_SdtDiscipline_Disciplineid_Z, false, includeNonInitialized);
            AddObjectProperty("DisciplineName_Z", gxTv_SdtDiscipline_Disciplinename_Z, false, includeNonInitialized);
         }
         return  ;
      }

      public void UpdateDirties( SdtDiscipline sdt )
      {
         if ( sdt.IsDirty("DisciplineId") )
         {
            gxTv_SdtDiscipline_Disciplineid = sdt.gxTv_SdtDiscipline_Disciplineid ;
         }
         if ( sdt.IsDirty("DisciplineName") )
         {
            gxTv_SdtDiscipline_Disciplinename = sdt.gxTv_SdtDiscipline_Disciplinename ;
         }
         return  ;
      }

      [  SoapElement( ElementName = "DisciplineId" )]
      [  XmlElement( ElementName = "DisciplineId"   )]
      public short gxTpr_Disciplineid
      {
         get {
            return gxTv_SdtDiscipline_Disciplineid ;
         }

         set {
            if ( gxTv_SdtDiscipline_Disciplineid != value )
            {
               gxTv_SdtDiscipline_Mode = "INS";
               this.gxTv_SdtDiscipline_Disciplineid_Z_SetNull( );
               this.gxTv_SdtDiscipline_Disciplinename_Z_SetNull( );
            }
            gxTv_SdtDiscipline_Disciplineid = value;
            SetDirty("Disciplineid");
         }

      }

      [  SoapElement( ElementName = "DisciplineName" )]
      [  XmlElement( ElementName = "DisciplineName"   )]
      public String gxTpr_Disciplinename
      {
         get {
            return gxTv_SdtDiscipline_Disciplinename ;
         }

         set {
            gxTv_SdtDiscipline_Disciplinename = value;
            SetDirty("Disciplinename");
         }

      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtDiscipline_Mode ;
         }

         set {
            gxTv_SdtDiscipline_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_SdtDiscipline_Mode_SetNull( )
      {
         gxTv_SdtDiscipline_Mode = "";
         return  ;
      }

      public bool gxTv_SdtDiscipline_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtDiscipline_Initialized ;
         }

         set {
            gxTv_SdtDiscipline_Initialized = value;
            SetDirty("Initialized");
         }

      }

      public void gxTv_SdtDiscipline_Initialized_SetNull( )
      {
         gxTv_SdtDiscipline_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtDiscipline_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "DisciplineId_Z" )]
      [  XmlElement( ElementName = "DisciplineId_Z"   )]
      public short gxTpr_Disciplineid_Z
      {
         get {
            return gxTv_SdtDiscipline_Disciplineid_Z ;
         }

         set {
            gxTv_SdtDiscipline_Disciplineid_Z = value;
            SetDirty("Disciplineid_Z");
         }

      }

      public void gxTv_SdtDiscipline_Disciplineid_Z_SetNull( )
      {
         gxTv_SdtDiscipline_Disciplineid_Z = 0;
         return  ;
      }

      public bool gxTv_SdtDiscipline_Disciplineid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "DisciplineName_Z" )]
      [  XmlElement( ElementName = "DisciplineName_Z"   )]
      public String gxTpr_Disciplinename_Z
      {
         get {
            return gxTv_SdtDiscipline_Disciplinename_Z ;
         }

         set {
            gxTv_SdtDiscipline_Disciplinename_Z = value;
            SetDirty("Disciplinename_Z");
         }

      }

      public void gxTv_SdtDiscipline_Disciplinename_Z_SetNull( )
      {
         gxTv_SdtDiscipline_Disciplinename_Z = "";
         return  ;
      }

      public bool gxTv_SdtDiscipline_Disciplinename_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtDiscipline_Disciplinename = "";
         gxTv_SdtDiscipline_Mode = "";
         gxTv_SdtDiscipline_Disciplinename_Z = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "discipline", "GeneXus.Programs.discipline_bc", new Object[] {context}, constructorCallingAssembly);;
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtDiscipline_Disciplineid ;
      private short gxTv_SdtDiscipline_Initialized ;
      private short gxTv_SdtDiscipline_Disciplineid_Z ;
      private String gxTv_SdtDiscipline_Disciplinename ;
      private String gxTv_SdtDiscipline_Mode ;
      private String gxTv_SdtDiscipline_Disciplinename_Z ;
   }

   [DataContract(Name = @"Discipline", Namespace = "TeamAthlete")]
   public class SdtDiscipline_RESTInterface : GxGenericCollectionItem<SdtDiscipline>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtDiscipline_RESTInterface( ) : base()
      {
      }

      public SdtDiscipline_RESTInterface( SdtDiscipline psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "DisciplineId" , Order = 0 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Disciplineid
      {
         get {
            return sdt.gxTpr_Disciplineid ;
         }

         set {
            sdt.gxTpr_Disciplineid = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "DisciplineName" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Disciplinename
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Disciplinename) ;
         }

         set {
            sdt.gxTpr_Disciplinename = value;
         }

      }

      public SdtDiscipline sdt
      {
         get {
            return (SdtDiscipline)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtDiscipline() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 2 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
