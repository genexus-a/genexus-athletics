/*
               File: Repair_DataProvider
        Description: Stub for Repair_DataProvider
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/14/2022 2:45:27.45
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class repair_dataprovider : GXProcedure
   {
      public repair_dataprovider( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public repair_dataprovider( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out GXBCCollection<SdtRepair> aP0_ReturnValue )
      {
         this.AV2ReturnValue = new GXBCCollection<SdtRepair>( context, "Repair", "Atracciones_Turistica") ;
         initialize();
         executePrivate();
         aP0_ReturnValue=this.AV2ReturnValue;
      }

      public GXBCCollection<SdtRepair> executeUdp( )
      {
         this.AV2ReturnValue = new GXBCCollection<SdtRepair>( context, "Repair", "Atracciones_Turistica") ;
         initialize();
         executePrivate();
         aP0_ReturnValue=this.AV2ReturnValue;
         return AV2ReturnValue ;
      }

      public void executeSubmit( out GXBCCollection<SdtRepair> aP0_ReturnValue )
      {
         repair_dataprovider objrepair_dataprovider;
         objrepair_dataprovider = new repair_dataprovider();
         objrepair_dataprovider.AV2ReturnValue = new GXBCCollection<SdtRepair>( context, "Repair", "Atracciones_Turistica") ;
         objrepair_dataprovider.context.SetSubmitInitialConfig(context);
         objrepair_dataprovider.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objrepair_dataprovider);
         aP0_ReturnValue=this.AV2ReturnValue;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((repair_dataprovider)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(GXBCCollection<SdtRepair>)AV2ReturnValue} ;
         ClassLoader.Execute("arepair_dataprovider","GeneXus.Programs","arepair_dataprovider", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 1 ) )
         {
            AV2ReturnValue = (GXBCCollection<SdtRepair>)(args[0]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private IGxDataStore dsDefault ;
      private Object[] args ;
      private GXBCCollection<SdtRepair> aP0_ReturnValue ;
      private GXBCCollection<SdtRepair> AV2ReturnValue ;
   }

}
