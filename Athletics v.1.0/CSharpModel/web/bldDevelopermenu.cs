using System;
using GeneXus.Builder;
using System.IO;
public class bldDevelopermenu : GxBaseBuilder
{
   string cs_path = "." ;
   public bldDevelopermenu( ) : base()
   {
   }

   public override int BeforeCompile( )
   {
      return 0 ;
   }

   public override int AfterCompile( )
   {
      int ErrCode ;
      ErrCode = 0;
      if ( ! File.Exists(@"bin\client.exe.config") || checkTime(@"bin\client.exe.config",cs_path + @"\client.exe.config") )
      {
         File.Copy( cs_path + @"\client.exe.config", @"bin\client.exe.config", true);
      }
      return ErrCode ;
   }

   static public int Main( string[] args )
   {
      bldDevelopermenu x = new bldDevelopermenu() ;
      x.SetMainSourceFile( "bldDevelopermenu.cs");
      x.LoadVariables( args);
      return x.CompileAll( );
   }

   public override ItemCollection GetSortedBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\GeneXus.Programs.Common.dll", cs_path + @"\genexus.programs.common.rsp");
      return sc ;
   }

   public override TargetCollection GetRuntimeBuildList( )
   {
      TargetCollection sc = new TargetCollection() ;
      sc.Add( @"arepair_dataprovider", "dll");
      sc.Add( @"aamusement_parks_list", "dll");
      sc.Add( @"aamusement_parks_list", "dll");
      sc.Add( @"agames_parks_list", "dll");
      sc.Add( @"agames_parks_list", "dll");
      sc.Add( @"ashows_per_park_list", "dll");
      sc.Add( @"ashows_per_park_list", "dll");
      sc.Add( @"aamusement_parks_listfromto", "dll");
      sc.Add( @"aamusement_parks_listfromto", "dll");
      sc.Add( @"aatraccionesbyname", "dll");
      sc.Add( @"aatraccionesbyname", "dll");
      sc.Add( @"appmasterpage", "dll");
      sc.Add( @"recentlinks", "dll");
      sc.Add( @"promptmasterpage", "dll");
      sc.Add( @"rwdmasterpage", "dll");
      sc.Add( @"rwdrecentlinks", "dll");
      sc.Add( @"rwdpromptmasterpage", "dll");
      sc.Add( @"gx0020", "dll");
      sc.Add( @"gx0030", "dll");
      sc.Add( @"gx0040", "dll");
      sc.Add( @"gx0060", "dll");
      sc.Add( @"home", "dll");
      sc.Add( @"home", "dll");
      sc.Add( @"notauthorized", "dll");
      sc.Add( @"tabbedview", "dll");
      sc.Add( @"gx0071", "dll");
      sc.Add( @"viewcountry", "dll");
      sc.Add( @"wwcountry", "dll");
      sc.Add( @"countrygeneral", "dll");
      sc.Add( @"countrycitywc", "dll");
      sc.Add( @"countryamusementparkwc", "dll");
      sc.Add( @"viewamusementpark", "dll");
      sc.Add( @"wwamusementpark", "dll");
      sc.Add( @"amusementparkgeneral", "dll");
      sc.Add( @"amusementparkgamewc", "dll");
      sc.Add( @"gx0080", "dll");
      sc.Add( @"gx0090", "dll");
      sc.Add( @"gx00c0", "dll");
      sc.Add( @"gx00d1", "dll");
      sc.Add( @"wp_filtrado_parques", "dll");
      sc.Add( @"wp_filtrado_parques", "dll");
      sc.Add( @"actualizacioncostoreparacion", "dll");
      sc.Add( @"deletrepairs", "dll");
      sc.Add( @"actualizacion_costos", "dll");
      sc.Add( @"wwatractionnamep", "dll");
      sc.Add( @"wwatractionnamep", "dll");
      sc.Add( @"gx00e0", "dll");
      sc.Add( @"amusementparkattractionwc", "dll");
      sc.Add( @"gx00f1", "dll");
      sc.Add( @"wpgridfreestyle", "dll");
      sc.Add( @"wpcountry_and_parks", "dll");
      sc.Add( @"wp_showsperparks", "dll");
      sc.Add( @"wp_employecards", "dll");
      sc.Add( @"gx00g0", "dll");
      sc.Add( @"amusementpark", "dll");
      sc.Add( @"country", "dll");
      sc.Add( @"show", "dll");
      sc.Add( @"category", "dll");
      sc.Add( @"game", "dll");
      sc.Add( @"technican", "dll");
      sc.Add( @"repair", "dll");
      sc.Add( @"trip", "dll");
      sc.Add( @"employee", "dll");
      return sc ;
   }

   public override ItemCollection GetResBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\messages.spa.dll", cs_path + @"\messages.spa.txt");
      return sc ;
   }

   public override bool ToBuild( String obj )
   {
      if (checkTime(obj, cs_path + @"\bin\GxClasses.dll" ))
         return true;
      if ( obj == @"bin\GeneXus.Programs.Common.dll" )
      {
         if (checkTime(obj, cs_path + @"\GxWebStd.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\SoapParm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxObjectCollection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxFullTextSearchReindexer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxModelInfoProvider.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\genexus.programs.sdt.rsp" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtRepair.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtRepair_Kind.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainpage.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainkindname.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewercharttype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryvieweroutputtype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerplotseries.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewershowdataas.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerorientation.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewertrendperiod.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerxaxislabels.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryvieweraggregationtype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryvieweraxisordertype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryvieweraxistype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerconditionoperator.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerexpandcollapse.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerfiltertype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewerobjecttype.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewersubtotals.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\gxdomainqueryviewershowdatalabelsin.cs" ))
            return true;
      }
      if ( obj == @"bin\messages.spa.dll" )
      {
         if (checkTime(obj, cs_path + @"\messages.spa.txt" ))
            return true;
      }
      return false ;
   }

}

