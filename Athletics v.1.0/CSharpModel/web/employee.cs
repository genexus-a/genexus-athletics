/*
               File: Employee
        Description: Employee
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/14/2022 2:50:34.19
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class employee : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_3") == 0 )
         {
            A7AmusementParkId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7AmusementParkId", StringUtil.LTrim( StringUtil.Str( (decimal)(A7AmusementParkId), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_3( A7AmusementParkId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_5-135614", 0) ;
            Form.Meta.addItem("description", "Employee", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtEmployeeId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public employee( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public employee( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbEmployeeName = new GXCombobox();
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( cmbEmployeeName.ItemCount > 0 )
         {
            A2EmployeeName = cmbEmployeeName.getValidValue(A2EmployeeName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2EmployeeName", A2EmployeeName);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbEmployeeName.CurrentValue = StringUtil.RTrim( A2EmployeeName);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmployeeName_Internalname, "Values", cmbEmployeeName.ToJavascriptSource(), true);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Employee", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Seleccionar", bttBtn_select_Jsonclick, 4, "Seleccionar", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00g0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"EMPLOYEEID"+"'), id:'"+"EMPLOYEEID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmployeeId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtEmployeeId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtEmployeeId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A1EmployeeId), 4, 0, ",", "")), ((edtEmployeeId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A1EmployeeId), "ZZZ9")) : context.localUtil.Format( (decimal)(A1EmployeeId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmployeeId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmployeeId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbEmployeeName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, cmbEmployeeName_Internalname, "Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         /* ComboBox */
         GxWebStd.gx_combobox_ctrl1( context, cmbEmployeeName, cmbEmployeeName_Internalname, StringUtil.RTrim( A2EmployeeName), 1, cmbEmployeeName_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbEmployeeName.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "", true, "HLP_Employee.htm");
         cmbEmployeeName.CurrentValue = StringUtil.RTrim( A2EmployeeName);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmployeeName_Internalname, "Values", (String)(cmbEmployeeName.ToJavascriptSource()), true);
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmployeeLastName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtEmployeeLastName_Internalname, "Last Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtEmployeeLastName_Internalname, StringUtil.RTrim( A3EmployeeLastName), StringUtil.RTrim( context.localUtil.Format( A3EmployeeLastName, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmployeeLastName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmployeeLastName_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmployeeAddress_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtEmployeeAddress_Internalname, "Address", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Multiple line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
         ClassString = "Attribute";
         StyleString = "";
         ClassString = "Attribute";
         StyleString = "";
         GxWebStd.gx_html_textarea( context, edtEmployeeAddress_Internalname, A4EmployeeAddress, "http://maps.google.com/maps?q="+GXUtil.UrlEncode( A4EmployeeAddress), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", 0, 1, edtEmployeeAddress_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "1024", -1, 0, "_blank", "", 0, true, "GeneXus\\Address", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmployeePhone_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtEmployeePhone_Internalname, "Phone", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         if ( context.isSmartDevice( ) )
         {
            gxphoneLink = "tel:" + StringUtil.RTrim( A5EmployeePhone);
         }
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtEmployeePhone_Internalname, StringUtil.RTrim( A5EmployeePhone), StringUtil.RTrim( context.localUtil.Format( A5EmployeePhone, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", gxphoneLink, "", "", "", edtEmployeePhone_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmployeePhone_Enabled, 0, "tel", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, 0, true, "GeneXus\\Phone", "left", true, "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmployeeEmail_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtEmployeeEmail_Internalname, "Email", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtEmployeeEmail_Internalname, A6EmployeeEmail, StringUtil.RTrim( context.localUtil.Format( A6EmployeeEmail, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "mailto:"+A6EmployeeEmail, "", "", "", edtEmployeeEmail_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmployeeEmail_Enabled, 0, "email", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GeneXus\\Email", "left", true, "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+imgEmployeePhoto_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, "", "Photo", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Static Bitmap Variable */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
         ClassString = "Attribute";
         StyleString = "";
         A49EmployeePhoto_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto))&&String.IsNullOrEmpty(StringUtil.RTrim( A40000EmployeePhoto_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)));
         sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)) ? A40000EmployeePhoto_GXI : context.PathToRelativeUrl( A49EmployeePhoto));
         GxWebStd.gx_bitmap( context, imgEmployeePhoto_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, imgEmployeePhoto_Enabled, "", "", 1, -1, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", "", "", 0, A49EmployeePhoto_IsBlob, true, context.GetImageSrcSet( sImgUrl), "HLP_Employee.htm");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "URL", (String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)) ? A40000EmployeePhoto_GXI : context.PathToRelativeUrl( A49EmployeePhoto)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "IsBlob", StringUtil.BoolToStr( A49EmployeePhoto_IsBlob), true);
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtAmusementParkId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtAmusementParkId_Internalname, "Amusement Park Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtAmusementParkId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A7AmusementParkId), 4, 0, ",", "")), ((edtAmusementParkId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A7AmusementParkId), "ZZZ9")) : context.localUtil.Format( (decimal)(A7AmusementParkId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmusementParkId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtAmusementParkId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Employee.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_7_Internalname, sImgUrl, imgprompt_7_Link, "", "", context.GetTheme( ), imgprompt_7_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtAmusementParkName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtAmusementParkName_Internalname, "Amusement Park Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtAmusementParkName_Internalname, StringUtil.RTrim( A8AmusementParkName), StringUtil.RTrim( context.localUtil.Format( A8AmusementParkName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtAmusementParkName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtAmusementParkName_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Employee.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtEmployeeId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtEmployeeId_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "EMPLOYEEID");
               AnyError = 1;
               GX_FocusControl = edtEmployeeId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A1EmployeeId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
            }
            else
            {
               A1EmployeeId = (short)(context.localUtil.CToN( cgiGet( edtEmployeeId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
            }
            cmbEmployeeName.CurrentValue = cgiGet( cmbEmployeeName_Internalname);
            A2EmployeeName = cgiGet( cmbEmployeeName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2EmployeeName", A2EmployeeName);
            A3EmployeeLastName = cgiGet( edtEmployeeLastName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3EmployeeLastName", A3EmployeeLastName);
            A4EmployeeAddress = cgiGet( edtEmployeeAddress_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4EmployeeAddress", A4EmployeeAddress);
            A5EmployeePhone = cgiGet( edtEmployeePhone_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5EmployeePhone", A5EmployeePhone);
            A6EmployeeEmail = cgiGet( edtEmployeeEmail_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6EmployeeEmail", A6EmployeeEmail);
            A49EmployeePhoto = cgiGet( imgEmployeePhoto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmployeePhoto", A49EmployeePhoto);
            if ( ( ( context.localUtil.CToN( cgiGet( edtAmusementParkId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAmusementParkId_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "AMUSEMENTPARKID");
               AnyError = 1;
               GX_FocusControl = edtAmusementParkId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A7AmusementParkId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7AmusementParkId", StringUtil.LTrim( StringUtil.Str( (decimal)(A7AmusementParkId), 4, 0)));
            }
            else
            {
               A7AmusementParkId = (short)(context.localUtil.CToN( cgiGet( edtAmusementParkId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7AmusementParkId", StringUtil.LTrim( StringUtil.Str( (decimal)(A7AmusementParkId), 4, 0)));
            }
            A8AmusementParkName = cgiGet( edtAmusementParkName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", A8AmusementParkName);
            /* Read saved values. */
            Z1EmployeeId = (short)(context.localUtil.CToN( cgiGet( "Z1EmployeeId"), ",", "."));
            Z2EmployeeName = cgiGet( "Z2EmployeeName");
            Z3EmployeeLastName = cgiGet( "Z3EmployeeLastName");
            Z4EmployeeAddress = cgiGet( "Z4EmployeeAddress");
            Z5EmployeePhone = cgiGet( "Z5EmployeePhone");
            Z6EmployeeEmail = cgiGet( "Z6EmployeeEmail");
            Z7AmusementParkId = (short)(context.localUtil.CToN( cgiGet( "Z7AmusementParkId"), ",", "."));
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
            Gx_mode = cgiGet( "Mode");
            A40000EmployeePhoto_GXI = cgiGet( "EMPLOYEEPHOTO_GXI");
            Gx_mode = cgiGet( "vMODE");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            getMultimediaValue(imgEmployeePhoto_Internalname, ref  A49EmployeePhoto, ref  A40000EmployeePhoto_GXI);
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A1EmployeeId = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0E16( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes0E16( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption0E0( )
      {
      }

      protected void ZM0E16( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2EmployeeName = T000E3_A2EmployeeName[0];
               Z3EmployeeLastName = T000E3_A3EmployeeLastName[0];
               Z4EmployeeAddress = T000E3_A4EmployeeAddress[0];
               Z5EmployeePhone = T000E3_A5EmployeePhone[0];
               Z6EmployeeEmail = T000E3_A6EmployeeEmail[0];
               Z7AmusementParkId = T000E3_A7AmusementParkId[0];
            }
            else
            {
               Z2EmployeeName = A2EmployeeName;
               Z3EmployeeLastName = A3EmployeeLastName;
               Z4EmployeeAddress = A4EmployeeAddress;
               Z5EmployeePhone = A5EmployeePhone;
               Z6EmployeeEmail = A6EmployeeEmail;
               Z7AmusementParkId = A7AmusementParkId;
            }
         }
         if ( GX_JID == -2 )
         {
            Z1EmployeeId = A1EmployeeId;
            Z2EmployeeName = A2EmployeeName;
            Z3EmployeeLastName = A3EmployeeLastName;
            Z4EmployeeAddress = A4EmployeeAddress;
            Z5EmployeePhone = A5EmployeePhone;
            Z6EmployeeEmail = A6EmployeeEmail;
            Z49EmployeePhoto = A49EmployeePhoto;
            Z40000EmployeePhoto_GXI = A40000EmployeePhoto_GXI;
            Z7AmusementParkId = A7AmusementParkId;
            Z8AmusementParkName = A8AmusementParkName;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_7_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0020.aspx"+"',["+"{Ctrl:gx.dom.el('"+"AMUSEMENTPARKID"+"'), id:'"+"AMUSEMENTPARKID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load0E16( )
      {
         /* Using cursor T000E5 */
         pr_default.execute(3, new Object[] {A1EmployeeId});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound16 = 1;
            A2EmployeeName = T000E5_A2EmployeeName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2EmployeeName", A2EmployeeName);
            A3EmployeeLastName = T000E5_A3EmployeeLastName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3EmployeeLastName", A3EmployeeLastName);
            A4EmployeeAddress = T000E5_A4EmployeeAddress[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4EmployeeAddress", A4EmployeeAddress);
            A5EmployeePhone = T000E5_A5EmployeePhone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5EmployeePhone", A5EmployeePhone);
            A6EmployeeEmail = T000E5_A6EmployeeEmail[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6EmployeeEmail", A6EmployeeEmail);
            A40000EmployeePhoto_GXI = T000E5_A40000EmployeePhoto_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)) ? A40000EmployeePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A49EmployeePhoto))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A49EmployeePhoto), true);
            A8AmusementParkName = T000E5_A8AmusementParkName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", A8AmusementParkName);
            A7AmusementParkId = T000E5_A7AmusementParkId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7AmusementParkId", StringUtil.LTrim( StringUtil.Str( (decimal)(A7AmusementParkId), 4, 0)));
            A49EmployeePhoto = T000E5_A49EmployeePhoto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmployeePhoto", A49EmployeePhoto);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)) ? A40000EmployeePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A49EmployeePhoto))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A49EmployeePhoto), true);
            ZM0E16( -2) ;
         }
         pr_default.close(3);
         OnLoadActions0E16( ) ;
      }

      protected void OnLoadActions0E16( )
      {
      }

      protected void CheckExtendedTable0E16( )
      {
         nIsDirty_16 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( GxRegex.IsMatch(A6EmployeeEmail,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") ) )
         {
            GX_msglist.addItem("El valor de Employee Email no coincide con el patr�n especificado", "OutOfRange", 1, "EMPLOYEEEMAIL");
            AnyError = 1;
            GX_FocusControl = edtEmployeeEmail_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T000E4 */
         pr_default.execute(2, new Object[] {A7AmusementParkId});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No existe 'Amusement Park'.", "ForeignKeyNotFound", 1, "AMUSEMENTPARKID");
            AnyError = 1;
            GX_FocusControl = edtAmusementParkId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A8AmusementParkName = T000E4_A8AmusementParkName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", A8AmusementParkName);
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0E16( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_3( short A7AmusementParkId )
      {
         /* Using cursor T000E6 */
         pr_default.execute(4, new Object[] {A7AmusementParkId});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No existe 'Amusement Park'.", "ForeignKeyNotFound", 1, "AMUSEMENTPARKID");
            AnyError = 1;
            GX_FocusControl = edtAmusementParkId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A8AmusementParkName = T000E6_A8AmusementParkName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", A8AmusementParkName);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A8AmusementParkName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(4);
      }

      protected void GetKey0E16( )
      {
         /* Using cursor T000E7 */
         pr_default.execute(5, new Object[] {A1EmployeeId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound16 = 1;
         }
         else
         {
            RcdFound16 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000E3 */
         pr_default.execute(1, new Object[] {A1EmployeeId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0E16( 2) ;
            RcdFound16 = 1;
            A1EmployeeId = T000E3_A1EmployeeId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
            A2EmployeeName = T000E3_A2EmployeeName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2EmployeeName", A2EmployeeName);
            A3EmployeeLastName = T000E3_A3EmployeeLastName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3EmployeeLastName", A3EmployeeLastName);
            A4EmployeeAddress = T000E3_A4EmployeeAddress[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4EmployeeAddress", A4EmployeeAddress);
            A5EmployeePhone = T000E3_A5EmployeePhone[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5EmployeePhone", A5EmployeePhone);
            A6EmployeeEmail = T000E3_A6EmployeeEmail[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6EmployeeEmail", A6EmployeeEmail);
            A40000EmployeePhoto_GXI = T000E3_A40000EmployeePhoto_GXI[0];
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)) ? A40000EmployeePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A49EmployeePhoto))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A49EmployeePhoto), true);
            A7AmusementParkId = T000E3_A7AmusementParkId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7AmusementParkId", StringUtil.LTrim( StringUtil.Str( (decimal)(A7AmusementParkId), 4, 0)));
            A49EmployeePhoto = T000E3_A49EmployeePhoto[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmployeePhoto", A49EmployeePhoto);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)) ? A40000EmployeePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A49EmployeePhoto))), true);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A49EmployeePhoto), true);
            Z1EmployeeId = A1EmployeeId;
            sMode16 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0E16( ) ;
            if ( AnyError == 1 )
            {
               RcdFound16 = 0;
               InitializeNonKey0E16( ) ;
            }
            Gx_mode = sMode16;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound16 = 0;
            InitializeNonKey0E16( ) ;
            sMode16 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode16;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey0E16( ) ;
         if ( RcdFound16 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound16 = 0;
         /* Using cursor T000E8 */
         pr_default.execute(6, new Object[] {A1EmployeeId});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( T000E8_A1EmployeeId[0] < A1EmployeeId ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( T000E8_A1EmployeeId[0] > A1EmployeeId ) ) )
            {
               A1EmployeeId = T000E8_A1EmployeeId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
               RcdFound16 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound16 = 0;
         /* Using cursor T000E9 */
         pr_default.execute(7, new Object[] {A1EmployeeId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T000E9_A1EmployeeId[0] > A1EmployeeId ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T000E9_A1EmployeeId[0] < A1EmployeeId ) ) )
            {
               A1EmployeeId = T000E9_A1EmployeeId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
               RcdFound16 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0E16( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtEmployeeId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0E16( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound16 == 1 )
            {
               if ( A1EmployeeId != Z1EmployeeId )
               {
                  A1EmployeeId = Z1EmployeeId;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "EMPLOYEEID");
                  AnyError = 1;
                  GX_FocusControl = edtEmployeeId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtEmployeeId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0E16( ) ;
                  GX_FocusControl = edtEmployeeId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1EmployeeId != Z1EmployeeId )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtEmployeeId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0E16( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "EMPLOYEEID");
                     AnyError = 1;
                     GX_FocusControl = edtEmployeeId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtEmployeeId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0E16( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A1EmployeeId != Z1EmployeeId )
         {
            A1EmployeeId = Z1EmployeeId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "EMPLOYEEID");
            AnyError = 1;
            GX_FocusControl = edtEmployeeId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtEmployeeId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound16 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "EMPLOYEEID");
            AnyError = 1;
            GX_FocusControl = edtEmployeeId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = cmbEmployeeName_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0E16( ) ;
         if ( RcdFound16 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbEmployeeName_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0E16( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound16 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbEmployeeName_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound16 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbEmployeeName_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0E16( ) ;
         if ( RcdFound16 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound16 != 0 )
            {
               ScanNext0E16( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = cmbEmployeeName_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0E16( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0E16( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000E2 */
            pr_default.execute(0, new Object[] {A1EmployeeId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Employee"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2EmployeeName, T000E2_A2EmployeeName[0]) != 0 ) || ( StringUtil.StrCmp(Z3EmployeeLastName, T000E2_A3EmployeeLastName[0]) != 0 ) || ( StringUtil.StrCmp(Z4EmployeeAddress, T000E2_A4EmployeeAddress[0]) != 0 ) || ( StringUtil.StrCmp(Z5EmployeePhone, T000E2_A5EmployeePhone[0]) != 0 ) || ( StringUtil.StrCmp(Z6EmployeeEmail, T000E2_A6EmployeeEmail[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z7AmusementParkId != T000E2_A7AmusementParkId[0] ) )
            {
               if ( StringUtil.StrCmp(Z2EmployeeName, T000E2_A2EmployeeName[0]) != 0 )
               {
                  GXUtil.WriteLog("employee:[seudo value changed for attri]"+"EmployeeName");
                  GXUtil.WriteLogRaw("Old: ",Z2EmployeeName);
                  GXUtil.WriteLogRaw("Current: ",T000E2_A2EmployeeName[0]);
               }
               if ( StringUtil.StrCmp(Z3EmployeeLastName, T000E2_A3EmployeeLastName[0]) != 0 )
               {
                  GXUtil.WriteLog("employee:[seudo value changed for attri]"+"EmployeeLastName");
                  GXUtil.WriteLogRaw("Old: ",Z3EmployeeLastName);
                  GXUtil.WriteLogRaw("Current: ",T000E2_A3EmployeeLastName[0]);
               }
               if ( StringUtil.StrCmp(Z4EmployeeAddress, T000E2_A4EmployeeAddress[0]) != 0 )
               {
                  GXUtil.WriteLog("employee:[seudo value changed for attri]"+"EmployeeAddress");
                  GXUtil.WriteLogRaw("Old: ",Z4EmployeeAddress);
                  GXUtil.WriteLogRaw("Current: ",T000E2_A4EmployeeAddress[0]);
               }
               if ( StringUtil.StrCmp(Z5EmployeePhone, T000E2_A5EmployeePhone[0]) != 0 )
               {
                  GXUtil.WriteLog("employee:[seudo value changed for attri]"+"EmployeePhone");
                  GXUtil.WriteLogRaw("Old: ",Z5EmployeePhone);
                  GXUtil.WriteLogRaw("Current: ",T000E2_A5EmployeePhone[0]);
               }
               if ( StringUtil.StrCmp(Z6EmployeeEmail, T000E2_A6EmployeeEmail[0]) != 0 )
               {
                  GXUtil.WriteLog("employee:[seudo value changed for attri]"+"EmployeeEmail");
                  GXUtil.WriteLogRaw("Old: ",Z6EmployeeEmail);
                  GXUtil.WriteLogRaw("Current: ",T000E2_A6EmployeeEmail[0]);
               }
               if ( Z7AmusementParkId != T000E2_A7AmusementParkId[0] )
               {
                  GXUtil.WriteLog("employee:[seudo value changed for attri]"+"AmusementParkId");
                  GXUtil.WriteLogRaw("Old: ",Z7AmusementParkId);
                  GXUtil.WriteLogRaw("Current: ",T000E2_A7AmusementParkId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Employee"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0E16( )
      {
         BeforeValidate0E16( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0E16( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0E16( 0) ;
            CheckOptimisticConcurrency0E16( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0E16( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0E16( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000E10 */
                     pr_default.execute(8, new Object[] {A2EmployeeName, A3EmployeeLastName, A4EmployeeAddress, A5EmployeePhone, A6EmployeeEmail, A49EmployeePhoto, A40000EmployeePhoto_GXI, A7AmusementParkId});
                     A1EmployeeId = T000E10_A1EmployeeId[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Employee") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption0E0( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0E16( ) ;
            }
            EndLevel0E16( ) ;
         }
         CloseExtendedTableCursors0E16( ) ;
      }

      protected void Update0E16( )
      {
         BeforeValidate0E16( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0E16( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0E16( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0E16( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0E16( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000E11 */
                     pr_default.execute(9, new Object[] {A2EmployeeName, A3EmployeeLastName, A4EmployeeAddress, A5EmployeePhone, A6EmployeeEmail, A7AmusementParkId, A1EmployeeId});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Employee") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Employee"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0E16( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption0E0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0E16( ) ;
         }
         CloseExtendedTableCursors0E16( ) ;
      }

      protected void DeferredUpdate0E16( )
      {
         if ( AnyError == 0 )
         {
            /* Using cursor T000E12 */
            pr_default.execute(10, new Object[] {A49EmployeePhoto, A40000EmployeePhoto_GXI, A1EmployeeId});
            pr_default.close(10);
            dsDefault.SmartCacheProvider.SetUpdated("Employee") ;
         }
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0E16( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0E16( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0E16( ) ;
            AfterConfirm0E16( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0E16( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000E13 */
                  pr_default.execute(11, new Object[] {A1EmployeeId});
                  pr_default.close(11);
                  dsDefault.SmartCacheProvider.SetUpdated("Employee") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound16 == 0 )
                        {
                           InitAll0E16( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption0E0( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode16 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0E16( ) ;
         Gx_mode = sMode16;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0E16( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000E14 */
            pr_default.execute(12, new Object[] {A7AmusementParkId});
            A8AmusementParkName = T000E14_A8AmusementParkName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", A8AmusementParkName);
            pr_default.close(12);
         }
      }

      protected void EndLevel0E16( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0E16( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(12);
            context.CommitDataStores("employee",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0E0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(12);
            context.RollbackDataStores("employee",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0E16( )
      {
         /* Using cursor T000E15 */
         pr_default.execute(13);
         RcdFound16 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound16 = 1;
            A1EmployeeId = T000E15_A1EmployeeId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0E16( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound16 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound16 = 1;
            A1EmployeeId = T000E15_A1EmployeeId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
         }
      }

      protected void ScanEnd0E16( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm0E16( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0E16( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0E16( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0E16( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0E16( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0E16( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0E16( )
      {
         edtEmployeeId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmployeeId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmployeeId_Enabled), 5, 0)), true);
         cmbEmployeeName.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmployeeName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbEmployeeName.Enabled), 5, 0)), true);
         edtEmployeeLastName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmployeeLastName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmployeeLastName_Enabled), 5, 0)), true);
         edtEmployeeAddress_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmployeeAddress_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmployeeAddress_Enabled), 5, 0)), true);
         edtEmployeePhone_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmployeePhone_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmployeePhone_Enabled), 5, 0)), true);
         edtEmployeeEmail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmployeeEmail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmployeeEmail_Enabled), 5, 0)), true);
         imgEmployeePhoto_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(imgEmployeePhoto_Enabled), 5, 0)), true);
         edtAmusementParkId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkId_Enabled), 5, 0)), true);
         edtAmusementParkName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkName_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes0E16( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues0E0( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 135614), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxcfg.js", "?20226142503534", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("employee.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z1EmployeeId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1EmployeeId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z2EmployeeName", StringUtil.RTrim( Z2EmployeeName));
         GxWebStd.gx_hidden_field( context, "Z3EmployeeLastName", StringUtil.RTrim( Z3EmployeeLastName));
         GxWebStd.gx_hidden_field( context, "Z4EmployeeAddress", Z4EmployeeAddress);
         GxWebStd.gx_hidden_field( context, "Z5EmployeePhone", StringUtil.RTrim( Z5EmployeePhone));
         GxWebStd.gx_hidden_field( context, "Z6EmployeeEmail", Z6EmployeeEmail);
         GxWebStd.gx_hidden_field( context, "Z7AmusementParkId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7AmusementParkId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "EMPLOYEEPHOTO_GXI", A40000EmployeePhoto_GXI);
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GXCCtlgxBlob = "EMPLOYEEPHOTO" + "_gxBlob";
         GxWebStd.gx_hidden_field( context, GXCCtlgxBlob, A49EmployeePhoto);
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("employee.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Employee" ;
      }

      public override String GetPgmdesc( )
      {
         return "Employee" ;
      }

      protected void InitializeNonKey0E16( )
      {
         A2EmployeeName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2EmployeeName", A2EmployeeName);
         A3EmployeeLastName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3EmployeeLastName", A3EmployeeLastName);
         A4EmployeeAddress = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4EmployeeAddress", A4EmployeeAddress);
         A5EmployeePhone = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5EmployeePhone", A5EmployeePhone);
         A6EmployeeEmail = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6EmployeeEmail", A6EmployeeEmail);
         A49EmployeePhoto = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmployeePhoto", A49EmployeePhoto);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)) ? A40000EmployeePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A49EmployeePhoto))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A49EmployeePhoto), true);
         A40000EmployeePhoto_GXI = "";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( A49EmployeePhoto)) ? A40000EmployeePhoto_GXI : context.convertURL( context.PathToRelativeUrl( A49EmployeePhoto))), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgEmployeePhoto_Internalname, "SrcSet", context.GetImageSrcSet( A49EmployeePhoto), true);
         A7AmusementParkId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7AmusementParkId", StringUtil.LTrim( StringUtil.Str( (decimal)(A7AmusementParkId), 4, 0)));
         A8AmusementParkName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", A8AmusementParkName);
         Z2EmployeeName = "";
         Z3EmployeeLastName = "";
         Z4EmployeeAddress = "";
         Z5EmployeePhone = "";
         Z6EmployeeEmail = "";
         Z7AmusementParkId = 0;
      }

      protected void InitAll0E16( )
      {
         A1EmployeeId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A1EmployeeId", StringUtil.LTrim( StringUtil.Str( (decimal)(A1EmployeeId), 4, 0)));
         InitializeNonKey0E16( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20226142503542", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("employee.js", "?20226142503542", false, true);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtEmployeeId_Internalname = "EMPLOYEEID";
         cmbEmployeeName_Internalname = "EMPLOYEENAME";
         edtEmployeeLastName_Internalname = "EMPLOYEELASTNAME";
         edtEmployeeAddress_Internalname = "EMPLOYEEADDRESS";
         edtEmployeePhone_Internalname = "EMPLOYEEPHONE";
         edtEmployeeEmail_Internalname = "EMPLOYEEEMAIL";
         imgEmployeePhoto_Internalname = "EMPLOYEEPHOTO";
         edtAmusementParkId_Internalname = "AMUSEMENTPARKID";
         edtAmusementParkName_Internalname = "AMUSEMENTPARKNAME";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_7_Internalname = "PROMPT_7";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Employee";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtAmusementParkName_Jsonclick = "";
         edtAmusementParkName_Enabled = 0;
         imgprompt_7_Visible = 1;
         imgprompt_7_Link = "";
         edtAmusementParkId_Jsonclick = "";
         edtAmusementParkId_Enabled = 1;
         imgEmployeePhoto_Enabled = 1;
         edtEmployeeEmail_Jsonclick = "";
         edtEmployeeEmail_Enabled = 1;
         edtEmployeePhone_Jsonclick = "";
         edtEmployeePhone_Enabled = 1;
         edtEmployeeAddress_Enabled = 1;
         edtEmployeeLastName_Jsonclick = "";
         edtEmployeeLastName_Enabled = 1;
         cmbEmployeeName_Jsonclick = "";
         cmbEmployeeName.Enabled = 1;
         edtEmployeeId_Jsonclick = "";
         edtEmployeeId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void init_web_controls( )
      {
         cmbEmployeeName.Name = "EMPLOYEENAME";
         cmbEmployeeName.WebTags = "";
         if ( cmbEmployeeName.ItemCount > 0 )
         {
            A2EmployeeName = cmbEmployeeName.getValidValue(A2EmployeeName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2EmployeeName", A2EmployeeName);
         }
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = cmbEmployeeName_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Employeeid( )
      {
         A2EmployeeName = cmbEmployeeName.CurrentValue;
         cmbEmployeeName.CurrentValue = A2EmployeeName;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         if ( cmbEmployeeName.ItemCount > 0 )
         {
            A2EmployeeName = cmbEmployeeName.getValidValue(A2EmployeeName);
            cmbEmployeeName.CurrentValue = A2EmployeeName;
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbEmployeeName.CurrentValue = StringUtil.RTrim( A2EmployeeName);
         }
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A2EmployeeName", StringUtil.RTrim( A2EmployeeName));
         cmbEmployeeName.CurrentValue = StringUtil.RTrim( A2EmployeeName);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmployeeName_Internalname, "Values", cmbEmployeeName.ToJavascriptSource(), true);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A3EmployeeLastName", StringUtil.RTrim( A3EmployeeLastName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A4EmployeeAddress", A4EmployeeAddress);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A5EmployeePhone", StringUtil.RTrim( A5EmployeePhone));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A6EmployeeEmail", A6EmployeeEmail);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmployeePhoto", context.PathToRelativeUrl( A49EmployeePhoto));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40000EmployeePhoto_GXI", A40000EmployeePhoto_GXI);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A7AmusementParkId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7AmusementParkId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", StringUtil.RTrim( A8AmusementParkName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z1EmployeeId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z1EmployeeId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z2EmployeeName", StringUtil.RTrim( Z2EmployeeName));
         GxWebStd.gx_hidden_field( context, "Z3EmployeeLastName", StringUtil.RTrim( Z3EmployeeLastName));
         GxWebStd.gx_hidden_field( context, "Z4EmployeeAddress", Z4EmployeeAddress);
         GxWebStd.gx_hidden_field( context, "Z5EmployeePhone", StringUtil.RTrim( Z5EmployeePhone));
         GxWebStd.gx_hidden_field( context, "Z6EmployeeEmail", Z6EmployeeEmail);
         GxWebStd.gx_hidden_field( context, "Z49EmployeePhoto", context.PathToRelativeUrl( Z49EmployeePhoto));
         GxWebStd.gx_hidden_field( context, "Z40000EmployeePhoto_GXI", Z40000EmployeePhoto_GXI);
         GxWebStd.gx_hidden_field( context, "Z7AmusementParkId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7AmusementParkId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z8AmusementParkName", StringUtil.RTrim( Z8AmusementParkName));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Amusementparkid( )
      {
         /* Using cursor T000E14 */
         pr_default.execute(12, new Object[] {A7AmusementParkId});
         if ( (pr_default.getStatus(12) == 101) )
         {
            GX_msglist.addItem("No existe 'Amusement Park'.", "ForeignKeyNotFound", 1, "AMUSEMENTPARKID");
            AnyError = 1;
            GX_FocusControl = edtAmusementParkId_Internalname;
         }
         A8AmusementParkName = T000E14_A8AmusementParkName[0];
         pr_default.close(12);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", StringUtil.RTrim( A8AmusementParkName));
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_EMPLOYEEID","{handler:'Valid_Employeeid',iparms:[{av:'cmbEmployeeName'},{av:'A2EmployeeName',fld:'EMPLOYEENAME',pic:''},{av:'A1EmployeeId',fld:'EMPLOYEEID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_EMPLOYEEID",",oparms:[{av:'cmbEmployeeName'},{av:'A2EmployeeName',fld:'EMPLOYEENAME',pic:''},{av:'A3EmployeeLastName',fld:'EMPLOYEELASTNAME',pic:''},{av:'A4EmployeeAddress',fld:'EMPLOYEEADDRESS',pic:''},{av:'A5EmployeePhone',fld:'EMPLOYEEPHONE',pic:''},{av:'A6EmployeeEmail',fld:'EMPLOYEEEMAIL',pic:''},{av:'A49EmployeePhoto',fld:'EMPLOYEEPHOTO',pic:''},{av:'A40000EmployeePhoto_GXI',fld:'EMPLOYEEPHOTO_GXI',pic:''},{av:'A7AmusementParkId',fld:'AMUSEMENTPARKID',pic:'ZZZ9'},{av:'A8AmusementParkName',fld:'AMUSEMENTPARKNAME',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z1EmployeeId'},{av:'Z2EmployeeName'},{av:'Z3EmployeeLastName'},{av:'Z4EmployeeAddress'},{av:'Z5EmployeePhone'},{av:'Z6EmployeeEmail'},{av:'Z49EmployeePhoto'},{av:'Z40000EmployeePhoto_GXI'},{av:'Z7AmusementParkId'},{av:'Z8AmusementParkName'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_EMPLOYEEEMAIL","{handler:'Valid_Employeeemail',iparms:[]");
         setEventMetadata("VALID_EMPLOYEEEMAIL",",oparms:[]}");
         setEventMetadata("VALID_AMUSEMENTPARKID","{handler:'Valid_Amusementparkid',iparms:[{av:'A7AmusementParkId',fld:'AMUSEMENTPARKID',pic:'ZZZ9'},{av:'A8AmusementParkName',fld:'AMUSEMENTPARKNAME',pic:''}]");
         setEventMetadata("VALID_AMUSEMENTPARKID",",oparms:[{av:'A8AmusementParkName',fld:'AMUSEMENTPARKNAME',pic:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z2EmployeeName = "";
         Z3EmployeeLastName = "";
         Z4EmployeeAddress = "";
         Z5EmployeePhone = "";
         Z6EmployeeEmail = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         A2EmployeeName = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A3EmployeeLastName = "";
         A4EmployeeAddress = "";
         gxphoneLink = "";
         A5EmployeePhone = "";
         A6EmployeeEmail = "";
         A49EmployeePhoto = "";
         A40000EmployeePhoto_GXI = "";
         sImgUrl = "";
         A8AmusementParkName = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z49EmployeePhoto = "";
         Z40000EmployeePhoto_GXI = "";
         Z8AmusementParkName = "";
         T000E5_A1EmployeeId = new short[1] ;
         T000E5_A2EmployeeName = new String[] {""} ;
         T000E5_A3EmployeeLastName = new String[] {""} ;
         T000E5_A4EmployeeAddress = new String[] {""} ;
         T000E5_A5EmployeePhone = new String[] {""} ;
         T000E5_A6EmployeeEmail = new String[] {""} ;
         T000E5_A40000EmployeePhoto_GXI = new String[] {""} ;
         T000E5_A8AmusementParkName = new String[] {""} ;
         T000E5_A7AmusementParkId = new short[1] ;
         T000E5_A49EmployeePhoto = new String[] {""} ;
         T000E4_A8AmusementParkName = new String[] {""} ;
         T000E6_A8AmusementParkName = new String[] {""} ;
         T000E7_A1EmployeeId = new short[1] ;
         T000E3_A1EmployeeId = new short[1] ;
         T000E3_A2EmployeeName = new String[] {""} ;
         T000E3_A3EmployeeLastName = new String[] {""} ;
         T000E3_A4EmployeeAddress = new String[] {""} ;
         T000E3_A5EmployeePhone = new String[] {""} ;
         T000E3_A6EmployeeEmail = new String[] {""} ;
         T000E3_A40000EmployeePhoto_GXI = new String[] {""} ;
         T000E3_A7AmusementParkId = new short[1] ;
         T000E3_A49EmployeePhoto = new String[] {""} ;
         sMode16 = "";
         T000E8_A1EmployeeId = new short[1] ;
         T000E9_A1EmployeeId = new short[1] ;
         T000E2_A1EmployeeId = new short[1] ;
         T000E2_A2EmployeeName = new String[] {""} ;
         T000E2_A3EmployeeLastName = new String[] {""} ;
         T000E2_A4EmployeeAddress = new String[] {""} ;
         T000E2_A5EmployeePhone = new String[] {""} ;
         T000E2_A6EmployeeEmail = new String[] {""} ;
         T000E2_A40000EmployeePhoto_GXI = new String[] {""} ;
         T000E2_A7AmusementParkId = new short[1] ;
         T000E2_A49EmployeePhoto = new String[] {""} ;
         T000E10_A1EmployeeId = new short[1] ;
         T000E14_A8AmusementParkName = new String[] {""} ;
         T000E15_A1EmployeeId = new short[1] ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXCCtlgxBlob = "";
         ZZ2EmployeeName = "";
         ZZ3EmployeeLastName = "";
         ZZ4EmployeeAddress = "";
         ZZ5EmployeePhone = "";
         ZZ6EmployeeEmail = "";
         ZZ49EmployeePhoto = "";
         ZZ40000EmployeePhoto_GXI = "";
         ZZ8AmusementParkName = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.employee__default(),
            new Object[][] {
                new Object[] {
               T000E2_A1EmployeeId, T000E2_A2EmployeeName, T000E2_A3EmployeeLastName, T000E2_A4EmployeeAddress, T000E2_A5EmployeePhone, T000E2_A6EmployeeEmail, T000E2_A40000EmployeePhoto_GXI, T000E2_A7AmusementParkId, T000E2_A49EmployeePhoto
               }
               , new Object[] {
               T000E3_A1EmployeeId, T000E3_A2EmployeeName, T000E3_A3EmployeeLastName, T000E3_A4EmployeeAddress, T000E3_A5EmployeePhone, T000E3_A6EmployeeEmail, T000E3_A40000EmployeePhoto_GXI, T000E3_A7AmusementParkId, T000E3_A49EmployeePhoto
               }
               , new Object[] {
               T000E4_A8AmusementParkName
               }
               , new Object[] {
               T000E5_A1EmployeeId, T000E5_A2EmployeeName, T000E5_A3EmployeeLastName, T000E5_A4EmployeeAddress, T000E5_A5EmployeePhone, T000E5_A6EmployeeEmail, T000E5_A40000EmployeePhoto_GXI, T000E5_A8AmusementParkName, T000E5_A7AmusementParkId, T000E5_A49EmployeePhoto
               }
               , new Object[] {
               T000E6_A8AmusementParkName
               }
               , new Object[] {
               T000E7_A1EmployeeId
               }
               , new Object[] {
               T000E8_A1EmployeeId
               }
               , new Object[] {
               T000E9_A1EmployeeId
               }
               , new Object[] {
               T000E10_A1EmployeeId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000E14_A8AmusementParkName
               }
               , new Object[] {
               T000E15_A1EmployeeId
               }
            }
         );
      }

      private short Z1EmployeeId ;
      private short Z7AmusementParkId ;
      private short GxWebError ;
      private short A7AmusementParkId ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A1EmployeeId ;
      private short GX_JID ;
      private short RcdFound16 ;
      private short nIsDirty_16 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short ZZ1EmployeeId ;
      private short ZZ7AmusementParkId ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtEmployeeId_Enabled ;
      private int edtEmployeeLastName_Enabled ;
      private int edtEmployeeAddress_Enabled ;
      private int edtEmployeePhone_Enabled ;
      private int edtEmployeeEmail_Enabled ;
      private int imgEmployeePhoto_Enabled ;
      private int edtAmusementParkId_Enabled ;
      private int imgprompt_7_Visible ;
      private int edtAmusementParkName_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String Z2EmployeeName ;
      private String Z3EmployeeLastName ;
      private String Z5EmployeePhone ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtEmployeeId_Internalname ;
      private String A2EmployeeName ;
      private String cmbEmployeeName_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtEmployeeId_Jsonclick ;
      private String cmbEmployeeName_Jsonclick ;
      private String edtEmployeeLastName_Internalname ;
      private String A3EmployeeLastName ;
      private String edtEmployeeLastName_Jsonclick ;
      private String edtEmployeeAddress_Internalname ;
      private String edtEmployeePhone_Internalname ;
      private String gxphoneLink ;
      private String A5EmployeePhone ;
      private String edtEmployeePhone_Jsonclick ;
      private String edtEmployeeEmail_Internalname ;
      private String edtEmployeeEmail_Jsonclick ;
      private String imgEmployeePhoto_Internalname ;
      private String sImgUrl ;
      private String edtAmusementParkId_Internalname ;
      private String edtAmusementParkId_Jsonclick ;
      private String imgprompt_7_Internalname ;
      private String imgprompt_7_Link ;
      private String edtAmusementParkName_Internalname ;
      private String A8AmusementParkName ;
      private String edtAmusementParkName_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Z8AmusementParkName ;
      private String sMode16 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXCCtlgxBlob ;
      private String ZZ2EmployeeName ;
      private String ZZ3EmployeeLastName ;
      private String ZZ5EmployeePhone ;
      private String ZZ8AmusementParkName ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool A49EmployeePhoto_IsBlob ;
      private bool Gx_longc ;
      private String Z4EmployeeAddress ;
      private String Z6EmployeeEmail ;
      private String A4EmployeeAddress ;
      private String A6EmployeeEmail ;
      private String A40000EmployeePhoto_GXI ;
      private String Z40000EmployeePhoto_GXI ;
      private String ZZ4EmployeeAddress ;
      private String ZZ6EmployeeEmail ;
      private String ZZ40000EmployeePhoto_GXI ;
      private String A49EmployeePhoto ;
      private String Z49EmployeePhoto ;
      private String ZZ49EmployeePhoto ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbEmployeeName ;
      private IDataStoreProvider pr_default ;
      private short[] T000E5_A1EmployeeId ;
      private String[] T000E5_A2EmployeeName ;
      private String[] T000E5_A3EmployeeLastName ;
      private String[] T000E5_A4EmployeeAddress ;
      private String[] T000E5_A5EmployeePhone ;
      private String[] T000E5_A6EmployeeEmail ;
      private String[] T000E5_A40000EmployeePhoto_GXI ;
      private String[] T000E5_A8AmusementParkName ;
      private short[] T000E5_A7AmusementParkId ;
      private String[] T000E5_A49EmployeePhoto ;
      private String[] T000E4_A8AmusementParkName ;
      private String[] T000E6_A8AmusementParkName ;
      private short[] T000E7_A1EmployeeId ;
      private short[] T000E3_A1EmployeeId ;
      private String[] T000E3_A2EmployeeName ;
      private String[] T000E3_A3EmployeeLastName ;
      private String[] T000E3_A4EmployeeAddress ;
      private String[] T000E3_A5EmployeePhone ;
      private String[] T000E3_A6EmployeeEmail ;
      private String[] T000E3_A40000EmployeePhoto_GXI ;
      private short[] T000E3_A7AmusementParkId ;
      private String[] T000E3_A49EmployeePhoto ;
      private short[] T000E8_A1EmployeeId ;
      private short[] T000E9_A1EmployeeId ;
      private short[] T000E2_A1EmployeeId ;
      private String[] T000E2_A2EmployeeName ;
      private String[] T000E2_A3EmployeeLastName ;
      private String[] T000E2_A4EmployeeAddress ;
      private String[] T000E2_A5EmployeePhone ;
      private String[] T000E2_A6EmployeeEmail ;
      private String[] T000E2_A40000EmployeePhoto_GXI ;
      private short[] T000E2_A7AmusementParkId ;
      private String[] T000E2_A49EmployeePhoto ;
      private short[] T000E10_A1EmployeeId ;
      private String[] T000E14_A8AmusementParkName ;
      private short[] T000E15_A1EmployeeId ;
      private GXWebForm Form ;
   }

   public class employee__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new UpdateCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000E5 ;
          prmT000E5 = new Object[] {
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E4 ;
          prmT000E4 = new Object[] {
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E6 ;
          prmT000E6 = new Object[] {
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E7 ;
          prmT000E7 = new Object[] {
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E3 ;
          prmT000E3 = new Object[] {
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E8 ;
          prmT000E8 = new Object[] {
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E9 ;
          prmT000E9 = new Object[] {
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E2 ;
          prmT000E2 = new Object[] {
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E10 ;
          prmT000E10 = new Object[] {
          new Object[] {"@EmployeeName",SqlDbType.NChar,50,0} ,
          new Object[] {"@EmployeeLastName",SqlDbType.NChar,20,0} ,
          new Object[] {"@EmployeeAddress",SqlDbType.NVarChar,1024,0} ,
          new Object[] {"@EmployeePhone",SqlDbType.NChar,20,0} ,
          new Object[] {"@EmployeeEmail",SqlDbType.NVarChar,100,0} ,
          new Object[] {"@EmployeePhoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@EmployeePhoto_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E11 ;
          prmT000E11 = new Object[] {
          new Object[] {"@EmployeeName",SqlDbType.NChar,50,0} ,
          new Object[] {"@EmployeeLastName",SqlDbType.NChar,20,0} ,
          new Object[] {"@EmployeeAddress",SqlDbType.NVarChar,1024,0} ,
          new Object[] {"@EmployeePhone",SqlDbType.NChar,20,0} ,
          new Object[] {"@EmployeeEmail",SqlDbType.NVarChar,100,0} ,
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E12 ;
          prmT000E12 = new Object[] {
          new Object[] {"@EmployeePhoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@EmployeePhoto_GXI",SqlDbType.NVarChar,2048,0} ,
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E13 ;
          prmT000E13 = new Object[] {
          new Object[] {"@EmployeeId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000E15 ;
          prmT000E15 = new Object[] {
          } ;
          Object[] prmT000E14 ;
          prmT000E14 = new Object[] {
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000E2", "SELECT [EmployeeId], [EmployeeName], [EmployeeLastName], [EmployeeAddress], [EmployeePhone], [EmployeeEmail], [EmployeePhoto_GXI], [AmusementParkId], [EmployeePhoto] FROM [Employee] WITH (UPDLOCK) WHERE [EmployeeId] = @EmployeeId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000E2,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000E3", "SELECT [EmployeeId], [EmployeeName], [EmployeeLastName], [EmployeeAddress], [EmployeePhone], [EmployeeEmail], [EmployeePhoto_GXI], [AmusementParkId], [EmployeePhoto] FROM [Employee] WHERE [EmployeeId] = @EmployeeId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000E3,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000E4", "SELECT [AmusementParkName] FROM [AmusementPark] WHERE [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000E4,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000E5", "SELECT TM1.[EmployeeId], TM1.[EmployeeName], TM1.[EmployeeLastName], TM1.[EmployeeAddress], TM1.[EmployeePhone], TM1.[EmployeeEmail], TM1.[EmployeePhoto_GXI], T2.[AmusementParkName], TM1.[AmusementParkId], TM1.[EmployeePhoto] FROM ([Employee] TM1 INNER JOIN [AmusementPark] T2 ON T2.[AmusementParkId] = TM1.[AmusementParkId]) WHERE TM1.[EmployeeId] = @EmployeeId ORDER BY TM1.[EmployeeId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000E5,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000E6", "SELECT [AmusementParkName] FROM [AmusementPark] WHERE [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000E6,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000E7", "SELECT [EmployeeId] FROM [Employee] WHERE [EmployeeId] = @EmployeeId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000E7,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000E8", "SELECT TOP 1 [EmployeeId] FROM [Employee] WHERE ( [EmployeeId] > @EmployeeId) ORDER BY [EmployeeId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000E8,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000E9", "SELECT TOP 1 [EmployeeId] FROM [Employee] WHERE ( [EmployeeId] < @EmployeeId) ORDER BY [EmployeeId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000E9,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000E10", "INSERT INTO [Employee]([EmployeeName], [EmployeeLastName], [EmployeeAddress], [EmployeePhone], [EmployeeEmail], [EmployeePhoto], [EmployeePhoto_GXI], [AmusementParkId]) VALUES(@EmployeeName, @EmployeeLastName, @EmployeeAddress, @EmployeePhone, @EmployeeEmail, @EmployeePhoto, @EmployeePhoto_GXI, @AmusementParkId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000E10)
             ,new CursorDef("T000E11", "UPDATE [Employee] SET [EmployeeName]=@EmployeeName, [EmployeeLastName]=@EmployeeLastName, [EmployeeAddress]=@EmployeeAddress, [EmployeePhone]=@EmployeePhone, [EmployeeEmail]=@EmployeeEmail, [AmusementParkId]=@AmusementParkId  WHERE [EmployeeId] = @EmployeeId", GxErrorMask.GX_NOMASK,prmT000E11)
             ,new CursorDef("T000E12", "UPDATE [Employee] SET [EmployeePhoto]=@EmployeePhoto, [EmployeePhoto_GXI]=@EmployeePhoto_GXI  WHERE [EmployeeId] = @EmployeeId", GxErrorMask.GX_NOMASK,prmT000E12)
             ,new CursorDef("T000E13", "DELETE FROM [Employee]  WHERE [EmployeeId] = @EmployeeId", GxErrorMask.GX_NOMASK,prmT000E13)
             ,new CursorDef("T000E14", "SELECT [AmusementParkName] FROM [AmusementPark] WHERE [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000E14,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000E15", "SELECT [EmployeeId] FROM [Employee] ORDER BY [EmployeeId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000E15,100, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getMultimediaUri(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(9, rslt.getVarchar(7)) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getMultimediaUri(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((String[]) buf[8])[0] = rslt.getMultimediaFile(9, rslt.getVarchar(7)) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[4])[0] = rslt.getString(5, 20) ;
                ((String[]) buf[5])[0] = rslt.getVarchar(6) ;
                ((String[]) buf[6])[0] = rslt.getMultimediaUri(7) ;
                ((String[]) buf[7])[0] = rslt.getString(8, 50) ;
                ((short[]) buf[8])[0] = rslt.getShort(9) ;
                ((String[]) buf[9])[0] = rslt.getMultimediaFile(10, rslt.getVarchar(7)) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameterBlob(6, (String)parms[5], false);
                stmt.SetParameterMultimedia(7, (String)parms[6], (String)parms[5], "Employee", "EmployeePhoto");
                stmt.SetParameter(8, (short)parms[7]);
                return;
             case 9 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                stmt.SetParameter(5, (String)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                return;
             case 10 :
                stmt.SetParameterBlob(1, (String)parms[0], false);
                stmt.SetParameterMultimedia(2, (String)parms[1], (String)parms[0], "Employee", "EmployeePhoto");
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
