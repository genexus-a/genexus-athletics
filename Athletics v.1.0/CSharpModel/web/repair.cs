/*
               File: Repair
        Description: Repair
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 5/31/2022 18:2:1.61
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class repair : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A18GameId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18GameId", StringUtil.LTrim( StringUtil.Str( (decimal)(A18GameId), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A18GameId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A27TechnicianId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27TechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A27TechnicianId), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A27TechnicianId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A40RepairTechnicianId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40RepairTechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A40RepairTechnicianId), 4, 0)));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A40RepairTechnicianId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridrepair_kind") == 0 )
         {
            nRC_GXsfl_103 = (int)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_103_idx = (int)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_103_idx = GetNextPar( );
            Gx_mode = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridrepair_kind_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_5-135614", 0) ;
            Form.Meta.addItem("description", "Repair", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtRepairId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public repair( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public repair( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbRepairKindName = new GXCombobox();
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Repair", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Seleccionar", bttBtn_select_Jsonclick, 4, "Seleccionar", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00c0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"REPAIRID"+"'), id:'"+"REPAIRID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtRepairId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29RepairId), 4, 0, ",", "")), ((edtRepairId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A29RepairId), "ZZZ9")) : context.localUtil.Format( (decimal)(A29RepairId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairDateFrom_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairDateFrom_Internalname, "Date From", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         context.WriteHtmlText( "<div id=\""+edtRepairDateFrom_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( context, edtRepairDateFrom_Internalname, context.localUtil.Format(A30RepairDateFrom, "99/99/99"), context.localUtil.Format( A30RepairDateFrom, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairDateFrom_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairDateFrom_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Repair.htm");
         GxWebStd.gx_bitmap( context, edtRepairDateFrom_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtRepairDateFrom_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Repair.htm");
         context.WriteHtmlTextNl( "</div>") ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairDaysQuantity_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairDaysQuantity_Internalname, "Days Quantity", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtRepairDaysQuantity_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A31RepairDaysQuantity), 4, 0, ",", "")), ((edtRepairDaysQuantity_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A31RepairDaysQuantity), "ZZZ9")) : context.localUtil.Format( (decimal)(A31RepairDaysQuantity), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairDaysQuantity_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairDaysQuantity_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairDateTo_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairDateTo_Internalname, "Date To", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         context.WriteHtmlText( "<div id=\""+edtRepairDateTo_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( context, edtRepairDateTo_Internalname, context.localUtil.Format(A32RepairDateTo, "99/99/99"), context.localUtil.Format( A32RepairDateTo, "99/99/99"), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairDateTo_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairDateTo_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Repair.htm");
         GxWebStd.gx_bitmap( context, edtRepairDateTo_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtRepairDateTo_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Repair.htm");
         context.WriteHtmlTextNl( "</div>") ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtGameId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtGameId_Internalname, "Game Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtGameId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A18GameId), 4, 0, ",", "")), ((edtGameId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A18GameId), "ZZZ9")) : context.localUtil.Format( (decimal)(A18GameId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGameId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtGameId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Repair.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_18_Internalname, sImgUrl, imgprompt_18_Link, "", "", context.GetTheme( ), imgprompt_18_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtGameName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtGameName_Internalname, "Game Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtGameName_Internalname, StringUtil.RTrim( A19GameName), StringUtil.RTrim( context.localUtil.Format( A19GameName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtGameName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtGameName_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairCost_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairCost_Internalname, "Cost", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtRepairCost_Internalname, StringUtil.LTrim( StringUtil.NToC( A33RepairCost, 8, 2, ",", "")), ((edtRepairCost_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A33RepairCost, "ZZZZ9.99")) : context.localUtil.Format( A33RepairCost, "ZZZZ9.99")), TempTags+" onchange=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairCost_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairCost_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Cost", "right", false, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTechnicianId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtTechnicianId_Internalname, "Technician Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtTechnicianId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A27TechnicianId), 4, 0, ",", "")), ((edtTechnicianId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A27TechnicianId), "ZZZ9")) : context.localUtil.Format( (decimal)(A27TechnicianId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTechnicianId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTechnicianId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Repair.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_27_Internalname, sImgUrl, imgprompt_27_Link, "", "", context.GetTheme( ), imgprompt_27_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTechnicianName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtTechnicianName_Internalname, "Technician Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtTechnicianName_Internalname, StringUtil.RTrim( A28TechnicianName), StringUtil.RTrim( context.localUtil.Format( A28TechnicianName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTechnicianName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTechnicianName_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairTechnicianId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairTechnicianId_Internalname, "Technician Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtRepairTechnicianId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A40RepairTechnicianId), 4, 0, ",", "")), ((edtRepairTechnicianId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A40RepairTechnicianId), "ZZZ9")) : context.localUtil.Format( (decimal)(A40RepairTechnicianId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,79);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairTechnicianId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairTechnicianId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Repair.htm");
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         GxWebStd.gx_bitmap( context, imgprompt_40_Internalname, sImgUrl, imgprompt_40_Link, "", "", context.GetTheme( ), imgprompt_40_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairTechniciaName_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairTechniciaName_Internalname, "Technicia Name", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtRepairTechniciaName_Internalname, StringUtil.RTrim( A41RepairTechniciaName), StringUtil.RTrim( context.localUtil.Format( A41RepairTechniciaName, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairTechniciaName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairTechniciaName_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "Name", "left", true, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairDiscountPercentage_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairDiscountPercentage_Internalname, "Discount Percentage", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtRepairDiscountPercentage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A36RepairDiscountPercentage), 4, 0, ",", "")), ((edtRepairDiscountPercentage_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A36RepairDiscountPercentage), "ZZZ9")) : context.localUtil.Format( (decimal)(A36RepairDiscountPercentage), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,89);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairDiscountPercentage_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairDiscountPercentage_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Percentage", "right", false, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRepairFinalCost_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtRepairFinalCost_Internalname, "Final Cost", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         GxWebStd.gx_single_line_edit( context, edtRepairFinalCost_Internalname, StringUtil.LTrim( StringUtil.NToC( A37RepairFinalCost, 8, 2, ",", "")), ((edtRepairFinalCost_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( A37RepairFinalCost, "ZZZZ9.99")) : context.localUtil.Format( A37RepairFinalCost, "ZZZZ9.99")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRepairFinalCost_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRepairFinalCost_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "Cost", "right", false, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divKindtable_Internalname, 1, 0, "px", 0, "px", "LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitlekind_Internalname, "Kind", "", "", lblTitlekind_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         gxdraw_Gridrepair_kind( ) ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Repair.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void gxdraw_Gridrepair_kind( )
      {
         /*  Grid Control  */
         Gridrepair_kindContainer.AddObjectProperty("GridName", "Gridrepair_kind");
         Gridrepair_kindContainer.AddObjectProperty("Header", subGridrepair_kind_Header);
         Gridrepair_kindContainer.AddObjectProperty("Class", "Grid");
         Gridrepair_kindContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrepair_kind_Backcolorstyle), 1, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("CmpContext", "");
         Gridrepair_kindContainer.AddObjectProperty("InMasterPage", "false");
         Gridrepair_kindColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrepair_kindColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A42RepairKindId), 1, 0, ".", "")));
         Gridrepair_kindColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRepairKindId_Enabled), 5, 0, ".", "")));
         Gridrepair_kindContainer.AddColumnProperties(Gridrepair_kindColumn);
         Gridrepair_kindColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrepair_kindColumn.AddObjectProperty("Value", StringUtil.RTrim( A43RepairKindName));
         Gridrepair_kindColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbRepairKindName.Enabled), 5, 0, ".", "")));
         Gridrepair_kindContainer.AddColumnProperties(Gridrepair_kindColumn);
         Gridrepair_kindColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrepair_kindColumn.AddObjectProperty("Value", StringUtil.RTrim( A44RepairKindRemaks));
         Gridrepair_kindColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRepairKindRemaks_Enabled), 5, 0, ".", "")));
         Gridrepair_kindContainer.AddColumnProperties(Gridrepair_kindColumn);
         Gridrepair_kindContainer.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrepair_kind_Selectedindex), 4, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrepair_kind_Allowselection), 1, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrepair_kind_Selectioncolor), 9, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrepair_kind_Allowhovering), 1, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrepair_kind_Hoveringcolor), 9, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrepair_kind_Allowcollapsing), 1, 0, ".", "")));
         Gridrepair_kindContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrepair_kind_Collapsed), 1, 0, ".", "")));
         nGXsfl_103_idx = 0;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount13 = 5;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               /* Display confirmed (stored) records */
               nRcdExists_13 = 1;
               ScanStart0C13( ) ;
               while ( RcdFound13 != 0 )
               {
                  init_level_properties13( ) ;
                  getByPrimaryKey0C13( ) ;
                  AddRow0C13( ) ;
                  ScanNext0C13( ) ;
               }
               ScanEnd0C13( ) ;
               nBlankRcdCount13 = 5;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            standaloneNotModal0C13( ) ;
            standaloneModal0C13( ) ;
            sMode13 = Gx_mode;
            while ( nGXsfl_103_idx < nRC_GXsfl_103 )
            {
               bGXsfl_103_Refreshing = true;
               ReadRow0C13( ) ;
               edtRepairKindId_Enabled = (int)(context.localUtil.CToN( cgiGet( "REPAIRKINDID_"+sGXsfl_103_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairKindId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairKindId_Enabled), 5, 0)), !bGXsfl_103_Refreshing);
               cmbRepairKindName.Enabled = (int)(context.localUtil.CToN( cgiGet( "REPAIRKINDNAME_"+sGXsfl_103_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRepairKindName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbRepairKindName.Enabled), 5, 0)), !bGXsfl_103_Refreshing);
               edtRepairKindRemaks_Enabled = (int)(context.localUtil.CToN( cgiGet( "REPAIRKINDREMAKS_"+sGXsfl_103_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairKindRemaks_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairKindRemaks_Enabled), 5, 0)), !bGXsfl_103_Refreshing);
               if ( ( nRcdExists_13 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal0C13( ) ;
               }
               SendRow0C13( ) ;
               bGXsfl_103_Refreshing = false;
            }
            Gx_mode = sMode13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount13 = 5;
            nRcdExists_13 = 1;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               ScanStart0C13( ) ;
               while ( RcdFound13 != 0 )
               {
                  sGXsfl_103_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_103_idx+1), 4, 0)), 4, "0");
                  SubsflControlProps_10313( ) ;
                  init_level_properties13( ) ;
                  standaloneNotModal0C13( ) ;
                  getByPrimaryKey0C13( ) ;
                  standaloneModal0C13( ) ;
                  AddRow0C13( ) ;
                  ScanNext0C13( ) ;
               }
               ScanEnd0C13( ) ;
            }
         }
         /* Initialize fields for 'new' records and send them. */
         sMode13 = Gx_mode;
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sGXsfl_103_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_103_idx+1), 4, 0)), 4, "0");
         SubsflControlProps_10313( ) ;
         InitAll0C13( ) ;
         init_level_properties13( ) ;
         nRcdExists_13 = 0;
         nIsMod_13 = 0;
         nRcdDeleted_13 = 0;
         nBlankRcdCount13 = (short)(nBlankRcdUsr13+nBlankRcdCount13);
         fRowAdded = 0;
         while ( nBlankRcdCount13 > 0 )
         {
            standaloneNotModal0C13( ) ;
            standaloneModal0C13( ) ;
            AddRow0C13( ) ;
            if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
            {
               fRowAdded = 1;
               GX_FocusControl = edtRepairKindId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nBlankRcdCount13 = (short)(nBlankRcdCount13-1);
         }
         Gx_mode = sMode13;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sStyleString = "";
         context.WriteHtmlText( "<div id=\""+"Gridrepair_kindContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridrepair_kind", Gridrepair_kindContainer);
         if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridrepair_kindContainerData", Gridrepair_kindContainer.ToJavascriptSource());
         }
         if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridrepair_kindContainerData"+"V", Gridrepair_kindContainer.GridValuesHidden());
         }
         else
         {
            context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridrepair_kindContainerData"+"V"+"\" value='"+Gridrepair_kindContainer.GridValuesHidden()+"'/>") ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtRepairId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRepairId_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REPAIRID");
               AnyError = 1;
               GX_FocusControl = edtRepairId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A29RepairId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
            }
            else
            {
               A29RepairId = (short)(context.localUtil.CToN( cgiGet( edtRepairId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
            }
            if ( context.localUtil.VCDate( cgiGet( edtRepairDateFrom_Internalname), 2) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Repair Date From"}), 1, "REPAIRDATEFROM");
               AnyError = 1;
               GX_FocusControl = edtRepairDateFrom_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A30RepairDateFrom = DateTime.MinValue;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30RepairDateFrom", context.localUtil.Format(A30RepairDateFrom, "99/99/99"));
            }
            else
            {
               A30RepairDateFrom = context.localUtil.CToD( cgiGet( edtRepairDateFrom_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30RepairDateFrom", context.localUtil.Format(A30RepairDateFrom, "99/99/99"));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtRepairDaysQuantity_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRepairDaysQuantity_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REPAIRDAYSQUANTITY");
               AnyError = 1;
               GX_FocusControl = edtRepairDaysQuantity_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A31RepairDaysQuantity = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31RepairDaysQuantity", StringUtil.LTrim( StringUtil.Str( (decimal)(A31RepairDaysQuantity), 4, 0)));
            }
            else
            {
               A31RepairDaysQuantity = (short)(context.localUtil.CToN( cgiGet( edtRepairDaysQuantity_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31RepairDaysQuantity", StringUtil.LTrim( StringUtil.Str( (decimal)(A31RepairDaysQuantity), 4, 0)));
            }
            A32RepairDateTo = context.localUtil.CToD( cgiGet( edtRepairDateTo_Internalname), 2);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32RepairDateTo", context.localUtil.Format(A32RepairDateTo, "99/99/99"));
            if ( ( ( context.localUtil.CToN( cgiGet( edtGameId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtGameId_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "GAMEID");
               AnyError = 1;
               GX_FocusControl = edtGameId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A18GameId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18GameId", StringUtil.LTrim( StringUtil.Str( (decimal)(A18GameId), 4, 0)));
            }
            else
            {
               A18GameId = (short)(context.localUtil.CToN( cgiGet( edtGameId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18GameId", StringUtil.LTrim( StringUtil.Str( (decimal)(A18GameId), 4, 0)));
            }
            A19GameName = cgiGet( edtGameName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19GameName", A19GameName);
            if ( ( ( context.localUtil.CToN( cgiGet( edtRepairCost_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRepairCost_Internalname), ",", ".") > 99999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REPAIRCOST");
               AnyError = 1;
               GX_FocusControl = edtRepairCost_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A33RepairCost = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33RepairCost", StringUtil.LTrim( StringUtil.Str( A33RepairCost, 8, 2)));
            }
            else
            {
               A33RepairCost = context.localUtil.CToN( cgiGet( edtRepairCost_Internalname), ",", ".");
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33RepairCost", StringUtil.LTrim( StringUtil.Str( A33RepairCost, 8, 2)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtTechnicianId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTechnicianId_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TECHNICIANID");
               AnyError = 1;
               GX_FocusControl = edtTechnicianId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A27TechnicianId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27TechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A27TechnicianId), 4, 0)));
            }
            else
            {
               A27TechnicianId = (short)(context.localUtil.CToN( cgiGet( edtTechnicianId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27TechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A27TechnicianId), 4, 0)));
            }
            A28TechnicianName = cgiGet( edtTechnicianName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28TechnicianName", A28TechnicianName);
            if ( ( ( context.localUtil.CToN( cgiGet( edtRepairTechnicianId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRepairTechnicianId_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REPAIRTECHNICIANID");
               AnyError = 1;
               GX_FocusControl = edtRepairTechnicianId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A40RepairTechnicianId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40RepairTechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A40RepairTechnicianId), 4, 0)));
            }
            else
            {
               A40RepairTechnicianId = (short)(context.localUtil.CToN( cgiGet( edtRepairTechnicianId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40RepairTechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A40RepairTechnicianId), 4, 0)));
            }
            A41RepairTechniciaName = cgiGet( edtRepairTechniciaName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41RepairTechniciaName", A41RepairTechniciaName);
            if ( ( ( context.localUtil.CToN( cgiGet( edtRepairDiscountPercentage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRepairDiscountPercentage_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "REPAIRDISCOUNTPERCENTAGE");
               AnyError = 1;
               GX_FocusControl = edtRepairDiscountPercentage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A36RepairDiscountPercentage = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36RepairDiscountPercentage", StringUtil.LTrim( StringUtil.Str( (decimal)(A36RepairDiscountPercentage), 4, 0)));
            }
            else
            {
               A36RepairDiscountPercentage = (short)(context.localUtil.CToN( cgiGet( edtRepairDiscountPercentage_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36RepairDiscountPercentage", StringUtil.LTrim( StringUtil.Str( (decimal)(A36RepairDiscountPercentage), 4, 0)));
            }
            A37RepairFinalCost = context.localUtil.CToN( cgiGet( edtRepairFinalCost_Internalname), ",", ".");
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37RepairFinalCost", StringUtil.LTrim( StringUtil.Str( A37RepairFinalCost, 8, 2)));
            /* Read saved values. */
            Z29RepairId = (short)(context.localUtil.CToN( cgiGet( "Z29RepairId"), ",", "."));
            Z30RepairDateFrom = context.localUtil.CToD( cgiGet( "Z30RepairDateFrom"), 0);
            Z31RepairDaysQuantity = (short)(context.localUtil.CToN( cgiGet( "Z31RepairDaysQuantity"), ",", "."));
            Z33RepairCost = context.localUtil.CToN( cgiGet( "Z33RepairCost"), ",", ".");
            Z36RepairDiscountPercentage = (short)(context.localUtil.CToN( cgiGet( "Z36RepairDiscountPercentage"), ",", "."));
            Z18GameId = (short)(context.localUtil.CToN( cgiGet( "Z18GameId"), ",", "."));
            Z27TechnicianId = (short)(context.localUtil.CToN( cgiGet( "Z27TechnicianId"), ",", "."));
            Z40RepairTechnicianId = (short)(context.localUtil.CToN( cgiGet( "Z40RepairTechnicianId"), ",", "."));
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
            Gx_mode = cgiGet( "Mode");
            nRC_GXsfl_103 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_103"), ",", "."));
            Gx_mode = cgiGet( "vMODE");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A29RepairId = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0C12( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes0C12( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0C13( )
      {
         nGXsfl_103_idx = 0;
         while ( nGXsfl_103_idx < nRC_GXsfl_103 )
         {
            ReadRow0C13( ) ;
            if ( ( nRcdExists_13 != 0 ) || ( nIsMod_13 != 0 ) )
            {
               GetKey0C13( ) ;
               if ( ( nRcdExists_13 == 0 ) && ( nRcdDeleted_13 == 0 ) )
               {
                  if ( RcdFound13 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate0C13( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0C13( ) ;
                        CloseExtendedTableCursors0C13( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "REPAIRKINDID_" + sGXsfl_103_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtRepairKindId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound13 != 0 )
                  {
                     if ( nRcdDeleted_13 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey0C13( ) ;
                        Load0C13( ) ;
                        BeforeValidate0C13( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0C13( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_13 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate0C13( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0C13( ) ;
                              CloseExtendedTableCursors0C13( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_13 == 0 )
                     {
                        GXCCtl = "REPAIRKINDID_" + sGXsfl_103_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtRepairKindId_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtRepairKindId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A42RepairKindId), 1, 0, ",", ""))) ;
            ChangePostValue( cmbRepairKindName_Internalname, StringUtil.RTrim( A43RepairKindName)) ;
            ChangePostValue( edtRepairKindRemaks_Internalname, StringUtil.RTrim( A44RepairKindRemaks)) ;
            ChangePostValue( "ZT_"+"Z42RepairKindId_"+sGXsfl_103_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z42RepairKindId), 1, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z43RepairKindName_"+sGXsfl_103_idx, StringUtil.RTrim( Z43RepairKindName)) ;
            ChangePostValue( "ZT_"+"Z44RepairKindRemaks_"+sGXsfl_103_idx, StringUtil.RTrim( Z44RepairKindRemaks)) ;
            ChangePostValue( "nRcdDeleted_13_"+sGXsfl_103_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_13), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_13_"+sGXsfl_103_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_13), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_13_"+sGXsfl_103_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_13), 4, 0, ",", ""))) ;
            if ( nIsMod_13 != 0 )
            {
               ChangePostValue( "REPAIRKINDID_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRepairKindId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "REPAIRKINDNAME_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbRepairKindName.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "REPAIRKINDREMAKS_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRepairKindRemaks_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption0C0( )
      {
      }

      protected void ZM0C12( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z30RepairDateFrom = T000C5_A30RepairDateFrom[0];
               Z31RepairDaysQuantity = T000C5_A31RepairDaysQuantity[0];
               Z33RepairCost = T000C5_A33RepairCost[0];
               Z36RepairDiscountPercentage = T000C5_A36RepairDiscountPercentage[0];
               Z18GameId = T000C5_A18GameId[0];
               Z27TechnicianId = T000C5_A27TechnicianId[0];
               Z40RepairTechnicianId = T000C5_A40RepairTechnicianId[0];
            }
            else
            {
               Z30RepairDateFrom = A30RepairDateFrom;
               Z31RepairDaysQuantity = A31RepairDaysQuantity;
               Z33RepairCost = A33RepairCost;
               Z36RepairDiscountPercentage = A36RepairDiscountPercentage;
               Z18GameId = A18GameId;
               Z27TechnicianId = A27TechnicianId;
               Z40RepairTechnicianId = A40RepairTechnicianId;
            }
         }
         if ( GX_JID == -5 )
         {
            Z29RepairId = A29RepairId;
            Z30RepairDateFrom = A30RepairDateFrom;
            Z31RepairDaysQuantity = A31RepairDaysQuantity;
            Z33RepairCost = A33RepairCost;
            Z36RepairDiscountPercentage = A36RepairDiscountPercentage;
            Z18GameId = A18GameId;
            Z27TechnicianId = A27TechnicianId;
            Z40RepairTechnicianId = A40RepairTechnicianId;
            Z19GameName = A19GameName;
            Z28TechnicianName = A28TechnicianName;
            Z41RepairTechniciaName = A41RepairTechniciaName;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_18_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0080.aspx"+"',["+"{Ctrl:gx.dom.el('"+"GAMEID"+"'), id:'"+"GAMEID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_27_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0090.aspx"+"',["+"{Ctrl:gx.dom.el('"+"TECHNICIANID"+"'), id:'"+"TECHNICIANID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_40_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0090.aspx"+"',["+"{Ctrl:gx.dom.el('"+"REPAIRTECHNICIANID"+"'), id:'"+"REPAIRTECHNICIANID"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load0C12( )
      {
         /* Using cursor T000C9 */
         pr_default.execute(7, new Object[] {A29RepairId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound12 = 1;
            A30RepairDateFrom = T000C9_A30RepairDateFrom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30RepairDateFrom", context.localUtil.Format(A30RepairDateFrom, "99/99/99"));
            A31RepairDaysQuantity = T000C9_A31RepairDaysQuantity[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31RepairDaysQuantity", StringUtil.LTrim( StringUtil.Str( (decimal)(A31RepairDaysQuantity), 4, 0)));
            A19GameName = T000C9_A19GameName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19GameName", A19GameName);
            A33RepairCost = T000C9_A33RepairCost[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33RepairCost", StringUtil.LTrim( StringUtil.Str( A33RepairCost, 8, 2)));
            A28TechnicianName = T000C9_A28TechnicianName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28TechnicianName", A28TechnicianName);
            A41RepairTechniciaName = T000C9_A41RepairTechniciaName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41RepairTechniciaName", A41RepairTechniciaName);
            A36RepairDiscountPercentage = T000C9_A36RepairDiscountPercentage[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36RepairDiscountPercentage", StringUtil.LTrim( StringUtil.Str( (decimal)(A36RepairDiscountPercentage), 4, 0)));
            A18GameId = T000C9_A18GameId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18GameId", StringUtil.LTrim( StringUtil.Str( (decimal)(A18GameId), 4, 0)));
            A27TechnicianId = T000C9_A27TechnicianId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27TechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A27TechnicianId), 4, 0)));
            A40RepairTechnicianId = T000C9_A40RepairTechnicianId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40RepairTechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A40RepairTechnicianId), 4, 0)));
            ZM0C12( -5) ;
         }
         pr_default.close(7);
         OnLoadActions0C12( ) ;
      }

      protected void OnLoadActions0C12( )
      {
         A32RepairDateTo = DateTimeUtil.DAdd(A30RepairDateFrom,+((int)(A31RepairDaysQuantity)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32RepairDateTo", context.localUtil.Format(A32RepairDateTo, "99/99/99"));
         A37RepairFinalCost = (decimal)(A33RepairCost-((A33RepairCost*A36RepairDiscountPercentage)/ (decimal)(100)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37RepairFinalCost", StringUtil.LTrim( StringUtil.Str( A37RepairFinalCost, 8, 2)));
      }

      protected void CheckExtendedTable0C12( )
      {
         nIsDirty_12 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A30RepairDateFrom) || ( A30RepairDateFrom >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Repair Date From fuera de rango", "OutOfRange", 1, "REPAIRDATEFROM");
            AnyError = 1;
            GX_FocusControl = edtRepairDateFrom_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         nIsDirty_12 = 1;
         A32RepairDateTo = DateTimeUtil.DAdd(A30RepairDateFrom,+((int)(A31RepairDaysQuantity)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32RepairDateTo", context.localUtil.Format(A32RepairDateTo, "99/99/99"));
         /* Using cursor T000C6 */
         pr_default.execute(4, new Object[] {A18GameId});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No existe 'Game'.", "ForeignKeyNotFound", 1, "GAMEID");
            AnyError = 1;
            GX_FocusControl = edtGameId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A19GameName = T000C6_A19GameName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19GameName", A19GameName);
         pr_default.close(4);
         nIsDirty_12 = 1;
         A37RepairFinalCost = (decimal)(A33RepairCost-((A33RepairCost*A36RepairDiscountPercentage)/ (decimal)(100)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37RepairFinalCost", StringUtil.LTrim( StringUtil.Str( A37RepairFinalCost, 8, 2)));
         /* Using cursor T000C7 */
         pr_default.execute(5, new Object[] {A27TechnicianId});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("No existe 'Technican'.", "ForeignKeyNotFound", 1, "TECHNICIANID");
            AnyError = 1;
            GX_FocusControl = edtTechnicianId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A28TechnicianName = T000C7_A28TechnicianName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28TechnicianName", A28TechnicianName);
         pr_default.close(5);
         /* Using cursor T000C8 */
         pr_default.execute(6, new Object[] {A40RepairTechnicianId});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No existe 'Repair Technician Alternative'.", "ForeignKeyNotFound", 1, "REPAIRTECHNICIANID");
            AnyError = 1;
            GX_FocusControl = edtRepairTechnicianId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A41RepairTechniciaName = T000C8_A41RepairTechniciaName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41RepairTechniciaName", A41RepairTechniciaName);
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors0C12( )
      {
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_6( short A18GameId )
      {
         /* Using cursor T000C10 */
         pr_default.execute(8, new Object[] {A18GameId});
         if ( (pr_default.getStatus(8) == 101) )
         {
            GX_msglist.addItem("No existe 'Game'.", "ForeignKeyNotFound", 1, "GAMEID");
            AnyError = 1;
            GX_FocusControl = edtGameId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A19GameName = T000C10_A19GameName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19GameName", A19GameName);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A19GameName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(8) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(8);
      }

      protected void gxLoad_7( short A27TechnicianId )
      {
         /* Using cursor T000C11 */
         pr_default.execute(9, new Object[] {A27TechnicianId});
         if ( (pr_default.getStatus(9) == 101) )
         {
            GX_msglist.addItem("No existe 'Technican'.", "ForeignKeyNotFound", 1, "TECHNICIANID");
            AnyError = 1;
            GX_FocusControl = edtTechnicianId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A28TechnicianName = T000C11_A28TechnicianName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28TechnicianName", A28TechnicianName);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A28TechnicianName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(9) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(9);
      }

      protected void gxLoad_8( short A40RepairTechnicianId )
      {
         /* Using cursor T000C12 */
         pr_default.execute(10, new Object[] {A40RepairTechnicianId});
         if ( (pr_default.getStatus(10) == 101) )
         {
            GX_msglist.addItem("No existe 'Repair Technician Alternative'.", "ForeignKeyNotFound", 1, "REPAIRTECHNICIANID");
            AnyError = 1;
            GX_FocusControl = edtRepairTechnicianId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A41RepairTechniciaName = T000C12_A41RepairTechniciaName[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41RepairTechniciaName", A41RepairTechniciaName);
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A41RepairTechniciaName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(10) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(10);
      }

      protected void GetKey0C12( )
      {
         /* Using cursor T000C13 */
         pr_default.execute(11, new Object[] {A29RepairId});
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound12 = 1;
         }
         else
         {
            RcdFound12 = 0;
         }
         pr_default.close(11);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000C5 */
         pr_default.execute(3, new Object[] {A29RepairId});
         if ( (pr_default.getStatus(3) != 101) )
         {
            ZM0C12( 5) ;
            RcdFound12 = 1;
            A29RepairId = T000C5_A29RepairId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
            A30RepairDateFrom = T000C5_A30RepairDateFrom[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30RepairDateFrom", context.localUtil.Format(A30RepairDateFrom, "99/99/99"));
            A31RepairDaysQuantity = T000C5_A31RepairDaysQuantity[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31RepairDaysQuantity", StringUtil.LTrim( StringUtil.Str( (decimal)(A31RepairDaysQuantity), 4, 0)));
            A33RepairCost = T000C5_A33RepairCost[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33RepairCost", StringUtil.LTrim( StringUtil.Str( A33RepairCost, 8, 2)));
            A36RepairDiscountPercentage = T000C5_A36RepairDiscountPercentage[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36RepairDiscountPercentage", StringUtil.LTrim( StringUtil.Str( (decimal)(A36RepairDiscountPercentage), 4, 0)));
            A18GameId = T000C5_A18GameId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18GameId", StringUtil.LTrim( StringUtil.Str( (decimal)(A18GameId), 4, 0)));
            A27TechnicianId = T000C5_A27TechnicianId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27TechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A27TechnicianId), 4, 0)));
            A40RepairTechnicianId = T000C5_A40RepairTechnicianId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40RepairTechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A40RepairTechnicianId), 4, 0)));
            Z29RepairId = A29RepairId;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0C12( ) ;
            if ( AnyError == 1 )
            {
               RcdFound12 = 0;
               InitializeNonKey0C12( ) ;
            }
            Gx_mode = sMode12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound12 = 0;
            InitializeNonKey0C12( ) ;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode12;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(3);
      }

      protected void getEqualNoModal( )
      {
         GetKey0C12( ) ;
         if ( RcdFound12 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound12 = 0;
         /* Using cursor T000C14 */
         pr_default.execute(12, new Object[] {A29RepairId});
         if ( (pr_default.getStatus(12) != 101) )
         {
            while ( (pr_default.getStatus(12) != 101) && ( ( T000C14_A29RepairId[0] < A29RepairId ) ) )
            {
               pr_default.readNext(12);
            }
            if ( (pr_default.getStatus(12) != 101) && ( ( T000C14_A29RepairId[0] > A29RepairId ) ) )
            {
               A29RepairId = T000C14_A29RepairId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
               RcdFound12 = 1;
            }
         }
         pr_default.close(12);
      }

      protected void move_previous( )
      {
         RcdFound12 = 0;
         /* Using cursor T000C15 */
         pr_default.execute(13, new Object[] {A29RepairId});
         if ( (pr_default.getStatus(13) != 101) )
         {
            while ( (pr_default.getStatus(13) != 101) && ( ( T000C15_A29RepairId[0] > A29RepairId ) ) )
            {
               pr_default.readNext(13);
            }
            if ( (pr_default.getStatus(13) != 101) && ( ( T000C15_A29RepairId[0] < A29RepairId ) ) )
            {
               A29RepairId = T000C15_A29RepairId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
               RcdFound12 = 1;
            }
         }
         pr_default.close(13);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0C12( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtRepairId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0C12( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound12 == 1 )
            {
               if ( A29RepairId != Z29RepairId )
               {
                  A29RepairId = Z29RepairId;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "REPAIRID");
                  AnyError = 1;
                  GX_FocusControl = edtRepairId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtRepairId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0C12( ) ;
                  GX_FocusControl = edtRepairId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A29RepairId != Z29RepairId )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtRepairId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0C12( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "REPAIRID");
                     AnyError = 1;
                     GX_FocusControl = edtRepairId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtRepairId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0C12( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A29RepairId != Z29RepairId )
         {
            A29RepairId = Z29RepairId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "REPAIRID");
            AnyError = 1;
            GX_FocusControl = edtRepairId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtRepairId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "REPAIRID");
            AnyError = 1;
            GX_FocusControl = edtRepairId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtRepairDateFrom_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0C12( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRepairDateFrom_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0C12( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRepairDateFrom_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRepairDateFrom_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0C12( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound12 != 0 )
            {
               ScanNext0C12( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRepairDateFrom_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0C12( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0C12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000C4 */
            pr_default.execute(2, new Object[] {A29RepairId});
            if ( (pr_default.getStatus(2) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Repair"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(2) == 101) || ( Z30RepairDateFrom != T000C4_A30RepairDateFrom[0] ) || ( Z31RepairDaysQuantity != T000C4_A31RepairDaysQuantity[0] ) || ( Z33RepairCost != T000C4_A33RepairCost[0] ) || ( Z36RepairDiscountPercentage != T000C4_A36RepairDiscountPercentage[0] ) || ( Z18GameId != T000C4_A18GameId[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z27TechnicianId != T000C4_A27TechnicianId[0] ) || ( Z40RepairTechnicianId != T000C4_A40RepairTechnicianId[0] ) )
            {
               if ( Z30RepairDateFrom != T000C4_A30RepairDateFrom[0] )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"RepairDateFrom");
                  GXUtil.WriteLogRaw("Old: ",Z30RepairDateFrom);
                  GXUtil.WriteLogRaw("Current: ",T000C4_A30RepairDateFrom[0]);
               }
               if ( Z31RepairDaysQuantity != T000C4_A31RepairDaysQuantity[0] )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"RepairDaysQuantity");
                  GXUtil.WriteLogRaw("Old: ",Z31RepairDaysQuantity);
                  GXUtil.WriteLogRaw("Current: ",T000C4_A31RepairDaysQuantity[0]);
               }
               if ( Z33RepairCost != T000C4_A33RepairCost[0] )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"RepairCost");
                  GXUtil.WriteLogRaw("Old: ",Z33RepairCost);
                  GXUtil.WriteLogRaw("Current: ",T000C4_A33RepairCost[0]);
               }
               if ( Z36RepairDiscountPercentage != T000C4_A36RepairDiscountPercentage[0] )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"RepairDiscountPercentage");
                  GXUtil.WriteLogRaw("Old: ",Z36RepairDiscountPercentage);
                  GXUtil.WriteLogRaw("Current: ",T000C4_A36RepairDiscountPercentage[0]);
               }
               if ( Z18GameId != T000C4_A18GameId[0] )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"GameId");
                  GXUtil.WriteLogRaw("Old: ",Z18GameId);
                  GXUtil.WriteLogRaw("Current: ",T000C4_A18GameId[0]);
               }
               if ( Z27TechnicianId != T000C4_A27TechnicianId[0] )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"TechnicianId");
                  GXUtil.WriteLogRaw("Old: ",Z27TechnicianId);
                  GXUtil.WriteLogRaw("Current: ",T000C4_A27TechnicianId[0]);
               }
               if ( Z40RepairTechnicianId != T000C4_A40RepairTechnicianId[0] )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"RepairTechnicianId");
                  GXUtil.WriteLogRaw("Old: ",Z40RepairTechnicianId);
                  GXUtil.WriteLogRaw("Current: ",T000C4_A40RepairTechnicianId[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Repair"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0C12( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0C12( 0) ;
            CheckOptimisticConcurrency0C12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0C12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000C16 */
                     pr_default.execute(14, new Object[] {A30RepairDateFrom, A31RepairDaysQuantity, A33RepairCost, A36RepairDiscountPercentage, A18GameId, A27TechnicianId, A40RepairTechnicianId});
                     A29RepairId = T000C16_A29RepairId[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("Repair") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0C12( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                              ResetCaption0C0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0C12( ) ;
            }
            EndLevel0C12( ) ;
         }
         CloseExtendedTableCursors0C12( ) ;
      }

      protected void Update0C12( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0C12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000C17 */
                     pr_default.execute(15, new Object[] {A30RepairDateFrom, A31RepairDaysQuantity, A33RepairCost, A36RepairDiscountPercentage, A18GameId, A27TechnicianId, A40RepairTechnicianId, A29RepairId});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("Repair") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Repair"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0C12( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0C12( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                              ResetCaption0C0( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0C12( ) ;
         }
         CloseExtendedTableCursors0C12( ) ;
      }

      protected void DeferredUpdate0C12( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0C12( ) ;
            AfterConfirm0C12( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0C12( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart0C13( ) ;
                  while ( RcdFound13 != 0 )
                  {
                     getByPrimaryKey0C13( ) ;
                     Delete0C13( ) ;
                     ScanNext0C13( ) ;
                  }
                  ScanEnd0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000C18 */
                     pr_default.execute(16, new Object[] {A29RepairId});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("Repair") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound12 == 0 )
                           {
                              InitAll0C12( ) ;
                              Gx_mode = "INS";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                           ResetCaption0C0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode12 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0C12( ) ;
         Gx_mode = sMode12;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0C12( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            A32RepairDateTo = DateTimeUtil.DAdd(A30RepairDateFrom,+((int)(A31RepairDaysQuantity)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32RepairDateTo", context.localUtil.Format(A32RepairDateTo, "99/99/99"));
            /* Using cursor T000C19 */
            pr_default.execute(17, new Object[] {A18GameId});
            A19GameName = T000C19_A19GameName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19GameName", A19GameName);
            pr_default.close(17);
            /* Using cursor T000C20 */
            pr_default.execute(18, new Object[] {A27TechnicianId});
            A28TechnicianName = T000C20_A28TechnicianName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28TechnicianName", A28TechnicianName);
            pr_default.close(18);
            /* Using cursor T000C21 */
            pr_default.execute(19, new Object[] {A40RepairTechnicianId});
            A41RepairTechniciaName = T000C21_A41RepairTechniciaName[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41RepairTechniciaName", A41RepairTechniciaName);
            pr_default.close(19);
            A37RepairFinalCost = (decimal)(A33RepairCost-((A33RepairCost*A36RepairDiscountPercentage)/ (decimal)(100)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37RepairFinalCost", StringUtil.LTrim( StringUtil.Str( A37RepairFinalCost, 8, 2)));
         }
      }

      protected void ProcessNestedLevel0C13( )
      {
         nGXsfl_103_idx = 0;
         while ( nGXsfl_103_idx < nRC_GXsfl_103 )
         {
            ReadRow0C13( ) ;
            if ( ( nRcdExists_13 != 0 ) || ( nIsMod_13 != 0 ) )
            {
               standaloneNotModal0C13( ) ;
               GetKey0C13( ) ;
               if ( ( nRcdExists_13 == 0 ) && ( nRcdDeleted_13 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  Insert0C13( ) ;
               }
               else
               {
                  if ( RcdFound13 != 0 )
                  {
                     if ( ( nRcdDeleted_13 != 0 ) && ( nRcdExists_13 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        Delete0C13( ) ;
                     }
                     else
                     {
                        if ( nRcdExists_13 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           Update0C13( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_13 == 0 )
                     {
                        GXCCtl = "REPAIRKINDID_" + sGXsfl_103_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtRepairKindId_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtRepairKindId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A42RepairKindId), 1, 0, ",", ""))) ;
            ChangePostValue( cmbRepairKindName_Internalname, StringUtil.RTrim( A43RepairKindName)) ;
            ChangePostValue( edtRepairKindRemaks_Internalname, StringUtil.RTrim( A44RepairKindRemaks)) ;
            ChangePostValue( "ZT_"+"Z42RepairKindId_"+sGXsfl_103_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z42RepairKindId), 1, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z43RepairKindName_"+sGXsfl_103_idx, StringUtil.RTrim( Z43RepairKindName)) ;
            ChangePostValue( "ZT_"+"Z44RepairKindRemaks_"+sGXsfl_103_idx, StringUtil.RTrim( Z44RepairKindRemaks)) ;
            ChangePostValue( "nRcdDeleted_13_"+sGXsfl_103_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_13), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_13_"+sGXsfl_103_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_13), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_13_"+sGXsfl_103_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_13), 4, 0, ",", ""))) ;
            if ( nIsMod_13 != 0 )
            {
               ChangePostValue( "REPAIRKINDID_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRepairKindId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "REPAIRKINDNAME_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbRepairKindName.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "REPAIRKINDREMAKS_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRepairKindRemaks_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0C13( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_13 = 0;
         nIsMod_13 = 0;
         nRcdDeleted_13 = 0;
      }

      protected void ProcessLevel0C12( )
      {
         /* Save parent mode. */
         sMode12 = Gx_mode;
         ProcessNestedLevel0C13( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode12;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel0C12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(2);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(3);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(17);
            pr_default.close(18);
            pr_default.close(19);
            context.CommitDataStores("repair",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0C0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(3);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(17);
            pr_default.close(18);
            pr_default.close(19);
            context.RollbackDataStores("repair",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0C12( )
      {
         /* Using cursor T000C22 */
         pr_default.execute(20);
         RcdFound12 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound12 = 1;
            A29RepairId = T000C22_A29RepairId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0C12( )
      {
         /* Scan next routine */
         pr_default.readNext(20);
         RcdFound12 = 0;
         if ( (pr_default.getStatus(20) != 101) )
         {
            RcdFound12 = 1;
            A29RepairId = T000C22_A29RepairId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
         }
      }

      protected void ScanEnd0C12( )
      {
         pr_default.close(20);
      }

      protected void AfterConfirm0C12( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0C12( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0C12( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0C12( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0C12( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0C12( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0C12( )
      {
         edtRepairId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairId_Enabled), 5, 0)), true);
         edtRepairDateFrom_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairDateFrom_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairDateFrom_Enabled), 5, 0)), true);
         edtRepairDaysQuantity_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairDaysQuantity_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairDaysQuantity_Enabled), 5, 0)), true);
         edtRepairDateTo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairDateTo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairDateTo_Enabled), 5, 0)), true);
         edtGameId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGameId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGameId_Enabled), 5, 0)), true);
         edtGameName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtGameName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtGameName_Enabled), 5, 0)), true);
         edtRepairCost_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairCost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairCost_Enabled), 5, 0)), true);
         edtTechnicianId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTechnicianId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTechnicianId_Enabled), 5, 0)), true);
         edtTechnicianName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTechnicianName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTechnicianName_Enabled), 5, 0)), true);
         edtRepairTechnicianId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairTechnicianId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairTechnicianId_Enabled), 5, 0)), true);
         edtRepairTechniciaName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairTechniciaName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairTechniciaName_Enabled), 5, 0)), true);
         edtRepairDiscountPercentage_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairDiscountPercentage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairDiscountPercentage_Enabled), 5, 0)), true);
         edtRepairFinalCost_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairFinalCost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairFinalCost_Enabled), 5, 0)), true);
      }

      protected void ZM0C13( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z43RepairKindName = T000C3_A43RepairKindName[0];
               Z44RepairKindRemaks = T000C3_A44RepairKindRemaks[0];
            }
            else
            {
               Z43RepairKindName = A43RepairKindName;
               Z44RepairKindRemaks = A44RepairKindRemaks;
            }
         }
         if ( GX_JID == -9 )
         {
            Z29RepairId = A29RepairId;
            Z42RepairKindId = A42RepairKindId;
            Z43RepairKindName = A43RepairKindName;
            Z44RepairKindRemaks = A44RepairKindRemaks;
         }
      }

      protected void standaloneNotModal0C13( )
      {
      }

      protected void standaloneModal0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtRepairKindId_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairKindId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairKindId_Enabled), 5, 0)), !bGXsfl_103_Refreshing);
         }
         else
         {
            edtRepairKindId_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairKindId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairKindId_Enabled), 5, 0)), !bGXsfl_103_Refreshing);
         }
      }

      protected void Load0C13( )
      {
         /* Using cursor T000C23 */
         pr_default.execute(21, new Object[] {A29RepairId, A42RepairKindId});
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound13 = 1;
            A43RepairKindName = T000C23_A43RepairKindName[0];
            A44RepairKindRemaks = T000C23_A44RepairKindRemaks[0];
            ZM0C13( -9) ;
         }
         pr_default.close(21);
         OnLoadActions0C13( ) ;
      }

      protected void OnLoadActions0C13( )
      {
      }

      protected void CheckExtendedTable0C13( )
      {
         nIsDirty_13 = 0;
         Gx_BScreen = 1;
         standaloneModal0C13( ) ;
         if ( ! ( ( StringUtil.StrCmp(A43RepairKindName, "E") == 0 ) || ( StringUtil.StrCmp(A43RepairKindName, "M") == 0 ) || ( StringUtil.StrCmp(A43RepairKindName, "R") == 0 ) ) )
         {
            GXCCtl = "REPAIRKINDNAME_" + sGXsfl_103_idx;
            GX_msglist.addItem("Campo Repair Kind Name fuera de rango", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = cmbRepairKindName_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors0C13( )
      {
      }

      protected void enableDisable0C13( )
      {
      }

      protected void GetKey0C13( )
      {
         /* Using cursor T000C24 */
         pr_default.execute(22, new Object[] {A29RepairId, A42RepairKindId});
         if ( (pr_default.getStatus(22) != 101) )
         {
            RcdFound13 = 1;
         }
         else
         {
            RcdFound13 = 0;
         }
         pr_default.close(22);
      }

      protected void getByPrimaryKey0C13( )
      {
         /* Using cursor T000C3 */
         pr_default.execute(1, new Object[] {A29RepairId, A42RepairKindId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0C13( 9) ;
            RcdFound13 = 1;
            InitializeNonKey0C13( ) ;
            A42RepairKindId = T000C3_A42RepairKindId[0];
            A43RepairKindName = T000C3_A43RepairKindName[0];
            A44RepairKindRemaks = T000C3_A44RepairKindRemaks[0];
            Z29RepairId = A29RepairId;
            Z42RepairKindId = A42RepairKindId;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal0C13( ) ;
            Load0C13( ) ;
            Gx_mode = sMode13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound13 = 0;
            InitializeNonKey0C13( ) ;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal0C13( ) ;
            Gx_mode = sMode13;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes0C13( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000C2 */
            pr_default.execute(0, new Object[] {A29RepairId, A42RepairKindId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RepairKind"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z43RepairKindName, T000C2_A43RepairKindName[0]) != 0 ) || ( StringUtil.StrCmp(Z44RepairKindRemaks, T000C2_A44RepairKindRemaks[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z43RepairKindName, T000C2_A43RepairKindName[0]) != 0 )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"RepairKindName");
                  GXUtil.WriteLogRaw("Old: ",Z43RepairKindName);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A43RepairKindName[0]);
               }
               if ( StringUtil.StrCmp(Z44RepairKindRemaks, T000C2_A44RepairKindRemaks[0]) != 0 )
               {
                  GXUtil.WriteLog("repair:[seudo value changed for attri]"+"RepairKindRemaks");
                  GXUtil.WriteLogRaw("Old: ",Z44RepairKindRemaks);
                  GXUtil.WriteLogRaw("Current: ",T000C2_A44RepairKindRemaks[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"RepairKind"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0C13( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0C13( 0) ;
            CheckOptimisticConcurrency0C13( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C13( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000C25 */
                     pr_default.execute(23, new Object[] {A29RepairId, A42RepairKindId, A43RepairKindName, A44RepairKindRemaks});
                     pr_default.close(23);
                     dsDefault.SmartCacheProvider.SetUpdated("RepairKind") ;
                     if ( (pr_default.getStatus(23) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0C13( ) ;
            }
            EndLevel0C13( ) ;
         }
         CloseExtendedTableCursors0C13( ) ;
      }

      protected void Update0C13( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C13( ) ;
         }
         if ( ( nIsMod_13 != 0 ) || ( nIsDirty_13 != 0 ) )
         {
            if ( AnyError == 0 )
            {
               CheckOptimisticConcurrency0C13( ) ;
               if ( AnyError == 0 )
               {
                  AfterConfirm0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     BeforeUpdate0C13( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Using cursor T000C26 */
                        pr_default.execute(24, new Object[] {A43RepairKindName, A44RepairKindRemaks, A29RepairId, A42RepairKindId});
                        pr_default.close(24);
                        dsDefault.SmartCacheProvider.SetUpdated("RepairKind") ;
                        if ( (pr_default.getStatus(24) == 103) )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RepairKind"}), "RecordIsLocked", 1, "");
                           AnyError = 1;
                        }
                        DeferredUpdate0C13( ) ;
                        if ( AnyError == 0 )
                        {
                           /* Start of After( update) rules */
                           /* End of After( update) rules */
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey0C13( ) ;
                           }
                        }
                        else
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                           AnyError = 1;
                        }
                     }
                  }
               }
               EndLevel0C13( ) ;
            }
         }
         CloseExtendedTableCursors0C13( ) ;
      }

      protected void DeferredUpdate0C13( )
      {
      }

      protected void Delete0C13( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0C13( ) ;
            AfterConfirm0C13( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0C13( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000C27 */
                  pr_default.execute(25, new Object[] {A29RepairId, A42RepairKindId});
                  pr_default.close(25);
                  dsDefault.SmartCacheProvider.SetUpdated("RepairKind") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode13 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0C13( ) ;
         Gx_mode = sMode13;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0C13( )
      {
         standaloneModal0C13( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0C13( )
      {
         /* Scan By routine */
         /* Using cursor T000C28 */
         pr_default.execute(26, new Object[] {A29RepairId});
         RcdFound13 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound13 = 1;
            A42RepairKindId = T000C28_A42RepairKindId[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0C13( )
      {
         /* Scan next routine */
         pr_default.readNext(26);
         RcdFound13 = 0;
         if ( (pr_default.getStatus(26) != 101) )
         {
            RcdFound13 = 1;
            A42RepairKindId = T000C28_A42RepairKindId[0];
         }
      }

      protected void ScanEnd0C13( )
      {
         pr_default.close(26);
      }

      protected void AfterConfirm0C13( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0C13( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0C13( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0C13( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0C13( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0C13( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0C13( )
      {
         edtRepairKindId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairKindId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairKindId_Enabled), 5, 0)), !bGXsfl_103_Refreshing);
         cmbRepairKindName.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRepairKindName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbRepairKindName.Enabled), 5, 0)), !bGXsfl_103_Refreshing);
         edtRepairKindRemaks_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairKindRemaks_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairKindRemaks_Enabled), 5, 0)), !bGXsfl_103_Refreshing);
      }

      protected void send_integrity_lvl_hashes0C13( )
      {
      }

      protected void send_integrity_lvl_hashes0C12( )
      {
      }

      protected void SubsflControlProps_10313( )
      {
         edtRepairKindId_Internalname = "REPAIRKINDID_"+sGXsfl_103_idx;
         cmbRepairKindName_Internalname = "REPAIRKINDNAME_"+sGXsfl_103_idx;
         edtRepairKindRemaks_Internalname = "REPAIRKINDREMAKS_"+sGXsfl_103_idx;
      }

      protected void SubsflControlProps_fel_10313( )
      {
         edtRepairKindId_Internalname = "REPAIRKINDID_"+sGXsfl_103_fel_idx;
         cmbRepairKindName_Internalname = "REPAIRKINDNAME_"+sGXsfl_103_fel_idx;
         edtRepairKindRemaks_Internalname = "REPAIRKINDREMAKS_"+sGXsfl_103_fel_idx;
      }

      protected void AddRow0C13( )
      {
         nGXsfl_103_idx = (int)(nGXsfl_103_idx+1);
         sGXsfl_103_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_103_idx), 4, 0)), 4, "0");
         SubsflControlProps_10313( ) ;
         SendRow0C13( ) ;
      }

      protected void SendRow0C13( )
      {
         Gridrepair_kindRow = GXWebRow.GetNew(context);
         if ( subGridrepair_kind_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridrepair_kind_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridrepair_kind_Class, "") != 0 )
            {
               subGridrepair_kind_Linesclass = subGridrepair_kind_Class+"Odd";
            }
         }
         else if ( subGridrepair_kind_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridrepair_kind_Backstyle = 0;
            subGridrepair_kind_Backcolor = subGridrepair_kind_Allbackcolor;
            if ( StringUtil.StrCmp(subGridrepair_kind_Class, "") != 0 )
            {
               subGridrepair_kind_Linesclass = subGridrepair_kind_Class+"Uniform";
            }
         }
         else if ( subGridrepair_kind_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridrepair_kind_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridrepair_kind_Class, "") != 0 )
            {
               subGridrepair_kind_Linesclass = subGridrepair_kind_Class+"Odd";
            }
            subGridrepair_kind_Backcolor = (int)(0x0);
         }
         else if ( subGridrepair_kind_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridrepair_kind_Backstyle = 1;
            if ( ((int)((nGXsfl_103_idx) % (2))) == 0 )
            {
               subGridrepair_kind_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridrepair_kind_Class, "") != 0 )
               {
                  subGridrepair_kind_Linesclass = subGridrepair_kind_Class+"Even";
               }
            }
            else
            {
               subGridrepair_kind_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridrepair_kind_Class, "") != 0 )
               {
                  subGridrepair_kind_Linesclass = subGridrepair_kind_Class+"Odd";
               }
            }
         }
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_13_" + sGXsfl_103_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 104,'',false,'" + sGXsfl_103_idx + "',103)\"";
         ROClassString = "Attribute";
         Gridrepair_kindRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRepairKindId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A42RepairKindId), 1, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A42RepairKindId), "9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,104);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRepairKindId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtRepairKindId_Enabled,(short)1,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)103,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_13_" + sGXsfl_103_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 105,'',false,'" + sGXsfl_103_idx + "',103)\"";
         if ( ( cmbRepairKindName.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "REPAIRKINDNAME_" + sGXsfl_103_idx;
            cmbRepairKindName.Name = GXCCtl;
            cmbRepairKindName.WebTags = "";
            cmbRepairKindName.addItem("E", "Electricity", 0);
            cmbRepairKindName.addItem("M", "Mechanical", 0);
            cmbRepairKindName.addItem("R", "Replacement", 0);
            if ( cmbRepairKindName.ItemCount > 0 )
            {
               A43RepairKindName = cmbRepairKindName.getValidValue(A43RepairKindName);
            }
         }
         /* ComboBox */
         Gridrepair_kindRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbRepairKindName,(String)cmbRepairKindName_Internalname,StringUtil.RTrim( A43RepairKindName),(short)1,(String)cmbRepairKindName_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,cmbRepairKindName.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,105);\"",(String)"",(bool)true});
         cmbRepairKindName.CurrentValue = StringUtil.RTrim( A43RepairKindName);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbRepairKindName_Internalname, "Values", (String)(cmbRepairKindName.ToJavascriptSource()), !bGXsfl_103_Refreshing);
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_13_" + sGXsfl_103_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 106,'',false,'" + sGXsfl_103_idx + "',103)\"";
         ROClassString = "Attribute";
         Gridrepair_kindRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRepairKindRemaks_Internalname,StringUtil.RTrim( A44RepairKindRemaks),(String)"",TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRepairKindRemaks_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtRepairKindRemaks_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)120,(short)0,(short)0,(short)103,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         context.httpAjaxContext.ajax_sending_grid_row(Gridrepair_kindRow);
         send_integrity_lvl_hashes0C13( ) ;
         GXCCtl = "Z42RepairKindId_" + sGXsfl_103_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z42RepairKindId), 1, 0, ",", "")));
         GXCCtl = "Z43RepairKindName_" + sGXsfl_103_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Z43RepairKindName));
         GXCCtl = "Z44RepairKindRemaks_" + sGXsfl_103_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.RTrim( Z44RepairKindRemaks));
         GXCCtl = "nRcdDeleted_13_" + sGXsfl_103_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_13), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_13_" + sGXsfl_103_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_13), 4, 0, ",", "")));
         GXCCtl = "nIsMod_13_" + sGXsfl_103_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_13), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REPAIRKINDID_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRepairKindId_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "REPAIRKINDNAME_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbRepairKindName.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "REPAIRKINDREMAKS_"+sGXsfl_103_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRepairKindRemaks_Enabled), 5, 0, ".", "")));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridrepair_kindContainer.AddRow(Gridrepair_kindRow);
      }

      protected void ReadRow0C13( )
      {
         nGXsfl_103_idx = (int)(nGXsfl_103_idx+1);
         sGXsfl_103_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_103_idx), 4, 0)), 4, "0");
         SubsflControlProps_10313( ) ;
         edtRepairKindId_Enabled = (int)(context.localUtil.CToN( cgiGet( "REPAIRKINDID_"+sGXsfl_103_idx+"Enabled"), ",", "."));
         cmbRepairKindName.Enabled = (int)(context.localUtil.CToN( cgiGet( "REPAIRKINDNAME_"+sGXsfl_103_idx+"Enabled"), ",", "."));
         edtRepairKindRemaks_Enabled = (int)(context.localUtil.CToN( cgiGet( "REPAIRKINDREMAKS_"+sGXsfl_103_idx+"Enabled"), ",", "."));
         if ( ( ( context.localUtil.CToN( cgiGet( edtRepairKindId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtRepairKindId_Internalname), ",", ".") > Convert.ToDecimal( 9 )) ) )
         {
            GXCCtl = "REPAIRKINDID_" + sGXsfl_103_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtRepairKindId_Internalname;
            wbErr = true;
            A42RepairKindId = 0;
         }
         else
         {
            A42RepairKindId = (short)(context.localUtil.CToN( cgiGet( edtRepairKindId_Internalname), ",", "."));
         }
         cmbRepairKindName.Name = cmbRepairKindName_Internalname;
         cmbRepairKindName.CurrentValue = cgiGet( cmbRepairKindName_Internalname);
         A43RepairKindName = cgiGet( cmbRepairKindName_Internalname);
         A44RepairKindRemaks = cgiGet( edtRepairKindRemaks_Internalname);
         GXCCtl = "Z42RepairKindId_" + sGXsfl_103_idx;
         Z42RepairKindId = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "Z43RepairKindName_" + sGXsfl_103_idx;
         Z43RepairKindName = cgiGet( GXCCtl);
         GXCCtl = "Z44RepairKindRemaks_" + sGXsfl_103_idx;
         Z44RepairKindRemaks = cgiGet( GXCCtl);
         GXCCtl = "nRcdDeleted_13_" + sGXsfl_103_idx;
         nRcdDeleted_13 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_13_" + sGXsfl_103_idx;
         nRcdExists_13 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_13_" + sGXsfl_103_idx;
         nIsMod_13 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtRepairKindId_Enabled = edtRepairKindId_Enabled;
      }

      protected void ConfirmValues0C0( )
      {
         nGXsfl_103_idx = 0;
         sGXsfl_103_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_103_idx), 4, 0)), 4, "0");
         SubsflControlProps_10313( ) ;
         while ( nGXsfl_103_idx < nRC_GXsfl_103 )
         {
            nGXsfl_103_idx = (int)(nGXsfl_103_idx+1);
            sGXsfl_103_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_103_idx), 4, 0)), 4, "0");
            SubsflControlProps_10313( ) ;
            ChangePostValue( "Z42RepairKindId_"+sGXsfl_103_idx, cgiGet( "ZT_"+"Z42RepairKindId_"+sGXsfl_103_idx)) ;
            DeletePostValue( "ZT_"+"Z42RepairKindId_"+sGXsfl_103_idx) ;
            ChangePostValue( "Z43RepairKindName_"+sGXsfl_103_idx, cgiGet( "ZT_"+"Z43RepairKindName_"+sGXsfl_103_idx)) ;
            DeletePostValue( "ZT_"+"Z43RepairKindName_"+sGXsfl_103_idx) ;
            ChangePostValue( "Z44RepairKindRemaks_"+sGXsfl_103_idx, cgiGet( "ZT_"+"Z44RepairKindRemaks_"+sGXsfl_103_idx)) ;
            DeletePostValue( "ZT_"+"Z44RepairKindRemaks_"+sGXsfl_103_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 135614), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxcfg.js", "?2022531182480", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("repair.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z29RepairId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z29RepairId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z30RepairDateFrom", context.localUtil.DToC( Z30RepairDateFrom, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z31RepairDaysQuantity", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z31RepairDaysQuantity), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z33RepairCost", StringUtil.LTrim( StringUtil.NToC( Z33RepairCost, 8, 2, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z36RepairDiscountPercentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z36RepairDiscountPercentage), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z18GameId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z18GameId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z27TechnicianId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z27TechnicianId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z40RepairTechnicianId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z40RepairTechnicianId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_103", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_103_idx), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("repair.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Repair" ;
      }

      public override String GetPgmdesc( )
      {
         return "Repair" ;
      }

      protected void InitializeNonKey0C12( )
      {
         A37RepairFinalCost = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37RepairFinalCost", StringUtil.LTrim( StringUtil.Str( A37RepairFinalCost, 8, 2)));
         A32RepairDateTo = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32RepairDateTo", context.localUtil.Format(A32RepairDateTo, "99/99/99"));
         A30RepairDateFrom = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30RepairDateFrom", context.localUtil.Format(A30RepairDateFrom, "99/99/99"));
         A31RepairDaysQuantity = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31RepairDaysQuantity", StringUtil.LTrim( StringUtil.Str( (decimal)(A31RepairDaysQuantity), 4, 0)));
         A18GameId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18GameId", StringUtil.LTrim( StringUtil.Str( (decimal)(A18GameId), 4, 0)));
         A19GameName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19GameName", A19GameName);
         A33RepairCost = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33RepairCost", StringUtil.LTrim( StringUtil.Str( A33RepairCost, 8, 2)));
         A27TechnicianId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27TechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A27TechnicianId), 4, 0)));
         A28TechnicianName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28TechnicianName", A28TechnicianName);
         A40RepairTechnicianId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40RepairTechnicianId", StringUtil.LTrim( StringUtil.Str( (decimal)(A40RepairTechnicianId), 4, 0)));
         A41RepairTechniciaName = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41RepairTechniciaName", A41RepairTechniciaName);
         A36RepairDiscountPercentage = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36RepairDiscountPercentage", StringUtil.LTrim( StringUtil.Str( (decimal)(A36RepairDiscountPercentage), 4, 0)));
         Z30RepairDateFrom = DateTime.MinValue;
         Z31RepairDaysQuantity = 0;
         Z33RepairCost = 0;
         Z36RepairDiscountPercentage = 0;
         Z18GameId = 0;
         Z27TechnicianId = 0;
         Z40RepairTechnicianId = 0;
      }

      protected void InitAll0C12( )
      {
         A29RepairId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A29RepairId", StringUtil.LTrim( StringUtil.Str( (decimal)(A29RepairId), 4, 0)));
         InitializeNonKey0C12( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey0C13( )
      {
         A43RepairKindName = "";
         A44RepairKindRemaks = "";
         Z43RepairKindName = "";
         Z44RepairKindRemaks = "";
      }

      protected void InitAll0C13( )
      {
         A42RepairKindId = 0;
         InitializeNonKey0C13( ) ;
      }

      protected void StandaloneModalInsert0C13( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2022531182489", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("repair.js", "?2022531182490", false, true);
         /* End function include_jscripts */
      }

      protected void init_level_properties13( )
      {
         edtRepairKindId_Enabled = defedtRepairKindId_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRepairKindId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRepairKindId_Enabled), 5, 0)), !bGXsfl_103_Refreshing);
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtRepairId_Internalname = "REPAIRID";
         edtRepairDateFrom_Internalname = "REPAIRDATEFROM";
         edtRepairDaysQuantity_Internalname = "REPAIRDAYSQUANTITY";
         edtRepairDateTo_Internalname = "REPAIRDATETO";
         edtGameId_Internalname = "GAMEID";
         edtGameName_Internalname = "GAMENAME";
         edtRepairCost_Internalname = "REPAIRCOST";
         edtTechnicianId_Internalname = "TECHNICIANID";
         edtTechnicianName_Internalname = "TECHNICIANNAME";
         edtRepairTechnicianId_Internalname = "REPAIRTECHNICIANID";
         edtRepairTechniciaName_Internalname = "REPAIRTECHNICIANAME";
         edtRepairDiscountPercentage_Internalname = "REPAIRDISCOUNTPERCENTAGE";
         edtRepairFinalCost_Internalname = "REPAIRFINALCOST";
         lblTitlekind_Internalname = "TITLEKIND";
         edtRepairKindId_Internalname = "REPAIRKINDID";
         cmbRepairKindName_Internalname = "REPAIRKINDNAME";
         edtRepairKindRemaks_Internalname = "REPAIRKINDREMAKS";
         divKindtable_Internalname = "KINDTABLE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_18_Internalname = "PROMPT_18";
         imgprompt_27_Internalname = "PROMPT_27";
         imgprompt_40_Internalname = "PROMPT_40";
         subGridrepair_kind_Internalname = "GRIDREPAIR_KIND";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Repair";
         edtRepairKindRemaks_Jsonclick = "";
         cmbRepairKindName_Jsonclick = "";
         edtRepairKindId_Jsonclick = "";
         subGridrepair_kind_Class = "Grid";
         subGridrepair_kind_Backcolorstyle = 0;
         subGridrepair_kind_Allowcollapsing = 0;
         subGridrepair_kind_Allowselection = 0;
         edtRepairKindRemaks_Enabled = 1;
         cmbRepairKindName.Enabled = 1;
         edtRepairKindId_Enabled = 1;
         subGridrepair_kind_Header = "";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtRepairFinalCost_Jsonclick = "";
         edtRepairFinalCost_Enabled = 0;
         edtRepairDiscountPercentage_Jsonclick = "";
         edtRepairDiscountPercentage_Enabled = 1;
         edtRepairTechniciaName_Jsonclick = "";
         edtRepairTechniciaName_Enabled = 0;
         imgprompt_40_Visible = 1;
         imgprompt_40_Link = "";
         edtRepairTechnicianId_Jsonclick = "";
         edtRepairTechnicianId_Enabled = 1;
         edtTechnicianName_Jsonclick = "";
         edtTechnicianName_Enabled = 0;
         imgprompt_27_Visible = 1;
         imgprompt_27_Link = "";
         edtTechnicianId_Jsonclick = "";
         edtTechnicianId_Enabled = 1;
         edtRepairCost_Jsonclick = "";
         edtRepairCost_Enabled = 1;
         edtGameName_Jsonclick = "";
         edtGameName_Enabled = 0;
         imgprompt_18_Visible = 1;
         imgprompt_18_Link = "";
         edtGameId_Jsonclick = "";
         edtGameId_Enabled = 1;
         edtRepairDateTo_Jsonclick = "";
         edtRepairDateTo_Enabled = 0;
         edtRepairDaysQuantity_Jsonclick = "";
         edtRepairDaysQuantity_Enabled = 1;
         edtRepairDateFrom_Jsonclick = "";
         edtRepairDateFrom_Enabled = 1;
         edtRepairId_Jsonclick = "";
         edtRepairId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridrepair_kind_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_10313( ) ;
         while ( nGXsfl_103_idx <= nRC_GXsfl_103 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal0C13( ) ;
            standaloneModal0C13( ) ;
            init_web_controls( ) ;
            dynload_actions( ) ;
            SendRow0C13( ) ;
            nGXsfl_103_idx = (int)(nGXsfl_103_idx+1);
            sGXsfl_103_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_103_idx), 4, 0)), 4, "0");
            SubsflControlProps_10313( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Gridrepair_kindContainer));
         /* End function gxnrGridrepair_kind_newrow */
      }

      protected void init_web_controls( )
      {
         GXCCtl = "REPAIRKINDNAME_" + sGXsfl_103_idx;
         cmbRepairKindName.Name = GXCCtl;
         cmbRepairKindName.WebTags = "";
         cmbRepairKindName.addItem("E", "Electricity", 0);
         cmbRepairKindName.addItem("M", "Mechanical", 0);
         cmbRepairKindName.addItem("R", "Replacement", 0);
         if ( cmbRepairKindName.ItemCount > 0 )
         {
            A43RepairKindName = cmbRepairKindName.getValidValue(A43RepairKindName);
         }
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtRepairDateFrom_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Repairid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A30RepairDateFrom", context.localUtil.Format(A30RepairDateFrom, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A31RepairDaysQuantity", StringUtil.LTrim( StringUtil.NToC( (decimal)(A31RepairDaysQuantity), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18GameId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A18GameId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A33RepairCost", StringUtil.LTrim( StringUtil.NToC( A33RepairCost, 8, 2, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A27TechnicianId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A27TechnicianId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40RepairTechnicianId", StringUtil.LTrim( StringUtil.NToC( (decimal)(A40RepairTechnicianId), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36RepairDiscountPercentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(A36RepairDiscountPercentage), 4, 0, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A32RepairDateTo", context.localUtil.Format(A32RepairDateTo, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19GameName", StringUtil.RTrim( A19GameName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37RepairFinalCost", StringUtil.LTrim( StringUtil.NToC( A37RepairFinalCost, 8, 2, ".", "")));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28TechnicianName", StringUtil.RTrim( A28TechnicianName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41RepairTechniciaName", StringUtil.RTrim( A41RepairTechniciaName));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z29RepairId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z29RepairId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z30RepairDateFrom", context.localUtil.Format(Z30RepairDateFrom, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "Z31RepairDaysQuantity", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z31RepairDaysQuantity), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z18GameId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z18GameId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z33RepairCost", StringUtil.LTrim( StringUtil.NToC( Z33RepairCost, 8, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z27TechnicianId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z27TechnicianId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z40RepairTechnicianId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z40RepairTechnicianId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z36RepairDiscountPercentage", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z36RepairDiscountPercentage), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z32RepairDateTo", context.localUtil.Format(Z32RepairDateTo, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "Z19GameName", StringUtil.RTrim( Z19GameName));
         GxWebStd.gx_hidden_field( context, "Z37RepairFinalCost", StringUtil.LTrim( StringUtil.NToC( Z37RepairFinalCost, 8, 2, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z28TechnicianName", StringUtil.RTrim( Z28TechnicianName));
         GxWebStd.gx_hidden_field( context, "Z41RepairTechniciaName", StringUtil.RTrim( Z41RepairTechniciaName));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Gameid( )
      {
         /* Using cursor T000C19 */
         pr_default.execute(17, new Object[] {A18GameId});
         if ( (pr_default.getStatus(17) == 101) )
         {
            GX_msglist.addItem("No existe 'Game'.", "ForeignKeyNotFound", 1, "GAMEID");
            AnyError = 1;
            GX_FocusControl = edtGameId_Internalname;
         }
         A19GameName = T000C19_A19GameName[0];
         pr_default.close(17);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19GameName", StringUtil.RTrim( A19GameName));
      }

      public void Valid_Technicianid( )
      {
         /* Using cursor T000C20 */
         pr_default.execute(18, new Object[] {A27TechnicianId});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("No existe 'Technican'.", "ForeignKeyNotFound", 1, "TECHNICIANID");
            AnyError = 1;
            GX_FocusControl = edtTechnicianId_Internalname;
         }
         A28TechnicianName = T000C20_A28TechnicianName[0];
         pr_default.close(18);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A28TechnicianName", StringUtil.RTrim( A28TechnicianName));
      }

      public void Valid_Repairtechnicianid( )
      {
         /* Using cursor T000C21 */
         pr_default.execute(19, new Object[] {A40RepairTechnicianId});
         if ( (pr_default.getStatus(19) == 101) )
         {
            GX_msglist.addItem("No existe 'Repair Technician Alternative'.", "ForeignKeyNotFound", 1, "REPAIRTECHNICIANID");
            AnyError = 1;
            GX_FocusControl = edtRepairTechnicianId_Internalname;
         }
         A41RepairTechniciaName = T000C21_A41RepairTechniciaName[0];
         pr_default.close(19);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41RepairTechniciaName", StringUtil.RTrim( A41RepairTechniciaName));
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_REPAIRID","{handler:'Valid_Repairid',iparms:[{av:'A29RepairId',fld:'REPAIRID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_REPAIRID",",oparms:[{av:'A30RepairDateFrom',fld:'REPAIRDATEFROM',pic:''},{av:'A31RepairDaysQuantity',fld:'REPAIRDAYSQUANTITY',pic:'ZZZ9'},{av:'A18GameId',fld:'GAMEID',pic:'ZZZ9'},{av:'A33RepairCost',fld:'REPAIRCOST',pic:'ZZZZ9.99'},{av:'A27TechnicianId',fld:'TECHNICIANID',pic:'ZZZ9'},{av:'A40RepairTechnicianId',fld:'REPAIRTECHNICIANID',pic:'ZZZ9'},{av:'A36RepairDiscountPercentage',fld:'REPAIRDISCOUNTPERCENTAGE',pic:'ZZZ9'},{av:'A32RepairDateTo',fld:'REPAIRDATETO',pic:''},{av:'A19GameName',fld:'GAMENAME',pic:''},{av:'A37RepairFinalCost',fld:'REPAIRFINALCOST',pic:'ZZZZ9.99'},{av:'A28TechnicianName',fld:'TECHNICIANNAME',pic:''},{av:'A41RepairTechniciaName',fld:'REPAIRTECHNICIANAME',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z29RepairId'},{av:'Z30RepairDateFrom'},{av:'Z31RepairDaysQuantity'},{av:'Z18GameId'},{av:'Z33RepairCost'},{av:'Z27TechnicianId'},{av:'Z40RepairTechnicianId'},{av:'Z36RepairDiscountPercentage'},{av:'Z32RepairDateTo'},{av:'Z19GameName'},{av:'Z37RepairFinalCost'},{av:'Z28TechnicianName'},{av:'Z41RepairTechniciaName'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_REPAIRDATEFROM","{handler:'Valid_Repairdatefrom',iparms:[]");
         setEventMetadata("VALID_REPAIRDATEFROM",",oparms:[]}");
         setEventMetadata("VALID_REPAIRDAYSQUANTITY","{handler:'Valid_Repairdaysquantity',iparms:[]");
         setEventMetadata("VALID_REPAIRDAYSQUANTITY",",oparms:[]}");
         setEventMetadata("VALID_GAMEID","{handler:'Valid_Gameid',iparms:[{av:'A18GameId',fld:'GAMEID',pic:'ZZZ9'},{av:'A19GameName',fld:'GAMENAME',pic:''}]");
         setEventMetadata("VALID_GAMEID",",oparms:[{av:'A19GameName',fld:'GAMENAME',pic:''}]}");
         setEventMetadata("VALID_REPAIRCOST","{handler:'Valid_Repaircost',iparms:[]");
         setEventMetadata("VALID_REPAIRCOST",",oparms:[]}");
         setEventMetadata("VALID_TECHNICIANID","{handler:'Valid_Technicianid',iparms:[{av:'A27TechnicianId',fld:'TECHNICIANID',pic:'ZZZ9'},{av:'A28TechnicianName',fld:'TECHNICIANNAME',pic:''}]");
         setEventMetadata("VALID_TECHNICIANID",",oparms:[{av:'A28TechnicianName',fld:'TECHNICIANNAME',pic:''}]}");
         setEventMetadata("VALID_REPAIRTECHNICIANID","{handler:'Valid_Repairtechnicianid',iparms:[{av:'A40RepairTechnicianId',fld:'REPAIRTECHNICIANID',pic:'ZZZ9'},{av:'A41RepairTechniciaName',fld:'REPAIRTECHNICIANAME',pic:''}]");
         setEventMetadata("VALID_REPAIRTECHNICIANID",",oparms:[{av:'A41RepairTechniciaName',fld:'REPAIRTECHNICIANAME',pic:''}]}");
         setEventMetadata("VALID_REPAIRDISCOUNTPERCENTAGE","{handler:'Valid_Repairdiscountpercentage',iparms:[]");
         setEventMetadata("VALID_REPAIRDISCOUNTPERCENTAGE",",oparms:[]}");
         setEventMetadata("VALID_REPAIRKINDID","{handler:'Valid_Repairkindid',iparms:[]");
         setEventMetadata("VALID_REPAIRKINDID",",oparms:[]}");
         setEventMetadata("VALID_REPAIRKINDNAME","{handler:'Valid_Repairkindname',iparms:[]");
         setEventMetadata("VALID_REPAIRKINDNAME",",oparms:[]}");
         setEventMetadata("NULL","{handler:'Valid_Repairkindremaks',iparms:[]");
         setEventMetadata("NULL",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(3);
         pr_default.close(17);
         pr_default.close(18);
         pr_default.close(19);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z30RepairDateFrom = DateTime.MinValue;
         Z43RepairKindName = "";
         Z44RepairKindRemaks = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         Gx_mode = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A30RepairDateFrom = DateTime.MinValue;
         A32RepairDateTo = DateTime.MinValue;
         sImgUrl = "";
         A19GameName = "";
         A28TechnicianName = "";
         A41RepairTechniciaName = "";
         lblTitlekind_Jsonclick = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gridrepair_kindContainer = new GXWebGrid( context);
         Gridrepair_kindColumn = new GXWebColumn();
         A43RepairKindName = "";
         A44RepairKindRemaks = "";
         sMode13 = "";
         sStyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         Z19GameName = "";
         Z28TechnicianName = "";
         Z41RepairTechniciaName = "";
         T000C9_A29RepairId = new short[1] ;
         T000C9_A30RepairDateFrom = new DateTime[] {DateTime.MinValue} ;
         T000C9_A31RepairDaysQuantity = new short[1] ;
         T000C9_A19GameName = new String[] {""} ;
         T000C9_A33RepairCost = new decimal[1] ;
         T000C9_A28TechnicianName = new String[] {""} ;
         T000C9_A41RepairTechniciaName = new String[] {""} ;
         T000C9_A36RepairDiscountPercentage = new short[1] ;
         T000C9_A18GameId = new short[1] ;
         T000C9_A27TechnicianId = new short[1] ;
         T000C9_A40RepairTechnicianId = new short[1] ;
         T000C6_A19GameName = new String[] {""} ;
         T000C7_A28TechnicianName = new String[] {""} ;
         T000C8_A41RepairTechniciaName = new String[] {""} ;
         T000C10_A19GameName = new String[] {""} ;
         T000C11_A28TechnicianName = new String[] {""} ;
         T000C12_A41RepairTechniciaName = new String[] {""} ;
         T000C13_A29RepairId = new short[1] ;
         T000C5_A29RepairId = new short[1] ;
         T000C5_A30RepairDateFrom = new DateTime[] {DateTime.MinValue} ;
         T000C5_A31RepairDaysQuantity = new short[1] ;
         T000C5_A33RepairCost = new decimal[1] ;
         T000C5_A36RepairDiscountPercentage = new short[1] ;
         T000C5_A18GameId = new short[1] ;
         T000C5_A27TechnicianId = new short[1] ;
         T000C5_A40RepairTechnicianId = new short[1] ;
         sMode12 = "";
         T000C14_A29RepairId = new short[1] ;
         T000C15_A29RepairId = new short[1] ;
         T000C4_A29RepairId = new short[1] ;
         T000C4_A30RepairDateFrom = new DateTime[] {DateTime.MinValue} ;
         T000C4_A31RepairDaysQuantity = new short[1] ;
         T000C4_A33RepairCost = new decimal[1] ;
         T000C4_A36RepairDiscountPercentage = new short[1] ;
         T000C4_A18GameId = new short[1] ;
         T000C4_A27TechnicianId = new short[1] ;
         T000C4_A40RepairTechnicianId = new short[1] ;
         T000C16_A29RepairId = new short[1] ;
         T000C19_A19GameName = new String[] {""} ;
         T000C20_A28TechnicianName = new String[] {""} ;
         T000C21_A41RepairTechniciaName = new String[] {""} ;
         T000C22_A29RepairId = new short[1] ;
         T000C23_A29RepairId = new short[1] ;
         T000C23_A42RepairKindId = new short[1] ;
         T000C23_A43RepairKindName = new String[] {""} ;
         T000C23_A44RepairKindRemaks = new String[] {""} ;
         T000C24_A29RepairId = new short[1] ;
         T000C24_A42RepairKindId = new short[1] ;
         T000C3_A29RepairId = new short[1] ;
         T000C3_A42RepairKindId = new short[1] ;
         T000C3_A43RepairKindName = new String[] {""} ;
         T000C3_A44RepairKindRemaks = new String[] {""} ;
         T000C2_A29RepairId = new short[1] ;
         T000C2_A42RepairKindId = new short[1] ;
         T000C2_A43RepairKindName = new String[] {""} ;
         T000C2_A44RepairKindRemaks = new String[] {""} ;
         T000C28_A29RepairId = new short[1] ;
         T000C28_A42RepairKindId = new short[1] ;
         Gridrepair_kindRow = new GXWebRow();
         subGridrepair_kind_Linesclass = "";
         ROClassString = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         Z32RepairDateTo = DateTime.MinValue;
         ZZ30RepairDateFrom = DateTime.MinValue;
         ZZ32RepairDateTo = DateTime.MinValue;
         ZZ19GameName = "";
         ZZ28TechnicianName = "";
         ZZ41RepairTechniciaName = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.repair__default(),
            new Object[][] {
                new Object[] {
               T000C2_A29RepairId, T000C2_A42RepairKindId, T000C2_A43RepairKindName, T000C2_A44RepairKindRemaks
               }
               , new Object[] {
               T000C3_A29RepairId, T000C3_A42RepairKindId, T000C3_A43RepairKindName, T000C3_A44RepairKindRemaks
               }
               , new Object[] {
               T000C4_A29RepairId, T000C4_A30RepairDateFrom, T000C4_A31RepairDaysQuantity, T000C4_A33RepairCost, T000C4_A36RepairDiscountPercentage, T000C4_A18GameId, T000C4_A27TechnicianId, T000C4_A40RepairTechnicianId
               }
               , new Object[] {
               T000C5_A29RepairId, T000C5_A30RepairDateFrom, T000C5_A31RepairDaysQuantity, T000C5_A33RepairCost, T000C5_A36RepairDiscountPercentage, T000C5_A18GameId, T000C5_A27TechnicianId, T000C5_A40RepairTechnicianId
               }
               , new Object[] {
               T000C6_A19GameName
               }
               , new Object[] {
               T000C7_A28TechnicianName
               }
               , new Object[] {
               T000C8_A41RepairTechniciaName
               }
               , new Object[] {
               T000C9_A29RepairId, T000C9_A30RepairDateFrom, T000C9_A31RepairDaysQuantity, T000C9_A19GameName, T000C9_A33RepairCost, T000C9_A28TechnicianName, T000C9_A41RepairTechniciaName, T000C9_A36RepairDiscountPercentage, T000C9_A18GameId, T000C9_A27TechnicianId,
               T000C9_A40RepairTechnicianId
               }
               , new Object[] {
               T000C10_A19GameName
               }
               , new Object[] {
               T000C11_A28TechnicianName
               }
               , new Object[] {
               T000C12_A41RepairTechniciaName
               }
               , new Object[] {
               T000C13_A29RepairId
               }
               , new Object[] {
               T000C14_A29RepairId
               }
               , new Object[] {
               T000C15_A29RepairId
               }
               , new Object[] {
               T000C16_A29RepairId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000C19_A19GameName
               }
               , new Object[] {
               T000C20_A28TechnicianName
               }
               , new Object[] {
               T000C21_A41RepairTechniciaName
               }
               , new Object[] {
               T000C22_A29RepairId
               }
               , new Object[] {
               T000C23_A29RepairId, T000C23_A42RepairKindId, T000C23_A43RepairKindName, T000C23_A44RepairKindRemaks
               }
               , new Object[] {
               T000C24_A29RepairId, T000C24_A42RepairKindId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000C28_A29RepairId, T000C28_A42RepairKindId
               }
            }
         );
      }

      private short Z29RepairId ;
      private short Z31RepairDaysQuantity ;
      private short Z36RepairDiscountPercentage ;
      private short Z18GameId ;
      private short Z27TechnicianId ;
      private short Z40RepairTechnicianId ;
      private short Z42RepairKindId ;
      private short nRcdDeleted_13 ;
      private short nRcdExists_13 ;
      private short nIsMod_13 ;
      private short GxWebError ;
      private short A18GameId ;
      private short A27TechnicianId ;
      private short A40RepairTechnicianId ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A29RepairId ;
      private short A31RepairDaysQuantity ;
      private short A36RepairDiscountPercentage ;
      private short subGridrepair_kind_Backcolorstyle ;
      private short A42RepairKindId ;
      private short subGridrepair_kind_Allowselection ;
      private short subGridrepair_kind_Allowhovering ;
      private short subGridrepair_kind_Allowcollapsing ;
      private short subGridrepair_kind_Collapsed ;
      private short nBlankRcdCount13 ;
      private short RcdFound13 ;
      private short nBlankRcdUsr13 ;
      private short GX_JID ;
      private short RcdFound12 ;
      private short nIsDirty_12 ;
      private short Gx_BScreen ;
      private short nIsDirty_13 ;
      private short subGridrepair_kind_Backstyle ;
      private short gxajaxcallmode ;
      private short ZZ29RepairId ;
      private short ZZ31RepairDaysQuantity ;
      private short ZZ18GameId ;
      private short ZZ27TechnicianId ;
      private short ZZ40RepairTechnicianId ;
      private short ZZ36RepairDiscountPercentage ;
      private int nRC_GXsfl_103 ;
      private int nGXsfl_103_idx=1 ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtRepairId_Enabled ;
      private int edtRepairDateFrom_Enabled ;
      private int edtRepairDaysQuantity_Enabled ;
      private int edtRepairDateTo_Enabled ;
      private int edtGameId_Enabled ;
      private int imgprompt_18_Visible ;
      private int edtGameName_Enabled ;
      private int edtRepairCost_Enabled ;
      private int edtTechnicianId_Enabled ;
      private int imgprompt_27_Visible ;
      private int edtTechnicianName_Enabled ;
      private int edtRepairTechnicianId_Enabled ;
      private int imgprompt_40_Visible ;
      private int edtRepairTechniciaName_Enabled ;
      private int edtRepairDiscountPercentage_Enabled ;
      private int edtRepairFinalCost_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int edtRepairKindId_Enabled ;
      private int edtRepairKindRemaks_Enabled ;
      private int subGridrepair_kind_Selectedindex ;
      private int subGridrepair_kind_Selectioncolor ;
      private int subGridrepair_kind_Hoveringcolor ;
      private int fRowAdded ;
      private int subGridrepair_kind_Backcolor ;
      private int subGridrepair_kind_Allbackcolor ;
      private int defedtRepairKindId_Enabled ;
      private int idxLst ;
      private long GRIDREPAIR_KIND_nFirstRecordOnPage ;
      private decimal Z33RepairCost ;
      private decimal A33RepairCost ;
      private decimal A37RepairFinalCost ;
      private decimal Z37RepairFinalCost ;
      private decimal ZZ33RepairCost ;
      private decimal ZZ37RepairFinalCost ;
      private String sPrefix ;
      private String Z43RepairKindName ;
      private String Z44RepairKindRemaks ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_103_idx="0001" ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtRepairId_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtRepairId_Jsonclick ;
      private String edtRepairDateFrom_Internalname ;
      private String edtRepairDateFrom_Jsonclick ;
      private String edtRepairDaysQuantity_Internalname ;
      private String edtRepairDaysQuantity_Jsonclick ;
      private String edtRepairDateTo_Internalname ;
      private String edtRepairDateTo_Jsonclick ;
      private String edtGameId_Internalname ;
      private String edtGameId_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_18_Internalname ;
      private String imgprompt_18_Link ;
      private String edtGameName_Internalname ;
      private String A19GameName ;
      private String edtGameName_Jsonclick ;
      private String edtRepairCost_Internalname ;
      private String edtRepairCost_Jsonclick ;
      private String edtTechnicianId_Internalname ;
      private String edtTechnicianId_Jsonclick ;
      private String imgprompt_27_Internalname ;
      private String imgprompt_27_Link ;
      private String edtTechnicianName_Internalname ;
      private String A28TechnicianName ;
      private String edtTechnicianName_Jsonclick ;
      private String edtRepairTechnicianId_Internalname ;
      private String edtRepairTechnicianId_Jsonclick ;
      private String imgprompt_40_Internalname ;
      private String imgprompt_40_Link ;
      private String edtRepairTechniciaName_Internalname ;
      private String A41RepairTechniciaName ;
      private String edtRepairTechniciaName_Jsonclick ;
      private String edtRepairDiscountPercentage_Internalname ;
      private String edtRepairDiscountPercentage_Jsonclick ;
      private String edtRepairFinalCost_Internalname ;
      private String edtRepairFinalCost_Jsonclick ;
      private String divKindtable_Internalname ;
      private String lblTitlekind_Internalname ;
      private String lblTitlekind_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String subGridrepair_kind_Header ;
      private String A43RepairKindName ;
      private String A44RepairKindRemaks ;
      private String sMode13 ;
      private String edtRepairKindId_Internalname ;
      private String cmbRepairKindName_Internalname ;
      private String edtRepairKindRemaks_Internalname ;
      private String sStyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String Z19GameName ;
      private String Z28TechnicianName ;
      private String Z41RepairTechniciaName ;
      private String sMode12 ;
      private String sGXsfl_103_fel_idx="0001" ;
      private String subGridrepair_kind_Class ;
      private String subGridrepair_kind_Linesclass ;
      private String ROClassString ;
      private String edtRepairKindId_Jsonclick ;
      private String cmbRepairKindName_Jsonclick ;
      private String edtRepairKindRemaks_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridrepair_kind_Internalname ;
      private String ZZ19GameName ;
      private String ZZ28TechnicianName ;
      private String ZZ41RepairTechniciaName ;
      private DateTime Z30RepairDateFrom ;
      private DateTime A30RepairDateFrom ;
      private DateTime A32RepairDateTo ;
      private DateTime Z32RepairDateTo ;
      private DateTime ZZ30RepairDateFrom ;
      private DateTime ZZ32RepairDateTo ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool bGXsfl_103_Refreshing=false ;
      private bool Gx_longc ;
      private GXWebGrid Gridrepair_kindContainer ;
      private GXWebRow Gridrepair_kindRow ;
      private GXWebColumn Gridrepair_kindColumn ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbRepairKindName ;
      private IDataStoreProvider pr_default ;
      private short[] T000C9_A29RepairId ;
      private DateTime[] T000C9_A30RepairDateFrom ;
      private short[] T000C9_A31RepairDaysQuantity ;
      private String[] T000C9_A19GameName ;
      private decimal[] T000C9_A33RepairCost ;
      private String[] T000C9_A28TechnicianName ;
      private String[] T000C9_A41RepairTechniciaName ;
      private short[] T000C9_A36RepairDiscountPercentage ;
      private short[] T000C9_A18GameId ;
      private short[] T000C9_A27TechnicianId ;
      private short[] T000C9_A40RepairTechnicianId ;
      private String[] T000C6_A19GameName ;
      private String[] T000C7_A28TechnicianName ;
      private String[] T000C8_A41RepairTechniciaName ;
      private String[] T000C10_A19GameName ;
      private String[] T000C11_A28TechnicianName ;
      private String[] T000C12_A41RepairTechniciaName ;
      private short[] T000C13_A29RepairId ;
      private short[] T000C5_A29RepairId ;
      private DateTime[] T000C5_A30RepairDateFrom ;
      private short[] T000C5_A31RepairDaysQuantity ;
      private decimal[] T000C5_A33RepairCost ;
      private short[] T000C5_A36RepairDiscountPercentage ;
      private short[] T000C5_A18GameId ;
      private short[] T000C5_A27TechnicianId ;
      private short[] T000C5_A40RepairTechnicianId ;
      private short[] T000C14_A29RepairId ;
      private short[] T000C15_A29RepairId ;
      private short[] T000C4_A29RepairId ;
      private DateTime[] T000C4_A30RepairDateFrom ;
      private short[] T000C4_A31RepairDaysQuantity ;
      private decimal[] T000C4_A33RepairCost ;
      private short[] T000C4_A36RepairDiscountPercentage ;
      private short[] T000C4_A18GameId ;
      private short[] T000C4_A27TechnicianId ;
      private short[] T000C4_A40RepairTechnicianId ;
      private short[] T000C16_A29RepairId ;
      private String[] T000C19_A19GameName ;
      private String[] T000C20_A28TechnicianName ;
      private String[] T000C21_A41RepairTechniciaName ;
      private short[] T000C22_A29RepairId ;
      private short[] T000C23_A29RepairId ;
      private short[] T000C23_A42RepairKindId ;
      private String[] T000C23_A43RepairKindName ;
      private String[] T000C23_A44RepairKindRemaks ;
      private short[] T000C24_A29RepairId ;
      private short[] T000C24_A42RepairKindId ;
      private short[] T000C3_A29RepairId ;
      private short[] T000C3_A42RepairKindId ;
      private String[] T000C3_A43RepairKindName ;
      private String[] T000C3_A44RepairKindRemaks ;
      private short[] T000C2_A29RepairId ;
      private short[] T000C2_A42RepairKindId ;
      private String[] T000C2_A43RepairKindName ;
      private String[] T000C2_A44RepairKindRemaks ;
      private short[] T000C28_A29RepairId ;
      private short[] T000C28_A42RepairKindId ;
      private GXWebForm Form ;
   }

   public class repair__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new ForEachCursor(def[10])
         ,new ForEachCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new UpdateCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
         ,new ForEachCursor(def[20])
         ,new ForEachCursor(def[21])
         ,new ForEachCursor(def[22])
         ,new UpdateCursor(def[23])
         ,new UpdateCursor(def[24])
         ,new UpdateCursor(def[25])
         ,new ForEachCursor(def[26])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000C9 ;
          prmT000C9 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C6 ;
          prmT000C6 = new Object[] {
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C7 ;
          prmT000C7 = new Object[] {
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C8 ;
          prmT000C8 = new Object[] {
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C10 ;
          prmT000C10 = new Object[] {
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C11 ;
          prmT000C11 = new Object[] {
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C12 ;
          prmT000C12 = new Object[] {
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C13 ;
          prmT000C13 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C5 ;
          prmT000C5 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C14 ;
          prmT000C14 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C15 ;
          prmT000C15 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C4 ;
          prmT000C4 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C16 ;
          prmT000C16 = new Object[] {
          new Object[] {"@RepairDateFrom",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RepairDaysQuantity",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairCost",SqlDbType.Decimal,8,2} ,
          new Object[] {"@RepairDiscountPercentage",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C17 ;
          prmT000C17 = new Object[] {
          new Object[] {"@RepairDateFrom",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RepairDaysQuantity",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairCost",SqlDbType.Decimal,8,2} ,
          new Object[] {"@RepairDiscountPercentage",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C18 ;
          prmT000C18 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C22 ;
          prmT000C22 = new Object[] {
          } ;
          Object[] prmT000C23 ;
          prmT000C23 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmT000C24 ;
          prmT000C24 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmT000C3 ;
          prmT000C3 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmT000C2 ;
          prmT000C2 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmT000C25 ;
          prmT000C25 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@RepairKindName",SqlDbType.NChar,1,0} ,
          new Object[] {"@RepairKindRemaks",SqlDbType.NChar,120,0}
          } ;
          Object[] prmT000C26 ;
          prmT000C26 = new Object[] {
          new Object[] {"@RepairKindName",SqlDbType.NChar,1,0} ,
          new Object[] {"@RepairKindRemaks",SqlDbType.NChar,120,0} ,
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmT000C27 ;
          prmT000C27 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmT000C28 ;
          prmT000C28 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C19 ;
          prmT000C19 = new Object[] {
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C20 ;
          prmT000C20 = new Object[] {
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000C21 ;
          prmT000C21 = new Object[] {
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000C2", "SELECT [RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks] FROM [RepairKind] WITH (UPDLOCK) WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C2,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C3", "SELECT [RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks] FROM [RepairKind] WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C3,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C4", "SELECT [RepairId], [RepairDateFrom], [RepairDaysQuantity], [RepairCost], [RepairDiscountPercentage], [GameId], [TechnicianId], [RepairTechnicianId] AS RepairTechnicianId FROM [Repair] WITH (UPDLOCK) WHERE [RepairId] = @RepairId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C4,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C5", "SELECT [RepairId], [RepairDateFrom], [RepairDaysQuantity], [RepairCost], [RepairDiscountPercentage], [GameId], [TechnicianId], [RepairTechnicianId] AS RepairTechnicianId FROM [Repair] WHERE [RepairId] = @RepairId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C5,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C6", "SELECT [GameName] FROM [Game] WHERE [GameId] = @GameId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C6,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C7", "SELECT [TechnicianName] FROM [Technican] WHERE [TechnicianId] = @TechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C7,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C8", "SELECT [TechnicianName] AS RepairTechniciaName FROM [Technican] WHERE [TechnicianId] = @RepairTechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C8,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C9", "SELECT TM1.[RepairId], TM1.[RepairDateFrom], TM1.[RepairDaysQuantity], T2.[GameName], TM1.[RepairCost], T3.[TechnicianName], T4.[TechnicianName] AS RepairTechniciaName, TM1.[RepairDiscountPercentage], TM1.[GameId], TM1.[TechnicianId], TM1.[RepairTechnicianId] AS RepairTechnicianId FROM ((([Repair] TM1 INNER JOIN [Game] T2 ON T2.[GameId] = TM1.[GameId]) INNER JOIN [Technican] T3 ON T3.[TechnicianId] = TM1.[TechnicianId]) INNER JOIN [Technican] T4 ON T4.[TechnicianId] = TM1.[RepairTechnicianId]) WHERE TM1.[RepairId] = @RepairId ORDER BY TM1.[RepairId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C9,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C10", "SELECT [GameName] FROM [Game] WHERE [GameId] = @GameId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C10,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C11", "SELECT [TechnicianName] FROM [Technican] WHERE [TechnicianId] = @TechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C11,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C12", "SELECT [TechnicianName] AS RepairTechniciaName FROM [Technican] WHERE [TechnicianId] = @RepairTechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C12,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C13", "SELECT [RepairId] FROM [Repair] WHERE [RepairId] = @RepairId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C13,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C14", "SELECT TOP 1 [RepairId] FROM [Repair] WHERE ( [RepairId] > @RepairId) ORDER BY [RepairId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C14,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000C15", "SELECT TOP 1 [RepairId] FROM [Repair] WHERE ( [RepairId] < @RepairId) ORDER BY [RepairId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C15,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000C16", "INSERT INTO [Repair]([RepairDateFrom], [RepairDaysQuantity], [RepairCost], [RepairDiscountPercentage], [GameId], [TechnicianId], [RepairTechnicianId]) VALUES(@RepairDateFrom, @RepairDaysQuantity, @RepairCost, @RepairDiscountPercentage, @GameId, @TechnicianId, @RepairTechnicianId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000C16)
             ,new CursorDef("T000C17", "UPDATE [Repair] SET [RepairDateFrom]=@RepairDateFrom, [RepairDaysQuantity]=@RepairDaysQuantity, [RepairCost]=@RepairCost, [RepairDiscountPercentage]=@RepairDiscountPercentage, [GameId]=@GameId, [TechnicianId]=@TechnicianId, [RepairTechnicianId]=@RepairTechnicianId  WHERE [RepairId] = @RepairId", GxErrorMask.GX_NOMASK,prmT000C17)
             ,new CursorDef("T000C18", "DELETE FROM [Repair]  WHERE [RepairId] = @RepairId", GxErrorMask.GX_NOMASK,prmT000C18)
             ,new CursorDef("T000C19", "SELECT [GameName] FROM [Game] WHERE [GameId] = @GameId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C19,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C20", "SELECT [TechnicianName] FROM [Technican] WHERE [TechnicianId] = @TechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C20,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C21", "SELECT [TechnicianName] AS RepairTechniciaName FROM [Technican] WHERE [TechnicianId] = @RepairTechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C21,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C22", "SELECT [RepairId] FROM [Repair] ORDER BY [RepairId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000C22,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C23", "SELECT [RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks] FROM [RepairKind] WHERE [RepairId] = @RepairId and [RepairKindId] = @RepairKindId ORDER BY [RepairId], [RepairKindId] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C23,11, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C24", "SELECT [RepairId], [RepairKindId] FROM [RepairKind] WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C24,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000C25", "INSERT INTO [RepairKind]([RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks]) VALUES(@RepairId, @RepairKindId, @RepairKindName, @RepairKindRemaks)", GxErrorMask.GX_NOMASK,prmT000C25)
             ,new CursorDef("T000C26", "UPDATE [RepairKind] SET [RepairKindName]=@RepairKindName, [RepairKindRemaks]=@RepairKindRemaks  WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId", GxErrorMask.GX_NOMASK,prmT000C26)
             ,new CursorDef("T000C27", "DELETE FROM [RepairKind]  WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId", GxErrorMask.GX_NOMASK,prmT000C27)
             ,new CursorDef("T000C28", "SELECT [RepairId], [RepairKindId] FROM [RepairKind] WHERE [RepairId] = @RepairId ORDER BY [RepairId], [RepairKindId] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000C28,11, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 120) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 120) ;
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((short[]) buf[8])[0] = rslt.getShort(9) ;
                ((short[]) buf[9])[0] = rslt.getShort(10) ;
                ((short[]) buf[10])[0] = rslt.getShort(11) ;
                return;
             case 8 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 9 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 10 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 11 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 14 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 17 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 19 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 20 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 21 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 120) ;
                return;
             case 22 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 26 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 10 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                return;
             case 15 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                stmt.SetParameter(8, (short)parms[7]);
                return;
             case 16 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 17 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 18 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 21 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 22 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 23 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 24 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 25 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 26 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
