/*
               File: Repair_DataProvider
        Description: Repair_Data Provider
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/14/2022 2:45:27.55
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class arepair_dataprovider : GXProcedure
   {
      public arepair_dataprovider( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public arepair_dataprovider( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out GXBCCollection<SdtRepair> aP0_Gxm2rootcol )
      {
         this.Gxm2rootcol = new GXBCCollection<SdtRepair>( context, "Repair", "Atracciones_Turistica") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      public GXBCCollection<SdtRepair> executeUdp( )
      {
         this.Gxm2rootcol = new GXBCCollection<SdtRepair>( context, "Repair", "Atracciones_Turistica") ;
         initialize();
         executePrivate();
         aP0_Gxm2rootcol=this.Gxm2rootcol;
         return Gxm2rootcol ;
      }

      public void executeSubmit( out GXBCCollection<SdtRepair> aP0_Gxm2rootcol )
      {
         arepair_dataprovider objarepair_dataprovider;
         objarepair_dataprovider = new arepair_dataprovider();
         objarepair_dataprovider.Gxm2rootcol = new GXBCCollection<SdtRepair>( context, "Repair", "Atracciones_Turistica") ;
         objarepair_dataprovider.context.SetSubmitInitialConfig(context);
         objarepair_dataprovider.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objarepair_dataprovider);
         aP0_Gxm2rootcol=this.Gxm2rootcol;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((arepair_dataprovider)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         Gxm1repair = new SdtRepair(context);
         Gxm2rootcol.Add(Gxm1repair, 0);
         Gxm1repair.gxTpr_Repairdatefrom = context.localUtil.CToD( "31/05/2022", 2);
         Gxm1repair.gxTpr_Repairdaysquantity = 2;
         Gxm1repair.gxTpr_Gameid = 1;
         Gxm1repair.gxTpr_Repaircost = 100.00m;
         Gxm1repair.gxTpr_Technicianid = 1;
         Gxm1repair.gxTpr_Repairtechnicianid = 2;
         Gxm1repair.gxTpr_Repairdiscountpercentage = 15;
         Gxm3repair_kind = new SdtRepair_Kind(context);
         Gxm1repair.gxTpr_Kind.Add(Gxm3repair_kind, 0);
         Gxm3repair_kind.gxTpr_Repairkindid = 1;
         Gxm3repair_kind.gxTpr_Repairkindremaks = "Se cambio un cable";
         Gxm3repair_kind = new SdtRepair_Kind(context);
         Gxm1repair.gxTpr_Kind.Add(Gxm3repair_kind, 0);
         Gxm3repair_kind.gxTpr_Repairkindid = 2;
         Gxm3repair_kind.gxTpr_Repairkindremaks = "Remplazo de tornillos";
         Gxm1repair = new SdtRepair(context);
         Gxm2rootcol.Add(Gxm1repair, 0);
         Gxm1repair.gxTpr_Repairdatefrom = context.localUtil.CToD( "06/01/2022", 2);
         Gxm1repair.gxTpr_Repairdaysquantity = 1;
         Gxm1repair.gxTpr_Gameid = 1;
         Gxm1repair.gxTpr_Repaircost = 500.00m;
         Gxm1repair.gxTpr_Technicianid = 1;
         Gxm1repair.gxTpr_Repairtechnicianid = 2;
         Gxm1repair.gxTpr_Repairdiscountpercentage = 8;
         Gxm3repair_kind = new SdtRepair_Kind(context);
         Gxm1repair.gxTpr_Kind.Add(Gxm3repair_kind, 0);
         Gxm3repair_kind.gxTpr_Repairkindid = 1;
         Gxm3repair_kind.gxTpr_Repairkindremaks = "Se cambio la placa";
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Gxm1repair = new SdtRepair(context);
         Gxm3repair_kind = new SdtRepair_Kind(context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private GXBCCollection<SdtRepair> aP0_Gxm2rootcol ;
      private GXBCCollection<SdtRepair> Gxm2rootcol ;
      private SdtRepair Gxm1repair ;
      private SdtRepair_Kind Gxm3repair_kind ;
   }

}
