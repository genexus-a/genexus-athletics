/*
               File: Shows_per_Park_List
        Description: Stub for Shows_per_Park_List
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/13/2022 0:30:28.44
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class shows_per_park_list : GXProcedure
   {
      public shows_per_park_list( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public shows_per_park_list( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( DateTime aP0_FechaShow ,
                           ref DateTime aP1_FechaToP )
      {
         this.AV2FechaShow = aP0_FechaShow;
         this.AV3FechaToP = aP1_FechaToP;
         initialize();
         executePrivate();
         aP1_FechaToP=this.AV3FechaToP;
      }

      public DateTime executeUdp( DateTime aP0_FechaShow )
      {
         this.AV2FechaShow = aP0_FechaShow;
         this.AV3FechaToP = aP1_FechaToP;
         initialize();
         executePrivate();
         aP1_FechaToP=this.AV3FechaToP;
         return AV3FechaToP ;
      }

      public void executeSubmit( DateTime aP0_FechaShow ,
                                 ref DateTime aP1_FechaToP )
      {
         shows_per_park_list objshows_per_park_list;
         objshows_per_park_list = new shows_per_park_list();
         objshows_per_park_list.AV2FechaShow = aP0_FechaShow;
         objshows_per_park_list.AV3FechaToP = aP1_FechaToP;
         objshows_per_park_list.context.SetSubmitInitialConfig(context);
         objshows_per_park_list.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objshows_per_park_list);
         aP1_FechaToP=this.AV3FechaToP;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((shows_per_park_list)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(DateTime)AV2FechaShow,(DateTime)AV3FechaToP} ;
         ClassLoader.Execute("ashows_per_park_list","GeneXus.Programs","ashows_per_park_list", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 2 ) )
         {
            AV3FechaToP = (DateTime)(args[1]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private DateTime AV2FechaShow ;
      private DateTime AV3FechaToP ;
      private IGxDataStore dsDefault ;
      private DateTime aP1_FechaToP ;
      private Object[] args ;
   }

}
