/*
               File: GameConversion
        Description: Conversion for table Game
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 5/3/2022 23:50:41.4
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
namespace GeneXus.Programs {
   public class gameconversion : GXProcedure
   {
      public gameconversion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gameconversion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         initialize();
         executePrivate();
      }

      public void executeSubmit( )
      {
         gameconversion objgameconversion;
         objgameconversion = new gameconversion();
         objgameconversion.context.SetSubmitInitialConfig(context);
         objgameconversion.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objgameconversion);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((gameconversion)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         cmdBuffer=" SET IDENTITY_INSERT [GXA0008] ON "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         /* Using cursor GAMECONVER2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A20CategoryId = GAMECONVER2_A20CategoryId[0];
            n20CategoryId = GAMECONVER2_n20CategoryId[0];
            A7AmusementParkId = GAMECONVER2_A7AmusementParkId[0];
            A19GameName = GAMECONVER2_A19GameName[0];
            A18GameId = GAMECONVER2_A18GameId[0];
            /*
               INSERT RECORD ON TABLE GXA0008

            */
            AV2GameId = A18GameId;
            AV3GameName = A19GameName;
            AV4AmusementParkId = A7AmusementParkId;
            if ( GAMECONVER2_n20CategoryId[0] )
            {
               AV5CategoryId = 0;
            }
            else
            {
               AV5CategoryId = A20CategoryId;
            }
            AV6GamePhoto = "";
            AV7GamePhoto_GXI = " ";
            /* Using cursor GAMECONVER3 */
            pr_default.execute(1, new Object[] {AV2GameId, AV3GameName, AV4AmusementParkId, AV5CategoryId, AV6GamePhoto, AV7GamePhoto_GXI});
            pr_default.close(1);
            dsDefault.SmartCacheProvider.SetUpdated("GXA0008") ;
            if ( (pr_default.getStatus(1) == 1) )
            {
               context.Gx_err = 1;
               Gx_emsg = (String)(GXResourceManager.GetMessage("GXM_noupdate"));
            }
            else
            {
               context.Gx_err = 0;
               Gx_emsg = "";
            }
            /* End Insert */
            pr_default.readNext(0);
         }
         pr_default.close(0);
         cmdBuffer=" SET IDENTITY_INSERT [GXA0008] OFF "
         ;
         RGZ = new GxCommand(dsDefault.Db, cmdBuffer, dsDefault,0,true,false,null);
         RGZ.ErrorMask = GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK;
         RGZ.ExecuteStmt() ;
         RGZ.Drop();
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         cmdBuffer = "";
         scmdbuf = "";
         GAMECONVER2_A20CategoryId = new short[1] ;
         GAMECONVER2_n20CategoryId = new bool[] {false} ;
         GAMECONVER2_A7AmusementParkId = new short[1] ;
         GAMECONVER2_A19GameName = new String[] {""} ;
         GAMECONVER2_A18GameId = new short[1] ;
         A19GameName = "";
         AV3GameName = "";
         AV6GamePhoto = "";
         AV7GamePhoto_GXI = "";
         Gx_emsg = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gameconversion__default(),
            new Object[][] {
                new Object[] {
               GAMECONVER2_A20CategoryId, GAMECONVER2_n20CategoryId, GAMECONVER2_A7AmusementParkId, GAMECONVER2_A19GameName, GAMECONVER2_A18GameId
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short A20CategoryId ;
      private short A7AmusementParkId ;
      private short A18GameId ;
      private short AV2GameId ;
      private short AV4AmusementParkId ;
      private short AV5CategoryId ;
      private int GIGXA0008 ;
      private String cmdBuffer ;
      private String scmdbuf ;
      private String A19GameName ;
      private String AV3GameName ;
      private String Gx_emsg ;
      private bool n20CategoryId ;
      private String AV7GamePhoto_GXI ;
      private String AV6GamePhoto ;
      private IGxDataStore dsDefault ;
      private GxCommand RGZ ;
      private IDataStoreProvider pr_default ;
      private short[] GAMECONVER2_A20CategoryId ;
      private bool[] GAMECONVER2_n20CategoryId ;
      private short[] GAMECONVER2_A7AmusementParkId ;
      private String[] GAMECONVER2_A19GameName ;
      private short[] GAMECONVER2_A18GameId ;
   }

   public class gameconversion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new UpdateCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmGAMECONVER2 ;
          prmGAMECONVER2 = new Object[] {
          } ;
          Object[] prmGAMECONVER3 ;
          prmGAMECONVER3 = new Object[] {
          new Object[] {"@AV2GameId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV3GameName",SqlDbType.NChar,50,0} ,
          new Object[] {"@AV4AmusementParkId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV5CategoryId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV6GamePhoto",SqlDbType.VarBinary,1024,0} ,
          new Object[] {"@AV7GamePhoto_GXI",SqlDbType.NVarChar,2048,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("GAMECONVER2", "SELECT [CategoryId], [AmusementParkId], [GameName], [GameId] FROM [Game] ORDER BY [GameId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmGAMECONVER2,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("GAMECONVER3", "INSERT INTO [GXA0008]([GameId], [GameName], [AmusementParkId], [CategoryId], [GamePhoto], [GamePhoto_GXI]) VALUES(@AV2GameId, @AV3GameName, @AV4AmusementParkId, @AV5CategoryId, @AV6GamePhoto, @AV7GamePhoto_GXI)", GxErrorMask.GX_NOMASK,prmGAMECONVER3)
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((String[]) buf[3])[0] = rslt.getString(3, 50) ;
                ((short[]) buf[4])[0] = rslt.getShort(4) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameterBlob(5, (String)parms[4], false);
                stmt.SetParameter(6, (String)parms[5]);
                return;
       }
    }

 }

}
