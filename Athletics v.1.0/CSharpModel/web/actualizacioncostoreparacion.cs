/*
               File: ActualizacionCostoReparacion
        Description: Actualizacion Costo Reparacion
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 5/24/2022 18:56:25.53
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class actualizacioncostoreparacion : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public actualizacioncostoreparacion( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public actualizacioncostoreparacion( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavCtlrepairkindname = new GXCombobox();
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_85 = (int)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_85_idx = (int)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_85_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( ) ;
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA192( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START192( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 135614), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxcfg.js", "?202252418562561", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("actualizacioncostoreparacion.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "Actualiza_costo", AV8Actualiza_Costo);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("Actualiza_costo", AV8Actualiza_Costo);
         }
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_85", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_85), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "REPAIRID", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29RepairId), 4, 0, ",", "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken((String)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE192( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT192( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("actualizacioncostoreparacion.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "ActualizacionCostoReparacion" ;
      }

      public override String GetPgmdesc( )
      {
         return "Actualizacion Costo Reparacion" ;
      }

      protected void WB190( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavPercentagei_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPercentagei_Internalname, "Percentagei", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPercentagei_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6Percentagei), 4, 0, ",", "")), ((edtavPercentagei_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV6Percentagei), "ZZZ9")) : context.localUtil.Format( (decimal)(AV6Percentagei), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,8);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPercentagei_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavPercentagei_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttActualizacostoo_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(85), 2, 0)+","+"null"+");", "Actualizacostoo", bttActualizacostoo_Jsonclick, 5, "Actualizacostoo", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ACTUALIZACOSTOO\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttActualizacioncosto_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(85), 2, 0)+","+"null"+");", "Actualizacion_Costo", bttActualizacioncosto_Jsonclick, 5, "Actualizacion_Costo", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'ACTUALIZACIONCOSTO\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepairid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepairid_Internalname, "Repair Id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlrepairid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Actualiza_Costo.gxTpr_Repairid), 4, 0, ",", "")), ((edtavCtlrepairid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Repairid), "ZZZ9")) : context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Repairid), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,22);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepairid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepairid_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepairdatefrom_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepairdatefrom_Internalname, "Repair Date From", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'" + sGXsfl_85_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCtlrepairdatefrom_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCtlrepairdatefrom_Internalname, context.localUtil.Format(AV8Actualiza_Costo.gxTpr_Repairdatefrom, "99/99/99"), context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Repairdatefrom, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepairdatefrom_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepairdatefrom_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_bitmap( context, edtavCtlrepairdatefrom_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavCtlrepairdatefrom_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_ActualizacionCostoReparacion.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepairdaysquantity_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepairdaysquantity_Internalname, "Repair Days Quantity", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlrepairdaysquantity_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Actualiza_Costo.gxTpr_Repairdaysquantity), 4, 0, ",", "")), ((edtavCtlrepairdaysquantity_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Repairdaysquantity), "ZZZ9")) : context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Repairdaysquantity), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepairdaysquantity_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepairdaysquantity_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepairdateto_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepairdateto_Internalname, "Repair Date To", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_85_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCtlrepairdateto_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCtlrepairdateto_Internalname, context.localUtil.Format(AV8Actualiza_Costo.gxTpr_Repairdateto, "99/99/99"), context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Repairdateto, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepairdateto_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepairdateto_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_bitmap( context, edtavCtlrepairdateto_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavCtlrepairdateto_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_ActualizacionCostoReparacion.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlgameid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlgameid_Internalname, "Game Id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlgameid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Actualiza_Costo.gxTpr_Gameid), 4, 0, ",", "")), ((edtavCtlgameid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Gameid), "ZZZ9")) : context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Gameid), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,42);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlgameid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlgameid_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlgamename_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlgamename_Internalname, "Game Name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlgamename_Internalname, StringUtil.RTrim( AV8Actualiza_Costo.gxTpr_Gamename), StringUtil.RTrim( context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Gamename, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlgamename_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlgamename_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepaircost_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepaircost_Internalname, "Repair Cost", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlrepaircost_Internalname, StringUtil.LTrim( StringUtil.NToC( AV8Actualiza_Costo.gxTpr_Repaircost, 8, 2, ",", "")), ((edtavCtlrepaircost_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Repaircost, "ZZZZ9.99")) : context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Repaircost, "ZZZZ9.99")), TempTags+" onchange=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,52);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepaircost_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepaircost_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtltechnicianid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtltechnicianid_Internalname, "Technician Id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtltechnicianid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Actualiza_Costo.gxTpr_Technicianid), 4, 0, ",", "")), ((edtavCtltechnicianid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Technicianid), "ZZZ9")) : context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Technicianid), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,57);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtltechnicianid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtltechnicianid_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtltechnicianname_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtltechnicianname_Internalname, "Technician Name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtltechnicianname_Internalname, StringUtil.RTrim( AV8Actualiza_Costo.gxTpr_Technicianname), StringUtil.RTrim( context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Technicianname, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtltechnicianname_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtltechnicianname_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepairtechnicianid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepairtechnicianid_Internalname, "Repair Technician Id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlrepairtechnicianid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Actualiza_Costo.gxTpr_Repairtechnicianid), 4, 0, ",", "")), ((edtavCtlrepairtechnicianid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Repairtechnicianid), "ZZZ9")) : context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Repairtechnicianid), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,67);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepairtechnicianid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepairtechnicianid_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepairtechnicianame_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepairtechnicianame_Internalname, "Repair Technicia Name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlrepairtechnicianame_Internalname, StringUtil.RTrim( AV8Actualiza_Costo.gxTpr_Repairtechnicianame), StringUtil.RTrim( context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Repairtechnicianame, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,72);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepairtechnicianame_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepairtechnicianame_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepairdiscountpercentage_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepairdiscountpercentage_Internalname, "Repair Discount Percentage", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlrepairdiscountpercentage_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8Actualiza_Costo.gxTpr_Repairdiscountpercentage), 4, 0, ",", "")), ((edtavCtlrepairdiscountpercentage_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Repairdiscountpercentage), "ZZZ9")) : context.localUtil.Format( (decimal)(AV8Actualiza_Costo.gxTpr_Repairdiscountpercentage), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepairdiscountpercentage_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepairdiscountpercentage_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlrepairfinalcost_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlrepairfinalcost_Internalname, "Repair Final Cost", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_85_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCtlrepairfinalcost_Internalname, StringUtil.LTrim( StringUtil.NToC( AV8Actualiza_Costo.gxTpr_Repairfinalcost, 8, 2, ",", "")), ((edtavCtlrepairfinalcost_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Repairfinalcost, "ZZZZ9.99")) : context.localUtil.Format( AV8Actualiza_Costo.gxTpr_Repairfinalcost, "ZZZZ9.99")), TempTags+" onchange=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_decimal( this, '.',',','2');"+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlrepairfinalcost_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCtlrepairfinalcost_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_ActualizacionCostoReparacion.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"85\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "Grid", 0, "", "", 1, 2, sStyleString, "", "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Repair Kind Id") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Repair Kind Name") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Repair Kind Remaks") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Header", subGrid1_Header);
               Grid1Container.AddObjectProperty("Class", "Grid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectedindex), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 85 )
         {
            wbEnd = 0;
            nRC_GXsfl_85 = (int)(nGXsfl_85_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV25GXV14 = nGXsfl_85_idx;
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         if ( wbEnd == 85 )
         {
            wbEnd = 0;
            if ( isFullAjaxMode( ) )
            {
               if ( Grid1Container.GetWrapped() == 1 )
               {
                  context.WriteHtmlText( "</table>") ;
                  context.WriteHtmlText( "</div>") ;
               }
               else
               {
                  AV25GXV14 = nGXsfl_85_idx;
                  sStyleString = "";
                  context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
                  context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
                  if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
                  }
                  if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
                  {
                     GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
                  }
                  else
                  {
                     context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
                  }
               }
            }
         }
         wbLoad = true;
      }

      protected void START192( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_5-135614", 0) ;
            Form.Meta.addItem("description", "Actualizacion Costo Reparacion", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP190( ) ;
      }

      protected void WS192( )
      {
         START192( ) ;
         EVT192( ) ;
      }

      protected void EVT192( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ACTUALIZACOSTOO'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'Actualizacostoo' */
                              E11192 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'ACTUALIZACIONCOSTO'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "GRID1.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_85_idx = (int)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
                              SubsflControlProps_852( ) ;
                              AV25GXV14 = nGXsfl_85_idx;
                              if ( ( AV8Actualiza_Costo.gxTpr_Kind.Count >= AV25GXV14 ) && ( AV25GXV14 > 0 ) )
                              {
                                 AV8Actualiza_Costo.gxTpr_Kind.CurrentItem = ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14));
                                 ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindid = (short)(context.localUtil.CToN( cgiGet( edtavCtlrepairkindid_Internalname), ",", "."));
                                 cmbavCtlrepairkindname.Name = cmbavCtlrepairkindname_Internalname;
                                 cmbavCtlrepairkindname.CurrentValue = cgiGet( cmbavCtlrepairkindname_Internalname);
                                 ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname = cgiGet( cmbavCtlrepairkindname_Internalname);
                                 ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindremaks = cgiGet( edtavCtlrepairkindremaks_Internalname);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRID1.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E12192 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                    /* No code required for Cancel button. It is implemented as the Reset button. */
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE192( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA192( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            init_web_controls( ) ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPercentagei_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_852( ) ;
         while ( nGXsfl_85_idx <= nRC_GXsfl_85 )
         {
            sendrow_852( ) ;
            nGXsfl_85_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_85_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_85_idx+1);
            sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
            SubsflControlProps_852( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Grid1Container));
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID1_nCurrentRecord = 0;
         RF192( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void clear_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
            dynload_actions( ) ;
         }
      }

      protected void fix_multi_value_controls( )
      {
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF192( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF192( )
      {
         initialize_formulas( ) ;
         clear_multi_value_controls( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 85;
         nGXsfl_85_idx = 1;
         sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
         SubsflControlProps_852( ) ;
         bGXsfl_85_Refreshing = true;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "Grid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         gxdyncontrolsrefreshing = true;
         fix_multi_value_controls( ) ;
         gxdyncontrolsrefreshing = false;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_852( ) ;
            E12192 ();
            wbEnd = 85;
            WB190( ) ;
         }
         bGXsfl_85_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes192( )
      {
      }

      protected int subGrid1_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUP190( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "Actualiza_costo"), AV8Actualiza_Costo);
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavPercentagei_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavPercentagei_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vPERCENTAGEI");
               GX_FocusControl = edtavPercentagei_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV6Percentagei = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Percentagei", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Percentagei), 4, 0)));
            }
            else
            {
               AV6Percentagei = (short)(context.localUtil.CToN( cgiGet( edtavPercentagei_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6Percentagei", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6Percentagei), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairid_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairid_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLREPAIRID");
               GX_FocusControl = edtavCtlrepairid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Repairid = 0;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Repairid = (short)(context.localUtil.CToN( cgiGet( edtavCtlrepairid_Internalname), ",", "."));
            }
            if ( context.localUtil.VCDate( cgiGet( edtavCtlrepairdatefrom_Internalname), 2) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Repair Date From"}), 1, "CTLREPAIRDATEFROM");
               GX_FocusControl = edtavCtlrepairdatefrom_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Repairdatefrom = DateTime.MinValue;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Repairdatefrom = context.localUtil.CToD( cgiGet( edtavCtlrepairdatefrom_Internalname), 2);
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairdaysquantity_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairdaysquantity_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLREPAIRDAYSQUANTITY");
               GX_FocusControl = edtavCtlrepairdaysquantity_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Repairdaysquantity = 0;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Repairdaysquantity = (short)(context.localUtil.CToN( cgiGet( edtavCtlrepairdaysquantity_Internalname), ",", "."));
            }
            if ( context.localUtil.VCDate( cgiGet( edtavCtlrepairdateto_Internalname), 2) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Repair Date To"}), 1, "CTLREPAIRDATETO");
               GX_FocusControl = edtavCtlrepairdateto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Repairdateto = DateTime.MinValue;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Repairdateto = context.localUtil.CToD( cgiGet( edtavCtlrepairdateto_Internalname), 2);
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtlgameid_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtlgameid_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLGAMEID");
               GX_FocusControl = edtavCtlgameid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Gameid = 0;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Gameid = (short)(context.localUtil.CToN( cgiGet( edtavCtlgameid_Internalname), ",", "."));
            }
            AV8Actualiza_Costo.gxTpr_Gamename = cgiGet( edtavCtlgamename_Internalname);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtlrepaircost_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtlrepaircost_Internalname), ",", ".") > 99999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLREPAIRCOST");
               GX_FocusControl = edtavCtlrepaircost_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Repaircost = 0;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Repaircost = context.localUtil.CToN( cgiGet( edtavCtlrepaircost_Internalname), ",", ".");
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtltechnicianid_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtltechnicianid_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLTECHNICIANID");
               GX_FocusControl = edtavCtltechnicianid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Technicianid = 0;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Technicianid = (short)(context.localUtil.CToN( cgiGet( edtavCtltechnicianid_Internalname), ",", "."));
            }
            AV8Actualiza_Costo.gxTpr_Technicianname = cgiGet( edtavCtltechnicianname_Internalname);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairtechnicianid_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairtechnicianid_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLREPAIRTECHNICIANID");
               GX_FocusControl = edtavCtlrepairtechnicianid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Repairtechnicianid = 0;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Repairtechnicianid = (short)(context.localUtil.CToN( cgiGet( edtavCtlrepairtechnicianid_Internalname), ",", "."));
            }
            AV8Actualiza_Costo.gxTpr_Repairtechnicianame = cgiGet( edtavCtlrepairtechnicianame_Internalname);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairdiscountpercentage_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairdiscountpercentage_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLREPAIRDISCOUNTPERCENTAGE");
               GX_FocusControl = edtavCtlrepairdiscountpercentage_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Repairdiscountpercentage = 0;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Repairdiscountpercentage = (short)(context.localUtil.CToN( cgiGet( edtavCtlrepairdiscountpercentage_Internalname), ",", "."));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairfinalcost_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCtlrepairfinalcost_Internalname), ",", ".") > 99999.99m ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "CTLREPAIRFINALCOST");
               GX_FocusControl = edtavCtlrepairfinalcost_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8Actualiza_Costo.gxTpr_Repairfinalcost = 0;
            }
            else
            {
               AV8Actualiza_Costo.gxTpr_Repairfinalcost = context.localUtil.CToN( cgiGet( edtavCtlrepairfinalcost_Internalname), ",", ".");
            }
            /* Read saved values. */
            nRC_GXsfl_85 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_85"), ",", "."));
            nRC_GXsfl_85 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_85"), ",", "."));
            nGXsfl_85_fel_idx = 0;
            while ( nGXsfl_85_fel_idx < nRC_GXsfl_85 )
            {
               nGXsfl_85_fel_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_85_fel_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_85_fel_idx+1);
               sGXsfl_85_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_852( ) ;
               AV25GXV14 = nGXsfl_85_fel_idx;
               if ( ( AV8Actualiza_Costo.gxTpr_Kind.Count >= AV25GXV14 ) && ( AV25GXV14 > 0 ) )
               {
                  AV8Actualiza_Costo.gxTpr_Kind.CurrentItem = ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14));
                  ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindid = (short)(context.localUtil.CToN( cgiGet( edtavCtlrepairkindid_Internalname), ",", "."));
                  cmbavCtlrepairkindname.Name = cmbavCtlrepairkindname_Internalname;
                  cmbavCtlrepairkindname.CurrentValue = cgiGet( cmbavCtlrepairkindname_Internalname);
                  ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname = cgiGet( cmbavCtlrepairkindname_Internalname);
                  ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindremaks = cgiGet( edtavCtlrepairkindremaks_Internalname);
               }
            }
            if ( nGXsfl_85_fel_idx == 0 )
            {
               nGXsfl_85_idx = 1;
               sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
               SubsflControlProps_852( ) ;
            }
            nGXsfl_85_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void E11192( )
      {
         AV25GXV14 = nGXsfl_85_idx;
         if ( AV8Actualiza_Costo.gxTpr_Kind.Count >= AV25GXV14 )
         {
            AV8Actualiza_Costo.gxTpr_Kind.CurrentItem = ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14));
         }
         /* 'Actualizacostoo' Routine */
         /* Using cursor H00192 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A29RepairId = H00192_A29RepairId[0];
            AV8Actualiza_Costo.Load(A29RepairId);
            gx_BV85 = true;
            AV8Actualiza_Costo.gxTpr_Repaircost = (decimal)(AV8Actualiza_Costo.gxTpr_Repaircost*(1+AV6Percentagei/ (decimal)(100)));
            AV8Actualiza_Costo.Update();
            gx_BV85 = true;
            pr_default.readNext(0);
         }
         pr_default.close(0);
         if ( AV8Actualiza_Costo.Update() )
         {
            context.CommitDataStores("actualizacioncostoreparacion",pr_default);
         }
         else
         {
            context.RollbackDataStores("actualizacioncostoreparacion",pr_default);
         }
         /*  Sending Event outputs  */
         if ( gx_BV85 )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8Actualiza_Costo", AV8Actualiza_Costo);
            nGXsfl_85_bak_idx = nGXsfl_85_idx;
            gxgrGrid1_refresh( ) ;
            nGXsfl_85_idx = nGXsfl_85_bak_idx;
            sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
            SubsflControlProps_852( ) ;
         }
      }

      private void E12192( )
      {
         /* Grid1_Load Routine */
         AV25GXV14 = 1;
         while ( AV25GXV14 <= AV8Actualiza_Costo.gxTpr_Kind.Count )
         {
            AV8Actualiza_Costo.gxTpr_Kind.CurrentItem = ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14));
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 85;
            }
            sendrow_852( ) ;
            if ( isFullAjaxMode( ) && ! bGXsfl_85_Refreshing )
            {
               context.DoAjaxLoad(85, Grid1Row);
            }
            AV25GXV14 = (int)(AV25GXV14+1);
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA192( ) ;
         WS192( ) ;
         WE192( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?202252418562641", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("actualizacioncostoreparacion.js", "?202252418562643", false, true);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_852( )
      {
         edtavCtlrepairkindid_Internalname = "CTLREPAIRKINDID_"+sGXsfl_85_idx;
         cmbavCtlrepairkindname_Internalname = "CTLREPAIRKINDNAME_"+sGXsfl_85_idx;
         edtavCtlrepairkindremaks_Internalname = "CTLREPAIRKINDREMAKS_"+sGXsfl_85_idx;
      }

      protected void SubsflControlProps_fel_852( )
      {
         edtavCtlrepairkindid_Internalname = "CTLREPAIRKINDID_"+sGXsfl_85_fel_idx;
         cmbavCtlrepairkindname_Internalname = "CTLREPAIRKINDNAME_"+sGXsfl_85_fel_idx;
         edtavCtlrepairkindremaks_Internalname = "CTLREPAIRKINDREMAKS_"+sGXsfl_85_fel_idx;
      }

      protected void sendrow_852( )
      {
         SubsflControlProps_852( ) ;
         WB190( ) ;
         Grid1Row = GXWebRow.GetNew(context,Grid1Container);
         if ( subGrid1_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid1_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
         }
         else if ( subGrid1_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid1_Backstyle = 0;
            subGrid1_Backcolor = subGrid1_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Uniform";
            }
         }
         else if ( subGrid1_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
            {
               subGrid1_Linesclass = subGrid1_Class+"Odd";
            }
            subGrid1_Backcolor = (int)(0x0);
         }
         else if ( subGrid1_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid1_Backstyle = 1;
            if ( ((int)((nGXsfl_85_idx) % (2))) == 0 )
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Even";
               }
            }
            else
            {
               subGrid1_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
         }
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+"Grid"+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_85_idx+"\">") ;
         }
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtlrepairkindid_Enabled!=0)&&(edtavCtlrepairkindid_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 86,'',false,'"+sGXsfl_85_idx+"',85)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlrepairkindid_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindid), 1, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindid), "9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+((edtavCtlrepairkindid_Enabled!=0)&&(edtavCtlrepairkindid_Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,86);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlrepairkindid_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(short)1,(short)0,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         TempTags = " " + ((cmbavCtlrepairkindname.Enabled!=0)&&(cmbavCtlrepairkindname.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 87,'',false,'"+sGXsfl_85_idx+"',85)\"" : " ");
         if ( ( cmbavCtlrepairkindname.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "CTLREPAIRKINDNAME_" + sGXsfl_85_idx;
            cmbavCtlrepairkindname.Name = GXCCtl;
            cmbavCtlrepairkindname.WebTags = "";
            cmbavCtlrepairkindname.addItem("E", "Electricity", 0);
            cmbavCtlrepairkindname.addItem("M", "Mechanical", 0);
            cmbavCtlrepairkindname.addItem("R", "Replacement", 0);
            if ( cmbavCtlrepairkindname.ItemCount > 0 )
            {
               if ( ( AV25GXV14 > 0 ) && ( AV8Actualiza_Costo.gxTpr_Kind.Count >= AV25GXV14 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname)) )
               {
                  ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname = cmbavCtlrepairkindname.getValidValue(((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname);
               }
            }
         }
         /* ComboBox */
         Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavCtlrepairkindname,(String)cmbavCtlrepairkindname_Internalname,StringUtil.RTrim( ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname),(short)1,(String)cmbavCtlrepairkindname_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"char",(String)"",(short)-1,(short)1,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+((cmbavCtlrepairkindname.Enabled!=0)&&(cmbavCtlrepairkindname.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,87);\"" : " "),(String)"",(bool)true});
         cmbavCtlrepairkindname.CurrentValue = StringUtil.RTrim( ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtlrepairkindname_Internalname, "Values", (String)(cmbavCtlrepairkindname.ToJavascriptSource()), !bGXsfl_85_Refreshing);
         /* Subfile cell */
         if ( Grid1Container.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCtlrepairkindremaks_Enabled!=0)&&(edtavCtlrepairkindremaks_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 88,'',false,'"+sGXsfl_85_idx+"',85)\"" : " ");
         ROClassString = "Attribute";
         Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlrepairkindremaks_Internalname,StringUtil.RTrim( ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindremaks),(String)"",TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+((edtavCtlrepairkindremaks_Enabled!=0)&&(edtavCtlrepairkindremaks_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,88);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCtlrepairkindremaks_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(short)1,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)120,(short)0,(short)0,(short)85,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         send_integrity_lvl_hashes192( ) ;
         Grid1Container.AddRow(Grid1Row);
         nGXsfl_85_idx = ((subGrid1_Islastpage==1)&&(nGXsfl_85_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_85_idx+1);
         sGXsfl_85_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_85_idx), 4, 0)), 4, "0");
         SubsflControlProps_852( ) ;
         /* End function sendrow_852 */
      }

      protected void init_web_controls( )
      {
         GXCCtl = "CTLREPAIRKINDNAME_" + sGXsfl_85_idx;
         cmbavCtlrepairkindname.Name = GXCCtl;
         cmbavCtlrepairkindname.WebTags = "";
         cmbavCtlrepairkindname.addItem("E", "Electricity", 0);
         cmbavCtlrepairkindname.addItem("M", "Mechanical", 0);
         cmbavCtlrepairkindname.addItem("R", "Replacement", 0);
         if ( cmbavCtlrepairkindname.ItemCount > 0 )
         {
            if ( ( AV25GXV14 > 0 ) && ( AV8Actualiza_Costo.gxTpr_Kind.Count >= AV25GXV14 ) && String.IsNullOrEmpty(StringUtil.RTrim( ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname)) )
            {
               ((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname = cmbavCtlrepairkindname.getValidValue(((SdtRepair_Kind)AV8Actualiza_Costo.gxTpr_Kind.Item(AV25GXV14)).gxTpr_Repairkindname);
            }
         }
         /* End function init_web_controls */
      }

      protected void init_default_properties( )
      {
         edtavPercentagei_Internalname = "vPERCENTAGEI";
         bttActualizacostoo_Internalname = "ACTUALIZACOSTOO";
         bttActualizacioncosto_Internalname = "ACTUALIZACIONCOSTO";
         edtavCtlrepairid_Internalname = "CTLREPAIRID";
         edtavCtlrepairdatefrom_Internalname = "CTLREPAIRDATEFROM";
         edtavCtlrepairdaysquantity_Internalname = "CTLREPAIRDAYSQUANTITY";
         edtavCtlrepairdateto_Internalname = "CTLREPAIRDATETO";
         edtavCtlgameid_Internalname = "CTLGAMEID";
         edtavCtlgamename_Internalname = "CTLGAMENAME";
         edtavCtlrepaircost_Internalname = "CTLREPAIRCOST";
         edtavCtltechnicianid_Internalname = "CTLTECHNICIANID";
         edtavCtltechnicianname_Internalname = "CTLTECHNICIANNAME";
         edtavCtlrepairtechnicianid_Internalname = "CTLREPAIRTECHNICIANID";
         edtavCtlrepairtechnicianame_Internalname = "CTLREPAIRTECHNICIANAME";
         edtavCtlrepairdiscountpercentage_Internalname = "CTLREPAIRDISCOUNTPERCENTAGE";
         edtavCtlrepairfinalcost_Internalname = "CTLREPAIRFINALCOST";
         edtavCtlrepairkindid_Internalname = "CTLREPAIRKINDID";
         cmbavCtlrepairkindname_Internalname = "CTLREPAIRKINDNAME";
         edtavCtlrepairkindremaks_Internalname = "CTLREPAIRKINDREMAKS";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlrepairkindremaks_Jsonclick = "";
         edtavCtlrepairkindremaks_Visible = -1;
         edtavCtlrepairkindremaks_Enabled = 1;
         cmbavCtlrepairkindname_Jsonclick = "";
         cmbavCtlrepairkindname.Visible = -1;
         cmbavCtlrepairkindname.Enabled = 1;
         edtavCtlrepairkindid_Jsonclick = "";
         edtavCtlrepairkindid_Visible = -1;
         edtavCtlrepairkindid_Enabled = 1;
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         subGrid1_Header = "";
         subGrid1_Class = "Grid";
         subGrid1_Backcolorstyle = 0;
         edtavCtlrepairfinalcost_Jsonclick = "";
         edtavCtlrepairfinalcost_Enabled = 1;
         edtavCtlrepairdiscountpercentage_Jsonclick = "";
         edtavCtlrepairdiscountpercentage_Enabled = 1;
         edtavCtlrepairtechnicianame_Jsonclick = "";
         edtavCtlrepairtechnicianame_Enabled = 1;
         edtavCtlrepairtechnicianid_Jsonclick = "";
         edtavCtlrepairtechnicianid_Enabled = 1;
         edtavCtltechnicianname_Jsonclick = "";
         edtavCtltechnicianname_Enabled = 1;
         edtavCtltechnicianid_Jsonclick = "";
         edtavCtltechnicianid_Enabled = 1;
         edtavCtlrepaircost_Jsonclick = "";
         edtavCtlrepaircost_Enabled = 1;
         edtavCtlgamename_Jsonclick = "";
         edtavCtlgamename_Enabled = 1;
         edtavCtlgameid_Jsonclick = "";
         edtavCtlgameid_Enabled = 1;
         edtavCtlrepairdateto_Jsonclick = "";
         edtavCtlrepairdateto_Enabled = 1;
         edtavCtlrepairdaysquantity_Jsonclick = "";
         edtavCtlrepairdaysquantity_Enabled = 1;
         edtavCtlrepairdatefrom_Jsonclick = "";
         edtavCtlrepairdatefrom_Enabled = 1;
         edtavCtlrepairid_Jsonclick = "";
         edtavCtlrepairid_Enabled = 1;
         edtavPercentagei_Jsonclick = "";
         edtavPercentagei_Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Actualizacion Costo Reparacion";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage'},{av:'GRID1_nEOF'},{av:'AV8Actualiza_Costo',fld:'vACTUALIZA_COSTO',pic:''},{av:'nRC_GXsfl_85',ctrl:'GRID1',prop:'GridRC'}]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("'ACTUALIZACOSTOO'","{handler:'E11192',iparms:[{av:'A29RepairId',fld:'REPAIRID',pic:'ZZZ9'},{av:'AV6Percentagei',fld:'vPERCENTAGEI',pic:'ZZZ9'},{av:'AV8Actualiza_Costo',fld:'vACTUALIZA_COSTO',pic:''},{av:'GRID1_nFirstRecordOnPage'},{av:'nRC_GXsfl_85',ctrl:'GRID1',prop:'GridRC'},{av:'GRID1_nEOF'}]");
         setEventMetadata("'ACTUALIZACOSTOO'",",oparms:[{av:'AV8Actualiza_Costo',fld:'vACTUALIZA_COSTO',pic:''},{av:'GRID1_nFirstRecordOnPage'},{av:'nRC_GXsfl_85',ctrl:'GRID1',prop:'GridRC'}]}");
         setEventMetadata("NULL","{handler:'Validv_Gxv17',iparms:[]");
         setEventMetadata("NULL",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV8Actualiza_Costo = new SdtRepair(context);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttActualizacostoo_Jsonclick = "";
         bttActualizacioncosto_Jsonclick = "";
         Grid1Container = new GXWebGrid( context);
         sStyleString = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         scmdbuf = "";
         H00192_A29RepairId = new short[1] ;
         Grid1Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         GXCCtl = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.actualizacioncostoreparacion__default(),
            new Object[][] {
                new Object[] {
               H00192_A29RepairId
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short A29RepairId ;
      private short wbEnd ;
      private short wbStart ;
      private short AV6Percentagei ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short GRID1_nEOF ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int nRC_GXsfl_85 ;
      private int nGXsfl_85_idx=1 ;
      private int edtavPercentagei_Enabled ;
      private int edtavCtlrepairid_Enabled ;
      private int edtavCtlrepairdatefrom_Enabled ;
      private int edtavCtlrepairdaysquantity_Enabled ;
      private int edtavCtlrepairdateto_Enabled ;
      private int edtavCtlgameid_Enabled ;
      private int edtavCtlgamename_Enabled ;
      private int edtavCtlrepaircost_Enabled ;
      private int edtavCtltechnicianid_Enabled ;
      private int edtavCtltechnicianname_Enabled ;
      private int edtavCtlrepairtechnicianid_Enabled ;
      private int edtavCtlrepairtechnicianame_Enabled ;
      private int edtavCtlrepairdiscountpercentage_Enabled ;
      private int edtavCtlrepairfinalcost_Enabled ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectedindex ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int AV25GXV14 ;
      private int subGrid1_Islastpage ;
      private int nGXsfl_85_fel_idx=1 ;
      private int nGXsfl_85_bak_idx=1 ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private int edtavCtlrepairkindid_Enabled ;
      private int edtavCtlrepairkindid_Visible ;
      private int edtavCtlrepairkindremaks_Enabled ;
      private int edtavCtlrepairkindremaks_Visible ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_85_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String edtavPercentagei_Internalname ;
      private String TempTags ;
      private String edtavPercentagei_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttActualizacostoo_Internalname ;
      private String bttActualizacostoo_Jsonclick ;
      private String bttActualizacioncosto_Internalname ;
      private String bttActualizacioncosto_Jsonclick ;
      private String edtavCtlrepairid_Internalname ;
      private String edtavCtlrepairid_Jsonclick ;
      private String edtavCtlrepairdatefrom_Internalname ;
      private String edtavCtlrepairdatefrom_Jsonclick ;
      private String edtavCtlrepairdaysquantity_Internalname ;
      private String edtavCtlrepairdaysquantity_Jsonclick ;
      private String edtavCtlrepairdateto_Internalname ;
      private String edtavCtlrepairdateto_Jsonclick ;
      private String edtavCtlgameid_Internalname ;
      private String edtavCtlgameid_Jsonclick ;
      private String edtavCtlgamename_Internalname ;
      private String edtavCtlgamename_Jsonclick ;
      private String edtavCtlrepaircost_Internalname ;
      private String edtavCtlrepaircost_Jsonclick ;
      private String edtavCtltechnicianid_Internalname ;
      private String edtavCtltechnicianid_Jsonclick ;
      private String edtavCtltechnicianname_Internalname ;
      private String edtavCtltechnicianname_Jsonclick ;
      private String edtavCtlrepairtechnicianid_Internalname ;
      private String edtavCtlrepairtechnicianid_Jsonclick ;
      private String edtavCtlrepairtechnicianame_Internalname ;
      private String edtavCtlrepairtechnicianame_Jsonclick ;
      private String edtavCtlrepairdiscountpercentage_Internalname ;
      private String edtavCtlrepairdiscountpercentage_Jsonclick ;
      private String edtavCtlrepairfinalcost_Internalname ;
      private String edtavCtlrepairfinalcost_Jsonclick ;
      private String sStyleString ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String subGrid1_Header ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavCtlrepairkindid_Internalname ;
      private String cmbavCtlrepairkindname_Internalname ;
      private String edtavCtlrepairkindremaks_Internalname ;
      private String sGXsfl_85_fel_idx="0001" ;
      private String scmdbuf ;
      private String ROClassString ;
      private String edtavCtlrepairkindid_Jsonclick ;
      private String GXCCtl ;
      private String cmbavCtlrepairkindname_Jsonclick ;
      private String edtavCtlrepairkindremaks_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_85_Refreshing=false ;
      private bool gxdyncontrolsrefreshing ;
      private bool gx_BV85 ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavCtlrepairkindname ;
      private IDataStoreProvider pr_default ;
      private short[] H00192_A29RepairId ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private SdtRepair AV8Actualiza_Costo ;
   }

   public class actualizacioncostoreparacion__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH00192 ;
          prmH00192 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H00192", "SELECT [RepairId] FROM [Repair] ORDER BY [RepairId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH00192,100, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
