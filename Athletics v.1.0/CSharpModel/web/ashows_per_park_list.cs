/*
               File: Shows_per_Park_List
        Description: Shows_per_Park_List
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/13/2022 0:30:28.30
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.Printer;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ashows_per_park_list : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      public override void webExecute( )
      {
         context.SetDefaultTheme("Carmine");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV9FechaShow = context.localUtil.ParseDateParm( gxfirstwebparm);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV11FechaToP = context.localUtil.ParseDateParm( GetNextPar( ));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public ashows_per_park_list( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public ashows_per_park_list( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( DateTime aP0_FechaShow ,
                           ref DateTime aP1_FechaToP )
      {
         this.AV9FechaShow = aP0_FechaShow;
         this.AV11FechaToP = aP1_FechaToP;
         initialize();
         executePrivate();
         aP1_FechaToP=this.AV11FechaToP;
      }

      public DateTime executeUdp( DateTime aP0_FechaShow )
      {
         this.AV9FechaShow = aP0_FechaShow;
         this.AV11FechaToP = aP1_FechaToP;
         initialize();
         executePrivate();
         aP1_FechaToP=this.AV11FechaToP;
         return AV11FechaToP ;
      }

      public void executeSubmit( DateTime aP0_FechaShow ,
                                 ref DateTime aP1_FechaToP )
      {
         ashows_per_park_list objashows_per_park_list;
         objashows_per_park_list = new ashows_per_park_list();
         objashows_per_park_list.AV9FechaShow = aP0_FechaShow;
         objashows_per_park_list.AV11FechaToP = aP1_FechaToP;
         objashows_per_park_list.context.SetSubmitInitialConfig(context);
         objashows_per_park_list.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objashows_per_park_list);
         aP1_FechaToP=this.AV11FechaToP;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((ashows_per_park_list)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         M_top = 0;
         M_bot = 6;
         P_lines = (int)(66-M_bot);
         getPrinter().GxClearAttris() ;
         add_metrics( ) ;
         lineHeight = 15;
         PrtOffset = 0;
         gxXPage = 100;
         gxYPage = 100;
         getPrinter().GxSetDocName("") ;
         try
         {
            Gx_out = "FIL" ;
            if (!initPrinter (Gx_out, gxXPage, gxYPage, "GXPRN.INI", "", "", 2, 1, 256, 16834, 11909, 0, 1, 1, 0, 1, 1) )
            {
               cleanup();
               return;
            }
            getPrinter().setModal(false) ;
            P_lines = (int)(gxYPage-(lineHeight*6));
            Gx_line = (int)(P_lines+1);
            getPrinter().setPageLines(P_lines);
            getPrinter().setLineHeight(lineHeight);
            getPrinter().setM_top(M_top);
            getPrinter().setM_bot(M_bot);
            AV8ID_ParkK = 3;
            H0E0( false, 193) ;
            getPrinter().GxDrawBitMap(context.GetImagePath( "b552ee4c-9d80-4df4-a8e5-cc5bb2fcbb7e", "", context.GetTheme( )), 33, Gx_line+17, 150, Gx_line+184) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 26, false, false, false, false, 0, 30, 97, 255, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Shows Per Park", 275, Gx_line+33, 534, Gx_line+74, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, false, false, false, false, 0, 71, 135, 255, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Selected Park ID:", 392, Gx_line+100, 522, Gx_line+121, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 12, false, false, false, false, 0, 20, 79, 255, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("Shows From:", 400, Gx_line+133, 498, Gx_line+154, 0+256, 0, 0, 0) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText(StringUtil.LTrim( context.localUtil.Format( (decimal)(AV8ID_ParkK), "ZZZ9")), 550, Gx_line+100, 625, Gx_line+115, 2, 0, 0, 0) ;
            getPrinter().GxDrawText(context.localUtil.Format( AV9FechaShow, "99/99/99"), 550, Gx_line+150, 633, Gx_line+165, 2, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+193);
            H0E0( false, 71) ;
            getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
            getPrinter().GxDrawText("ShowName", 42, Gx_line+33, 100, Gx_line+47, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Image", 250, Gx_line+33, 281, Gx_line+47, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Date", 442, Gx_line+33, 467, Gx_line+47, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawText("Schedule", 608, Gx_line+33, 656, Gx_line+47, 0+256, 0, 0, 0) ;
            getPrinter().GxDrawLine(17, Gx_line+50, 667, Gx_line+50, 1, 0, 0, 0, 0) ;
            Gx_OldLine = Gx_line;
            Gx_line = (int)(Gx_line+71);
            /* Using cursor P000E2 */
            pr_default.execute(0, new Object[] {AV9FechaShow, AV11FechaToP});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A25Date = P000E2_A25Date[0];
               A40000ShowImage_GXI = P000E2_A40000ShowImage_GXI[0];
               A26Schedule = P000E2_A26Schedule[0];
               A15ShowName = P000E2_A15ShowName[0];
               A14ShowId = P000E2_A14ShowId[0];
               A16ShowImage = P000E2_A16ShowImage[0];
               H0E0( false, 100) ;
               getPrinter().GxAttris("Microsoft Sans Serif", 8, false, false, false, false, 0, 0, 0, 0, 0, 255, 255, 255) ;
               getPrinter().GxDrawText(StringUtil.RTrim( context.localUtil.Format( A15ShowName, "")), 25, Gx_line+17, 150, Gx_line+50, 0, 0, 0, 0) ;
               sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( A16ShowImage)) ? A40000ShowImage_GXI : A16ShowImage);
               getPrinter().GxDrawBitMap(sImgUrl, 208, Gx_line+17, 308, Gx_line+84) ;
               getPrinter().GxDrawText(context.localUtil.Format( A25Date, "99/99/99"), 408, Gx_line+33, 508, Gx_line+66, 2, 0, 0, 0) ;
               getPrinter().GxDrawText(context.localUtil.Format( A26Schedule, "99:99"), 542, Gx_line+33, 659, Gx_line+64, 2, 0, 0, 0) ;
               Gx_OldLine = Gx_line;
               Gx_line = (int)(Gx_line+100);
               pr_default.readNext(0);
            }
            pr_default.close(0);
            /* Print footer for last page */
            ToSkip = (int)(P_lines+1);
            H0E0( true, 0) ;
         }
         catch ( GeneXus.Printer.ProcessInterruptedException e )
         {
         }
         finally
         {
            /* Close printer file */
            try
            {
               getPrinter().GxEndPage() ;
               getPrinter().GxEndDocument() ;
            }
            catch ( GeneXus.Printer.ProcessInterruptedException e )
            {
            }
            endPrinter();
         }
         if ( context.WillRedirect( ) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void H0E0( bool bFoot ,
                           int Inc )
      {
         /* Skip the required number of lines */
         while ( ( ToSkip > 0 ) || ( Gx_line + Inc > P_lines ) )
         {
            if ( Gx_line + Inc >= P_lines )
            {
               if ( Gx_page > 0 )
               {
                  /* Print footers */
                  Gx_line = P_lines;
                  getPrinter().GxEndPage() ;
                  if ( bFoot )
                  {
                     return  ;
                  }
               }
               ToSkip = 0;
               Gx_line = 0;
               Gx_page = (int)(Gx_page+1);
               /* Skip Margin Top Lines */
               Gx_line = (int)(Gx_line+(M_top*lineHeight));
               /* Print headers */
               getPrinter().GxStartPage() ;
               if (true) break;
            }
            else
            {
               PrtOffset = 0;
               Gx_line = (int)(Gx_line+1);
            }
            ToSkip = (int)(ToSkip-1);
         }
         getPrinter().setPage(Gx_page);
      }

      protected void add_metrics( )
      {
         add_metrics0( ) ;
      }

      protected void add_metrics0( )
      {
         getPrinter().setMetrics("Microsoft Sans Serif", false, false, 58, 14, 72, 171,  new int[] {48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 23, 36, 36, 57, 43, 12, 21, 21, 25, 37, 18, 21, 18, 18, 36, 36, 36, 36, 36, 36, 36, 36, 36, 36, 18, 18, 37, 37, 37, 36, 65, 43, 43, 46, 46, 43, 39, 50, 46, 18, 32, 43, 36, 53, 46, 50, 43, 50, 46, 43, 40, 46, 43, 64, 41, 42, 39, 18, 18, 18, 27, 36, 21, 36, 36, 32, 36, 36, 18, 36, 36, 14, 15, 33, 14, 55, 36, 36, 36, 36, 21, 32, 18, 36, 33, 47, 31, 31, 31, 21, 17, 21, 37, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 48, 18, 20, 36, 36, 36, 36, 17, 36, 21, 47, 24, 36, 37, 21, 47, 35, 26, 35, 21, 21, 21, 37, 34, 21, 21, 21, 23, 36, 53, 53, 53, 39, 43, 43, 43, 43, 43, 43, 64, 46, 43, 43, 43, 43, 18, 18, 18, 18, 46, 46, 50, 50, 50, 50, 50, 37, 50, 46, 46, 46, 46, 43, 43, 39, 36, 36, 36, 36, 36, 36, 57, 32, 36, 36, 36, 36, 18, 18, 18, 18, 36, 36, 36, 36, 36, 36, 36, 35, 39, 36, 36, 36, 36, 32, 36, 32}) ;
      }

      public override int getOutputType( )
      {
         return GxReportUtils.OUTPUT_PDF ;
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if (IsMain)	waitPrinterEnd();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         scmdbuf = "";
         P000E2_A25Date = new DateTime[] {DateTime.MinValue} ;
         P000E2_A40000ShowImage_GXI = new String[] {""} ;
         P000E2_A26Schedule = new DateTime[] {DateTime.MinValue} ;
         P000E2_A15ShowName = new String[] {""} ;
         P000E2_A14ShowId = new short[1] ;
         P000E2_A16ShowImage = new String[] {""} ;
         A25Date = DateTime.MinValue;
         A40000ShowImage_GXI = "";
         A26Schedule = (DateTime)(DateTime.MinValue);
         A15ShowName = "";
         A16ShowImage = "";
         sImgUrl = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.ashows_per_park_list__default(),
            new Object[][] {
                new Object[] {
               P000E2_A25Date, P000E2_A40000ShowImage_GXI, P000E2_A26Schedule, P000E2_A15ShowName, P000E2_A14ShowId, P000E2_A16ShowImage
               }
            }
         );
         /* GeneXus formulas. */
         Gx_line = 0;
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short GxWebError ;
      private short AV8ID_ParkK ;
      private short A14ShowId ;
      private int M_top ;
      private int M_bot ;
      private int Line ;
      private int ToSkip ;
      private int PrtOffset ;
      private int Gx_OldLine ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String scmdbuf ;
      private String A15ShowName ;
      private String sImgUrl ;
      private DateTime A26Schedule ;
      private DateTime AV9FechaShow ;
      private DateTime AV11FechaToP ;
      private DateTime A25Date ;
      private bool entryPointCalled ;
      private String A40000ShowImage_GXI ;
      private String A16ShowImage ;
      private IGxDataStore dsDefault ;
      private DateTime aP1_FechaToP ;
      private IDataStoreProvider pr_default ;
      private DateTime[] P000E2_A25Date ;
      private String[] P000E2_A40000ShowImage_GXI ;
      private DateTime[] P000E2_A26Schedule ;
      private String[] P000E2_A15ShowName ;
      private short[] P000E2_A14ShowId ;
      private String[] P000E2_A16ShowImage ;
   }

   public class ashows_per_park_list__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP000E2 ;
          prmP000E2 = new Object[] {
          new Object[] {"@AV9FechaShow",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV11FechaToP",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P000E2", "SELECT [Date], [ShowImage_GXI], [Schedule], [ShowName], [ShowId], [ShowImage] FROM [Show] WHERE ([Date] >= @AV9FechaShow) AND ([Date] <= @AV11FechaToP) ORDER BY [ShowId] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP000E2,100, GxCacheFrequency.OFF ,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((String[]) buf[1])[0] = rslt.getMultimediaUri(2) ;
                ((DateTime[]) buf[2])[0] = rslt.getGXDateTime(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((String[]) buf[5])[0] = rslt.getMultimediaFile(6, rslt.getVarchar(2)) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (DateTime)parms[1]);
                return;
       }
    }

 }

}
