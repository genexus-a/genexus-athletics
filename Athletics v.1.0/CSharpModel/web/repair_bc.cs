/*
               File: Repair_BC
        Description: Repair
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 5/31/2022 18:2:6.7
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class repair_bc : GXHttpHandler, IGxSilentTrn
   {
      public repair_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public repair_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow0C12( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey0C12( ) ;
         standaloneModal( ) ;
         AddRow0C12( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z29RepairId = A29RepairId;
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_0C0( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls0C12( ) ;
            }
            else
            {
               CheckExtendedTable0C12( ) ;
               if ( AnyError == 0 )
               {
                  ZM0C12( 6) ;
                  ZM0C12( 7) ;
                  ZM0C12( 8) ;
               }
               CloseExtendedTableCursors0C12( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode12 = Gx_mode;
            CONFIRM_0C13( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode12;
               IsConfirmed = 1;
            }
            /* Restore parent mode. */
            Gx_mode = sMode12;
         }
      }

      protected void CONFIRM_0C13( )
      {
         nGXsfl_13_idx = 0;
         while ( nGXsfl_13_idx < bcRepair.gxTpr_Kind.Count )
         {
            ReadRow0C13( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound13 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_13 != 0 ) )
            {
               GetKey0C13( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound13 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate0C13( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0C13( ) ;
                        if ( AnyError == 0 )
                        {
                        }
                        CloseExtendedTableCursors0C13( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound13 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey0C13( ) ;
                        Load0C13( ) ;
                        BeforeValidate0C13( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0C13( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_13 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate0C13( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0C13( ) ;
                              if ( AnyError == 0 )
                              {
                              }
                              CloseExtendedTableCursors0C13( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow13( ((SdtRepair_Kind)bcRepair.gxTpr_Kind.Item(nGXsfl_13_idx))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ZM0C12( short GX_JID )
      {
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
            Z30RepairDateFrom = A30RepairDateFrom;
            Z31RepairDaysQuantity = A31RepairDaysQuantity;
            Z33RepairCost = A33RepairCost;
            Z36RepairDiscountPercentage = A36RepairDiscountPercentage;
            Z18GameId = A18GameId;
            Z27TechnicianId = A27TechnicianId;
            Z40RepairTechnicianId = A40RepairTechnicianId;
            Z32RepairDateTo = A32RepairDateTo;
            Z37RepairFinalCost = A37RepairFinalCost;
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z19GameName = A19GameName;
            Z32RepairDateTo = A32RepairDateTo;
            Z37RepairFinalCost = A37RepairFinalCost;
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z28TechnicianName = A28TechnicianName;
            Z32RepairDateTo = A32RepairDateTo;
            Z37RepairFinalCost = A37RepairFinalCost;
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z41RepairTechniciaName = A41RepairTechniciaName;
            Z32RepairDateTo = A32RepairDateTo;
            Z37RepairFinalCost = A37RepairFinalCost;
         }
         if ( GX_JID == -5 )
         {
            Z29RepairId = A29RepairId;
            Z30RepairDateFrom = A30RepairDateFrom;
            Z31RepairDaysQuantity = A31RepairDaysQuantity;
            Z33RepairCost = A33RepairCost;
            Z36RepairDiscountPercentage = A36RepairDiscountPercentage;
            Z18GameId = A18GameId;
            Z27TechnicianId = A27TechnicianId;
            Z40RepairTechnicianId = A40RepairTechnicianId;
            Z19GameName = A19GameName;
            Z28TechnicianName = A28TechnicianName;
            Z41RepairTechniciaName = A41RepairTechniciaName;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load0C12( )
      {
         /* Using cursor BC000C9 */
         pr_default.execute(7, new Object[] {A29RepairId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound12 = 1;
            A30RepairDateFrom = BC000C9_A30RepairDateFrom[0];
            A31RepairDaysQuantity = BC000C9_A31RepairDaysQuantity[0];
            A19GameName = BC000C9_A19GameName[0];
            A33RepairCost = BC000C9_A33RepairCost[0];
            A28TechnicianName = BC000C9_A28TechnicianName[0];
            A41RepairTechniciaName = BC000C9_A41RepairTechniciaName[0];
            A36RepairDiscountPercentage = BC000C9_A36RepairDiscountPercentage[0];
            A18GameId = BC000C9_A18GameId[0];
            A27TechnicianId = BC000C9_A27TechnicianId[0];
            A40RepairTechnicianId = BC000C9_A40RepairTechnicianId[0];
            ZM0C12( -5) ;
         }
         pr_default.close(7);
         OnLoadActions0C12( ) ;
      }

      protected void OnLoadActions0C12( )
      {
         A32RepairDateTo = DateTimeUtil.DAdd(A30RepairDateFrom,+((int)(A31RepairDaysQuantity)));
         A37RepairFinalCost = (decimal)(A33RepairCost-((A33RepairCost*A36RepairDiscountPercentage)/ (decimal)(100)));
      }

      protected void CheckExtendedTable0C12( )
      {
         nIsDirty_12 = 0;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A30RepairDateFrom) || ( A30RepairDateFrom >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Repair Date From fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         nIsDirty_12 = 1;
         A32RepairDateTo = DateTimeUtil.DAdd(A30RepairDateFrom,+((int)(A31RepairDaysQuantity)));
         /* Using cursor BC000C6 */
         pr_default.execute(4, new Object[] {A18GameId});
         if ( (pr_default.getStatus(4) == 101) )
         {
            GX_msglist.addItem("No existe 'Game'.", "ForeignKeyNotFound", 1, "GAMEID");
            AnyError = 1;
         }
         A19GameName = BC000C6_A19GameName[0];
         pr_default.close(4);
         nIsDirty_12 = 1;
         A37RepairFinalCost = (decimal)(A33RepairCost-((A33RepairCost*A36RepairDiscountPercentage)/ (decimal)(100)));
         /* Using cursor BC000C7 */
         pr_default.execute(5, new Object[] {A27TechnicianId});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("No existe 'Technican'.", "ForeignKeyNotFound", 1, "TECHNICIANID");
            AnyError = 1;
         }
         A28TechnicianName = BC000C7_A28TechnicianName[0];
         pr_default.close(5);
         /* Using cursor BC000C8 */
         pr_default.execute(6, new Object[] {A40RepairTechnicianId});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No existe 'Repair Technician Alternative'.", "ForeignKeyNotFound", 1, "REPAIRTECHNICIANID");
            AnyError = 1;
         }
         A41RepairTechniciaName = BC000C8_A41RepairTechniciaName[0];
         pr_default.close(6);
      }

      protected void CloseExtendedTableCursors0C12( )
      {
         pr_default.close(4);
         pr_default.close(5);
         pr_default.close(6);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0C12( )
      {
         /* Using cursor BC000C10 */
         pr_default.execute(8, new Object[] {A29RepairId});
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound12 = 1;
         }
         else
         {
            RcdFound12 = 0;
         }
         pr_default.close(8);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC000C5 */
         pr_default.execute(3, new Object[] {A29RepairId});
         if ( (pr_default.getStatus(3) != 101) )
         {
            ZM0C12( 5) ;
            RcdFound12 = 1;
            A29RepairId = BC000C5_A29RepairId[0];
            A30RepairDateFrom = BC000C5_A30RepairDateFrom[0];
            A31RepairDaysQuantity = BC000C5_A31RepairDaysQuantity[0];
            A33RepairCost = BC000C5_A33RepairCost[0];
            A36RepairDiscountPercentage = BC000C5_A36RepairDiscountPercentage[0];
            A18GameId = BC000C5_A18GameId[0];
            A27TechnicianId = BC000C5_A27TechnicianId[0];
            A40RepairTechnicianId = BC000C5_A40RepairTechnicianId[0];
            Z29RepairId = A29RepairId;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load0C12( ) ;
            if ( AnyError == 1 )
            {
               RcdFound12 = 0;
               InitializeNonKey0C12( ) ;
            }
            Gx_mode = sMode12;
         }
         else
         {
            RcdFound12 = 0;
            InitializeNonKey0C12( ) ;
            sMode12 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode12;
         }
         pr_default.close(3);
      }

      protected void getEqualNoModal( )
      {
         GetKey0C12( ) ;
         if ( RcdFound12 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_0C0( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency0C12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000C4 */
            pr_default.execute(2, new Object[] {A29RepairId});
            if ( (pr_default.getStatus(2) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Repair"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(2) == 101) || ( Z30RepairDateFrom != BC000C4_A30RepairDateFrom[0] ) || ( Z31RepairDaysQuantity != BC000C4_A31RepairDaysQuantity[0] ) || ( Z33RepairCost != BC000C4_A33RepairCost[0] ) || ( Z36RepairDiscountPercentage != BC000C4_A36RepairDiscountPercentage[0] ) || ( Z18GameId != BC000C4_A18GameId[0] ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z27TechnicianId != BC000C4_A27TechnicianId[0] ) || ( Z40RepairTechnicianId != BC000C4_A40RepairTechnicianId[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Repair"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0C12( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0C12( 0) ;
            CheckOptimisticConcurrency0C12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0C12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000C11 */
                     pr_default.execute(9, new Object[] {A30RepairDateFrom, A31RepairDaysQuantity, A33RepairCost, A36RepairDiscountPercentage, A18GameId, A27TechnicianId, A40RepairTechnicianId});
                     A29RepairId = BC000C11_A29RepairId[0];
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Repair") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0C12( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0C12( ) ;
            }
            EndLevel0C12( ) ;
         }
         CloseExtendedTableCursors0C12( ) ;
      }

      protected void Update0C12( )
      {
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C12( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C12( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0C12( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000C12 */
                     pr_default.execute(10, new Object[] {A30RepairDateFrom, A31RepairDaysQuantity, A33RepairCost, A36RepairDiscountPercentage, A18GameId, A27TechnicianId, A40RepairTechnicianId, A29RepairId});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Repair") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Repair"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0C12( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0C12( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0C12( ) ;
         }
         CloseExtendedTableCursors0C12( ) ;
      }

      protected void DeferredUpdate0C12( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate0C12( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0C12( ) ;
            AfterConfirm0C12( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0C12( ) ;
               if ( AnyError == 0 )
               {
                  ScanKeyStart0C13( ) ;
                  while ( RcdFound13 != 0 )
                  {
                     getByPrimaryKey0C13( ) ;
                     Delete0C13( ) ;
                     ScanKeyNext0C13( ) ;
                  }
                  ScanKeyEnd0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000C13 */
                     pr_default.execute(11, new Object[] {A29RepairId});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Repair") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode12 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0C12( ) ;
         Gx_mode = sMode12;
      }

      protected void OnDeleteControls0C12( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            A32RepairDateTo = DateTimeUtil.DAdd(A30RepairDateFrom,+((int)(A31RepairDaysQuantity)));
            /* Using cursor BC000C14 */
            pr_default.execute(12, new Object[] {A18GameId});
            A19GameName = BC000C14_A19GameName[0];
            pr_default.close(12);
            /* Using cursor BC000C15 */
            pr_default.execute(13, new Object[] {A27TechnicianId});
            A28TechnicianName = BC000C15_A28TechnicianName[0];
            pr_default.close(13);
            /* Using cursor BC000C16 */
            pr_default.execute(14, new Object[] {A40RepairTechnicianId});
            A41RepairTechniciaName = BC000C16_A41RepairTechniciaName[0];
            pr_default.close(14);
            A37RepairFinalCost = (decimal)(A33RepairCost-((A33RepairCost*A36RepairDiscountPercentage)/ (decimal)(100)));
         }
      }

      protected void ProcessNestedLevel0C13( )
      {
         nGXsfl_13_idx = 0;
         while ( nGXsfl_13_idx < bcRepair.gxTpr_Kind.Count )
         {
            ReadRow0C13( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound13 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_13 != 0 ) )
            {
               standaloneNotModal0C13( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert0C13( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete0C13( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update0C13( ) ;
                  }
               }
            }
            KeyVarsToRow13( ((SdtRepair_Kind)bcRepair.gxTpr_Kind.Item(nGXsfl_13_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_13_idx = 0;
            while ( nGXsfl_13_idx < bcRepair.gxTpr_Kind.Count )
            {
               ReadRow0C13( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound13 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcRepair.gxTpr_Kind.RemoveElement(nGXsfl_13_idx);
                  nGXsfl_13_idx = (int)(nGXsfl_13_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey0C13( ) ;
                  VarsToRow13( ((SdtRepair_Kind)bcRepair.gxTpr_Kind.Item(nGXsfl_13_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0C13( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_13 = 0;
         nIsMod_13 = 0;
         Gxremove13 = 0;
      }

      protected void ProcessLevel0C12( )
      {
         /* Save parent mode. */
         sMode12 = Gx_mode;
         ProcessNestedLevel0C13( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode12;
         /* ' Update level parameters */
      }

      protected void EndLevel0C12( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(2);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0C12( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0C12( )
      {
         /* Using cursor BC000C17 */
         pr_default.execute(15, new Object[] {A29RepairId});
         RcdFound12 = 0;
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound12 = 1;
            A29RepairId = BC000C17_A29RepairId[0];
            A30RepairDateFrom = BC000C17_A30RepairDateFrom[0];
            A31RepairDaysQuantity = BC000C17_A31RepairDaysQuantity[0];
            A19GameName = BC000C17_A19GameName[0];
            A33RepairCost = BC000C17_A33RepairCost[0];
            A28TechnicianName = BC000C17_A28TechnicianName[0];
            A41RepairTechniciaName = BC000C17_A41RepairTechniciaName[0];
            A36RepairDiscountPercentage = BC000C17_A36RepairDiscountPercentage[0];
            A18GameId = BC000C17_A18GameId[0];
            A27TechnicianId = BC000C17_A27TechnicianId[0];
            A40RepairTechnicianId = BC000C17_A40RepairTechnicianId[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0C12( )
      {
         /* Scan next routine */
         pr_default.readNext(15);
         RcdFound12 = 0;
         ScanKeyLoad0C12( ) ;
      }

      protected void ScanKeyLoad0C12( )
      {
         sMode12 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound12 = 1;
            A29RepairId = BC000C17_A29RepairId[0];
            A30RepairDateFrom = BC000C17_A30RepairDateFrom[0];
            A31RepairDaysQuantity = BC000C17_A31RepairDaysQuantity[0];
            A19GameName = BC000C17_A19GameName[0];
            A33RepairCost = BC000C17_A33RepairCost[0];
            A28TechnicianName = BC000C17_A28TechnicianName[0];
            A41RepairTechniciaName = BC000C17_A41RepairTechniciaName[0];
            A36RepairDiscountPercentage = BC000C17_A36RepairDiscountPercentage[0];
            A18GameId = BC000C17_A18GameId[0];
            A27TechnicianId = BC000C17_A27TechnicianId[0];
            A40RepairTechnicianId = BC000C17_A40RepairTechnicianId[0];
         }
         Gx_mode = sMode12;
      }

      protected void ScanKeyEnd0C12( )
      {
         pr_default.close(15);
      }

      protected void AfterConfirm0C12( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0C12( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0C12( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0C12( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0C12( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0C12( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0C12( )
      {
      }

      protected void ZM0C13( short GX_JID )
      {
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
            Z43RepairKindName = A43RepairKindName;
            Z44RepairKindRemaks = A44RepairKindRemaks;
         }
         if ( GX_JID == -9 )
         {
            Z29RepairId = A29RepairId;
            Z42RepairKindId = A42RepairKindId;
            Z43RepairKindName = A43RepairKindName;
            Z44RepairKindRemaks = A44RepairKindRemaks;
         }
      }

      protected void standaloneNotModal0C13( )
      {
      }

      protected void standaloneModal0C13( )
      {
      }

      protected void Load0C13( )
      {
         /* Using cursor BC000C18 */
         pr_default.execute(16, new Object[] {A29RepairId, A42RepairKindId});
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound13 = 1;
            A43RepairKindName = BC000C18_A43RepairKindName[0];
            A44RepairKindRemaks = BC000C18_A44RepairKindRemaks[0];
            ZM0C13( -9) ;
         }
         pr_default.close(16);
         OnLoadActions0C13( ) ;
      }

      protected void OnLoadActions0C13( )
      {
      }

      protected void CheckExtendedTable0C13( )
      {
         nIsDirty_13 = 0;
         Gx_BScreen = 1;
         standaloneModal0C13( ) ;
         Gx_BScreen = 0;
         if ( ! ( ( StringUtil.StrCmp(A43RepairKindName, "E") == 0 ) || ( StringUtil.StrCmp(A43RepairKindName, "M") == 0 ) || ( StringUtil.StrCmp(A43RepairKindName, "R") == 0 ) ) )
         {
            GX_msglist.addItem("Campo Repair Kind Name fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors0C13( )
      {
      }

      protected void enableDisable0C13( )
      {
      }

      protected void GetKey0C13( )
      {
         /* Using cursor BC000C19 */
         pr_default.execute(17, new Object[] {A29RepairId, A42RepairKindId});
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound13 = 1;
         }
         else
         {
            RcdFound13 = 0;
         }
         pr_default.close(17);
      }

      protected void getByPrimaryKey0C13( )
      {
         /* Using cursor BC000C3 */
         pr_default.execute(1, new Object[] {A29RepairId, A42RepairKindId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0C13( 9) ;
            RcdFound13 = 1;
            InitializeNonKey0C13( ) ;
            A42RepairKindId = BC000C3_A42RepairKindId[0];
            A43RepairKindName = BC000C3_A43RepairKindName[0];
            A44RepairKindRemaks = BC000C3_A44RepairKindRemaks[0];
            Z29RepairId = A29RepairId;
            Z42RepairKindId = A42RepairKindId;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal0C13( ) ;
            Load0C13( ) ;
            Gx_mode = sMode13;
         }
         else
         {
            RcdFound13 = 0;
            InitializeNonKey0C13( ) ;
            sMode13 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal0C13( ) ;
            Gx_mode = sMode13;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes0C13( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC000C2 */
            pr_default.execute(0, new Object[] {A29RepairId, A42RepairKindId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RepairKind"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z43RepairKindName, BC000C2_A43RepairKindName[0]) != 0 ) || ( StringUtil.StrCmp(Z44RepairKindRemaks, BC000C2_A44RepairKindRemaks[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"RepairKind"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0C13( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0C13( 0) ;
            CheckOptimisticConcurrency0C13( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C13( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000C20 */
                     pr_default.execute(18, new Object[] {A29RepairId, A42RepairKindId, A43RepairKindName, A44RepairKindRemaks});
                     pr_default.close(18);
                     dsDefault.SmartCacheProvider.SetUpdated("RepairKind") ;
                     if ( (pr_default.getStatus(18) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0C13( ) ;
            }
            EndLevel0C13( ) ;
         }
         CloseExtendedTableCursors0C13( ) ;
      }

      protected void Update0C13( )
      {
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C13( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0C13( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0C13( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000C21 */
                     pr_default.execute(19, new Object[] {A43RepairKindName, A44RepairKindRemaks, A29RepairId, A42RepairKindId});
                     pr_default.close(19);
                     dsDefault.SmartCacheProvider.SetUpdated("RepairKind") ;
                     if ( (pr_default.getStatus(19) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RepairKind"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0C13( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey0C13( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0C13( ) ;
         }
         CloseExtendedTableCursors0C13( ) ;
      }

      protected void DeferredUpdate0C13( )
      {
      }

      protected void Delete0C13( )
      {
         Gx_mode = "DLT";
         BeforeValidate0C13( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0C13( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0C13( ) ;
            AfterConfirm0C13( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0C13( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000C22 */
                  pr_default.execute(20, new Object[] {A29RepairId, A42RepairKindId});
                  pr_default.close(20);
                  dsDefault.SmartCacheProvider.SetUpdated("RepairKind") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode13 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel0C13( ) ;
         Gx_mode = sMode13;
      }

      protected void OnDeleteControls0C13( )
      {
         standaloneModal0C13( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel0C13( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart0C13( )
      {
         /* Scan By routine */
         /* Using cursor BC000C23 */
         pr_default.execute(21, new Object[] {A29RepairId});
         RcdFound13 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound13 = 1;
            A42RepairKindId = BC000C23_A42RepairKindId[0];
            A43RepairKindName = BC000C23_A43RepairKindName[0];
            A44RepairKindRemaks = BC000C23_A44RepairKindRemaks[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext0C13( )
      {
         /* Scan next routine */
         pr_default.readNext(21);
         RcdFound13 = 0;
         ScanKeyLoad0C13( ) ;
      }

      protected void ScanKeyLoad0C13( )
      {
         sMode13 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound13 = 1;
            A42RepairKindId = BC000C23_A42RepairKindId[0];
            A43RepairKindName = BC000C23_A43RepairKindName[0];
            A44RepairKindRemaks = BC000C23_A44RepairKindRemaks[0];
         }
         Gx_mode = sMode13;
      }

      protected void ScanKeyEnd0C13( )
      {
         pr_default.close(21);
      }

      protected void AfterConfirm0C13( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0C13( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0C13( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0C13( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0C13( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0C13( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0C13( )
      {
      }

      protected void send_integrity_lvl_hashes0C13( )
      {
      }

      protected void send_integrity_lvl_hashes0C12( )
      {
      }

      protected void AddRow0C12( )
      {
         VarsToRow12( bcRepair) ;
      }

      protected void ReadRow0C12( )
      {
         RowToVars12( bcRepair, 1) ;
      }

      protected void AddRow0C13( )
      {
         SdtRepair_Kind obj13 ;
         obj13 = new SdtRepair_Kind(context);
         VarsToRow13( obj13) ;
         bcRepair.gxTpr_Kind.Add(obj13, 0);
         obj13.gxTpr_Mode = "UPD";
         obj13.gxTpr_Modified = 0;
      }

      protected void ReadRow0C13( )
      {
         nGXsfl_13_idx = (int)(nGXsfl_13_idx+1);
         RowToVars13( ((SdtRepair_Kind)bcRepair.gxTpr_Kind.Item(nGXsfl_13_idx)), 1) ;
      }

      protected void InitializeNonKey0C12( )
      {
         A37RepairFinalCost = 0;
         A32RepairDateTo = DateTime.MinValue;
         A30RepairDateFrom = DateTime.MinValue;
         A31RepairDaysQuantity = 0;
         A18GameId = 0;
         A19GameName = "";
         A33RepairCost = 0;
         A27TechnicianId = 0;
         A28TechnicianName = "";
         A40RepairTechnicianId = 0;
         A41RepairTechniciaName = "";
         A36RepairDiscountPercentage = 0;
         Z30RepairDateFrom = DateTime.MinValue;
         Z31RepairDaysQuantity = 0;
         Z33RepairCost = 0;
         Z36RepairDiscountPercentage = 0;
         Z18GameId = 0;
         Z27TechnicianId = 0;
         Z40RepairTechnicianId = 0;
      }

      protected void InitAll0C12( )
      {
         A29RepairId = 0;
         InitializeNonKey0C12( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey0C13( )
      {
         A43RepairKindName = "";
         A44RepairKindRemaks = "";
         Z43RepairKindName = "";
         Z44RepairKindRemaks = "";
      }

      protected void InitAll0C13( )
      {
         A42RepairKindId = 0;
         InitializeNonKey0C13( ) ;
      }

      protected void StandaloneModalInsert0C13( )
      {
      }

      public void VarsToRow12( SdtRepair obj12 )
      {
         obj12.gxTpr_Mode = Gx_mode;
         obj12.gxTpr_Repairfinalcost = A37RepairFinalCost;
         obj12.gxTpr_Repairdateto = A32RepairDateTo;
         obj12.gxTpr_Repairdatefrom = A30RepairDateFrom;
         obj12.gxTpr_Repairdaysquantity = A31RepairDaysQuantity;
         obj12.gxTpr_Gameid = A18GameId;
         obj12.gxTpr_Gamename = A19GameName;
         obj12.gxTpr_Repaircost = A33RepairCost;
         obj12.gxTpr_Technicianid = A27TechnicianId;
         obj12.gxTpr_Technicianname = A28TechnicianName;
         obj12.gxTpr_Repairtechnicianid = A40RepairTechnicianId;
         obj12.gxTpr_Repairtechnicianame = A41RepairTechniciaName;
         obj12.gxTpr_Repairdiscountpercentage = A36RepairDiscountPercentage;
         obj12.gxTpr_Repairid = A29RepairId;
         obj12.gxTpr_Repairid_Z = Z29RepairId;
         obj12.gxTpr_Repairdatefrom_Z = Z30RepairDateFrom;
         obj12.gxTpr_Repairdaysquantity_Z = Z31RepairDaysQuantity;
         obj12.gxTpr_Repairdateto_Z = Z32RepairDateTo;
         obj12.gxTpr_Gameid_Z = Z18GameId;
         obj12.gxTpr_Gamename_Z = Z19GameName;
         obj12.gxTpr_Repaircost_Z = Z33RepairCost;
         obj12.gxTpr_Technicianid_Z = Z27TechnicianId;
         obj12.gxTpr_Technicianname_Z = Z28TechnicianName;
         obj12.gxTpr_Repairtechnicianid_Z = Z40RepairTechnicianId;
         obj12.gxTpr_Repairtechnicianame_Z = Z41RepairTechniciaName;
         obj12.gxTpr_Repairdiscountpercentage_Z = Z36RepairDiscountPercentage;
         obj12.gxTpr_Repairfinalcost_Z = Z37RepairFinalCost;
         obj12.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow12( SdtRepair obj12 )
      {
         obj12.gxTpr_Repairid = A29RepairId;
         return  ;
      }

      public void RowToVars12( SdtRepair obj12 ,
                               int forceLoad )
      {
         Gx_mode = obj12.gxTpr_Mode;
         A37RepairFinalCost = obj12.gxTpr_Repairfinalcost;
         A32RepairDateTo = obj12.gxTpr_Repairdateto;
         A30RepairDateFrom = obj12.gxTpr_Repairdatefrom;
         A31RepairDaysQuantity = obj12.gxTpr_Repairdaysquantity;
         A18GameId = obj12.gxTpr_Gameid;
         A19GameName = obj12.gxTpr_Gamename;
         A33RepairCost = obj12.gxTpr_Repaircost;
         A27TechnicianId = obj12.gxTpr_Technicianid;
         A28TechnicianName = obj12.gxTpr_Technicianname;
         A40RepairTechnicianId = obj12.gxTpr_Repairtechnicianid;
         A41RepairTechniciaName = obj12.gxTpr_Repairtechnicianame;
         A36RepairDiscountPercentage = obj12.gxTpr_Repairdiscountpercentage;
         A29RepairId = obj12.gxTpr_Repairid;
         Z29RepairId = obj12.gxTpr_Repairid_Z;
         Z30RepairDateFrom = obj12.gxTpr_Repairdatefrom_Z;
         Z31RepairDaysQuantity = obj12.gxTpr_Repairdaysquantity_Z;
         Z32RepairDateTo = obj12.gxTpr_Repairdateto_Z;
         Z18GameId = obj12.gxTpr_Gameid_Z;
         Z19GameName = obj12.gxTpr_Gamename_Z;
         Z33RepairCost = obj12.gxTpr_Repaircost_Z;
         Z27TechnicianId = obj12.gxTpr_Technicianid_Z;
         Z28TechnicianName = obj12.gxTpr_Technicianname_Z;
         Z40RepairTechnicianId = obj12.gxTpr_Repairtechnicianid_Z;
         Z41RepairTechniciaName = obj12.gxTpr_Repairtechnicianame_Z;
         Z36RepairDiscountPercentage = obj12.gxTpr_Repairdiscountpercentage_Z;
         Z37RepairFinalCost = obj12.gxTpr_Repairfinalcost_Z;
         Gx_mode = obj12.gxTpr_Mode;
         return  ;
      }

      public void VarsToRow13( SdtRepair_Kind obj13 )
      {
         obj13.gxTpr_Mode = Gx_mode;
         obj13.gxTpr_Repairkindname = A43RepairKindName;
         obj13.gxTpr_Repairkindremaks = A44RepairKindRemaks;
         obj13.gxTpr_Repairkindid = A42RepairKindId;
         obj13.gxTpr_Repairkindid_Z = Z42RepairKindId;
         obj13.gxTpr_Repairkindname_Z = Z43RepairKindName;
         obj13.gxTpr_Repairkindremaks_Z = Z44RepairKindRemaks;
         obj13.gxTpr_Modified = nIsMod_13;
         return  ;
      }

      public void KeyVarsToRow13( SdtRepair_Kind obj13 )
      {
         obj13.gxTpr_Repairkindid = A42RepairKindId;
         return  ;
      }

      public void RowToVars13( SdtRepair_Kind obj13 ,
                               int forceLoad )
      {
         Gx_mode = obj13.gxTpr_Mode;
         A43RepairKindName = obj13.gxTpr_Repairkindname;
         A44RepairKindRemaks = obj13.gxTpr_Repairkindremaks;
         A42RepairKindId = obj13.gxTpr_Repairkindid;
         Z42RepairKindId = obj13.gxTpr_Repairkindid_Z;
         Z43RepairKindName = obj13.gxTpr_Repairkindname_Z;
         Z44RepairKindRemaks = obj13.gxTpr_Repairkindremaks_Z;
         nIsMod_13 = obj13.gxTpr_Modified;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A29RepairId = (short)getParm(obj,0);
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey0C12( ) ;
         ScanKeyStart0C12( ) ;
         if ( RcdFound12 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z29RepairId = A29RepairId;
         }
         ZM0C12( -5) ;
         OnLoadActions0C12( ) ;
         AddRow0C12( ) ;
         bcRepair.gxTpr_Kind.ClearCollection();
         if ( RcdFound12 == 1 )
         {
            ScanKeyStart0C13( ) ;
            nGXsfl_13_idx = 1;
            while ( RcdFound13 != 0 )
            {
               Z29RepairId = A29RepairId;
               Z42RepairKindId = A42RepairKindId;
               ZM0C13( -9) ;
               OnLoadActions0C13( ) ;
               nRcdExists_13 = 1;
               nIsMod_13 = 0;
               AddRow0C13( ) ;
               nGXsfl_13_idx = (int)(nGXsfl_13_idx+1);
               ScanKeyNext0C13( ) ;
            }
            ScanKeyEnd0C13( ) ;
         }
         ScanKeyEnd0C12( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars12( bcRepair, 0) ;
         ScanKeyStart0C12( ) ;
         if ( RcdFound12 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z29RepairId = A29RepairId;
         }
         ZM0C12( -5) ;
         OnLoadActions0C12( ) ;
         AddRow0C12( ) ;
         bcRepair.gxTpr_Kind.ClearCollection();
         if ( RcdFound12 == 1 )
         {
            ScanKeyStart0C13( ) ;
            nGXsfl_13_idx = 1;
            while ( RcdFound13 != 0 )
            {
               Z29RepairId = A29RepairId;
               Z42RepairKindId = A42RepairKindId;
               ZM0C13( -9) ;
               OnLoadActions0C13( ) ;
               nRcdExists_13 = 1;
               nIsMod_13 = 0;
               AddRow0C13( ) ;
               nGXsfl_13_idx = (int)(nGXsfl_13_idx+1);
               ScanKeyNext0C13( ) ;
            }
            ScanKeyEnd0C13( ) ;
         }
         ScanKeyEnd0C12( ) ;
         if ( RcdFound12 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      protected void SaveImpl( )
      {
         nKeyPressed = 1;
         GetKey0C12( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert0C12( ) ;
         }
         else
         {
            if ( RcdFound12 == 1 )
            {
               if ( A29RepairId != Z29RepairId )
               {
                  A29RepairId = Z29RepairId;
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update0C12( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A29RepairId != Z29RepairId )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0C12( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert0C12( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars12( bcRepair, 0) ;
         SaveImpl( ) ;
         VarsToRow12( bcRepair) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public bool Insert( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars12( bcRepair, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert0C12( ) ;
         AfterTrn( ) ;
         VarsToRow12( bcRepair) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      protected void UpdateImpl( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            SaveImpl( ) ;
         }
         else
         {
            SdtRepair auxBC = new SdtRepair(context) ;
            IGxSilentTrn auxTrn = auxBC.getTransaction() ;
            auxBC.Load(A29RepairId);
            if ( auxTrn.Errors() == 0 )
            {
               auxBC.UpdateDirties(bcRepair);
               auxBC.Save();
            }
            LclMsgLst = (msglist)(auxTrn.GetMessages());
            AnyError = (short)(auxTrn.Errors());
            context.GX_msglist = LclMsgLst;
            if ( auxTrn.Errors() == 0 )
            {
               Gx_mode = auxTrn.GetMode();
               AfterTrn( ) ;
            }
         }
      }

      public bool Update( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars12( bcRepair, 0) ;
         UpdateImpl( ) ;
         VarsToRow12( bcRepair) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public bool InsertOrUpdate( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars12( bcRepair, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert0C12( ) ;
         if ( AnyError == 1 )
         {
            if ( StringUtil.StrCmp(context.GX_msglist.getItemValue(1), "DuplicatePrimaryKey") == 0 )
            {
               AnyError = 0;
               context.GX_msglist.removeAllItems();
               UpdateImpl( ) ;
            }
         }
         else
         {
            AfterTrn( ) ;
         }
         VarsToRow12( bcRepair) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars12( bcRepair, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey0C12( ) ;
         if ( RcdFound12 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A29RepairId != Z29RepairId )
            {
               A29RepairId = Z29RepairId;
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A29RepairId != Z29RepairId )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(3);
         pr_default.close(1);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(14);
         context.RollbackDataStores("repair_bc",pr_default);
         VarsToRow12( bcRepair) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcRepair.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcRepair.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcRepair )
         {
            bcRepair = (SdtRepair)(sdt);
            if ( StringUtil.StrCmp(bcRepair.gxTpr_Mode, "") == 0 )
            {
               bcRepair.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow12( bcRepair) ;
            }
            else
            {
               RowToVars12( bcRepair, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcRepair.gxTpr_Mode, "") == 0 )
            {
               bcRepair.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars12( bcRepair, 1) ;
         return  ;
      }

      public void ForceCommitOnExit( )
      {
         mustCommit = true;
         return  ;
      }

      public SdtRepair Repair_BC
      {
         get {
            return bcRepair ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(3);
         pr_default.close(12);
         pr_default.close(13);
         pr_default.close(14);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         sMode12 = "";
         Z30RepairDateFrom = DateTime.MinValue;
         A30RepairDateFrom = DateTime.MinValue;
         Z32RepairDateTo = DateTime.MinValue;
         A32RepairDateTo = DateTime.MinValue;
         Z19GameName = "";
         A19GameName = "";
         Z28TechnicianName = "";
         A28TechnicianName = "";
         Z41RepairTechniciaName = "";
         A41RepairTechniciaName = "";
         BC000C9_A29RepairId = new short[1] ;
         BC000C9_A30RepairDateFrom = new DateTime[] {DateTime.MinValue} ;
         BC000C9_A31RepairDaysQuantity = new short[1] ;
         BC000C9_A19GameName = new String[] {""} ;
         BC000C9_A33RepairCost = new decimal[1] ;
         BC000C9_A28TechnicianName = new String[] {""} ;
         BC000C9_A41RepairTechniciaName = new String[] {""} ;
         BC000C9_A36RepairDiscountPercentage = new short[1] ;
         BC000C9_A18GameId = new short[1] ;
         BC000C9_A27TechnicianId = new short[1] ;
         BC000C9_A40RepairTechnicianId = new short[1] ;
         BC000C6_A19GameName = new String[] {""} ;
         BC000C7_A28TechnicianName = new String[] {""} ;
         BC000C8_A41RepairTechniciaName = new String[] {""} ;
         BC000C10_A29RepairId = new short[1] ;
         BC000C5_A29RepairId = new short[1] ;
         BC000C5_A30RepairDateFrom = new DateTime[] {DateTime.MinValue} ;
         BC000C5_A31RepairDaysQuantity = new short[1] ;
         BC000C5_A33RepairCost = new decimal[1] ;
         BC000C5_A36RepairDiscountPercentage = new short[1] ;
         BC000C5_A18GameId = new short[1] ;
         BC000C5_A27TechnicianId = new short[1] ;
         BC000C5_A40RepairTechnicianId = new short[1] ;
         BC000C4_A29RepairId = new short[1] ;
         BC000C4_A30RepairDateFrom = new DateTime[] {DateTime.MinValue} ;
         BC000C4_A31RepairDaysQuantity = new short[1] ;
         BC000C4_A33RepairCost = new decimal[1] ;
         BC000C4_A36RepairDiscountPercentage = new short[1] ;
         BC000C4_A18GameId = new short[1] ;
         BC000C4_A27TechnicianId = new short[1] ;
         BC000C4_A40RepairTechnicianId = new short[1] ;
         BC000C11_A29RepairId = new short[1] ;
         BC000C14_A19GameName = new String[] {""} ;
         BC000C15_A28TechnicianName = new String[] {""} ;
         BC000C16_A41RepairTechniciaName = new String[] {""} ;
         BC000C17_A29RepairId = new short[1] ;
         BC000C17_A30RepairDateFrom = new DateTime[] {DateTime.MinValue} ;
         BC000C17_A31RepairDaysQuantity = new short[1] ;
         BC000C17_A19GameName = new String[] {""} ;
         BC000C17_A33RepairCost = new decimal[1] ;
         BC000C17_A28TechnicianName = new String[] {""} ;
         BC000C17_A41RepairTechniciaName = new String[] {""} ;
         BC000C17_A36RepairDiscountPercentage = new short[1] ;
         BC000C17_A18GameId = new short[1] ;
         BC000C17_A27TechnicianId = new short[1] ;
         BC000C17_A40RepairTechnicianId = new short[1] ;
         Z43RepairKindName = "";
         A43RepairKindName = "";
         Z44RepairKindRemaks = "";
         A44RepairKindRemaks = "";
         BC000C18_A29RepairId = new short[1] ;
         BC000C18_A42RepairKindId = new short[1] ;
         BC000C18_A43RepairKindName = new String[] {""} ;
         BC000C18_A44RepairKindRemaks = new String[] {""} ;
         BC000C19_A29RepairId = new short[1] ;
         BC000C19_A42RepairKindId = new short[1] ;
         BC000C3_A29RepairId = new short[1] ;
         BC000C3_A42RepairKindId = new short[1] ;
         BC000C3_A43RepairKindName = new String[] {""} ;
         BC000C3_A44RepairKindRemaks = new String[] {""} ;
         sMode13 = "";
         BC000C2_A29RepairId = new short[1] ;
         BC000C2_A42RepairKindId = new short[1] ;
         BC000C2_A43RepairKindName = new String[] {""} ;
         BC000C2_A44RepairKindRemaks = new String[] {""} ;
         BC000C23_A29RepairId = new short[1] ;
         BC000C23_A42RepairKindId = new short[1] ;
         BC000C23_A43RepairKindName = new String[] {""} ;
         BC000C23_A44RepairKindRemaks = new String[] {""} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.repair_bc__default(),
            new Object[][] {
                new Object[] {
               BC000C2_A29RepairId, BC000C2_A42RepairKindId, BC000C2_A43RepairKindName, BC000C2_A44RepairKindRemaks
               }
               , new Object[] {
               BC000C3_A29RepairId, BC000C3_A42RepairKindId, BC000C3_A43RepairKindName, BC000C3_A44RepairKindRemaks
               }
               , new Object[] {
               BC000C4_A29RepairId, BC000C4_A30RepairDateFrom, BC000C4_A31RepairDaysQuantity, BC000C4_A33RepairCost, BC000C4_A36RepairDiscountPercentage, BC000C4_A18GameId, BC000C4_A27TechnicianId, BC000C4_A40RepairTechnicianId
               }
               , new Object[] {
               BC000C5_A29RepairId, BC000C5_A30RepairDateFrom, BC000C5_A31RepairDaysQuantity, BC000C5_A33RepairCost, BC000C5_A36RepairDiscountPercentage, BC000C5_A18GameId, BC000C5_A27TechnicianId, BC000C5_A40RepairTechnicianId
               }
               , new Object[] {
               BC000C6_A19GameName
               }
               , new Object[] {
               BC000C7_A28TechnicianName
               }
               , new Object[] {
               BC000C8_A41RepairTechniciaName
               }
               , new Object[] {
               BC000C9_A29RepairId, BC000C9_A30RepairDateFrom, BC000C9_A31RepairDaysQuantity, BC000C9_A19GameName, BC000C9_A33RepairCost, BC000C9_A28TechnicianName, BC000C9_A41RepairTechniciaName, BC000C9_A36RepairDiscountPercentage, BC000C9_A18GameId, BC000C9_A27TechnicianId,
               BC000C9_A40RepairTechnicianId
               }
               , new Object[] {
               BC000C10_A29RepairId
               }
               , new Object[] {
               BC000C11_A29RepairId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000C14_A19GameName
               }
               , new Object[] {
               BC000C15_A28TechnicianName
               }
               , new Object[] {
               BC000C16_A41RepairTechniciaName
               }
               , new Object[] {
               BC000C17_A29RepairId, BC000C17_A30RepairDateFrom, BC000C17_A31RepairDaysQuantity, BC000C17_A19GameName, BC000C17_A33RepairCost, BC000C17_A28TechnicianName, BC000C17_A41RepairTechniciaName, BC000C17_A36RepairDiscountPercentage, BC000C17_A18GameId, BC000C17_A27TechnicianId,
               BC000C17_A40RepairTechnicianId
               }
               , new Object[] {
               BC000C18_A29RepairId, BC000C18_A42RepairKindId, BC000C18_A43RepairKindName, BC000C18_A44RepairKindRemaks
               }
               , new Object[] {
               BC000C19_A29RepairId, BC000C19_A42RepairKindId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000C23_A29RepairId, BC000C23_A42RepairKindId, BC000C23_A43RepairKindName, BC000C23_A44RepairKindRemaks
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short Z29RepairId ;
      private short A29RepairId ;
      private short nIsMod_13 ;
      private short RcdFound13 ;
      private short GX_JID ;
      private short Z31RepairDaysQuantity ;
      private short A31RepairDaysQuantity ;
      private short Z36RepairDiscountPercentage ;
      private short A36RepairDiscountPercentage ;
      private short Z18GameId ;
      private short A18GameId ;
      private short Z27TechnicianId ;
      private short A27TechnicianId ;
      private short Z40RepairTechnicianId ;
      private short A40RepairTechnicianId ;
      private short RcdFound12 ;
      private short nIsDirty_12 ;
      private short nRcdExists_13 ;
      private short Gxremove13 ;
      private short Z42RepairKindId ;
      private short A42RepairKindId ;
      private short nIsDirty_13 ;
      private short Gx_BScreen ;
      private int trnEnded ;
      private int nGXsfl_13_idx=1 ;
      private decimal Z33RepairCost ;
      private decimal A33RepairCost ;
      private decimal Z37RepairFinalCost ;
      private decimal A37RepairFinalCost ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode12 ;
      private String Z19GameName ;
      private String A19GameName ;
      private String Z28TechnicianName ;
      private String A28TechnicianName ;
      private String Z41RepairTechniciaName ;
      private String A41RepairTechniciaName ;
      private String Z43RepairKindName ;
      private String A43RepairKindName ;
      private String Z44RepairKindRemaks ;
      private String A44RepairKindRemaks ;
      private String sMode13 ;
      private DateTime Z30RepairDateFrom ;
      private DateTime A30RepairDateFrom ;
      private DateTime Z32RepairDateTo ;
      private DateTime A32RepairDateTo ;
      private bool Gx_longc ;
      private bool mustCommit ;
      private SdtRepair bcRepair ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] BC000C9_A29RepairId ;
      private DateTime[] BC000C9_A30RepairDateFrom ;
      private short[] BC000C9_A31RepairDaysQuantity ;
      private String[] BC000C9_A19GameName ;
      private decimal[] BC000C9_A33RepairCost ;
      private String[] BC000C9_A28TechnicianName ;
      private String[] BC000C9_A41RepairTechniciaName ;
      private short[] BC000C9_A36RepairDiscountPercentage ;
      private short[] BC000C9_A18GameId ;
      private short[] BC000C9_A27TechnicianId ;
      private short[] BC000C9_A40RepairTechnicianId ;
      private String[] BC000C6_A19GameName ;
      private String[] BC000C7_A28TechnicianName ;
      private String[] BC000C8_A41RepairTechniciaName ;
      private short[] BC000C10_A29RepairId ;
      private short[] BC000C5_A29RepairId ;
      private DateTime[] BC000C5_A30RepairDateFrom ;
      private short[] BC000C5_A31RepairDaysQuantity ;
      private decimal[] BC000C5_A33RepairCost ;
      private short[] BC000C5_A36RepairDiscountPercentage ;
      private short[] BC000C5_A18GameId ;
      private short[] BC000C5_A27TechnicianId ;
      private short[] BC000C5_A40RepairTechnicianId ;
      private short[] BC000C4_A29RepairId ;
      private DateTime[] BC000C4_A30RepairDateFrom ;
      private short[] BC000C4_A31RepairDaysQuantity ;
      private decimal[] BC000C4_A33RepairCost ;
      private short[] BC000C4_A36RepairDiscountPercentage ;
      private short[] BC000C4_A18GameId ;
      private short[] BC000C4_A27TechnicianId ;
      private short[] BC000C4_A40RepairTechnicianId ;
      private short[] BC000C11_A29RepairId ;
      private String[] BC000C14_A19GameName ;
      private String[] BC000C15_A28TechnicianName ;
      private String[] BC000C16_A41RepairTechniciaName ;
      private short[] BC000C17_A29RepairId ;
      private DateTime[] BC000C17_A30RepairDateFrom ;
      private short[] BC000C17_A31RepairDaysQuantity ;
      private String[] BC000C17_A19GameName ;
      private decimal[] BC000C17_A33RepairCost ;
      private String[] BC000C17_A28TechnicianName ;
      private String[] BC000C17_A41RepairTechniciaName ;
      private short[] BC000C17_A36RepairDiscountPercentage ;
      private short[] BC000C17_A18GameId ;
      private short[] BC000C17_A27TechnicianId ;
      private short[] BC000C17_A40RepairTechnicianId ;
      private short[] BC000C18_A29RepairId ;
      private short[] BC000C18_A42RepairKindId ;
      private String[] BC000C18_A43RepairKindName ;
      private String[] BC000C18_A44RepairKindRemaks ;
      private short[] BC000C19_A29RepairId ;
      private short[] BC000C19_A42RepairKindId ;
      private short[] BC000C3_A29RepairId ;
      private short[] BC000C3_A42RepairKindId ;
      private String[] BC000C3_A43RepairKindName ;
      private String[] BC000C3_A44RepairKindRemaks ;
      private short[] BC000C2_A29RepairId ;
      private short[] BC000C2_A42RepairKindId ;
      private String[] BC000C2_A43RepairKindName ;
      private String[] BC000C2_A44RepairKindRemaks ;
      private short[] BC000C23_A29RepairId ;
      private short[] BC000C23_A42RepairKindId ;
      private String[] BC000C23_A43RepairKindName ;
      private String[] BC000C23_A44RepairKindRemaks ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
   }

   public class repair_bc__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new ForEachCursor(def[16])
         ,new ForEachCursor(def[17])
         ,new UpdateCursor(def[18])
         ,new UpdateCursor(def[19])
         ,new UpdateCursor(def[20])
         ,new ForEachCursor(def[21])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmBC000C9 ;
          prmBC000C9 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C6 ;
          prmBC000C6 = new Object[] {
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C7 ;
          prmBC000C7 = new Object[] {
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C8 ;
          prmBC000C8 = new Object[] {
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C10 ;
          prmBC000C10 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C5 ;
          prmBC000C5 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C4 ;
          prmBC000C4 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C11 ;
          prmBC000C11 = new Object[] {
          new Object[] {"@RepairDateFrom",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RepairDaysQuantity",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairCost",SqlDbType.Decimal,8,2} ,
          new Object[] {"@RepairDiscountPercentage",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C12 ;
          prmBC000C12 = new Object[] {
          new Object[] {"@RepairDateFrom",SqlDbType.DateTime,8,0} ,
          new Object[] {"@RepairDaysQuantity",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairCost",SqlDbType.Decimal,8,2} ,
          new Object[] {"@RepairDiscountPercentage",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C13 ;
          prmBC000C13 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C14 ;
          prmBC000C14 = new Object[] {
          new Object[] {"@GameId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C15 ;
          prmBC000C15 = new Object[] {
          new Object[] {"@TechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C16 ;
          prmBC000C16 = new Object[] {
          new Object[] {"@RepairTechnicianId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C17 ;
          prmBC000C17 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmBC000C18 ;
          prmBC000C18 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmBC000C19 ;
          prmBC000C19 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmBC000C3 ;
          prmBC000C3 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmBC000C2 ;
          prmBC000C2 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmBC000C20 ;
          prmBC000C20 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0} ,
          new Object[] {"@RepairKindName",SqlDbType.NChar,1,0} ,
          new Object[] {"@RepairKindRemaks",SqlDbType.NChar,120,0}
          } ;
          Object[] prmBC000C21 ;
          prmBC000C21 = new Object[] {
          new Object[] {"@RepairKindName",SqlDbType.NChar,1,0} ,
          new Object[] {"@RepairKindRemaks",SqlDbType.NChar,120,0} ,
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmBC000C22 ;
          prmBC000C22 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@RepairKindId",SqlDbType.SmallInt,1,0}
          } ;
          Object[] prmBC000C23 ;
          prmBC000C23 = new Object[] {
          new Object[] {"@RepairId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("BC000C2", "SELECT [RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks] FROM [RepairKind] WITH (UPDLOCK) WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C2,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C3", "SELECT [RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks] FROM [RepairKind] WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C3,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C4", "SELECT [RepairId], [RepairDateFrom], [RepairDaysQuantity], [RepairCost], [RepairDiscountPercentage], [GameId], [TechnicianId], [RepairTechnicianId] AS RepairTechnicianId FROM [Repair] WITH (UPDLOCK) WHERE [RepairId] = @RepairId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C4,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C5", "SELECT [RepairId], [RepairDateFrom], [RepairDaysQuantity], [RepairCost], [RepairDiscountPercentage], [GameId], [TechnicianId], [RepairTechnicianId] AS RepairTechnicianId FROM [Repair] WHERE [RepairId] = @RepairId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C5,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C6", "SELECT [GameName] FROM [Game] WHERE [GameId] = @GameId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C6,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C7", "SELECT [TechnicianName] FROM [Technican] WHERE [TechnicianId] = @TechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C7,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C8", "SELECT [TechnicianName] AS RepairTechniciaName FROM [Technican] WHERE [TechnicianId] = @RepairTechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C8,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C9", "SELECT TM1.[RepairId], TM1.[RepairDateFrom], TM1.[RepairDaysQuantity], T2.[GameName], TM1.[RepairCost], T3.[TechnicianName], T4.[TechnicianName] AS RepairTechniciaName, TM1.[RepairDiscountPercentage], TM1.[GameId], TM1.[TechnicianId], TM1.[RepairTechnicianId] AS RepairTechnicianId FROM ((([Repair] TM1 INNER JOIN [Game] T2 ON T2.[GameId] = TM1.[GameId]) INNER JOIN [Technican] T3 ON T3.[TechnicianId] = TM1.[TechnicianId]) INNER JOIN [Technican] T4 ON T4.[TechnicianId] = TM1.[RepairTechnicianId]) WHERE TM1.[RepairId] = @RepairId ORDER BY TM1.[RepairId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C9,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C10", "SELECT [RepairId] FROM [Repair] WHERE [RepairId] = @RepairId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C10,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C11", "INSERT INTO [Repair]([RepairDateFrom], [RepairDaysQuantity], [RepairCost], [RepairDiscountPercentage], [GameId], [TechnicianId], [RepairTechnicianId]) VALUES(@RepairDateFrom, @RepairDaysQuantity, @RepairCost, @RepairDiscountPercentage, @GameId, @TechnicianId, @RepairTechnicianId); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmBC000C11)
             ,new CursorDef("BC000C12", "UPDATE [Repair] SET [RepairDateFrom]=@RepairDateFrom, [RepairDaysQuantity]=@RepairDaysQuantity, [RepairCost]=@RepairCost, [RepairDiscountPercentage]=@RepairDiscountPercentage, [GameId]=@GameId, [TechnicianId]=@TechnicianId, [RepairTechnicianId]=@RepairTechnicianId  WHERE [RepairId] = @RepairId", GxErrorMask.GX_NOMASK,prmBC000C12)
             ,new CursorDef("BC000C13", "DELETE FROM [Repair]  WHERE [RepairId] = @RepairId", GxErrorMask.GX_NOMASK,prmBC000C13)
             ,new CursorDef("BC000C14", "SELECT [GameName] FROM [Game] WHERE [GameId] = @GameId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C14,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C15", "SELECT [TechnicianName] FROM [Technican] WHERE [TechnicianId] = @TechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C15,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C16", "SELECT [TechnicianName] AS RepairTechniciaName FROM [Technican] WHERE [TechnicianId] = @RepairTechnicianId ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C16,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C17", "SELECT TM1.[RepairId], TM1.[RepairDateFrom], TM1.[RepairDaysQuantity], T2.[GameName], TM1.[RepairCost], T3.[TechnicianName], T4.[TechnicianName] AS RepairTechniciaName, TM1.[RepairDiscountPercentage], TM1.[GameId], TM1.[TechnicianId], TM1.[RepairTechnicianId] AS RepairTechnicianId FROM ((([Repair] TM1 INNER JOIN [Game] T2 ON T2.[GameId] = TM1.[GameId]) INNER JOIN [Technican] T3 ON T3.[TechnicianId] = TM1.[TechnicianId]) INNER JOIN [Technican] T4 ON T4.[TechnicianId] = TM1.[RepairTechnicianId]) WHERE TM1.[RepairId] = @RepairId ORDER BY TM1.[RepairId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C17,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C18", "SELECT [RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks] FROM [RepairKind] WHERE [RepairId] = @RepairId and [RepairKindId] = @RepairKindId ORDER BY [RepairId], [RepairKindId]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C18,11, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C19", "SELECT [RepairId], [RepairKindId] FROM [RepairKind] WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C19,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("BC000C20", "INSERT INTO [RepairKind]([RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks]) VALUES(@RepairId, @RepairKindId, @RepairKindName, @RepairKindRemaks)", GxErrorMask.GX_NOMASK,prmBC000C20)
             ,new CursorDef("BC000C21", "UPDATE [RepairKind] SET [RepairKindName]=@RepairKindName, [RepairKindRemaks]=@RepairKindRemaks  WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId", GxErrorMask.GX_NOMASK,prmBC000C21)
             ,new CursorDef("BC000C22", "DELETE FROM [RepairKind]  WHERE [RepairId] = @RepairId AND [RepairKindId] = @RepairKindId", GxErrorMask.GX_NOMASK,prmBC000C22)
             ,new CursorDef("BC000C23", "SELECT [RepairId], [RepairKindId], [RepairKindName], [RepairKindRemaks] FROM [RepairKind] WHERE [RepairId] = @RepairId ORDER BY [RepairId], [RepairKindId]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000C23,11, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 120) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 120) ;
                return;
             case 2 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((decimal[]) buf[3])[0] = rslt.getDecimal(4) ;
                ((short[]) buf[4])[0] = rslt.getShort(5) ;
                ((short[]) buf[5])[0] = rslt.getShort(6) ;
                ((short[]) buf[6])[0] = rslt.getShort(7) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                return;
             case 4 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 5 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 6 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((short[]) buf[8])[0] = rslt.getShort(9) ;
                ((short[]) buf[9])[0] = rslt.getShort(10) ;
                ((short[]) buf[10])[0] = rslt.getShort(11) ;
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 9 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 12 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 13 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 15 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 50) ;
                ((decimal[]) buf[4])[0] = rslt.getDecimal(5) ;
                ((String[]) buf[5])[0] = rslt.getString(6, 50) ;
                ((String[]) buf[6])[0] = rslt.getString(7, 50) ;
                ((short[]) buf[7])[0] = rslt.getShort(8) ;
                ((short[]) buf[8])[0] = rslt.getShort(9) ;
                ((short[]) buf[9])[0] = rslt.getShort(10) ;
                ((short[]) buf[10])[0] = rslt.getShort(11) ;
                return;
             case 16 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 120) ;
                return;
             case 17 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 21 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 1) ;
                ((String[]) buf[3])[0] = rslt.getString(4, 120) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                return;
             case 10 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (decimal)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                stmt.SetParameter(5, (short)parms[4]);
                stmt.SetParameter(6, (short)parms[5]);
                stmt.SetParameter(7, (short)parms[6]);
                stmt.SetParameter(8, (short)parms[7]);
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 12 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 14 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 16 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 17 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                stmt.SetParameter(3, (String)parms[2]);
                stmt.SetParameter(4, (String)parms[3]);
                return;
             case 19 :
                stmt.SetParameter(1, (String)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                stmt.SetParameter(4, (short)parms[3]);
                return;
             case 20 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 21 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
