/*
               File: Amusement_Parks_ListFromTo
        Description: Stub for Amusement_Parks_ListFromTo
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 5/19/2022 18:36:7.87
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class amusement_parks_listfromto : GXProcedure
   {
      public amusement_parks_listfromto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public amusement_parks_listfromto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Nom_Inicial ,
                           ref String aP1_Nom_Final )
      {
         this.AV2Nom_Inicial = aP0_Nom_Inicial;
         this.AV3Nom_Final = aP1_Nom_Final;
         initialize();
         executePrivate();
         aP1_Nom_Final=this.AV3Nom_Final;
      }

      public String executeUdp( String aP0_Nom_Inicial )
      {
         this.AV2Nom_Inicial = aP0_Nom_Inicial;
         this.AV3Nom_Final = aP1_Nom_Final;
         initialize();
         executePrivate();
         aP1_Nom_Final=this.AV3Nom_Final;
         return AV3Nom_Final ;
      }

      public void executeSubmit( String aP0_Nom_Inicial ,
                                 ref String aP1_Nom_Final )
      {
         amusement_parks_listfromto objamusement_parks_listfromto;
         objamusement_parks_listfromto = new amusement_parks_listfromto();
         objamusement_parks_listfromto.AV2Nom_Inicial = aP0_Nom_Inicial;
         objamusement_parks_listfromto.AV3Nom_Final = aP1_Nom_Final;
         objamusement_parks_listfromto.context.SetSubmitInitialConfig(context);
         objamusement_parks_listfromto.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objamusement_parks_listfromto);
         aP1_Nom_Final=this.AV3Nom_Final;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((amusement_parks_listfromto)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2Nom_Inicial,(String)AV3Nom_Final} ;
         ClassLoader.Execute("aamusement_parks_listfromto","GeneXus.Programs","aamusement_parks_listfromto", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 2 ) )
         {
            AV3Nom_Final = (String)(args[1]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV2Nom_Inicial ;
      private String AV3Nom_Final ;
      private IGxDataStore dsDefault ;
      private String aP1_Nom_Final ;
      private Object[] args ;
   }

}
