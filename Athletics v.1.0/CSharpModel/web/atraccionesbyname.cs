/*
               File: AtraccionesByname
        Description: Stub for AtraccionesByname
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/7/2022 17:29:22.33
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class atraccionesbyname : GXProcedure
   {
      public atraccionesbyname( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public atraccionesbyname( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_NameForm ,
                           String aP1_NameTo )
      {
         this.AV2NameForm = aP0_NameForm;
         this.AV3NameTo = aP1_NameTo;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_NameForm ,
                                 String aP1_NameTo )
      {
         atraccionesbyname objatraccionesbyname;
         objatraccionesbyname = new atraccionesbyname();
         objatraccionesbyname.AV2NameForm = aP0_NameForm;
         objatraccionesbyname.AV3NameTo = aP1_NameTo;
         objatraccionesbyname.context.SetSubmitInitialConfig(context);
         objatraccionesbyname.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objatraccionesbyname);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((atraccionesbyname)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2NameForm,(String)AV3NameTo} ;
         ClassLoader.Execute("aatraccionesbyname","GeneXus.Programs","aatraccionesbyname", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 2 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV2NameForm ;
      private String AV3NameTo ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
