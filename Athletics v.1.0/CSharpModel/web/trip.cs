/*
               File: Trip
        Description: Trip
             Author: GeneXus C# Generator version 16_0_5-135614
       Generated on: 6/7/2022 18:14:0.77
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class trip : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_4") == 0 )
         {
            A7AmusementParkId = (short)(NumberUtil.Val( GetNextPar( ), "."));
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_4( A7AmusementParkId) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridtrip_attraction") == 0 )
         {
            nRC_GXsfl_53 = (int)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_53_idx = (int)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_53_idx = GetNextPar( );
            Gx_mode = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridtrip_attraction_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_web_controls( ) ;
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 16_0_5-135614", 0) ;
            Form.Meta.addItem("description", "Trip", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtTripId_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("Carmine");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public trip( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public trip( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdmasterpage", "GeneXus.Programs.rwdmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "no-store");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            DrawControls( ) ;
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void DrawControls( )
      {
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-gx-base-lib=\"bootstrapv3\""+" "+"data-abstract-form"+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "WWAdvancedContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divTitlecontainer_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Trip", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         ClassString = "ErrorViewer";
         StyleString = "";
         GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divFormcontainer_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divToolbarcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3 ToolbarCellClass", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 21,'',false,'',0)\"";
         ClassString = "BtnFirst";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "", bttBtn_first_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
         ClassString = "BtnPrevious";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "", bttBtn_previous_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',false,'',0)\"";
         ClassString = "BtnNext";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", "", bttBtn_next_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
         ClassString = "BtnLast";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", "", bttBtn_last_Jsonclick, 5, "", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
         ClassString = "BtnSelect";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Seleccionar", bttBtn_select_Jsonclick, 4, "Seleccionar", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx00e0.aspx"+"',["+"{Ctrl:gx.dom.el('"+"TRIPID"+"'), id:'"+"TRIPID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellAdvanced", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTripId_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtTripId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtTripId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A45TripId), 4, 0, ",", "")), ((edtTripId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A45TripId), "ZZZ9")) : context.localUtil.Format( (decimal)(A45TripId), "ZZZ9")), TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTripId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTripId_Enabled, 0, "number", "1", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "Id", "right", false, "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTripDate_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtTripDate_Internalname, "Date", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
         context.WriteHtmlText( "<div id=\""+edtTripDate_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
         GxWebStd.gx_single_line_edit( context, edtTripDate_Internalname, context.localUtil.Format(A48TripDate, "99/99/99"), context.localUtil.Format( A48TripDate, "99/99/99"), TempTags+" onchange=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTripDate_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTripDate_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Trip.htm");
         GxWebStd.gx_bitmap( context, edtTripDate_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtTripDate_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Trip.htm");
         context.WriteHtmlTextNl( "</div>") ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtTripDescription_Internalname+"\"", "", "div");
         /* Attribute/Variable Label */
         GxWebStd.gx_label_element( context, edtTripDescription_Internalname, "Description", "col-sm-3 AttributeLabel", 1, true);
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
         /* Single line edit */
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
         GxWebStd.gx_single_line_edit( context, edtTripDescription_Internalname, StringUtil.RTrim( A47TripDescription), StringUtil.RTrim( context.localUtil.Format( A47TripDescription, "")), TempTags+" onchange=\""+""+";gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtTripDescription_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtTripDescription_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, divAttractiontable_Internalname, 1, 0, "px", 0, "px", "LevelTable", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
         /* Text block */
         GxWebStd.gx_label_ctrl( context, lblTitleattraction_Internalname, "Attraction", "", "", lblTitleattraction_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
         gxdraw_Gridtrip_attraction( ) ;
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
         ClassString = "BtnEnter";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'',false,'',0)\"";
         ClassString = "BtnCancel";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         /* Div Control */
         GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
         TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
         ClassString = "BtnDelete";
         StyleString = "";
         GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Trip.htm");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "Center", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
         GxWebStd.gx_div_end( context, "left", "top", "div");
      }

      protected void gxdraw_Gridtrip_attraction( )
      {
         /*  Grid Control  */
         Gridtrip_attractionContainer.AddObjectProperty("GridName", "Gridtrip_attraction");
         Gridtrip_attractionContainer.AddObjectProperty("Header", subGridtrip_attraction_Header);
         Gridtrip_attractionContainer.AddObjectProperty("Class", "Grid");
         Gridtrip_attractionContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtrip_attraction_Backcolorstyle), 1, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("CmpContext", "");
         Gridtrip_attractionContainer.AddObjectProperty("InMasterPage", "false");
         Gridtrip_attractionColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridtrip_attractionColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A7AmusementParkId), 4, 0, ".", "")));
         Gridtrip_attractionColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmusementParkId_Enabled), 5, 0, ".", "")));
         Gridtrip_attractionContainer.AddColumnProperties(Gridtrip_attractionColumn);
         Gridtrip_attractionColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridtrip_attractionContainer.AddColumnProperties(Gridtrip_attractionColumn);
         Gridtrip_attractionColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridtrip_attractionColumn.AddObjectProperty("Value", StringUtil.RTrim( A8AmusementParkName));
         Gridtrip_attractionColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmusementParkName_Enabled), 5, 0, ".", "")));
         Gridtrip_attractionContainer.AddColumnProperties(Gridtrip_attractionColumn);
         Gridtrip_attractionContainer.AddObjectProperty("Selectedindex", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtrip_attraction_Selectedindex), 4, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtrip_attraction_Allowselection), 1, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtrip_attraction_Selectioncolor), 9, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtrip_attraction_Allowhovering), 1, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtrip_attraction_Hoveringcolor), 9, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtrip_attraction_Allowcollapsing), 1, 0, ".", "")));
         Gridtrip_attractionContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridtrip_attraction_Collapsed), 1, 0, ".", "")));
         nGXsfl_53_idx = 0;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount15 = 5;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               /* Display confirmed (stored) records */
               nRcdExists_15 = 1;
               ScanStart0D15( ) ;
               while ( RcdFound15 != 0 )
               {
                  init_level_properties15( ) ;
                  getByPrimaryKey0D15( ) ;
                  AddRow0D15( ) ;
                  ScanNext0D15( ) ;
               }
               ScanEnd0D15( ) ;
               nBlankRcdCount15 = 5;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            standaloneNotModal0D15( ) ;
            standaloneModal0D15( ) ;
            sMode15 = Gx_mode;
            while ( nGXsfl_53_idx < nRC_GXsfl_53 )
            {
               bGXsfl_53_Refreshing = true;
               ReadRow0D15( ) ;
               edtAmusementParkId_Enabled = (int)(context.localUtil.CToN( cgiGet( "AMUSEMENTPARKID_"+sGXsfl_53_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkId_Enabled), 5, 0)), !bGXsfl_53_Refreshing);
               edtAmusementParkName_Enabled = (int)(context.localUtil.CToN( cgiGet( "AMUSEMENTPARKNAME_"+sGXsfl_53_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkName_Enabled), 5, 0)), !bGXsfl_53_Refreshing);
               imgprompt_7_Link = cgiGet( "PROMPT_7_"+sGXsfl_53_idx+"Link");
               if ( ( nRcdExists_15 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal0D15( ) ;
               }
               SendRow0D15( ) ;
               bGXsfl_53_Refreshing = false;
            }
            Gx_mode = sMode15;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount15 = 5;
            nRcdExists_15 = 1;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               ScanStart0D15( ) ;
               while ( RcdFound15 != 0 )
               {
                  sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx+1), 4, 0)), 4, "0");
                  SubsflControlProps_5315( ) ;
                  init_level_properties15( ) ;
                  standaloneNotModal0D15( ) ;
                  getByPrimaryKey0D15( ) ;
                  standaloneModal0D15( ) ;
                  AddRow0D15( ) ;
                  ScanNext0D15( ) ;
               }
               ScanEnd0D15( ) ;
            }
         }
         /* Initialize fields for 'new' records and send them. */
         sMode15 = Gx_mode;
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx+1), 4, 0)), 4, "0");
         SubsflControlProps_5315( ) ;
         InitAll0D15( ) ;
         init_level_properties15( ) ;
         nRcdExists_15 = 0;
         nIsMod_15 = 0;
         nRcdDeleted_15 = 0;
         nBlankRcdCount15 = (short)(nBlankRcdUsr15+nBlankRcdCount15);
         fRowAdded = 0;
         while ( nBlankRcdCount15 > 0 )
         {
            standaloneNotModal0D15( ) ;
            standaloneModal0D15( ) ;
            AddRow0D15( ) ;
            if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
            {
               fRowAdded = 1;
               GX_FocusControl = edtAmusementParkId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nBlankRcdCount15 = (short)(nBlankRcdCount15-1);
         }
         Gx_mode = sMode15;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sStyleString = "";
         context.WriteHtmlText( "<div id=\""+"Gridtrip_attractionContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridtrip_attraction", Gridtrip_attractionContainer);
         if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridtrip_attractionContainerData", Gridtrip_attractionContainer.ToJavascriptSource());
         }
         if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridtrip_attractionContainerData"+"V", Gridtrip_attractionContainer.GridValuesHidden());
         }
         else
         {
            context.WriteHtmlText( "<input type=\"hidden\" "+"name=\""+"Gridtrip_attractionContainerData"+"V"+"\" value='"+Gridtrip_attractionContainer.GridValuesHidden()+"'/>") ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtTripId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtTripId_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "TRIPID");
               AnyError = 1;
               GX_FocusControl = edtTripId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A45TripId = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
            }
            else
            {
               A45TripId = (short)(context.localUtil.CToN( cgiGet( edtTripId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
            }
            if ( context.localUtil.VCDate( cgiGet( edtTripDate_Internalname), 2) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Trip Date"}), 1, "TRIPDATE");
               AnyError = 1;
               GX_FocusControl = edtTripDate_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               A48TripDate = DateTime.MinValue;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48TripDate", context.localUtil.Format(A48TripDate, "99/99/99"));
            }
            else
            {
               A48TripDate = context.localUtil.CToD( cgiGet( edtTripDate_Internalname), 2);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48TripDate", context.localUtil.Format(A48TripDate, "99/99/99"));
            }
            A47TripDescription = cgiGet( edtTripDescription_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47TripDescription", A47TripDescription);
            /* Read saved values. */
            Z45TripId = (short)(context.localUtil.CToN( cgiGet( "Z45TripId"), ",", "."));
            Z48TripDate = context.localUtil.CToD( cgiGet( "Z48TripDate"), 0);
            Z47TripDescription = cgiGet( "Z47TripDescription");
            IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
            IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
            Gx_mode = cgiGet( "Mode");
            nRC_GXsfl_53 = (int)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_53"), ",", "."));
            Gx_mode = cgiGet( "vMODE");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            standaloneNotModal( ) ;
         }
         else
         {
            standaloneNotModal( ) ;
            if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
            {
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               A45TripId = (short)(NumberUtil.Val( GetNextPar( ), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
               getEqualNoModal( ) ;
               Gx_mode = "DSP";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               disable_std_buttons_dsp( ) ;
               standaloneModal( ) ;
            }
            else
            {
               Gx_mode = "INS";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               standaloneModal( ) ;
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll0D14( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes0D14( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_0D15( )
      {
         nGXsfl_53_idx = 0;
         while ( nGXsfl_53_idx < nRC_GXsfl_53 )
         {
            ReadRow0D15( ) ;
            if ( ( nRcdExists_15 != 0 ) || ( nIsMod_15 != 0 ) )
            {
               GetKey0D15( ) ;
               if ( ( nRcdExists_15 == 0 ) && ( nRcdDeleted_15 == 0 ) )
               {
                  if ( RcdFound15 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate0D15( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable0D15( ) ;
                        CloseExtendedTableCursors0D15( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "AMUSEMENTPARKID_" + sGXsfl_53_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtAmusementParkId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound15 != 0 )
                  {
                     if ( nRcdDeleted_15 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey0D15( ) ;
                        Load0D15( ) ;
                        BeforeValidate0D15( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls0D15( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_15 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate0D15( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable0D15( ) ;
                              CloseExtendedTableCursors0D15( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_15 == 0 )
                     {
                        GXCCtl = "AMUSEMENTPARKID_" + sGXsfl_53_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtAmusementParkId_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtAmusementParkId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A7AmusementParkId), 4, 0, ",", ""))) ;
            ChangePostValue( edtAmusementParkName_Internalname, StringUtil.RTrim( A8AmusementParkName)) ;
            ChangePostValue( "ZT_"+"Z7AmusementParkId_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7AmusementParkId), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_15_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_15), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_15_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_15), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_15_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_15), 4, 0, ",", ""))) ;
            if ( nIsMod_15 != 0 )
            {
               ChangePostValue( "AMUSEMENTPARKID_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmusementParkId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "AMUSEMENTPARKNAME_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmusementParkName_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption0D0( )
      {
      }

      protected void ZM0D14( short GX_JID )
      {
         if ( ( GX_JID == 2 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z48TripDate = T000D6_A48TripDate[0];
               Z47TripDescription = T000D6_A47TripDescription[0];
            }
            else
            {
               Z48TripDate = A48TripDate;
               Z47TripDescription = A47TripDescription;
            }
         }
         if ( GX_JID == -2 )
         {
            Z45TripId = A45TripId;
            Z48TripDate = A48TripDate;
            Z47TripDescription = A47TripDescription;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load0D14( )
      {
         /* Using cursor T000D7 */
         pr_default.execute(5, new Object[] {A45TripId});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound14 = 1;
            A48TripDate = T000D7_A48TripDate[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48TripDate", context.localUtil.Format(A48TripDate, "99/99/99"));
            A47TripDescription = T000D7_A47TripDescription[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47TripDescription", A47TripDescription);
            ZM0D14( -2) ;
         }
         pr_default.close(5);
         OnLoadActions0D14( ) ;
      }

      protected void OnLoadActions0D14( )
      {
      }

      protected void CheckExtendedTable0D14( )
      {
         nIsDirty_14 = 0;
         Gx_BScreen = 1;
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A48TripDate) || ( A48TripDate >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Trip Date fuera de rango", "OutOfRange", 1, "TRIPDATE");
            AnyError = 1;
            GX_FocusControl = edtTripDate_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors0D14( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey0D14( )
      {
         /* Using cursor T000D8 */
         pr_default.execute(6, new Object[] {A45TripId});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound14 = 1;
         }
         else
         {
            RcdFound14 = 0;
         }
         pr_default.close(6);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T000D6 */
         pr_default.execute(4, new Object[] {A45TripId});
         if ( (pr_default.getStatus(4) != 101) )
         {
            ZM0D14( 2) ;
            RcdFound14 = 1;
            A45TripId = T000D6_A45TripId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
            A48TripDate = T000D6_A48TripDate[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48TripDate", context.localUtil.Format(A48TripDate, "99/99/99"));
            A47TripDescription = T000D6_A47TripDescription[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47TripDescription", A47TripDescription);
            Z45TripId = A45TripId;
            sMode14 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load0D14( ) ;
            if ( AnyError == 1 )
            {
               RcdFound14 = 0;
               InitializeNonKey0D14( ) ;
            }
            Gx_mode = sMode14;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound14 = 0;
            InitializeNonKey0D14( ) ;
            sMode14 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode14;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(4);
      }

      protected void getEqualNoModal( )
      {
         GetKey0D14( ) ;
         if ( RcdFound14 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound14 = 0;
         /* Using cursor T000D9 */
         pr_default.execute(7, new Object[] {A45TripId});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( T000D9_A45TripId[0] < A45TripId ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( T000D9_A45TripId[0] > A45TripId ) ) )
            {
               A45TripId = T000D9_A45TripId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
               RcdFound14 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void move_previous( )
      {
         RcdFound14 = 0;
         /* Using cursor T000D10 */
         pr_default.execute(8, new Object[] {A45TripId});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( T000D10_A45TripId[0] > A45TripId ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( T000D10_A45TripId[0] < A45TripId ) ) )
            {
               A45TripId = T000D10_A45TripId[0];
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
               RcdFound14 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey0D14( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtTripId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert0D14( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound14 == 1 )
            {
               if ( A45TripId != Z45TripId )
               {
                  A45TripId = Z45TripId;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "TRIPID");
                  AnyError = 1;
                  GX_FocusControl = edtTripId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtTripId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update0D14( ) ;
                  GX_FocusControl = edtTripId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A45TripId != Z45TripId )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtTripId_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert0D14( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "TRIPID");
                     AnyError = 1;
                     GX_FocusControl = edtTripId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtTripId_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert0D14( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A45TripId != Z45TripId )
         {
            A45TripId = Z45TripId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "TRIPID");
            AnyError = 1;
            GX_FocusControl = edtTripId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtTripId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "TRIPID");
            AnyError = 1;
            GX_FocusControl = edtTripId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtTripDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0D14( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtTripDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0D14( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtTripDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtTripDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart0D14( ) ;
         if ( RcdFound14 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound14 != 0 )
            {
               ScanNext0D14( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtTripDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd0D14( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency0D14( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000D5 */
            pr_default.execute(3, new Object[] {A45TripId});
            if ( (pr_default.getStatus(3) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Trip"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(3) == 101) || ( Z48TripDate != T000D5_A48TripDate[0] ) || ( StringUtil.StrCmp(Z47TripDescription, T000D5_A47TripDescription[0]) != 0 ) )
            {
               if ( Z48TripDate != T000D5_A48TripDate[0] )
               {
                  GXUtil.WriteLog("trip:[seudo value changed for attri]"+"TripDate");
                  GXUtil.WriteLogRaw("Old: ",Z48TripDate);
                  GXUtil.WriteLogRaw("Current: ",T000D5_A48TripDate[0]);
               }
               if ( StringUtil.StrCmp(Z47TripDescription, T000D5_A47TripDescription[0]) != 0 )
               {
                  GXUtil.WriteLog("trip:[seudo value changed for attri]"+"TripDescription");
                  GXUtil.WriteLogRaw("Old: ",Z47TripDescription);
                  GXUtil.WriteLogRaw("Current: ",T000D5_A47TripDescription[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Trip"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0D14( )
      {
         BeforeValidate0D14( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0D14( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0D14( 0) ;
            CheckOptimisticConcurrency0D14( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0D14( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0D14( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000D11 */
                     pr_default.execute(9, new Object[] {A48TripDate, A47TripDescription});
                     A45TripId = T000D11_A45TripId[0];
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Trip") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0D14( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                              ResetCaption0D0( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0D14( ) ;
            }
            EndLevel0D14( ) ;
         }
         CloseExtendedTableCursors0D14( ) ;
      }

      protected void Update0D14( )
      {
         BeforeValidate0D14( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0D14( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0D14( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0D14( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate0D14( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000D12 */
                     pr_default.execute(10, new Object[] {A48TripDate, A47TripDescription, A45TripId});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Trip") ;
                     if ( (pr_default.getStatus(10) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Trip"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate0D14( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel0D14( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                              ResetCaption0D0( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel0D14( ) ;
         }
         CloseExtendedTableCursors0D14( ) ;
      }

      protected void DeferredUpdate0D14( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0D14( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0D14( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0D14( ) ;
            AfterConfirm0D14( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0D14( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart0D15( ) ;
                  while ( RcdFound15 != 0 )
                  {
                     getByPrimaryKey0D15( ) ;
                     Delete0D15( ) ;
                     ScanNext0D15( ) ;
                  }
                  ScanEnd0D15( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000D13 */
                     pr_default.execute(11, new Object[] {A45TripId});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Trip") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound14 == 0 )
                           {
                              InitAll0D14( ) ;
                              Gx_mode = "INS";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                           ResetCaption0D0( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode14 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0D14( ) ;
         Gx_mode = sMode14;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0D14( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void ProcessNestedLevel0D15( )
      {
         nGXsfl_53_idx = 0;
         while ( nGXsfl_53_idx < nRC_GXsfl_53 )
         {
            ReadRow0D15( ) ;
            if ( ( nRcdExists_15 != 0 ) || ( nIsMod_15 != 0 ) )
            {
               standaloneNotModal0D15( ) ;
               GetKey0D15( ) ;
               if ( ( nRcdExists_15 == 0 ) && ( nRcdDeleted_15 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  Insert0D15( ) ;
               }
               else
               {
                  if ( RcdFound15 != 0 )
                  {
                     if ( ( nRcdDeleted_15 != 0 ) && ( nRcdExists_15 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        Delete0D15( ) ;
                     }
                     else
                     {
                        if ( nRcdExists_15 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           Update0D15( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_15 == 0 )
                     {
                        GXCCtl = "AMUSEMENTPARKID_" + sGXsfl_53_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtAmusementParkId_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtAmusementParkId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A7AmusementParkId), 4, 0, ",", ""))) ;
            ChangePostValue( edtAmusementParkName_Internalname, StringUtil.RTrim( A8AmusementParkName)) ;
            ChangePostValue( "ZT_"+"Z7AmusementParkId_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7AmusementParkId), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdDeleted_15_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_15), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_15_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_15), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_15_"+sGXsfl_53_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_15), 4, 0, ",", ""))) ;
            if ( nIsMod_15 != 0 )
            {
               ChangePostValue( "AMUSEMENTPARKID_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmusementParkId_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "AMUSEMENTPARKNAME_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmusementParkName_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll0D15( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_15 = 0;
         nIsMod_15 = 0;
         nRcdDeleted_15 = 0;
      }

      protected void ProcessLevel0D14( )
      {
         /* Save parent mode. */
         sMode14 = Gx_mode;
         ProcessNestedLevel0D15( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode14;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel0D14( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(3);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete0D14( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(2);
            context.CommitDataStores("trip",pr_default);
            if ( AnyError == 0 )
            {
               ConfirmValues0D0( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(4);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(2);
            context.RollbackDataStores("trip",pr_default);
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0D14( )
      {
         /* Using cursor T000D14 */
         pr_default.execute(12);
         RcdFound14 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound14 = 1;
            A45TripId = T000D14_A45TripId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0D14( )
      {
         /* Scan next routine */
         pr_default.readNext(12);
         RcdFound14 = 0;
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound14 = 1;
            A45TripId = T000D14_A45TripId[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
         }
      }

      protected void ScanEnd0D14( )
      {
         pr_default.close(12);
      }

      protected void AfterConfirm0D14( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0D14( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0D14( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0D14( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0D14( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0D14( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0D14( )
      {
         edtTripId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTripId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTripId_Enabled), 5, 0)), true);
         edtTripDate_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTripDate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTripDate_Enabled), 5, 0)), true);
         edtTripDescription_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTripDescription_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTripDescription_Enabled), 5, 0)), true);
      }

      protected void ZM0D15( short GX_JID )
      {
         if ( ( GX_JID == 3 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
            }
            else
            {
            }
         }
         if ( GX_JID == -3 )
         {
            Z45TripId = A45TripId;
            Z7AmusementParkId = A7AmusementParkId;
            Z8AmusementParkName = A8AmusementParkName;
         }
      }

      protected void standaloneNotModal0D15( )
      {
      }

      protected void standaloneModal0D15( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtAmusementParkId_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkId_Enabled), 5, 0)), !bGXsfl_53_Refreshing);
         }
         else
         {
            edtAmusementParkId_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkId_Enabled), 5, 0)), !bGXsfl_53_Refreshing);
         }
      }

      protected void Load0D15( )
      {
         /* Using cursor T000D15 */
         pr_default.execute(13, new Object[] {A45TripId, A7AmusementParkId});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound15 = 1;
            A8AmusementParkName = T000D15_A8AmusementParkName[0];
            ZM0D15( -3) ;
         }
         pr_default.close(13);
         OnLoadActions0D15( ) ;
      }

      protected void OnLoadActions0D15( )
      {
      }

      protected void CheckExtendedTable0D15( )
      {
         nIsDirty_15 = 0;
         Gx_BScreen = 1;
         standaloneModal0D15( ) ;
         /* Using cursor T000D4 */
         pr_default.execute(2, new Object[] {A7AmusementParkId});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GXCCtl = "AMUSEMENTPARKID_" + sGXsfl_53_idx;
            GX_msglist.addItem("No existe 'Amusement Park'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtAmusementParkId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A8AmusementParkName = T000D4_A8AmusementParkName[0];
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors0D15( )
      {
         pr_default.close(2);
      }

      protected void enableDisable0D15( )
      {
      }

      protected void gxLoad_4( short A7AmusementParkId )
      {
         /* Using cursor T000D16 */
         pr_default.execute(14, new Object[] {A7AmusementParkId});
         if ( (pr_default.getStatus(14) == 101) )
         {
            GXCCtl = "AMUSEMENTPARKID_" + sGXsfl_53_idx;
            GX_msglist.addItem("No existe 'Amusement Park'.", "ForeignKeyNotFound", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtAmusementParkId_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A8AmusementParkName = T000D16_A8AmusementParkName[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( StringUtil.RTrim( A8AmusementParkName))+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(14) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString("]");
         pr_default.close(14);
      }

      protected void GetKey0D15( )
      {
         /* Using cursor T000D17 */
         pr_default.execute(15, new Object[] {A45TripId, A7AmusementParkId});
         if ( (pr_default.getStatus(15) != 101) )
         {
            RcdFound15 = 1;
         }
         else
         {
            RcdFound15 = 0;
         }
         pr_default.close(15);
      }

      protected void getByPrimaryKey0D15( )
      {
         /* Using cursor T000D3 */
         pr_default.execute(1, new Object[] {A45TripId, A7AmusementParkId});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM0D15( 3) ;
            RcdFound15 = 1;
            InitializeNonKey0D15( ) ;
            A7AmusementParkId = T000D3_A7AmusementParkId[0];
            Z45TripId = A45TripId;
            Z7AmusementParkId = A7AmusementParkId;
            sMode15 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal0D15( ) ;
            Load0D15( ) ;
            Gx_mode = sMode15;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound15 = 0;
            InitializeNonKey0D15( ) ;
            sMode15 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal0D15( ) ;
            Gx_mode = sMode15;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes0D15( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency0D15( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T000D2 */
            pr_default.execute(0, new Object[] {A45TripId, A7AmusementParkId});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"TripAttraction"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"TripAttraction"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert0D15( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM0D15( 0) ;
            CheckOptimisticConcurrency0D15( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm0D15( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert0D15( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000D18 */
                     pr_default.execute(16, new Object[] {A45TripId, A7AmusementParkId});
                     pr_default.close(16);
                     dsDefault.SmartCacheProvider.SetUpdated("TripAttraction") ;
                     if ( (pr_default.getStatus(16) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load0D15( ) ;
            }
            EndLevel0D15( ) ;
         }
         CloseExtendedTableCursors0D15( ) ;
      }

      protected void Update0D15( )
      {
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable0D15( ) ;
         }
         if ( ( nIsMod_15 != 0 ) || ( nIsDirty_15 != 0 ) )
         {
            if ( AnyError == 0 )
            {
               CheckOptimisticConcurrency0D15( ) ;
               if ( AnyError == 0 )
               {
                  AfterConfirm0D15( ) ;
                  if ( AnyError == 0 )
                  {
                     BeforeUpdate0D15( ) ;
                     if ( AnyError == 0 )
                     {
                        /* No attributes to update on table [TripAttraction] */
                        DeferredUpdate0D15( ) ;
                        if ( AnyError == 0 )
                        {
                           /* Start of After( update) rules */
                           /* End of After( update) rules */
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey0D15( ) ;
                           }
                        }
                        else
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                           AnyError = 1;
                        }
                     }
                  }
               }
               EndLevel0D15( ) ;
            }
         }
         CloseExtendedTableCursors0D15( ) ;
      }

      protected void DeferredUpdate0D15( )
      {
      }

      protected void Delete0D15( )
      {
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate0D15( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency0D15( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls0D15( ) ;
            AfterConfirm0D15( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete0D15( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000D19 */
                  pr_default.execute(17, new Object[] {A45TripId, A7AmusementParkId});
                  pr_default.close(17);
                  dsDefault.SmartCacheProvider.SetUpdated("TripAttraction") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode15 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel0D15( ) ;
         Gx_mode = sMode15;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls0D15( )
      {
         standaloneModal0D15( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000D20 */
            pr_default.execute(18, new Object[] {A7AmusementParkId});
            A8AmusementParkName = T000D20_A8AmusementParkName[0];
            pr_default.close(18);
         }
      }

      protected void EndLevel0D15( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart0D15( )
      {
         /* Scan By routine */
         /* Using cursor T000D21 */
         pr_default.execute(19, new Object[] {A45TripId});
         RcdFound15 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound15 = 1;
            A7AmusementParkId = T000D21_A7AmusementParkId[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext0D15( )
      {
         /* Scan next routine */
         pr_default.readNext(19);
         RcdFound15 = 0;
         if ( (pr_default.getStatus(19) != 101) )
         {
            RcdFound15 = 1;
            A7AmusementParkId = T000D21_A7AmusementParkId[0];
         }
      }

      protected void ScanEnd0D15( )
      {
         pr_default.close(19);
      }

      protected void AfterConfirm0D15( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert0D15( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate0D15( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete0D15( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete0D15( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate0D15( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes0D15( )
      {
         edtAmusementParkId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkId_Enabled), 5, 0)), !bGXsfl_53_Refreshing);
         edtAmusementParkName_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkName_Enabled), 5, 0)), !bGXsfl_53_Refreshing);
      }

      protected void send_integrity_lvl_hashes0D15( )
      {
      }

      protected void send_integrity_lvl_hashes0D14( )
      {
      }

      protected void SubsflControlProps_5315( )
      {
         edtAmusementParkId_Internalname = "AMUSEMENTPARKID_"+sGXsfl_53_idx;
         imgprompt_7_Internalname = "PROMPT_7_"+sGXsfl_53_idx;
         edtAmusementParkName_Internalname = "AMUSEMENTPARKNAME_"+sGXsfl_53_idx;
      }

      protected void SubsflControlProps_fel_5315( )
      {
         edtAmusementParkId_Internalname = "AMUSEMENTPARKID_"+sGXsfl_53_fel_idx;
         imgprompt_7_Internalname = "PROMPT_7_"+sGXsfl_53_fel_idx;
         edtAmusementParkName_Internalname = "AMUSEMENTPARKNAME_"+sGXsfl_53_fel_idx;
      }

      protected void AddRow0D15( )
      {
         nGXsfl_53_idx = (int)(nGXsfl_53_idx+1);
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
         SubsflControlProps_5315( ) ;
         SendRow0D15( ) ;
      }

      protected void SendRow0D15( )
      {
         Gridtrip_attractionRow = GXWebRow.GetNew(context);
         if ( subGridtrip_attraction_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridtrip_attraction_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridtrip_attraction_Class, "") != 0 )
            {
               subGridtrip_attraction_Linesclass = subGridtrip_attraction_Class+"Odd";
            }
         }
         else if ( subGridtrip_attraction_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridtrip_attraction_Backstyle = 0;
            subGridtrip_attraction_Backcolor = subGridtrip_attraction_Allbackcolor;
            if ( StringUtil.StrCmp(subGridtrip_attraction_Class, "") != 0 )
            {
               subGridtrip_attraction_Linesclass = subGridtrip_attraction_Class+"Uniform";
            }
         }
         else if ( subGridtrip_attraction_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridtrip_attraction_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridtrip_attraction_Class, "") != 0 )
            {
               subGridtrip_attraction_Linesclass = subGridtrip_attraction_Class+"Odd";
            }
            subGridtrip_attraction_Backcolor = (int)(0x0);
         }
         else if ( subGridtrip_attraction_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridtrip_attraction_Backstyle = 1;
            if ( ((int)((nGXsfl_53_idx) % (2))) == 0 )
            {
               subGridtrip_attraction_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridtrip_attraction_Class, "") != 0 )
               {
                  subGridtrip_attraction_Linesclass = subGridtrip_attraction_Class+"Even";
               }
            }
            else
            {
               subGridtrip_attraction_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridtrip_attraction_Class, "") != 0 )
               {
                  subGridtrip_attraction_Linesclass = subGridtrip_attraction_Class+"Odd";
               }
            }
         }
         imgprompt_7_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0)||(StringUtil.StrCmp(Gx_mode, "UPD")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0020.aspx"+"',["+"{Ctrl:gx.dom.el('"+"AMUSEMENTPARKID_"+sGXsfl_53_idx+"'), id:'"+"AMUSEMENTPARKID_"+sGXsfl_53_idx+"'"+",IOType:'out'}"+"],"+"gx.dom.form()."+"nIsMod_15_"+sGXsfl_53_idx+","+"'', false"+","+"false"+");");
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_15_" + sGXsfl_53_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 54,'',false,'" + sGXsfl_53_idx + "',53)\"";
         ROClassString = "Attribute";
         Gridtrip_attractionRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmusementParkId_Internalname,StringUtil.LTrim( StringUtil.NToC( (decimal)(A7AmusementParkId), 4, 0, ",", "")),StringUtil.LTrim( context.localUtil.Format( (decimal)(A7AmusementParkId), "ZZZ9")),TempTags+" onchange=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,54);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmusementParkId_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtAmusementParkId_Enabled,(short)1,(String)"number",(String)"1",(short)0,(String)"px",(short)17,(String)"px",(short)4,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)0,(bool)true,(String)"Id",(String)"right",(bool)false});
         /* Subfile cell */
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "prompt.gif", "", context.GetTheme( )));
         Gridtrip_attractionRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgprompt_7_Internalname,(String)sImgUrl,(String)imgprompt_7_Link,(String)"",(String)"",context.GetTheme( ),(int)imgprompt_7_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         /* Single line edit */
         ROClassString = "Attribute";
         Gridtrip_attractionRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtAmusementParkName_Internalname,StringUtil.RTrim( A8AmusementParkName),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtAmusementParkName_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtAmusementParkName_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)53,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         context.httpAjaxContext.ajax_sending_grid_row(Gridtrip_attractionRow);
         send_integrity_lvl_hashes0D15( ) ;
         GXCCtl = "Z7AmusementParkId_" + sGXsfl_53_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z7AmusementParkId), 4, 0, ",", "")));
         GXCCtl = "nRcdDeleted_15_" + sGXsfl_53_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_15), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_15_" + sGXsfl_53_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_15), 4, 0, ",", "")));
         GXCCtl = "nIsMod_15_" + sGXsfl_53_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_15), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "AMUSEMENTPARKID_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmusementParkId_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "AMUSEMENTPARKNAME_"+sGXsfl_53_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtAmusementParkName_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROMPT_7_"+sGXsfl_53_idx+"Link", StringUtil.RTrim( imgprompt_7_Link));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridtrip_attractionContainer.AddRow(Gridtrip_attractionRow);
      }

      protected void ReadRow0D15( )
      {
         nGXsfl_53_idx = (int)(nGXsfl_53_idx+1);
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
         SubsflControlProps_5315( ) ;
         edtAmusementParkId_Enabled = (int)(context.localUtil.CToN( cgiGet( "AMUSEMENTPARKID_"+sGXsfl_53_idx+"Enabled"), ",", "."));
         edtAmusementParkName_Enabled = (int)(context.localUtil.CToN( cgiGet( "AMUSEMENTPARKNAME_"+sGXsfl_53_idx+"Enabled"), ",", "."));
         imgprompt_7_Link = cgiGet( "PROMPT_7_"+sGXsfl_53_idx+"Link");
         if ( ( ( context.localUtil.CToN( cgiGet( edtAmusementParkId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtAmusementParkId_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
         {
            GXCCtl = "AMUSEMENTPARKID_" + sGXsfl_53_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtAmusementParkId_Internalname;
            wbErr = true;
            A7AmusementParkId = 0;
         }
         else
         {
            A7AmusementParkId = (short)(context.localUtil.CToN( cgiGet( edtAmusementParkId_Internalname), ",", "."));
         }
         A8AmusementParkName = cgiGet( edtAmusementParkName_Internalname);
         GXCCtl = "Z7AmusementParkId_" + sGXsfl_53_idx;
         Z7AmusementParkId = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdDeleted_15_" + sGXsfl_53_idx;
         nRcdDeleted_15 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_15_" + sGXsfl_53_idx;
         nRcdExists_15 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_15_" + sGXsfl_53_idx;
         nIsMod_15 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtAmusementParkId_Enabled = edtAmusementParkId_Enabled;
      }

      protected void ConfirmValues0D0( )
      {
         nGXsfl_53_idx = 0;
         sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
         SubsflControlProps_5315( ) ;
         while ( nGXsfl_53_idx < nRC_GXsfl_53 )
         {
            nGXsfl_53_idx = (int)(nGXsfl_53_idx+1);
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
            SubsflControlProps_5315( ) ;
            ChangePostValue( "Z7AmusementParkId_"+sGXsfl_53_idx, cgiGet( "ZT_"+"Z7AmusementParkId_"+sGXsfl_53_idx)) ;
            DeletePostValue( "ZT_"+"Z7AmusementParkId_"+sGXsfl_53_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 135614), false, true);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("gxcfg.js", "?2022671814220", false, true);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 135614), false, true);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + "-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" autocomplete=\"off\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("trip.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" title=\"submit\" style=\"display:none\" disabled>") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z45TripId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z45TripId), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z48TripDate", context.localUtil.DToC( Z48TripDate, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z47TripDescription", StringUtil.RTrim( Z47TripDescription));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_53", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_53_idx), 8, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken(sPrefix);
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("trip.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Trip" ;
      }

      public override String GetPgmdesc( )
      {
         return "Trip" ;
      }

      protected void InitializeNonKey0D14( )
      {
         A48TripDate = DateTime.MinValue;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48TripDate", context.localUtil.Format(A48TripDate, "99/99/99"));
         A47TripDescription = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47TripDescription", A47TripDescription);
         Z48TripDate = DateTime.MinValue;
         Z47TripDescription = "";
      }

      protected void InitAll0D14( )
      {
         A45TripId = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45TripId", StringUtil.LTrim( StringUtil.Str( (decimal)(A45TripId), 4, 0)));
         InitializeNonKey0D14( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey0D15( )
      {
         A8AmusementParkName = "";
      }

      protected void InitAll0D15( )
      {
         A7AmusementParkId = 0;
         InitializeNonKey0D15( ) ;
      }

      protected void StandaloneModalInsert0D15( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ));
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2022671814224", true, true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false, true);
         context.AddJavascriptSource("trip.js", "?2022671814225", false, true);
         /* End function include_jscripts */
      }

      protected void init_level_properties15( )
      {
         edtAmusementParkId_Enabled = defedtAmusementParkId_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtAmusementParkId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtAmusementParkId_Enabled), 5, 0)), !bGXsfl_53_Refreshing);
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         divTitlecontainer_Internalname = "TITLECONTAINER";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         divToolbarcell_Internalname = "TOOLBARCELL";
         edtTripId_Internalname = "TRIPID";
         edtTripDate_Internalname = "TRIPDATE";
         edtTripDescription_Internalname = "TRIPDESCRIPTION";
         lblTitleattraction_Internalname = "TITLEATTRACTION";
         edtAmusementParkId_Internalname = "AMUSEMENTPARKID";
         edtAmusementParkName_Internalname = "AMUSEMENTPARKNAME";
         divAttractiontable_Internalname = "ATTRACTIONTABLE";
         divFormcontainer_Internalname = "FORMCONTAINER";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         imgprompt_7_Internalname = "PROMPT_7";
         subGridtrip_attraction_Internalname = "GRIDTRIP_ATTRACTION";
      }

      public override void initialize_properties( )
      {
         context.SetDefaultTheme("Carmine");
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Trip";
         edtAmusementParkName_Jsonclick = "";
         imgprompt_7_Visible = 1;
         imgprompt_7_Link = "";
         imgprompt_7_Visible = 1;
         edtAmusementParkId_Jsonclick = "";
         subGridtrip_attraction_Class = "Grid";
         subGridtrip_attraction_Backcolorstyle = 0;
         subGridtrip_attraction_Allowcollapsing = 0;
         subGridtrip_attraction_Allowselection = 0;
         edtAmusementParkName_Enabled = 0;
         edtAmusementParkId_Enabled = 1;
         subGridtrip_attraction_Header = "";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtTripDescription_Jsonclick = "";
         edtTripDescription_Enabled = 1;
         edtTripDate_Jsonclick = "";
         edtTripDate_Enabled = 1;
         edtTripId_Jsonclick = "";
         edtTripId_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridtrip_attraction_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_5315( ) ;
         while ( nGXsfl_53_idx <= nRC_GXsfl_53 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal0D15( ) ;
            standaloneModal0D15( ) ;
            init_web_controls( ) ;
            dynload_actions( ) ;
            SendRow0D15( ) ;
            nGXsfl_53_idx = (int)(nGXsfl_53_idx+1);
            sGXsfl_53_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_53_idx), 4, 0)), 4, "0");
            SubsflControlProps_5315( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Gridtrip_attractionContainer));
         /* End function gxnrGridtrip_attraction_newrow */
      }

      protected void init_web_controls( )
      {
         /* End function init_web_controls */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtTripDate_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Tripid( )
      {
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48TripDate", context.localUtil.Format(A48TripDate, "99/99/99"));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47TripDescription", StringUtil.RTrim( A47TripDescription));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "Z45TripId", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z45TripId), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "Z48TripDate", context.localUtil.Format(Z48TripDate, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "Z47TripDescription", StringUtil.RTrim( Z47TripDescription));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         SendCloseFormHiddens( ) ;
      }

      public void Valid_Amusementparkid( )
      {
         /* Using cursor T000D20 */
         pr_default.execute(18, new Object[] {A7AmusementParkId});
         if ( (pr_default.getStatus(18) == 101) )
         {
            GX_msglist.addItem("No existe 'Amusement Park'.", "ForeignKeyNotFound", 1, "AMUSEMENTPARKID");
            AnyError = 1;
            GX_FocusControl = edtAmusementParkId_Internalname;
         }
         A8AmusementParkName = T000D20_A8AmusementParkName[0];
         pr_default.close(18);
         dynload_actions( ) ;
         /*  Sending validation outputs */
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A8AmusementParkName", StringUtil.RTrim( A8AmusementParkName));
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}]");
         setEventMetadata("ENTER",",oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[]");
         setEventMetadata("REFRESH",",oparms:[]}");
         setEventMetadata("VALID_TRIPID","{handler:'Valid_Tripid',iparms:[{av:'A45TripId',fld:'TRIPID',pic:'ZZZ9'},{av:'Gx_mode',fld:'vMODE',pic:'@!'}]");
         setEventMetadata("VALID_TRIPID",",oparms:[{av:'A48TripDate',fld:'TRIPDATE',pic:''},{av:'A47TripDescription',fld:'TRIPDESCRIPTION',pic:''},{av:'Gx_mode',fld:'vMODE',pic:'@!'},{av:'Z45TripId'},{av:'Z48TripDate'},{av:'Z47TripDescription'},{ctrl:'BTN_DELETE',prop:'Enabled'},{ctrl:'BTN_ENTER',prop:'Enabled'}]}");
         setEventMetadata("VALID_TRIPDATE","{handler:'Valid_Tripdate',iparms:[]");
         setEventMetadata("VALID_TRIPDATE",",oparms:[]}");
         setEventMetadata("VALID_AMUSEMENTPARKID","{handler:'Valid_Amusementparkid',iparms:[{av:'A7AmusementParkId',fld:'AMUSEMENTPARKID',pic:'ZZZ9'},{av:'A8AmusementParkName',fld:'AMUSEMENTPARKNAME',pic:''}]");
         setEventMetadata("VALID_AMUSEMENTPARKID",",oparms:[{av:'A8AmusementParkName',fld:'AMUSEMENTPARKNAME',pic:''}]}");
         setEventMetadata("NULL","{handler:'Valid_Amusementparkname',iparms:[]");
         setEventMetadata("NULL",",oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(18);
         pr_default.close(4);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z48TripDate = DateTime.MinValue;
         Z47TripDescription = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         Gx_mode = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A48TripDate = DateTime.MinValue;
         A47TripDescription = "";
         lblTitleattraction_Jsonclick = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gridtrip_attractionContainer = new GXWebGrid( context);
         Gridtrip_attractionColumn = new GXWebColumn();
         A8AmusementParkName = "";
         sMode15 = "";
         sStyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         T000D7_A45TripId = new short[1] ;
         T000D7_A48TripDate = new DateTime[] {DateTime.MinValue} ;
         T000D7_A47TripDescription = new String[] {""} ;
         T000D8_A45TripId = new short[1] ;
         T000D6_A45TripId = new short[1] ;
         T000D6_A48TripDate = new DateTime[] {DateTime.MinValue} ;
         T000D6_A47TripDescription = new String[] {""} ;
         sMode14 = "";
         T000D9_A45TripId = new short[1] ;
         T000D10_A45TripId = new short[1] ;
         T000D5_A45TripId = new short[1] ;
         T000D5_A48TripDate = new DateTime[] {DateTime.MinValue} ;
         T000D5_A47TripDescription = new String[] {""} ;
         T000D11_A45TripId = new short[1] ;
         T000D14_A45TripId = new short[1] ;
         Z8AmusementParkName = "";
         T000D15_A45TripId = new short[1] ;
         T000D15_A8AmusementParkName = new String[] {""} ;
         T000D15_A7AmusementParkId = new short[1] ;
         T000D4_A8AmusementParkName = new String[] {""} ;
         T000D16_A8AmusementParkName = new String[] {""} ;
         T000D17_A45TripId = new short[1] ;
         T000D17_A7AmusementParkId = new short[1] ;
         T000D3_A45TripId = new short[1] ;
         T000D3_A7AmusementParkId = new short[1] ;
         T000D2_A45TripId = new short[1] ;
         T000D2_A7AmusementParkId = new short[1] ;
         T000D20_A8AmusementParkName = new String[] {""} ;
         T000D21_A45TripId = new short[1] ;
         T000D21_A7AmusementParkId = new short[1] ;
         Gridtrip_attractionRow = new GXWebRow();
         subGridtrip_attraction_Linesclass = "";
         ROClassString = "";
         sImgUrl = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         ZZ48TripDate = DateTime.MinValue;
         ZZ47TripDescription = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.trip__default(),
            new Object[][] {
                new Object[] {
               T000D2_A45TripId, T000D2_A7AmusementParkId
               }
               , new Object[] {
               T000D3_A45TripId, T000D3_A7AmusementParkId
               }
               , new Object[] {
               T000D4_A8AmusementParkName
               }
               , new Object[] {
               T000D5_A45TripId, T000D5_A48TripDate, T000D5_A47TripDescription
               }
               , new Object[] {
               T000D6_A45TripId, T000D6_A48TripDate, T000D6_A47TripDescription
               }
               , new Object[] {
               T000D7_A45TripId, T000D7_A48TripDate, T000D7_A47TripDescription
               }
               , new Object[] {
               T000D8_A45TripId
               }
               , new Object[] {
               T000D9_A45TripId
               }
               , new Object[] {
               T000D10_A45TripId
               }
               , new Object[] {
               T000D11_A45TripId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000D14_A45TripId
               }
               , new Object[] {
               T000D15_A45TripId, T000D15_A8AmusementParkName, T000D15_A7AmusementParkId
               }
               , new Object[] {
               T000D16_A8AmusementParkName
               }
               , new Object[] {
               T000D17_A45TripId, T000D17_A7AmusementParkId
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000D20_A8AmusementParkName
               }
               , new Object[] {
               T000D21_A45TripId, T000D21_A7AmusementParkId
               }
            }
         );
      }

      private short nIsMod_15 ;
      private short Z45TripId ;
      private short Z7AmusementParkId ;
      private short nRcdDeleted_15 ;
      private short nRcdExists_15 ;
      private short GxWebError ;
      private short A7AmusementParkId ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A45TripId ;
      private short subGridtrip_attraction_Backcolorstyle ;
      private short subGridtrip_attraction_Allowselection ;
      private short subGridtrip_attraction_Allowhovering ;
      private short subGridtrip_attraction_Allowcollapsing ;
      private short subGridtrip_attraction_Collapsed ;
      private short nBlankRcdCount15 ;
      private short RcdFound15 ;
      private short nBlankRcdUsr15 ;
      private short GX_JID ;
      private short RcdFound14 ;
      private short nIsDirty_14 ;
      private short Gx_BScreen ;
      private short nIsDirty_15 ;
      private short subGridtrip_attraction_Backstyle ;
      private short gxajaxcallmode ;
      private short ZZ45TripId ;
      private int nRC_GXsfl_53 ;
      private int nGXsfl_53_idx=1 ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtTripId_Enabled ;
      private int edtTripDate_Enabled ;
      private int edtTripDescription_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int edtAmusementParkId_Enabled ;
      private int edtAmusementParkName_Enabled ;
      private int subGridtrip_attraction_Selectedindex ;
      private int subGridtrip_attraction_Selectioncolor ;
      private int subGridtrip_attraction_Hoveringcolor ;
      private int fRowAdded ;
      private int subGridtrip_attraction_Backcolor ;
      private int subGridtrip_attraction_Allbackcolor ;
      private int imgprompt_7_Visible ;
      private int defedtAmusementParkId_Enabled ;
      private int idxLst ;
      private long GRIDTRIP_ATTRACTION_nFirstRecordOnPage ;
      private String sPrefix ;
      private String sGXsfl_53_idx="0001" ;
      private String Z47TripDescription ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtTripId_Internalname ;
      private String divMaintable_Internalname ;
      private String divTitlecontainer_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divFormcontainer_Internalname ;
      private String divToolbarcell_Internalname ;
      private String TempTags ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtTripId_Jsonclick ;
      private String edtTripDate_Internalname ;
      private String edtTripDate_Jsonclick ;
      private String edtTripDescription_Internalname ;
      private String A47TripDescription ;
      private String edtTripDescription_Jsonclick ;
      private String divAttractiontable_Internalname ;
      private String lblTitleattraction_Internalname ;
      private String lblTitleattraction_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String subGridtrip_attraction_Header ;
      private String A8AmusementParkName ;
      private String sMode15 ;
      private String edtAmusementParkId_Internalname ;
      private String edtAmusementParkName_Internalname ;
      private String imgprompt_7_Link ;
      private String sStyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GXCCtl ;
      private String sMode14 ;
      private String Z8AmusementParkName ;
      private String imgprompt_7_Internalname ;
      private String sGXsfl_53_fel_idx="0001" ;
      private String subGridtrip_attraction_Class ;
      private String subGridtrip_attraction_Linesclass ;
      private String ROClassString ;
      private String edtAmusementParkId_Jsonclick ;
      private String sImgUrl ;
      private String edtAmusementParkName_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridtrip_attraction_Internalname ;
      private String ZZ47TripDescription ;
      private DateTime Z48TripDate ;
      private DateTime A48TripDate ;
      private DateTime ZZ48TripDate ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool bGXsfl_53_Refreshing=false ;
      private GXWebGrid Gridtrip_attractionContainer ;
      private GXWebRow Gridtrip_attractionRow ;
      private GXWebColumn Gridtrip_attractionColumn ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private short[] T000D7_A45TripId ;
      private DateTime[] T000D7_A48TripDate ;
      private String[] T000D7_A47TripDescription ;
      private short[] T000D8_A45TripId ;
      private short[] T000D6_A45TripId ;
      private DateTime[] T000D6_A48TripDate ;
      private String[] T000D6_A47TripDescription ;
      private short[] T000D9_A45TripId ;
      private short[] T000D10_A45TripId ;
      private short[] T000D5_A45TripId ;
      private DateTime[] T000D5_A48TripDate ;
      private String[] T000D5_A47TripDescription ;
      private short[] T000D11_A45TripId ;
      private short[] T000D14_A45TripId ;
      private short[] T000D15_A45TripId ;
      private String[] T000D15_A8AmusementParkName ;
      private short[] T000D15_A7AmusementParkId ;
      private String[] T000D4_A8AmusementParkName ;
      private String[] T000D16_A8AmusementParkName ;
      private short[] T000D17_A45TripId ;
      private short[] T000D17_A7AmusementParkId ;
      private short[] T000D3_A45TripId ;
      private short[] T000D3_A7AmusementParkId ;
      private short[] T000D2_A45TripId ;
      private short[] T000D2_A7AmusementParkId ;
      private String[] T000D20_A8AmusementParkName ;
      private short[] T000D21_A45TripId ;
      private short[] T000D21_A7AmusementParkId ;
      private GXWebForm Form ;
   }

   public class trip__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
         ,new ForEachCursor(def[3])
         ,new ForEachCursor(def[4])
         ,new ForEachCursor(def[5])
         ,new ForEachCursor(def[6])
         ,new ForEachCursor(def[7])
         ,new ForEachCursor(def[8])
         ,new ForEachCursor(def[9])
         ,new UpdateCursor(def[10])
         ,new UpdateCursor(def[11])
         ,new ForEachCursor(def[12])
         ,new ForEachCursor(def[13])
         ,new ForEachCursor(def[14])
         ,new ForEachCursor(def[15])
         ,new UpdateCursor(def[16])
         ,new UpdateCursor(def[17])
         ,new ForEachCursor(def[18])
         ,new ForEachCursor(def[19])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmT000D7 ;
          prmT000D7 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D8 ;
          prmT000D8 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D6 ;
          prmT000D6 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D9 ;
          prmT000D9 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D10 ;
          prmT000D10 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D5 ;
          prmT000D5 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D11 ;
          prmT000D11 = new Object[] {
          new Object[] {"@TripDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@TripDescription",SqlDbType.NChar,20,0}
          } ;
          Object[] prmT000D12 ;
          prmT000D12 = new Object[] {
          new Object[] {"@TripDate",SqlDbType.DateTime,8,0} ,
          new Object[] {"@TripDescription",SqlDbType.NChar,20,0} ,
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D13 ;
          prmT000D13 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D14 ;
          prmT000D14 = new Object[] {
          } ;
          Object[] prmT000D15 ;
          prmT000D15 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D4 ;
          prmT000D4 = new Object[] {
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D16 ;
          prmT000D16 = new Object[] {
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D17 ;
          prmT000D17 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D3 ;
          prmT000D3 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D2 ;
          prmT000D2 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D18 ;
          prmT000D18 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D19 ;
          prmT000D19 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D21 ;
          prmT000D21 = new Object[] {
          new Object[] {"@TripId",SqlDbType.SmallInt,4,0}
          } ;
          Object[] prmT000D20 ;
          prmT000D20 = new Object[] {
          new Object[] {"@AmusementParkId",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("T000D2", "SELECT [TripId], [AmusementParkId] FROM [TripAttraction] WITH (UPDLOCK) WHERE [TripId] = @TripId AND [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D2,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D3", "SELECT [TripId], [AmusementParkId] FROM [TripAttraction] WHERE [TripId] = @TripId AND [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D3,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D4", "SELECT [AmusementParkName] FROM [AmusementPark] WHERE [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D4,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D5", "SELECT [TripId], [TripDate], [TripDescription] FROM [Trip] WITH (UPDLOCK) WHERE [TripId] = @TripId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D5,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D6", "SELECT [TripId], [TripDate], [TripDescription] FROM [Trip] WHERE [TripId] = @TripId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D6,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D7", "SELECT TM1.[TripId], TM1.[TripDate], TM1.[TripDescription] FROM [Trip] TM1 WHERE TM1.[TripId] = @TripId ORDER BY TM1.[TripId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D7,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D8", "SELECT [TripId] FROM [Trip] WHERE [TripId] = @TripId  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D8,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D9", "SELECT TOP 1 [TripId] FROM [Trip] WHERE ( [TripId] > @TripId) ORDER BY [TripId]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D9,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000D10", "SELECT TOP 1 [TripId] FROM [Trip] WHERE ( [TripId] < @TripId) ORDER BY [TripId] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D10,1, GxCacheFrequency.OFF ,true,true )
             ,new CursorDef("T000D11", "INSERT INTO [Trip]([TripDate], [TripDescription]) VALUES(@TripDate, @TripDescription); SELECT SCOPE_IDENTITY()", GxErrorMask.GX_NOMASK,prmT000D11)
             ,new CursorDef("T000D12", "UPDATE [Trip] SET [TripDate]=@TripDate, [TripDescription]=@TripDescription  WHERE [TripId] = @TripId", GxErrorMask.GX_NOMASK,prmT000D12)
             ,new CursorDef("T000D13", "DELETE FROM [Trip]  WHERE [TripId] = @TripId", GxErrorMask.GX_NOMASK,prmT000D13)
             ,new CursorDef("T000D14", "SELECT [TripId] FROM [Trip] ORDER BY [TripId]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000D14,100, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D15", "SELECT T1.[TripId], T2.[AmusementParkName], T1.[AmusementParkId] FROM ([TripAttraction] T1 INNER JOIN [AmusementPark] T2 ON T2.[AmusementParkId] = T1.[AmusementParkId]) WHERE T1.[TripId] = @TripId and T1.[AmusementParkId] = @AmusementParkId ORDER BY T1.[TripId], T1.[AmusementParkId] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D15,11, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D16", "SELECT [AmusementParkName] FROM [AmusementPark] WHERE [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D16,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D17", "SELECT [TripId], [AmusementParkId] FROM [TripAttraction] WHERE [TripId] = @TripId AND [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D17,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D18", "INSERT INTO [TripAttraction]([TripId], [AmusementParkId]) VALUES(@TripId, @AmusementParkId)", GxErrorMask.GX_NOMASK,prmT000D18)
             ,new CursorDef("T000D19", "DELETE FROM [TripAttraction]  WHERE [TripId] = @TripId AND [AmusementParkId] = @AmusementParkId", GxErrorMask.GX_NOMASK,prmT000D19)
             ,new CursorDef("T000D20", "SELECT [AmusementParkName] FROM [AmusementPark] WHERE [AmusementParkId] = @AmusementParkId ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D20,1, GxCacheFrequency.OFF ,true,false )
             ,new CursorDef("T000D21", "SELECT [TripId], [AmusementParkId] FROM [TripAttraction] WHERE [TripId] = @TripId ORDER BY [TripId], [AmusementParkId] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000D21,11, GxCacheFrequency.OFF ,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 1 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 2 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 3 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                return;
             case 4 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                return;
             case 5 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDate(2) ;
                ((String[]) buf[2])[0] = rslt.getString(3, 20) ;
                return;
             case 6 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 7 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 8 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 9 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 12 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                return;
             case 13 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((String[]) buf[1])[0] = rslt.getString(2, 50) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                return;
             case 14 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 15 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
             case 18 :
                ((String[]) buf[0])[0] = rslt.getString(1, 50) ;
                return;
             case 19 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((short[]) buf[1])[0] = rslt.getShort(2) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 1 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 2 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 3 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 4 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 5 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 6 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 7 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 8 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 9 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                return;
             case 10 :
                stmt.SetParameter(1, (DateTime)parms[0]);
                stmt.SetParameter(2, (String)parms[1]);
                stmt.SetParameter(3, (short)parms[2]);
                return;
             case 11 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 13 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 14 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 15 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 16 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 17 :
                stmt.SetParameter(1, (short)parms[0]);
                stmt.SetParameter(2, (short)parms[1]);
                return;
             case 18 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
             case 19 :
                stmt.SetParameter(1, (short)parms[0]);
                return;
       }
    }

 }

}
